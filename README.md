# README #
### What is this repository for? ###

* Version 2.0.1
* Designed as a flexible framework for Unity that can be incorporated into any project that follows composotion design patterns.

### How do I get set up? ###

* Install
	
	Add Bubblegum through your package manager.
		1 - Open Package Manager (Windows > Package Manager)
		2 - Select the + icon in the top left
		3 - Add package from GIT url
		4 - Paste in the following address: https://bitbucket.org/jaffajam/bubblegum.git OR if you use ssh: git@bitbucket.org:jaffajam/bubblegum.git
		5 - A new Package called Bubblegum will be installed

* Online Guide

	https://docs.google.com/document/d/15DeqSCbCSqwZ1KLj58_36BE9DD6wDXTjyXAnW2gLwJk/edit?usp=sharing

* Script Templates (for contribution)

	1. Go to the _Templates folder in File Explorer 
	2. Copy the templates  
	3. Go to Local Disc > Program Files > Unity > Editor > Data > Resources > ScriptTemplates  
	4. Delete the default ?NewBehaviour? script  
	5. Paste copied templates 
	6. Restart Unity 
	7. Pat self on back

### Contribution guidelines ###

* Follow coding standards https://msdn.microsoft.com/en-us/library/ff926074.aspx?f=255&MSPPError=-2147217396
* Use the templates for starting scripts (found in templates folder)
* Place code into the regions they belong to, with the exception of private member variables that are exposed in the inspector (these go into a PUBLIC region)
* Place xml summaries before variables, methods, and classes (even if the comment seems silly)

### Who do I talk to? ###

* Contact Alex Humphries at info@jaffajam.com