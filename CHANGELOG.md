# Changelog

## [2.0.1] - 13-02-2020

### Changed
- Set up project for publishing
- Integrated more interface functionality over UnityEvents

## [1.0.0] - 27-08-2019

### Added
- Added everything