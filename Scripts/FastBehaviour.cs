﻿using UnityEngine;

namespace Bubblegum
{

	/// <summary>
	/// Exists to provide a more efficient update system than the standard unity model by delaying updates.
	/// Should increase performance for update calls at an interval greater than 0.05f, depending on the content of the update loop.
	/// </summary>
	public abstract class FastBehaviour : MonoBehaviour
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// The interval at which the behaviour gets its update method called
		/// </summary>
		[Tooltip("The interval at which the behaviour gets its update method called"), Range(0.05f, 5f)]
		[SerializeField]
		protected float updateInterval = 1f;

		#endregion // PUBLIC_VARIABLES

		#region PRIVATE_VARIABLES

		/// <summary>
		/// The frame at which the behaviour gets its updates, this means that we don't have to waste resources updating the behaviour
		/// when we do not need to (useful for things like updating hunger or a targets position)
		/// </summary>
		private float updateTime;

		/// <summary>
		/// The offset at which we update this behaviour, this avoids all behaviours getting updated on the same frame
		/// </summary>
		private float updateOffset;

		#endregion // PRIVATE_VARIABLES

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Awake this instance, in base classes use Initialize() instead of the Awake method
		/// unless it is intended to hide this method
		/// </summary>
		void OnEnable()
		{
			//We might be causing errors by not resetting the updateTime here???
			updateOffset = Time.time % updateInterval;

			NewOnEnable();
		}

		/// <summary>
		/// Update is called once per frame, in base classes use IntervalUpdate() and NewUpdate() for updates every frame
		/// </summary>
		void Update()
		{
			if (Time.time > updateTime)
			{
				//Reset
				updateTime = Time.time + updateInterval + updateOffset;

				//Update
				IntervalUpdate();
			}

			NewUpdate();
		}

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Gets the update interval.
		/// </summary>
		/// <returns>The update interval.</returns>
		public float GetUpdateInterval()
		{
			return updateInterval;
		}

		/// <summary>
		/// Sets the update interval.
		/// </summary>
		/// <param name="updateInterval">Update interval.</param>
		public void SetUpdateInterval(float updateInterval)
		{
			this.updateInterval = updateInterval;
		}

		#endregion // PUBLIC_METHODS

		#region PROTECTED_METHODS

		/// <summary>
		/// Update that is called at the set interval
		/// </summary>
		protected abstract void IntervalUpdate();

		/// <summary>
		/// Any OnEnable calls should go here
		/// </summary>
		protected virtual void NewOnEnable() { }

		/// <summary>
		/// Any update calls should be in here instead of Update
		/// </summary>
		protected virtual void NewUpdate() { }

		#endregion // PROTECTED_METHODS

	}
}