﻿using UnityEngine;

namespace Bubblegum.Animations
{

	/// <summary>
	/// Sends events to the gameobjects event system
	/// </summary>
	public class StateDeactivate : StateMachineBehaviour
	{
		#region METHODS

		/// <summary>
		/// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
		/// </summary>
		/// <param name="animator"></param>
		/// <param name="stateInfo"></param>
		/// <param name="layerIndex"></param>
		public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (animator.gameObject.activeSelf && stateInfo.normalizedTime > 1f)
				animator.gameObject.SetActive(false);
		}

		#endregion
	}
}