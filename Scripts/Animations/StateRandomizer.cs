﻿using UnityEngine;

namespace Bubblegum.Animations
{
    /// <summary>
    /// Randomize an int parameter in the state machine
    /// </summary>
    public class StateRandomizer : StateMachineBehaviour
    {
        #region VARIABLES

        /// <summary>
        /// The key for the parameter we want to randomize
        /// </summary>
        [SerializeField, Tooltip("The key for the parameter we want to randomize")]
        private string parameterKey = "Random";

        /// <summary>
        /// Max value of the parameter
        /// </summary>
        [SerializeField, Tooltip("Max value of the parameter")]
        private int stateCount = 2;

        #endregion

        #region METHODS

        /// <summary>
        /// When we enter this state
        /// </summary>
        /// <param name="animator"></param>
        /// <param name="stateInfo"></param>
        /// <param name="layerIndex"></param>
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            base.OnStateEnter(animator, stateInfo, layerIndex);

            if (stateCount <= 1)
                throw new System.Exception("Need more than one state to use state randomizer");

            int currentState = animator.GetInteger(parameterKey);
            int randomState = currentState;

            while (randomState == currentState)
                randomState = Random.Range(0, stateCount);

            animator.SetInteger(parameterKey, randomState);
        }

        #endregion
    }
}