﻿using UnityEngine;

namespace Bubblegum.Animations
{
	/// <summary>
	/// Deactivate animator with this state
	/// </summary>
	public class StateAnimatorDeactivate : StateMachineBehaviour
	{
		#region VARIABLES

		/// <summary>
		/// Method to deactivate animator
		/// </summary>
		[SerializeField, Tooltip("Method to deactivate animator")]
		private StateMethod deactivateMethod = StateMethod.Exit;

		/// <summary>
		/// The different state methods
		/// </summary>
		public enum StateMethod { Enter, Exit }

		#endregion

		#region METHODS

		/// <summary>
		/// When we enter this state
		/// </summary>
		/// <param name="animator"></param>
		/// <param name="stateInfo"></param>
		/// <param name="layerIndex"></param>
		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			base.OnStateEnter(animator, stateInfo, layerIndex);

			if (deactivateMethod == StateMethod.Enter)
				animator.enabled = false;
		}

		/// <summary>
		/// When we exit this state
		/// </summary>
		/// <param name="animator"></param>
		/// <param name="stateInfo"></param>
		/// <param name="layerIndex"></param>
		public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			base.OnStateExit(animator, stateInfo, layerIndex);

			if (deactivateMethod == StateMethod.Exit)
				animator.enabled = false;
		}

		#endregion
	}
}