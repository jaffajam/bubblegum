﻿using UnityEngine;
using Bubblegum.Audio;

namespace Bubblegum.Animations
{
    /// <summary>
    /// Play a random audio when we enter this state
    /// </summary>
    public class StateAudio : StateMachineBehaviour
    {
        #region VARIABLES

        /// <summary>
        /// If we should play from a collection instead of a clip
        /// </summary>
        [SerializeField, Tooltip ("If we should play from a collection instead of a clip")]
        private bool playFromCollection;

        /// <summary>
        /// The clip to play
        /// </summary>
        [SerializeField, Tooltip("The clip to play"), DisplayIf("playFromCollection", false)]
        private AudioClip clip;


        /// <summary>
        /// The collection to find a clip to play
        /// </summary>
        [SerializeField, Tooltip("The collection to find a clip to play"), DisplayIf("playFromCollection", true)]
        private AudioCollection collection;

        #endregion

        #region METHODS

        /// <summary>
        /// When we enter this state
        /// </summary>
        /// <param name="animator"></param>
        /// <param name="stateInfo"></param>
        /// <param name="layerIndex"></param>
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            base.OnStateEnter(animator, stateInfo, layerIndex);

            if (playFromCollection && collection != null)
                AudioManager.Instance.PlayAudio(collection.Random, collection.Setting);
            else if (clip != null)
                AudioManager.Instance.PlayAudio(clip);
        }

        #endregion
    }
}