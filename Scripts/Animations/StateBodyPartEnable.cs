﻿using UnityEngine;

namespace Bubblegum.Animations
{

	/// <summary>
	/// Enable/disable body part during this state
	/// </summary>
	public class StateBodyPartControl : StateMachineBehaviour
	{
		#region VARIABLES

		/// <summary>
		/// If we should enable the part
		/// </summary>
		public bool enable;

		/// <summary>
		/// The path to the body part
		/// </summary>
		public string path;

		/// <summary>
		/// The part we are interested in
		/// </summary>
		private Transform part;

		#endregion

		#region METHODS

		/// <summary>
		/// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
		/// </summary>
		/// <param name="animator"></param>
		/// <param name="stateInfo"></param>
		/// <param name="layerIndex"></param>
		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (!part)
				part = animator.transform.Find(path);

			part.localScale = enable ? Vector3.one : Vector3.zero;
		}

		/// <summary>
		/// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
		/// </summary>
		/// <param name="animator"></param>
		/// <param name="stateInfo"></param>
		/// <param name="layerIndex"></param>
		public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			part.localScale = enable ? Vector3.zero : Vector3.one;
		}

		#endregion
	}
}