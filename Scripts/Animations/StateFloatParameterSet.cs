﻿using UnityEngine;

namespace Bubblegum.Animations
{
    /// <summary>
    /// Randomize the float with the key
    /// </summary>
    public class StateFloatParameterSet : StateMachineBehaviour
    {
        #region VARIABLES

        /// <summary>
        /// The parameter key
        /// </summary>
        [SerializeField, Tooltip("The parameter key")]
        private string parameterKey = "Offset";

        /// <summary>
        /// The range for the offset to pick from
        /// </summary>
        [SerializeField, Tooltip("The range for the offset to pick from")]
        private Vector2 range = new Vector2(0f, 1f);

        #endregion

        #region METHODS

        /// <summary>
        /// When we enter this state
        /// </summary>
        /// <param name="animator"></param>
        /// <param name="stateInfo"></param>
        /// <param name="layerIndex"></param>
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            base.OnStateEnter(animator, stateInfo, layerIndex);
            animator.SetFloat(parameterKey, Random.Range(range.x, range.y));
        }

        #endregion
    }
}