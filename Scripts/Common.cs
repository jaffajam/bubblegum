﻿using System;

namespace Bubblegum
{
	/// <summary>
	/// Differect varaible types
	/// </summary>
	public enum VariableType { None = -1, Bool = 0, Int = 1, Float = 2, String = 3, Object = 4 }

	/// <summary>
	/// Used as a link between VariableType enum and their actual types
	/// </summary>
	public static class Types { public static readonly Type[] VariableTypes = new Type[] { typeof(bool), typeof(int), typeof(float), typeof(string), typeof(object) }; }

	/// <summary>
	/// A method that can be used for initialization
	/// </summary>
	public enum InitializeMethod { Awake, Start, Enable, None }

	/// <summary>
	/// A method choice for updating components
	/// </summary>
	public enum UpdateMethod { Update, FixedUpdate, LateUpdate, None }

	/// <summary>
	/// Different ways the filter is handled
	/// </summary>
	public enum StateMethod { Disable, Destroy, None }

	/// <summary>
	/// The grounded mode per object (ie. if has two legs or wheels)
	/// </summary>
	public enum GroundedMode { Any, Grounded, NotGrounded }

	/// <summary>
	/// Different axis in 3 dimensional space
	/// </summary>
	public enum Axis3
    {
        x = 1 << 0,
        y = 1 << 1,
        z = 1 << 2
    }

	/// <summary>
	/// What parent to set for a transform
	/// </summary>
	public enum ParentSetting { Child, Sibling, RootChild, None }

	/// <summary>
	/// Types of colliders
	/// </summary>
	public enum ColliderType { Collider, Trigger }

	/// <summary>
	/// UV channels available in mesh
	/// </summary>
	public enum UVChannel { None, UV1, UV2, UV3, UV4 }

	/// <summary>
	/// Different types of pointer event
	/// </summary>
	public enum PointerEvent { PointerDown, PointerUp }

	/// <summary>
	/// Different directions
	/// </summary>
	public enum Direction2D { Up, Down, Left, Right }

	/// <summary>
	/// The different comparison types
	/// </summary>
	public enum ComparisonMode { Equal, NotEqual, GreaterThan, LessThan }

    /// <summary>
    /// The different conditional types
    /// </summary>
    public enum ConditionalType { And, Or }

    /// <summary>
    /// The axis selection type
    /// </summary>
    public enum DualAxisType { XY, XZ, YZ }

	/// <summary>
	/// Common input keys
	/// </summary>
	public static class CommonInput
	{
		/// <summary>
		/// Axis controls
		/// </summary>
		public const string HORIZONTAL_AXIS = "Horizontal", VERTICAL_AXIS = "Vertical";

		/// <summary>
		/// Button controls
		/// </summary>
		public const string FIRE_1 = "Fire1", FIRE_2 = "Fire2", FIRE_3 = "Fire3";

		/// <summary>
		/// Key array for common loops
		/// </summary>
		public static readonly string[] fireKeys = { FIRE_1, FIRE_2, FIRE_3 };
	}

	/// <summary>
	/// All of the unity standard tags
	/// </summary>
	public static class CommonTags
	{
		/// <summary>
		/// Unity player tag
		/// </summary>
		public const string PLAYER_TAG = "Player";

		/// <summary>
		/// Unity game controller tag
		/// </summary>
		public const string GAME_CONTROLLER = "GameController";

		/// <summary>
		/// Unity respawn tag
		/// </summary>
		public const string RESPAWN = "Respawn";

		/// <summary>
		/// Unity finish tag
		/// </summary>
		public const string FINISH = "Finish";

		/// <summary>
		/// Unity main camera tag
		/// </summary>
		public const string MAIN_CAMERA = "MainCamera";
	}
}