﻿using UnityEngine;
using System.Collections.Generic;

namespace Bubblegum
{

	/// <summary>
	/// A condition check that is exposed in the unity editor
	/// /summary>
	[System.Serializable]
	public class Condition
	{
		#region PUBLIC_VARIABLES

		/// <summary>
		/// Get the type of the value
		/// </summary>
		public System.Type ValueType
		{
			get
			{
				return value.ValueSetType;
			}
		}

		/// <summary>
		/// The key for this object
		/// </summary>
		[Tooltip("The key for this object")]
		public string key;

		/// <summary>
		/// The mode to compare values
		/// </summary>
		[SerializeField, Tooltip("The mode to compare values")]
		private ComparisonMode mode;

		/// <summary>
		/// The value we are comparing against
		/// </summary>
		[SerializeField, Tooltip("The value we are comparing against")]
		private DynamicValue value;

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Check if the check value matches what is required
		/// </summary>
		/// <returns></returns>
		public bool Compare(object check)
		{
			return value.Compare(check, mode);
		}

		#endregion
	}

    /// <summary>
    /// Criteria check
    /// </summary>
    [System.Serializable]
    public class Criteria
    {
        #region VARAIBLES

        /// <summary>
        /// Condition check type with following criteria
        /// </summary>
        public ConditionalType conditionalType;

        /// <summary>
        /// Required value
        /// </summary>
        public Condition condition;

        #endregion

        #region PUBLIC_METHODS

        /// <summary>
        /// Check if the check value matches what is required
        /// </summary>
        /// <returns></returns>
        public static bool Check(Criteria[] criteria, Dictionary<string, object> values)
        {
            bool pass = true;

            if (criteria.Length == 0)
                return false;

            for (int i = 0; i < criteria.Length; i++)
            {
                var value = values.ContainsKey(criteria[i].condition.key) ? values[criteria[i].condition.key] : System.Activator.CreateInstance(criteria[i].condition.ValueType);

                if (criteria[i].conditionalType == ConditionalType.And && pass)
                    pass = criteria[i].condition.Compare(value);
                else if (criteria[i].conditionalType == ConditionalType.Or)
                {
                    if (!pass)
                        pass = criteria[i].condition.Compare(value);
                    else
                        break;
                }
            }

            return pass;
        }

        #endregion
    }
}
