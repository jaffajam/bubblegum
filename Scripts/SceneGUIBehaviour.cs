﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace Bubblegum
{

	/// <summary>
	/// Special utility methods for scene gui calls
	/// </summary>
	public static class SceneGUIUtils
	{
		#region VARIABLES

		/// <summary>
		/// Sizes for the handles
		/// </summary>
		public const float
			HANDLES_SIZE_LARGE = 2f,
			HANDLES_SIZE_NORMAL = 1f,
			HANDLES_SIZE_SMALL = 0.5f,
			HANDLES_SIZE_MINI = 0.25f;

		#endregion

		#region METHODS

		/// <summary>
		/// Eat all mouse events
		/// </summary>
		public static void EatEvents(params EventType[] events)
		{
			for (int i = 0; i < events.Length; i++)
				if (Event.current.type == events[i])
				{
					Event.current.Use();
					GUIUtility.hotControl = 0;
					return;
				}
		}

		#endregion
	}

	/// <summary>
	/// Manager class for scene gui events
	/// </summary>
	public class SceneGUIManager
	{
		#region VARIABLES

		/// <summary>
		/// Get all of the behaviours we are managing
		/// </summary>
		public List<SceneGUIBehaviour> Behaviours { get { return behaviours; } }

		/// <summary>
		/// All of the beahviours to send events to
		/// </summary>
		private List<SceneGUIBehaviour> behaviours = new List<SceneGUIBehaviour>();

		/// <summary>
		/// Used for creating control IDs
		/// </summary>
		private const string CONTROL_KEY = "BubblegumSceneGUI";

		#endregion

		#region METHODS

		/// <summary>
		/// Clear all data
		/// </summary>
		public void Reset()
		{
			behaviours.Clear();
		}

		/// <summary>
		/// Register all of the behaviours under this manager
		/// </summary>
		/// <param name="behaviours"></param>
		public void Register(params SceneGUIBehaviour[] behaviours)
		{
			this.behaviours.AddRange(behaviours);
		}

		/// <summary>
		/// Deregister all of the behaviours from this manager
		/// </summary>
		/// <param name="behaviours"></param>
		public void Deregister(params SceneGUIBehaviour[] behaviours)
		{
			for (int i = 0; i < behaviours.Length; i++)
				this.behaviours.Remove(behaviours[i]);
		}

		/// <summary>
		/// Update this object
		/// </summary>
		public void Update()
		{
			for (int i = 0; i < behaviours.Count; i++)
			{
				if (behaviours[i] != null)
				{
					behaviours[i].Update();

					switch (Event.current.type)
					{
						case EventType.MouseDown:
						case EventType.MouseDrag:
						case EventType.MouseMove:
						case EventType.MouseUp:
						case EventType.ScrollWheel:
						case EventType.ContextClick:
							behaviours[i].Mouse();
							break;

						case EventType.KeyDown:
						case EventType.KeyUp:
							behaviours[i].Key();
							break;

						case EventType.Layout:
							behaviours[i].Layout();
							break;

						case EventType.Repaint:
							behaviours[i].Repaint();
							break;

						case EventType.DragExited:
						case EventType.DragPerform:
						case EventType.DragUpdated:
							behaviours[i].DragDrop();
							break;

						case EventType.ExecuteCommand:
						case EventType.ValidateCommand:
							behaviours[i].Command();
							break;
					}
				}
				else
				{
					behaviours.RemoveAt(i);
					i--;
				}
			}
		}

		/// <summary>
		/// Raycast onto objects
		/// </summary>
		public void Raycast()
		{
			if (Camera.current != null && Event.current != null)
			{
				//Raycast
				RaycastHit hit;
				Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

				if (Physics.Raycast(ray, out hit))
					if (hit.transform)
					{

					}
			}
		}

		/// <summary>
		/// Get he control ID for the given behaviour
		/// </summary>
		/// <param name="behaviour"></param>
		/// <returns></returns>
		public int GetControlID(SceneGUIBehaviour behaviour)
		{
			if (behaviours.Contains(behaviour))
				return CONTROL_KEY.GetHashCode() + behaviours.IndexOf(behaviour);

			return -1;
		}

		#endregion
	}

	/// <summary>
	/// A behaviour that responds to scene gui events
	/// </summary>
	public abstract class SceneGUIBehaviour
	{
		#region PUBLIC_METHODS

		/// <summary>
		/// Update this object
		/// </summary>
		public virtual void Update() { }

		/// <summary>
		/// Repaint the scene
		/// </summary>
		public virtual void Repaint() { }

		/// <summary>
		/// When a mouse event occurs
		/// </summary>
		public virtual void Mouse() { }

		/// <summary>
		/// When a key event occurs
		/// </summary>
		public virtual void Key() { }

		/// <summary>
		/// On a layout event
		/// </summary>
		public virtual void Layout() { }

		/// <summary>
		/// When a drag/drop operation occurs
		/// </summary>
		public virtual void DragDrop() { }

		/// <summary>
		/// When a special command (copy/paste) occurs
		/// </summary>
		public virtual void Command() { }

		#endregion
	}
}
#endif