﻿using UnityEngine;

namespace Bubblegum.Audio
{
	/// <summary>
	/// Pauses audio and can stack multiple at once
	/// </summary>
	public class AudioPause : MonoBehaviour
	{
        #region VARIABLES

		/// <summary>
		/// The pause count
		/// </summary>
		private static int pauseCount;

		#endregion

		#region METHODS

		/// <summary>
		/// Enaable this object
		/// </summary>
		void OnEnable()
		{
			pauseCount++;
			AudioListener.pause = true;
		}

		/// <summary>
		/// Disable this object
		/// </summary>
		void OnDisable()
		{
			pauseCount--;

            if (pauseCount == 0)
				AudioListener.pause = false;
        }

		#endregion
	}
}