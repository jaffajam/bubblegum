﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace Bubblegum.Audio
{
	/// <summary>
	/// Used to control mixer from anywhere in the application
	/// </summary>
	public class MixerControl : MonoBehaviour
	{
		#region PUBLIC_VARIABLES

		/// <summary>
		/// Getter and Setter of the parameter value
		/// </summary>
		public float Value
		{
			get
			{
				audioMixer.GetFloat(parameterName, out value);
				return value;
			}

			private set
			{
                this.value = value;
				audioMixer.SetFloat(parameterName, value);
			}
		}

		/// <summary>
		/// Get whether the parameter value is on or off
		/// </summary>
		public bool IsOn
		{
			get
			{
				bool on = Value != offValue;
				return inverseToggle ? !on : on;
			}
		}

		/// <summary>
		/// The Audio Mixer to control
		/// </summary>
		[SerializeField, Tooltip("The Audio Mixer to control")]
		private AudioMixer audioMixer;

		/// <summary>
		/// Name of the parameter to control
		/// </summary>
		[SerializeField, Tooltip("Name of the parameter to control")]
		private string parameterName;

		/// <summary>
		/// The value we are setting
		/// </summary>
		[SerializeField, Tooltip("The value we are setting")]
		public float value;

		/// <summary>
		/// If the controls should act like a toggle
		/// </summary>
		[SerializeField, Tooltip("If the controls should act like a toggle")]
		private bool isToggle;

		/// <summary>
		/// The ON value for this parameter, used for Toggles
		/// </summary>
		[SerializeField, Tooltip("The ON value for this parameter, used for Toggles"), DisplayIf("isToggle", true)]
		private float onValue;

		/// <summary>
		/// The value that's considered as OFF for this parameter
		/// </summary>
		[SerializeField, Tooltip("The value that's considered as OFF for this parameter"), DisplayIf("isToggle", true)]
		private float offValue;

		/// <summary>
		/// Whether the toggle needs to be inversed
		/// </summary>
		[SerializeField, Tooltip("Whether the toggle needs to be inversed"), DisplayIf("isToggle", true)]
		private bool inverseToggle;

		#endregion // PUBLIC_VARIABLES

		#region PRIVATE_VARIABLES

		/// <summary>
		/// Prefs prefix
		/// </summary>
		private const string PREFS_KEY = "MixerControl";

		/// <summary>
		/// Real key used for playerprefs, this is equal to PREFS_KEY + parameterName
		/// </summary>
		private string playerprefsKey;

		/// <summary>
		/// The value on the last frame
		/// </summary>
		private float lastValue;

        /// <summary>
        /// The attached slider
        /// </summary>
        private Slider slider;

        /// <summary>
        /// The attached toggle
        /// </summary>
        private Toggle toggle;

		#endregion // PRIVATE_VARIABLES

		#region PUBLIC_METHODS

		/// <summary>
		/// Revert this object
		/// </summary>
		public void Revert()
		{
            value = PersistentData.GetFloat(playerprefsKey, Value);

            if (toggle != null)
            {
                bool on = value != offValue;
                toggle.isOn = inverseToggle ? !on : on;
            }

            if (slider != null)
            {
                slider.value = Mathf.Pow(10f, value / 20f);
            }

            Value = value;
        }

		/// <summary>
		/// Toggle the value on/off
		/// </summary>
		/// <param name="on"></param>
		public void Toggle(bool on)
		{
			if (audioMixer == null || string.IsNullOrEmpty(parameterName))
				return;

			on = inverseToggle ? !on : on;

			if (!on)
			{
				float currentValue = Value;

				if (currentValue == offValue)
					return;
			}

			Value = on ? onValue : offValue;
		}

		/// <summary>
		/// Set the parameter value
		/// </summary>
		/// <param name="value"></param>
		public void SetValue(float value)
		{
			if (audioMixer == null || string.IsNullOrEmpty(parameterName))
				return;

			Value = Mathf.Clamp(Mathf.Log10(value) * 20f, -80f, 20f);
		}

		#endregion // PUBLIC_METHODS=

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Start this component
		/// </summary>
		void OnEnable()
		{
			playerprefsKey = PREFS_KEY + parameterName;
			slider = GetComponent<Slider>();
			toggle = GetComponent<Toggle>();

            Revert();
		}

		/// <summary>
		/// Save this object
		/// </summary>
		public void Save()
		{
            PersistentData.SetFloat(playerprefsKey, value);
        }

		/// <summary>
		/// Update this object
		/// </summary>
		void Update()
		{
			if (value != lastValue)
				Value = value;

			lastValue = value;
		}

		#endregion // MONOBEHAVIOUR_METHODS
	}
}