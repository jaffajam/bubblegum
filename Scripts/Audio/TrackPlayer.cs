﻿using System.Collections;
using UnityEngine;

namespace Bubblegum.Audio
{
	/// <summary>
	/// Acts as a single audio space like a stereo that plays CDs
	/// </summary>
	public class TrackPlayer : MonoBehaviour
	{
		#region VARIABLES

		/// <summary>
		/// If we should play our tracklist on enable
		/// </summary>
		[SerializeField, Tooltip("If we should play our tracklist on enable")]
		private bool playOnEnable = true;

		/// <summary>
		/// The fade time between tracklists
		/// </summary>
		[SerializeField, Tooltip("The fade time between tracklists")]
		private float fadeTime = 3f;

		/// <summary>
		/// The start time of the audio
		/// </summary>
		[SerializeField, Tooltip("The start time of the audio")]
		private float clipIn = 0f;

        /// <summary>
        /// The clip to play
        /// </summary>
        [SerializeField, Tooltip("The clip to play")]
        private AudioClip clip;

        /// <summary>
        /// The audio setting to use
        /// </summary>
        [SerializeField, Tooltip("The audio setting to use")]
        private AudioSource setting;

		/// <summary>
		/// The source we are playing through
		/// </summary>
		private static AudioSource trackSource;

		/// <summary>
		/// The current clip change method
		/// </summary>
		private static Coroutine fadeInRoutine;

        #endregion

        #region METHODS

		/// <summary>
		/// Enable this object
		/// </summary>
		void OnEnable()
		{
			if (playOnEnable)
				Play();
		}

        /// <summary>
        /// Play our clip
        /// </summary>
        public void Play()
        {
            PlayClip(clip);
        }

        /// <summary>
        /// Play the given clip
        /// </summary>
        /// <param name="clip"></param>
        public void PlayClip(AudioClip clip)
		{
            this.clip = clip;
			AudioSource newSource = AudioManager.Instance.PlayAudio(clip, setting);

			if (fadeInRoutine != null)
				AudioManager.Instance.StopCoroutine(fadeInRoutine);

			if (trackSource)
			{
				AudioManager.Instance.FadeAudio(trackSource, fadeTime, 1f, 0f);
				fadeInRoutine = AudioManager.Instance.FadeAudio(newSource, fadeTime);
			}
			else
				fadeInRoutine = AudioManager.Instance.FadeAudio(newSource, fadeTime);

			trackSource = newSource;
			trackSource.time = clipIn;
		}

		#endregion
	}
}