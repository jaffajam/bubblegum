﻿using UnityEngine;

namespace Bubblegum.Audio
{

	/// <summary>
	/// Use this class to play audio from animations and events or if you do not need to finely tune the audio settings, for highly customized and
	/// precise audio use Unitys default audio source component
	/// </summary>
	public class AudioPlayer : MonoBehaviour
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// The current setting for this audio player
		/// </summary>
		[Tooltip("The current setting for this audio player")]
		[SerializeField]
		private AudioSource setting;

		/// <summary>
		/// The time that it takes to fade from 0f - 1f volume
		/// </summary>
		[SerializeField, Tooltip("The time that it takes to fade from 0f - 1f volume")]
		private float fadeTime = 2f;

		#endregion // PUBLIC_VARIABLES

		#region ENUMERATORS

		/// <summary>
		/// The space in which audio can be played
		/// </summary>
		public enum AudioSpace { LOCAL, WORLD }

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Play the audio under the given key
		/// </summary>
		/// <param name="clip"></param>
		/// <param name="key"></param>
		public void PlayLocal(AudioClip clip)
		{
			AudioManager.Instance.PlayLocalAudio(clip, transform, setting);
		}

		/// <summary>
		/// Play the given audio under the key
		/// </summary>
		/// <param name="clip"></param>
		/// <param name="key"></param>
		public void PlayWorld(AudioClip clip)
		{
			AudioManager.Instance.PlayWorldAudio(clip, transform.position, setting);
		}

		/// <summary>
		/// Plays a random audio effect from the collection
		/// </summary>
		/// <param name="audioCollection">Audio collection.</param>
		public void PlayRandomLocalAudio(AudioCollection audioCollection)
		{
			PlayLocal(audioCollection.Random);
		}

		/// <summary>
		/// Plays a random audio effect from the collection
		/// </summary>
		/// <param name="audioCollection">Audio collection.</param>
		public void PlayRandomWorldAudio(AudioCollection audioCollection)
		{
			PlayWorld(audioCollection.Random);
		}

		/// <summary>
		/// Play the audio clip 
		/// </summary>
		/// <param name="clip"></param>
		public void PlayWorldAudioIfNotPlaying(AudioClip clip)
		{
			if (!AudioManager.Instance.IsPlaying(clip))
				AudioManager.Instance.PlayWorldAudio(clip, transform.position, setting);
		}

        /// <summary>
        /// Fade out the given audio source
        /// </summary>
        /// <param name="source"></param>
        public void FadeOutSource(AudioSource source)
        {
            AudioManager.Instance.FadeAudio(source, fadeTime, source.volume, 0f);
        }

		/// <summary>
		/// Cross fade into the given audio
		/// </summary>
		/// <param name="audio"></param>
		public void FadeToAudio(AudioClip audio)
		{
			int key = audio.GetInstanceID();
			AudioManager.Instance.FadeAudio(AudioManager.Instance.PlayLocalAudio(audio, transform, setting), key, setting.volume, fadeTime);
		}

		/// <summary>
		/// Fade into the audio clip given under the set key
		/// </summary>
		/// <param name="clip"></param>
		/// <param name="key"></param>
		/// <param name="toVolume"></param>
		/// <param name="space"></param>
		public void FadeAudio(AudioClip clip, float fromVolume, float toVolume, AudioSpace space)
		{
			AudioSource source;

			if (space == AudioSpace.WORLD)
				source = AudioManager.Instance.PlayWorldAudio(clip, transform.position, setting);
			else
				source = AudioManager.Instance.PlayLocalAudio(clip, transform, setting);

			AudioManager.Instance.FadeAudio(source, fadeTime);
		}

		#endregion // PUBLIC_METHODS
	}
}