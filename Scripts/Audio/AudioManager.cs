﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Bubblegum.Audio
{

    /// <summary>
    /// Model type class for managing the applications audio
    /// </summary>
    public class AudioManager : Singleton<AudioManager>
    {
        #region PUBLIC_VARIABLES

        /// <summary>
        /// The default setting to use
        /// </summary>
        [SerializeField, Tooltip("The default setting to use")]
        private AudioSource defaultSetting;

        /// <summary>
        /// The minimum amount of audio objects to create
        /// </summary>
        [SerializeField, Tooltip("The minimum amount of audio objects to create")]
        private int poolSize = 10;

        #endregion

        #region PRIVATE_VARIABLES

        /// <summary>
        /// The offset to kill audios
        /// </summary>
        private const float AUDIO_KILL_OFFSET = 0.2f;

        /// <summary>
        /// The fade calculation
        /// </summary>
        private const float FADE_DELAY = 0.02f;

        /// <summary>
        /// The pool containing all audio players
        /// </summary>
        private List<AudioSource> pool = new List<AudioSource>();

        #endregion // PRIVATE_VARIABLES

        #region MONOBEHAVIOUR_METHODS

        /// <summary>
        /// Called when all objects have been initialized regardless of whether the script is enabled
        /// </summary>
        protected override void Awake()
        {
            base.Awake();

            for (int i = 0; i < poolSize; i++)
                pool.Add(Instantiate(defaultSetting, transform));
        }

        #endregion // MONOBEHAVIOUR_METHODS

        #region PUBLIC_METHODS

        /// <summary>
        /// Play audio with default settings
        /// </summary>
        /// <param name="clip"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public AudioSource PlayAudio(AudioClip clip)
        {
            return PlayAudio(clip, defaultSetting);
        }

        /// <summary>
        /// Play audio with default settings
        /// </summary>
        /// <param name="clip"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public AudioSource PlayAudio(AudioClip clip, AudioSource setting)
        {
			AudioSource audio = pool.FirstOrDefault(item => !item.isPlaying);

			if (!audio)
				throw new System.Exception("Could not play audio as audio manager has reached max size");

            audio.CopySettings(setting);
			audio.clip = clip;
            audio.Play();

            return audio;
        }

        /// <summary>
        /// Plays the audio in world space using a special audio object
        /// The audio will NOT get cleaned up when the object is destroyed
        /// </summary>
        public AudioSource PlayWorldAudio(AudioClip clip, Vector3 position, AudioSource setting)
        {
            AudioSource audio = PlayAudio(clip, setting);
            audio.transform.position = position;

            return audio;
        }

        /// <summary>
        /// Plays the audio in local space using a special audio object
        /// The audio will get cleaned up when the object is destroyed
        /// </summary>
        public AudioSource PlayLocalAudio(AudioClip clip, Transform transform, AudioSource setting)
        {
            AudioSource audio = PlayAudio(clip, setting);
            audio.transform.SetParent(this.transform);
            audio.transform.localPosition = Vector3.zero;

            return audio;
        }

        /// <summary>
        /// Cross fade to the given audio
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public Coroutine FadeAudio(AudioSource audio, float fadeTime, float fromVolume = 0f, float toVolume = 1f)
        {
            return StartCoroutine(AnimateFadeAudio(audio, fadeTime, Mathf.Clamp01(fromVolume), toVolume));
        }

        /// <summary>
        /// Fade between the two audio clips
        /// </summary>
        /// <param name="fromAudio"></param>
        /// <param name="toAudio"></param>
        /// <param name="fadeTime"></param>
        public void CrossFadeAudio(AudioSource fromAudio, AudioSource toAudio, float fadeTime, float toVolume = 1f)
        {
            StartCoroutine(AnimateFadeAudio(fromAudio, fadeTime, fromAudio.volume, 0f));
            StartCoroutine(AnimateFadeAudio(toAudio, fadeTime, 0f, toVolume));
        }

		/// <summary>
		/// If we are playing the given clip
		/// </summary>
		/// <param name="clip"></param>
		/// <returns></returns>
		public bool IsPlaying(AudioClip clip)
		{
			for (int i = 0; i < pool.Count; i++)
				if (pool[i].clip == clip && pool[i].isPlaying)
					return true;

			return false;
		}

        #endregion // PUBLIC_METHODS

        #region PRIVATE_METHODS

        /// <summary>
        /// Cross fade to the given audio
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        private IEnumerator AnimateFadeAudio(AudioSource audio, float fadeTime, float fromVolume, float toVolume)
        {
            if (audio)
            {
                float fadeIncrement = FADE_DELAY / fadeTime;

                if (audio.volume > toVolume)
                    while (audio && audio.volume > toVolume)
                    {
                        audio.volume -= fadeIncrement;
                        yield return Yielder.Get(FADE_DELAY);
                    }
                else
                    while (audio && audio.volume < toVolume)
                    {
                        audio.volume += fadeIncrement;
                        yield return Yielder.Get(FADE_DELAY);
                    }

                if (audio)
                    audio.volume = toVolume;

				if (toVolume == 0f)
					audio.Stop();
            }
        }

        #endregion // PRIVATE_METHODS
    }
}