﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections.Generic;

namespace Bubblegum.Audio
{
    /// <summary>
    /// Enables a specific mixer snapshot when this object is enabled
    /// </summary>
    public class MixerSnapshotControl : MonoBehaviour
    {
        #region VARIABLES

        /// <summary>
        /// The snapshot for to enable
        /// </summary>
        [SerializeField, Tooltip("The snapshot for to enable")]
        private AudioMixerSnapshot snapshot;

        /// <summary>
        /// The time to transition into the snapshot
        /// </summary>
        [SerializeField, Tooltip("The time to transition into the snapshot")]
        private float transitionTime = 0.5f;

        #endregion

        #region ENUMS

        /// <summary>
        /// All of the enabled controls
        /// </summary>
        private static List<MixerSnapshotControl> controls = new List<MixerSnapshotControl>();

        #endregion

        #region METHODS

        /// <summary>
        /// Enable this object
        /// </summary>
        void OnEnable()
        {
            EnableSnapshot();
        }

        /// <summary>
        /// Disable this object
        /// </summary>
        void OnDisable()
        {
            DisableSnapshot();
        }

        /// <summary>
        /// Enable the snapshot
        /// </summary>
        public void EnableSnapshot()
        {
            controls.Add(this);
            snapshot.TransitionTo(transitionTime);
        }

        /// <summary>
        /// Disable this snapshot
        /// </summary>
        public void DisableSnapshot()
        {
            controls.Remove(this);

            if (controls.Count > 0)
                controls[controls.Count - 1].snapshot.TransitionTo(transitionTime);
        }

        #endregion

    }
}