﻿using UnityEngine;

namespace Bubblegum
{

	/// <summary>
	/// Returns height information based on coordinates
	/// </summary>
	public interface IHeightMap
	{
		#region METHODS

		/// <summary>
		/// More accurate form of getting map height
		/// </summary>
		/// <param name="position"></param>
		/// <returns></returns>
		float GetHeightAtPosition(Vector3 position);

		/// <summary>
		/// Get the height at the given coordinates
		/// </summary>
		float GetHeightAtCoordinates(Vector2Int coordinates);

		/// <summary>
		/// Set the height at the given coordinates
		/// </summary>
		void SetHeightAtCoordinates(Vector2Int coordinates, float height);

		#endregion
	}
}