﻿using UnityEngine;

namespace Bubblegum.Control
{

	/// <summary>
	/// Component used to alter the transform
	/// </summary>
	public abstract class TransformControl : UpdatingBehaviour
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// If input is enabled for this control
		/// </summary>
		public bool InputEnabled { get; set; } = true;

		/// <summary>
		/// The input object
		/// </summary>
		public IInput Input { get; set; }

		/// <summary>
		/// Our timekeeper object
		/// </summary>
		public ITimeKeeper TimeKeeper { get; set; }

		[Header("Input")]

		/// <summary>
		/// Object to generate input values from, must inherit IInput
		/// </summary>
		[SerializeField, Tooltip("Object to generate input values from, must inherit IInput")]
		private Object inputObject;

		/// <summary>
		/// The time object to use
		/// </summary>
		[SerializeField, Tooltip("The time object to use")]
		private Object timeObject;

		#endregion // PUBLIC_VARIABLES

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Awake this component
		/// </summary>
		protected virtual void Awake()
		{
			OnValidate();
		}

		/// <summary>
		/// Validate changes to this object
		/// </summary>
		void OnValidate()
		{
			Input = inputObject.Validate<IInput>();
			TimeKeeper = timeObject.Validate<ITimeKeeper>();
		}

		#endregion // MONOBEHAVIOUR_METHODS

	}
}