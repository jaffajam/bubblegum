﻿using UnityEngine;

namespace Bubblegum.Control
{

	/// <summary>
	/// Object that is selectable in some way
	/// </summary>
	public class Selectable : CacheBehaviour
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// If we are currently selected
		/// </summary>
		public virtual bool Selected { get; protected set; }

		[Header("Selecting")]

		/// <summary>
		/// Actions to invoke when the touch starts on this object
		/// </summary>
		public System.Action onSelectActions;

		/// <summary>
		/// Actions to invoke when the touch on this object ends
		/// </summary>
		public System.Action onReleaseActions;

		#endregion // PUBLIC_VARIABLES

		#region PUBLIC_METHODS

		/// <summary>
		/// When this object is released
		/// </summary>
		/// <param name="hit"></param>
		public void OnRelease()
		{
			if (onReleaseActions != null)
				onReleaseActions();
		}

		/// <summary>
		/// When this object is selected
		/// </summary>
		/// <param name="hit"></param>
		public void OnSelect()
		{
			if (onSelectActions != null)
				onSelectActions();
		}

		/// <summary>
		/// Toggle this item as selected
		/// </summary>
		public void ToggleSelected()
		{
			Selected = !Selected;

			if (Selected)
				OnSelect();
			else
				OnRelease();
		}

		#endregion // PUBLIC_METHODS
	}
}