﻿using UnityEngine;

namespace Bubblegum.Control
{

	/// <summary>
	/// Disables and enables control when an object is on/off the ground
	/// </summary>
	public class GroundedControl : MonoBehaviour, IContactControl
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// If even one foot/wheel is touching the ground
		/// </summary>
		public bool Contacting
		{
			get
			{
				for (int i = 0; i < checkPoints.Length; i++)
					if (checkPoints[i].Contacting)
						return true;

				return false;
			}
		}

		/// <summary>
		/// If all feet/wheels are touching the ground
		/// </summary>
		public bool ContactedAll
		{
			get
			{
				for (int i = 0; i < checkPoints.Length; i++)
					if (!checkPoints[i].Contacting)
						return false;

				return true;
			}
		}

		/// <summary>
		/// The ground we are currently on
		/// </summary>
		public Collider Contact
		{
			get
			{
				return checkPoints[0].Contact;
			}
		}

		/// <summary>
		/// The contact point
		/// </summary>
		public Vector3 ContactPoint
		{
			get
			{
				return checkPoints[0].ContactPoint;
			}
		}

		/// <summary>
		/// All of the points to check when monitoring if we are contacting
		/// </summary>
		[SerializeField, Tooltip("Point to use for checking if grounded")]
		private Object[] contactObjects;

		#endregion

		#region PRIVATE_VARIABLES

		/// <summary>
		/// All of the points to use for checking contact
		/// </summary>
		private IContactControl[] checkPoints;

		#endregion

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Awaken this instance
		/// </summary>
		void Awake()
		{
			OnValidate();
		}

		/// <summary>
		/// Validate the input in the editor
		/// </summary>
		void OnValidate()
		{
			checkPoints = contactObjects.Validate<IContactControl>();
		}

		#endregion
	}
}