﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;
using Bubblegum.Inputs;

namespace Bubblegum.Control
{

	/// <summary>
	/// An object that can be dragged with input touch
	/// </summary>
	public class Dragable : Selectable, IBeginDragHandler, IEndDragHandler, IInitializable
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// The input class we are using
		/// </summary>
		protected IInput Input { get; set; }

		/// <summary>
		/// Set the move type
		/// </summary>
		public DragAxisType MoveType
		{
			set
			{
				moveType = value;
			}

			get
			{
				return moveType;
			}
		}

		/// <summary>
		/// Returns false
		/// </summary>
		public bool Initialized
		{
			get
			{
				return false;
			}
		}

		/// <summary>
		/// Get the dragging transform
		/// </summary>
		public Transform DraggingTransform
		{
			get
			{
				return draggingTransform;
			}
		}

		/// <summary>
		/// The position we were selected in
		/// </summary>
		public Vector3 SelectedPosition { get; set; }

		[Header("Dragging")]

		/// <summary>
		/// Object to generate input values from, must inherit IInput
		/// </summary>
		[SerializeField, Tooltip("Object to generate input values from, must inherit IInput")]
		private Object inputObject;

		/// <summary>
		/// The move type to use
		/// </summary>
		[SerializeField, Tooltip("The move type to use")]
		private DragAxisType moveType = DragAxisType.XY;

		/// <summary>
		/// The actual transform that we move around once the collider is selected, uses this transform by default
		/// </summary>
		[SerializeField, Tooltip("The actual transform that we move around once the collider is selected, uses this transform by default")]
		protected Transform draggingTransform;

		/// <summary>
		/// If we should remove the parent when dragged
		/// </summary>
		[SerializeField, Tooltip("If we should remove the parent when dragged")]
		protected bool clearParentWhenDrag;

		/// <summary>
		/// If we should disable the colliders when dragging
		/// </summary>
		[Tooltip("If we should disable the colliders when dragging")]
		[SerializeField]
		protected bool disableColliders = true;

		/// <summary>
		/// If we should maintain pointer offset when dragging
		/// </summary>
		[SerializeField, Tooltip("If we should maintain pointer offset when dragging")]
		private bool maintainPointerOffset = true;

		/// <summary>
		/// How much to offset from the pointer position
		/// </summary>
		[SerializeField, Tooltip("How much to offset from the pointer position")]
		protected Vector3 offset;

		/// <summary>
		/// If we should snap the position
		/// </summary>
		[SerializeField, Tooltip("If we should snap the position")]
		public bool snap = true;

		/// <summary>
		/// The amount of rounding to do on the position
		/// </summary>
		[SerializeField, Tooltip("The amount of rounding to do on the position"), DisplayIf("snap", true)]
		private float snapSize = 1f;

		#endregion // PUBLIC_VARIABLES

		#region PRIVATE_VARIABLES

		/// <summary>
		/// If there is a rigidbody attached we will have to disable use gravity when it is dragged
		/// </summary>
		protected Rigidbody rBody;

		/// <summary>
		/// If we are kinematic by default
		/// </summary>
		private bool defaultKinematic;

		/// <summary>
		/// The colliders to disable when dragging
		/// </summary>
		private Collider[] colliders;

		/// <summary>
		/// Offset at which to drag the object
		/// </summary>
		private Vector3 dragOffset;

		/// <summary>
		/// The camera distance offset
		/// </summary>
		private float cameraDistance;

		/// <summary>
		/// The colliders that are enabled
		/// </summary>
		private bool[] collidersEnabled;

		#endregion // PRIVATE_VARIABLES

		#region ENUMS

		/// <summary>
		/// The axis selection type
		/// </summary>
		public enum DragAxisType { X, Y, Z, XY, XZ, YZ, XYZ }

		#endregion

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Called when all objects have been initialized regardless of whether the script is enabled
		/// </summary>
		protected virtual void Awake()
		{
			Initialize();
		}

		/// <summary>
		/// Check all variables changed in inspector
		/// </summary>
		void OnValidate()
		{
			Input = inputObject.Validate<IInput>();
		}

		/// <summary>
		/// Update this component
		/// </summary>
		protected virtual void Update()
		{
			if (Selected)
			{
				if (InputController.InputEnabled && !Input.GetButton(CommonInput.FIRE_1))
				{
					ForceDeselect();
					return;
				}

				UpdateMovement();
			}
		}

		#endregion // MONOBEHAVIOUR_METHODS

		#region PUBLIC_METHODS

		/// <summary>
		/// Intercept the pointer down event
		/// </summary>
		/// <param name="eventData"></param>
		public virtual void OnBeginDrag(PointerEventData eventData)
		{
			SelectedPosition = Camera.current.transform.InverseTransformPoint(transform.position);
			OnSelect();
			Select();
		}

		/// <summary>
		/// Intercept the pointer up event
		/// </summary>
		/// <param name="eventData"></param>
		public virtual void OnEndDrag(PointerEventData eventData)
		{
			OnRelease();
			Selected = false;

			EnablePhysics(true);

			if (disableColliders)
				EnableColliders(true);
		}

		/// <summary>
		/// Force select this object
		/// </summary>
		public void ForceSelect()
		{
			var pointerData = new PointerEventData(EventSystem.current);
			ExecuteEvents.Execute(gameObject, pointerData, ExecuteEvents.beginDragHandler);
		}

		/// <summary>
		/// Force select this object
		/// </summary>
		public void ForceDeselect()
		{
			var pointerData = new PointerEventData(EventSystem.current);
			ExecuteEvents.Execute(gameObject, pointerData, ExecuteEvents.endDragHandler);
		}

		/// <summary>
		/// Initialize this object
		/// </summary>
		public void Initialize()
		{
			Input = inputObject.Validate<IInput>();

			if (!draggingTransform)
				draggingTransform = transform;

			rBody = draggingTransform.GetComponent<Rigidbody>();
			colliders = draggingTransform.GetComponentsInChildren<Collider>();
			collidersEnabled = colliders.Select(collider => collider.enabled).ToArray();

			if (rBody)
				defaultKinematic = rBody.isKinematic;
		}

		/// <summary>
		/// Force movement to the pointer ignoring offsets
		/// </summary>
		public void MoveToPointer()
		{
			if (draggingTransform is RectTransform)
				transform.position = GetUIDragPosition();
			else
				transform.position = GetDragPosition();
		}

		/// <summary>
		/// Correct our position to the offset objects position
		/// </summary>
		/// <param name="offsetObject"></param>
		public void CorrectPosition(Transform offsetObject)
		{
			Vector3 position = offsetObject.position;
			transform.position = position;

			if (offsetObject.parent == transform)
				offsetObject.position = position;
		}

		#endregion // PUBLIC_METHODS

		#region PRIVATE_METHODS

		/// <summary>
		/// Update the objects movement
		/// </summary>
		protected void UpdateMovement()
		{
			if (draggingTransform is RectTransform)
				UpdateMovement(GetUIDragPosition() + dragOffset + offset);
			else
				UpdateMovement(GetDragPosition() + dragOffset + offset);
		}

		/// <summary>
		/// Updates the movement depending on the mode that is selected
		/// </summary>
		/// <param name="touchInfo">Touch info.</param>
		protected void UpdateMovement(Vector3 pos)
		{
			Vector3 cameraPos = Camera.current.transform.InverseTransformPoint(pos);

			switch (moveType)
			{
				case DragAxisType.X:
					cameraPos.y = SelectedPosition.y;
					cameraPos.z = SelectedPosition.z;
					break;

				case DragAxisType.Y:
					cameraPos.z = SelectedPosition.z;
					cameraPos.x = SelectedPosition.x;
					break;

				case DragAxisType.Z:
					cameraPos.x = SelectedPosition.x;
					cameraPos.y = SelectedPosition.y;
					break;

				case DragAxisType.XY:
					cameraPos.z = SelectedPosition.z;
					break;

				case DragAxisType.XZ:
					cameraPos.y = SelectedPosition.y;
					break;

				case DragAxisType.YZ:
					cameraPos.x = SelectedPosition.x;
					break;
			}

			pos = Camera.current.transform.TransformPoint(cameraPos);

			if (snap)
				pos = new Vector3(Mathf.Round(pos.x / snapSize) * snapSize, Mathf.Round(pos.y / snapSize) * snapSize, Mathf.Round(pos.z / snapSize) * snapSize);

			draggingTransform.position = pos;
		}

		/// <summary>
		/// Select this item
		/// </summary>
		private void Select()
		{
			Selected = true;
			cameraDistance = Vector3.Distance(Camera.current.transform.position, transform.position);
			EnablePhysics(false);

			if (disableColliders)
				EnableColliders(false);

			if (maintainPointerOffset)
				dragOffset = transform.position - GetDragPosition();

			if (clearParentWhenDrag)
				if (draggingTransform is RectTransform)
					draggingTransform.SetParent(draggingTransform.root);
				else
					transform.SetParent(null);
		}

		/// <summary>
		/// Enable or disable physics
		/// </summary>
		/// <param name="enabled"></param>
		protected void EnablePhysics(bool enabled)
		{
			if (rBody)
				rBody.isKinematic = enabled ? defaultKinematic : true;
		}

		/// <summary>
		/// Enable or disable colliders
		/// </summary>
		/// <param name="enabled"></param>
		protected void EnableColliders(bool enabled)
		{
			if (disableColliders)
				for (int i = 0; i < colliders.Length; i++)
					colliders[i].enabled = enabled ? collidersEnabled[i] : enabled;
		}

		/// <summary>
		/// Get the position of where the user is dragging
		/// </summary>
		protected Vector3 GetDragPosition()
		{
			return Camera.current.ScreenToWorldPoint(
				new Vector3(Input.mousePosition.x, Input.mousePosition.y, cameraDistance));
		}

		/// <summary>
		/// Get the position of where the user is dragging
		/// </summary>
		protected Vector3 GetUIDragPosition()
		{
			return Input.mousePosition;
		}

		#endregion // PRIVATE_METHODS
	}
}
