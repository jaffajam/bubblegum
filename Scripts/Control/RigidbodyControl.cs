﻿using UnityEngine;

namespace Bubblegum.Control
{

	/// <summary>
	/// Component used to alter the transform
	/// </summary>
	public abstract class RigidbodyControl : MonoBehaviour
	{
		#region PUBLIC_VARIABLES

		/// <summary>
		/// The input object
		/// </summary>
		public IInput Input { get; set; }

		/// <summary>
		/// Our timekeeper object
		/// </summary>
		public ITimeKeeper TimeKeeper { get; set; }

		/// <summary>
		/// Our rigidbody we are controlling
		/// </summary>
		public Rigidbody Rigidbody { get; private set; }

		[Header("Input")]

		/// <summary>
		/// If we should debug this component
		/// </summary>
		[SerializeField, Tooltip("If we should debug this component")]
		protected bool debug;

		/// <summary>
		/// Object to generate input values from, must inherit IInput
		/// </summary>
		[SerializeField, Tooltip("Object to generate input values from, must inherit IInput")]
		private Object inputObject;

		/// <summary>
		/// The time object to use
		/// </summary>
		[SerializeField, Tooltip("The time object to use")]
		private Object timeObject;

		#endregion // PUBLIC_VARIABLES

		#region PRIVATE_VARIABLES

		/// <summary>
		/// All of the grounded controls
		/// </summary>
		protected IContactControl[] groundedControls;

		#endregion

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Awake this component
		/// </summary>
		protected virtual void Awake()
		{
			OnValidate();
			Rigidbody = GetComponent<Rigidbody>();
			groundedControls = GetComponentsInChildren<IContactControl>();
		}

		/// <summary>
		/// Validate changes to this object
		/// </summary>
		void OnValidate()
		{
            Input = inputObject.Validate<IInput>();
			TimeKeeper = timeObject.Validate<ITimeKeeper>();
        }

        #endregion // MONOBEHAVIOUR_METHODS

    }
}