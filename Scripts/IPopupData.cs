﻿using System;
using UnityEngine;

namespace Bubblegum
{
    /// <summary>
    /// An object with popup data
    /// </summary>
    public interface IPopupData
    {
        #region VARIABLES

        /// <summary>
        /// Get the title string
        /// </summary>
        string Title { get; }

        /// <summary>
        /// Get the message string
        /// </summary>
        string Message { get; }

        /// <summary>
        /// Get the icon
        /// </summary>
        Sprite Icon { get; }

        /// <summary>
        /// Get the audio to play
        /// </summary>
        AudioClip Audio { get; }

        /// <summary>
        /// Type of popup
        /// </summary>
        string PopupType { get; }

        /// <summary>
        /// Get the additional content that we want to show
        /// </summary>
        GameObject AdditionalContent { get; }

        /// <summary>
        /// Get the confirm action
        /// </summary>
        Action ConfirmAction { get; }

        /// <summary>
        /// Get the cancel action
        /// </summary>
        Action CancelAction { get; }

        #endregion
    }
}