﻿using System;

namespace Bubblegum
{
    /// <summary>
    /// Display setters interface exposing OnSetDisplay event
    /// </summary>
    public interface IDisplaySetter<T>
    {
        /// <summary>
        /// Register on set display action
        /// </summary>
        void RegisterOnSetDisplay(Action<T> action, T value);

        /// <summary>
        /// Unregister on set display action
        /// </summary>
        void UnregisterOnSetDisplay(Action<T> action);
    }
}