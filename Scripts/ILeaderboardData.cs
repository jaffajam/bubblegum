﻿using System;
using System.Collections.Generic;

namespace Bubblegum
{
    /// <summary>
    /// Leaderboard entry interface
    /// </summary>
    public interface ILeaderboardData
    {
        /// <summary>
        /// Event invoked on leaderboard data update
        /// </summary>
        event Action onUpdate;

        /// <summary>
        /// Current Player highscore
        /// </summary>
        ILeaderboardEntry PlayerHighScore { get; }

        /// <summary>
        /// Leaderboard entries
        /// </summary>
        List<ILeaderboardEntry> LeaderboardEntries { get; }

        /// <summary>
        /// Leaderboard prizes
        /// </summary>
        List<ILeaderboardPrize> LeaderboardPrizes { get; }

        /// <summary>
        /// Default name if not available
        /// </summary>
        string DefaultName { get; }

        /// <summary>
        /// Leaderboard end time
        /// </summary>
        DateTime? EndDateTime { get; }

        /// <summary>
        /// Leaderboard display name
        /// </summary>
        string DisplayName { get; }
    }
}