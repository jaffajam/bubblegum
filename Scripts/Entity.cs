﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Bubblegum
{

    /// <summary>
    /// Acts as an Asset Object of data for various uses, mainly as a fixed reference point
    /// </summary>
    [CreateAssetMenu(menuName = "Scriptable Object/Entity")]
	public class Entity : Key, ISerializationCallbackReceiver, ISaveData
	{
		#region PUBLIC_VARIABLES

		/// <summary>
		/// Get the unsorted data
		/// </summary>
		public virtual object[] UnsortedData
		{
			get
			{
				return data.Select(item => item.dynamicValue.Value).ToArray();
			}
		}

		/// <summary>
		/// Get the description
		/// </summary>
		public virtual string Description { get { return description; } }

		/// <summary>
		/// When an entity is created
		/// </summary>
		public static System.Action<Entity, Object> onEntityRegistered, onEntityDeregistered;

        /// <summary>
        /// Invoked when an entity is unlocked
        /// </summary>
        public static System.Action<Entity> onEntityUnlocked;

        [Header("Entity")]

        /// <summary>
        /// The parent entity (if any)
        /// </summary>
        [SerializeField, Tooltip("The parent entity (if any)")]
		public Entity parent;

		/// <summary>
		/// If this entity should be unlocked by default
		/// </summary>
		[SerializeField, Tooltip("If this entity should be unlocked by default")]
		protected bool unlockedByDefault = true;

		/// <summary>
		/// The card description
		/// </summary>
		[SerializeField, Tooltip("The description to display for the card"), TextArea]
		protected string description;

		/// <summary>
		/// The data that we want to hold for this entity
		/// </summary>
		[SerializeField, Tooltip("The data that we want to hold for this entity")]
		protected DynamicValueData[] data = new DynamicValueData[0];

		#endregion // PUBLIC_VARIABLES

		#region PRIVATE_VARIABLES

		/// <summary>
		/// Check if the entity is unlocked
		/// </summary>
		private const string PREFS_PREFIX = "Unlocked";

		/// <summary>
		/// All of the different entities that exist for an entity type
		/// </summary>
		private static Dictionary<int, List<Object>> entities = new Dictionary<int, List<Object>>();

		/// <summary>
		/// All of the information about the entity
		/// </summary>
		private Dictionary<string, object> information = new Dictionary<string, object>();

        /// <summary>
        /// Key for playerprefs
        /// </summary>
        public string PrefsKey
        {
            get
            {
                if (string.IsNullOrEmpty(cachedPrefsKey))
                    cachedPrefsKey = PREFS_PREFIX + ID;

                return cachedPrefsKey;
            }
        }
        private string cachedPrefsKey;

        #endregion // PRIVATE_VARIABLES

        #region PUBLIC_METHODS

        /// <summary>
        /// Find all entity objects of the given type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static List<Object> FindEnityObjects(Entity type)
		{
			return entities[type.ID];
		}

		/// <summary>
		/// Unlock the given entity
		/// </summary>
		/// <param name="entity"></param>
		/// <returns></returns>
		public virtual void Unlock()
		{
            if (PersistentData.GetInt(PrefsKey, unlockedByDefault ? 1 : 0) == 1)
                return;

            PersistentData.SetInt(PrefsKey, 1);
            onEntityUnlocked?.Invoke(this);
		}

		/// <summary>
		/// Lock the given entity
		/// </summary>
		/// <param name="entity"></param>
		/// <returns></returns>
		public virtual void Lock()
		{
            PersistentData.SetInt(PrefsKey, 0);
		}

		/// <summary>
		/// Check if the given entity is locked
		/// </summary>
		/// <returns></returns>
		public bool IsUnlocked()
		{
			return unlockedByDefault || PersistentData.GetInt(PrefsKey, 0) == 1;
		}

		/// <summary>
		/// Registers the given entity to be monitored
		/// </summary>
		/// <param name="entity">Entity.</param>
		public static void RegisterEntity(Entity entity, Object obj)
		{
            if (!entities.ContainsKey(entity.ID))
                entities[entity.ID] = new List<Object>();

			entities[entity.ID].Add(obj);
			onEntityRegistered?.Invoke(entity, obj);
		}

		/// <summary>
		/// Deregister the entity from the application
		/// </summary>
		/// <param name="entity"></param>
		public static void DeregisterEntity(Entity entity, Object obj)
		{
			entities[entity.ID].Remove(obj);
			onEntityDeregistered?.Invoke(entity, obj);
		}

		/// <summary>
		/// Check if the given entity is or inherits this one
		/// </summary>
		/// <param name="entity"></param>
		/// <returns></returns>
		public bool IsEntity(Entity entity)
		{
			if (ID == entity.ID)
				return true;
			else if (parent)
				if (parent.ID == ID)
					Debug.LogError(name + " parent check caused infinite loop");
				else
					return parent.IsEntity(entity);

			return false;
		}

		/// <summary>
		/// Check if the data contains the following key
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key"></param>
		/// <returns></returns>
		public bool HasData(string key)
		{
			return information.ContainsKey(key);
		}

		/// <summary>
		/// Get the type of the data object with the given key
		/// </summary>
		/// <returns></returns>
		public System.Type GetDataType(string key)
		{
			if (information.ContainsKey(key))
				return information[key].GetType();

			return null;
		}

		/// <summary>
		/// Get data for this entity
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public virtual T GetData<T>(string key, T defaultValue = default(T))
		{
			try
			{
				if (information.ContainsKey(key))
					return (T)information[key];
			}
			catch (System.InvalidCastException)
			{
				Debug.LogError(name + " does not have data for key " + key + " of type " + typeof(T).Name);
			}

			return defaultValue;
		}

		/// <summary>
		/// Get the data from the parent hierarchy
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public T GetDataFromParent<T>(string key, T defaultValue = default(T))
		{
			return HasData(key) ? GetData(key, defaultValue) : parent ? parent.GetData(key, defaultValue) : defaultValue;
		}

		/// <summary>
		/// Set a temporary data value
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		public void SetData(string key, object value)
		{
			information[key] = value;
		}

		/// <summary>
		/// Get the root entity;
		/// </summary>
		/// <param name="entity"></param>
		/// <returns></returns>
		public Entity GetRoot()
		{
			if (parent != null)
				return parent.GetRoot();

			return this;
		}

		/// <summary>
		/// After we deserialize the object
		/// </summary>
		public void OnAfterDeserialize()
		{
			information.Clear();

			foreach (DynamicValueData value in data)
				if (value.key != null && !information.ContainsKey(value.key))
					information.Add(value.key, value.dynamicValue.Value);
		}

        /// <summary>
        /// Before this object is serialized
        /// </summary>
        public void OnBeforeSerialize() { }

        /// <summary>
        /// Apply the patch using the given object(will be the same object with updated data)
        /// </summary>
        public override bool ApplyPatch()
        {
            if (!base.ApplyPatch())
                return false;

            Entity real = GetKey<Entity>(id);
            real.description = description;
            real.unlockedByDefault = unlockedByDefault;
            real.information = new Dictionary<string, object>(information);

            return true;
        }

        /// <summary>
        /// Set data from dictionary to the object
        /// </summary>
        public void SetSaveData(Dictionary<string, SaveDataKeyValuePair> data)
        {
            PersistentData.SetInt(PrefsKey, data.ContainsKey(PrefsKey) ? System.Convert.ToInt32(data[PrefsKey].value) : (unlockedByDefault ? 1 : 0));
        }

        /// <summary>
        /// Get the save data from this object and save it to the dictionary
        /// </summary>
        public void GetSaveData(Dictionary<string, SaveDataKeyValuePair> data)
        {
            data[PrefsKey] = new SaveDataKeyValuePair(Name, PersistentData.GetInt(PrefsKey, unlockedByDefault ? 1 : 0));
        }

        #endregion // PUBLIC_METHODS

        #region SUB_CLASSES

#if UNITY_EDITOR

        /// <summary>
        /// Editor script for the entity objects
        /// </summary>
        [CustomEditor(typeof(Entity), true), CanEditMultipleObjects]
		public class EntityEditor : KeyEditor
		{
			#region VARIABLES

			/// <summary>
			/// The copied data
			/// </summary>
			private static DynamicValueData[] copiedData;

			#endregion

			#region METHODS

			/// <summary>
			/// Draw the inspector
			/// </summary>
			public override void OnInspectorGUI()
			{
				base.OnInspectorGUI();

                if (GUILayout.Button("Unlock"))
                    ((Entity)target).Unlock();

                if (GUILayout.Button("Copy Data"))
					copiedData = ((Entity)target).data;

				if (GUILayout.Button("Paste Data") && copiedData != null)
				{
                    foreach (Object obj in Selection.objects)
                    {
                        if (obj is Entity)
                        {
                            Entity entity = ((Entity)obj);
                            entity.data = new DynamicValueData[copiedData.Length];

                            for (int i = 0; i < copiedData.Length; i++)
                                entity.data[i] = new DynamicValueData(copiedData[i].key, copiedData[i].dynamicValue.Value);

                            EditorUtility.SetDirty(obj);
                        }
                    }
				}
			}

			#endregion
		}

#endif

		#endregion
	}
}