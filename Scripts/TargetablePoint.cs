﻿using UnityEngine;

namespace Bubblegum
{
	/// <summary>
	/// A point that can be targeted
	/// </summary>
	public class TargetablePoint : MonoBehaviour, ITargetable
	{
		#region VARIABLES

		/// <summary>
		/// Get the target point
		/// </summary>
		public Target MoveTarget
		{
			get
			{
				if (moveTarget.IsEmpty)
					moveTarget = new Target(movePoint);

				return moveTarget;
			}
		}

		/// <summary>
		/// Get the target point
		/// </summary>
		public Target LookTarget
		{
			get
			{
				if (lookTarget.IsEmpty)
					lookTarget = new Target(lookPoint);

				return lookTarget;
			}
		}

		/// <summary>
		/// Target to look at
		/// </summary>
		[SerializeField, Tooltip("Target to look at")]
		private Transform lookPoint;

		/// <summary>
		/// Target to move to
		/// </summary>
		[SerializeField, Tooltip("Target to move to")]
		private Transform movePoint;


		/// <summary>
		/// Target to look at
		/// </summary>
		private Target lookTarget;

		/// <summary>
		/// Target to move to
		/// </summary>
		private Target moveTarget;

		#endregion
	}
}