﻿using UnityEngine;

namespace Bubblegum.Utility
{
    /// <summary>
    /// Manual control over what lightmaps to use
    /// </summary>
    public class LightmapSettingsControl : MonoBehaviour, IInitializable
    {
        #region VARIABLES

        /// <summary>
        /// If this component is initialized
        /// </summary>
        public bool Initialized
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// The method that we use for initialization
        /// </summary>
        [SerializeField, Tooltip("The method that we use for initialization")]
        private InitializeMethod initializeMethod;

        /// <summary>
        /// All of the lightmaps to load
        /// </summary>
        [SerializeField, Tooltip("All of the lightmaps to load")]
        private Texture2D[] dirMaps, lightMaps;

        /// <summary>
        /// Data ready for lightmap settings
        /// </summary>
        private LightmapData[] maps;

        #endregion

        #region METHODS

        /// <summary>
        /// Awaken this object
        /// </summary>
        void Awake()
        {
            if (initializeMethod == InitializeMethod.Awake)
                Initialize();
        }

        /// <summary>
        /// Start this object
        /// </summary>
        void Start()
        {
            if (initializeMethod == InitializeMethod.Start)
                Initialize();
        }

        /// <summary>
        /// Awaken this object
        /// </summary>
        void OnEnable()
        {
            if (initializeMethod == InitializeMethod.Enable)
                Initialize();
        }

        /// <summary>
        /// Initialize this component
        /// </summary>
        public void Initialize()
        {
            if (dirMaps.Length != lightMaps.Length)
            {
                Debug.LogError("Lightmaps must be of equal length");
                return;
            }

            maps = new LightmapData[dirMaps.Length];

            for (int i = 0; i < maps.Length; i++)
            {
                maps[i] = new LightmapData();
                maps[i].lightmapDir= dirMaps[i];
                maps[i].lightmapColor = lightMaps[i];
            }

            LightmapSettings.lightmaps = maps;
        }

        #endregion
    }
}