using UnityEngine;

namespace Bubblegum.Rendering
{

	[RequireComponent(typeof(MeshFilter))]
	[RequireComponent(typeof(MeshRenderer))]
	[ExecuteInEditMode]

	/// <summary>
	/// Provides a set of expansion tools for editing the mesh
	/// </summary>
	public class MeshExtension : MonoBehaviour
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// Gets the shared mesh.
		/// </summary>
		/// <value>The shared mesh.</value>
		public Mesh TargetMesh
		{
			get
			{
				if (!mesh)
					mesh = useSharedMesh ? GetComponent<MeshFilter>().sharedMesh : GetComponent<MeshFilter>().mesh;

				return mesh;
			}
		}

		[Header("General")]

		/// <summary>
		/// If we should use the shared mesh or create a new instance
		/// </summary>
		[SerializeField, Tooltip("If we should use the shared mesh or create a new instance")]
		private bool useSharedMesh;

		/// <summary>
		/// If we should mark the mesh dynamic to receive frequent updates
		/// </summary>
		[SerializeField, Tooltip("If we should mark the mesh dynamic to receive frequent updates")]
		private bool markDynamic;

		[Header("Sorting")]

		/// <summary>
		/// If we should alter the sorting order of the mesh
		/// </summary>
		[SerializeField, Tooltip("If we should alter the sorting order of the mesh")]
		private bool alterSortingOrder;

		/// <summary>
		/// The layer to set
		/// </summary>
		[SerializeField, Tooltip("The layer to set")]
		private SortingSetLayer layer;

		/// <summary>
		/// The order that we want to be in the set layer
		/// </summary>
		[SerializeField, Tooltip("The order that we want to be in the set layer")]
		private int orderInLayer;

		#endregion // PUBLIC_VARIABLES

		#region PRIVATE_VARIABLES

		/// <summary>
		/// The targeted mesh
		/// </summary>
		private Mesh mesh;

		/// <summary>
		/// The mesh renderer component
		/// </summary>
		private MeshRenderer meshRenderer;

		/// <summary>
		/// The new vertices to apply to the mesh
		/// The positions of the base mesh
		/// </summary>
		private Vector3[] newVertices, currentVertices;

		#endregion // PRIVATE_VARIABLES

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Awake this component
		/// </summary>
		void Awake()
		{
			meshRenderer = GetComponent<MeshRenderer>();

			if (markDynamic)
				TargetMesh.MarkDynamic();

			if (alterSortingOrder)
			{
				layer.Set(meshRenderer);
				meshRenderer.sortingOrder = orderInLayer;
			}
		}

		#endregion // MONOBEHAVIOUR_METHODS
	}
}