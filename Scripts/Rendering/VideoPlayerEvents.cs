﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;
using System.Collections;

namespace Bubblegum.Rendering
{
    [RequireComponent(typeof(VideoPlayer))]

    /// <summary>
    /// Events that play during points in a video
    /// </summary>
    public class VideoPlayerEvents : MonoBehaviour
    {
        #region VARIABLES

        /// <summary>
        /// The attached player
        /// </summary>
        private VideoPlayer player;

        /// <summary>
        /// If we should trigger video to play on start
        /// </summary>
        [SerializeField, Tooltip("If we should trigger video to play on start")]
        private bool playOnStart = true;

        /// <summary>
        /// The delay before playing
        /// </summary>
        [SerializeField, Tooltip("The delay before playing")]
        private float playDelay = 1f;

        /// <summary>
        /// When the player starts
        /// </summary>
        [SerializeField, Tooltip("When the player starts")]
        private UnityEvent onStart;

        /// <summary>
        /// When the player reaches the loop point
        /// </summary>
        [SerializeField, Tooltip("When the player reaches the loop point")]
        private UnityEvent onLoop;

		/// <summary>
		/// When the player encounters an error
		/// </summary>
		[SerializeField, Tooltip("When the player encounters an error")]
		private UnityStringEvent onError;

		/// <summary>
		/// When we trigger a video skip
		/// </summary>
		[SerializeField, Tooltip("When we trigger a video skip")]
		private UnityEvent onSkip;

		/// <summary>
		/// If we skipped the video
		/// </summary>
		private bool skipped;

		#endregion

		#region METHODS

		/// <summary>
		/// Start this object
		/// </summary>
		/// <returns></returns>
		IEnumerator Start()
        {
            player = GetComponent<VideoPlayer>();
            player.started += Started;
            player.errorReceived += Error;
            player.loopPointReached += LoopPointReached;

            yield return new WaitForSeconds(playDelay);

            if (playOnStart)
                player.Play();
        }

        /// <summary>
        /// Destroy this object
        /// </summary>
        void OnDestroy()
        {
            player.errorReceived -= Error;
            player.started -= Started;
            player.loopPointReached -= LoopPointReached;
        }

        /// <summary>
        /// Skip to the end
        /// </summary>
        public void Skip()
        {
			skipped = true;
            player.time = player.clip.length;
			onSkip.Invoke();
        }

        /// <summary>
        /// When the loop point is reached
        /// </summary>
        /// <param name="source"></param>
        void LoopPointReached(VideoPlayer source)
        {
			if (skipped)
				return;

            onLoop.Invoke();
        }

        /// <summary>
        /// When a video error occurs
        /// </summary>
        /// <param name="source"></param>
        /// <param name="message"></param>
        void Error(VideoPlayer source, string message)
        {
            onError.Invoke(message);
        }

        /// <summary>
        /// Called when the player starts
        /// </summary>
        /// <param name="source"></param>
        void Started(VideoPlayer source)
        {
            onStart.Invoke();
        }

        #endregion
    }

}