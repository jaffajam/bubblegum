﻿using UnityEngine;

namespace Bubblegum.Rendering
{
	/// <summary>
	/// Makes a texture behave like a drop shadow
	/// </summary>
	[ExecuteInEditMode]
	public class ShadowObject : MonoBehaviour
	{
		#region VARIABLES

		/// <summary>
		/// Target that we are providing shadow for
		/// </summary>
		[SerializeField, Tooltip("Target that we are providing shadow for")]
		private Transform target;

		/// <summary>
		/// Scale for the shadow
		/// </summary>
		[SerializeField, Tooltip("Scale for the shadow")]
		private Vector3 scale = new Vector3(1f, 1f, 1f);

		/// <summary>
		/// The shadow offset from the target position
		/// </summary>
		[SerializeField, Tooltip("The shadow offset from the target position")]
		private Vector3 shadowOffset = new Vector3(0, -2);

		/// <summary>
		/// Rect transform if using one
		/// </summary>
		private RectTransform rectTransform, targetRectTransform;

		#endregion

		#region METHODS

		/// <summary>
		/// Enable this object
		/// </summary>
		void OnEnable()
		{
			OnValidate();
		}

		/// <summary>
		/// Validate inspector input
		/// </summary>
		void OnValidate()
		{
			if (!target)
				return;

			targetRectTransform = target as RectTransform;
			rectTransform = transform as RectTransform;
			name = target.name + " Shadow";
		}

		/// <summary>
		/// Update this object
		/// </summary>
		void Update()
		{
			if (!target)
				return;

			transform.position = target.position + shadowOffset;
			transform.rotation = target.rotation;
			transform.localScale = Vector3.Scale(target.localScale, scale);
			gameObject.SetActive(target.gameObject.activeSelf);

			if (rectTransform)
				rectTransform.sizeDelta = targetRectTransform.sizeDelta;
		}

		#endregion
	}
}