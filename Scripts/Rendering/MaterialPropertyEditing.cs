﻿using UnityEngine;

namespace Bubblegum.Rendering
{

	/// <summary>
	/// Sets the value for the selected property on a material
	/// </summary>
	public class MaterialPropertyEditing : MonoBehaviour
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// Get the property
		/// </summary>
		public string Property { get { return property; } }

        /// <summary>
        /// The first editor on the object
        /// </summary>
        public MaterialPropertyEditing FirstEditor { get; private set; }

		/// <summary>
		/// Index of the material to edit
		/// </summary>
		[SerializeField, Tooltip("Index of the material to edit")]
		private int materialIndex;

		/// <summary>
		/// The key to the selected property
		/// </summary>
		[SerializeField, Tooltip("The key to the selected property")]
		private string property = "_Shininess";

		/// <summary>
		/// The type of the property
		/// </summary>
		[SerializeField, Tooltip("The type of the property")]
		private PropertyType propertyType;

		/// <summary>
		/// The color to set
		/// </summary>
		[SerializeField, Tooltip("The color to set"), DisplayIf("propertyType", PropertyType.Color)]
		public Color colorValue;

		/// <summary>
		/// The float value to set
		/// </summary>
		[SerializeField, Tooltip("The float value to set"), DisplayIf("propertyType", PropertyType.Float)]
		public float floatValue;

		/// <summary>
		/// The vector value to set
		/// </summary>
		[SerializeField, Tooltip("The vector value to set"), DisplayIf("propertyType", PropertyType.Vector)]
		public Vector4 vectorValue;

        /// <summary>
        /// The texture value to set
        /// </summary>
        [SerializeField, Tooltip("The texture value to set"), DisplayIf("propertyType", PropertyType.Texture)]
        public Texture textureValue;

        #endregion // PUBLIC_VARIABLES

        #region ENUMERATORS

        /// <summary>
        /// The type of the property
        /// </summary>
        public enum PropertyType { Float, Color, Vector, Texture }

		/// <summary>
		/// The last float value we had
		/// </summary>
		private float lastFloatValue;

		/// <summary>
		/// The last color value we had
		/// </summary>
		private Color lastColorValue;

		/// <summary>
		/// The last value of the vector
		/// </summary>
		private Vector4 lastVectorValue;

        /// <summary>
        /// The last value of the texture
        /// </summary>
        private Texture lastTextureValue;

        /// <summary>
        /// The material propertyblock
        /// </summary>
        private MaterialPropertyBlock propertyBlock;

		/// <summary>
		/// The renderer component
		/// </summary>
		private Renderer ourRenderer;

        /// <summary>
        /// All of the editor objects
        /// </summary>
        private MaterialPropertyEditing[] editors;

		#endregion // ENUMERATORS

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Awake this component
		/// </summary>
		void Awake()
		{
			editors = GetComponents<MaterialPropertyEditing>();
            FirstEditor = editors[0];

            ourRenderer = GetComponent<Renderer>();
            propertyBlock = new MaterialPropertyBlock();
            ourRenderer.GetPropertyBlock(propertyBlock, materialIndex);

            Update();
		}

		/// <summary>
		/// Update this instance
		/// </summary>
		void Update()
		{
            if (FirstEditor != this)
                return;

            bool dirty = false;

            for (int i = 0; i < editors.Length; i++)
                if (editors[i].UpdatePropertyBlock(propertyBlock))
                    dirty = true;

            if (dirty)
                ourRenderer.SetPropertyBlock(propertyBlock, materialIndex);
        }

        /// <summary>
        /// Update the property block
        /// </summary>
        bool UpdatePropertyBlock(MaterialPropertyBlock propertyBlock)
        {
            switch (propertyType)
            {
                case PropertyType.Color:
                    if (lastColorValue != colorValue)
                    {
                        lastColorValue = colorValue;
                        propertyBlock.SetColor(property, colorValue);
                        return true;
                    }
                    break;

                case PropertyType.Float:
                    if (lastFloatValue != floatValue)
                    {
                        lastFloatValue = floatValue;
                        propertyBlock.SetFloat(property, floatValue);
                        return true;
                    }
                    break;

                case PropertyType.Vector:
                    if (lastVectorValue != vectorValue)
                    {
                        lastVectorValue = vectorValue;
                        propertyBlock.SetVector(property, vectorValue);
                        return true;
                    }
                    break;

                case PropertyType.Texture:
                    if (lastTextureValue != textureValue)
                    {
                        lastTextureValue = textureValue;
                        propertyBlock.SetTexture(property, textureValue);
                        return true;
                    }
                    break;
            }

            return false;
        }

        /// <summary>
        /// Set the color value
        /// </summary>
        /// <param name="color"></param>
        public void SetColorValue(Color color)
		{
			colorValue = color;
		}

		/// <summary>
		/// Set the float value
		/// </summary>
		/// <param name="value"></param>
		public void SetFloatValue(float value)
		{
			floatValue = value;
		}

		/// <summary>
		/// Set the vector value
		/// </summary>
		/// <param name="value"></param>
		public void SetVectorValue(Vector4 value)
		{
			vectorValue = value;
		}

		#endregion // MONOBEHAVIOUR_METHODS
	}
}