﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Bubblegum.Rendering
{

	/// <summary>
	/// Sets the value for the selected property as global shader values
	/// </summary>
	[ExecuteInEditMode]
	public class ShaderPropertyEditing : UpdatingBehaviour
	{
		#region PUBLIC_VARIABLES

		/// <summary>
		/// Get the property
		/// </summary>
		public string Property { get { return property; } }

		/// <summary>
		/// The key to the selected property
		/// </summary>
		[SerializeField, Tooltip("The key to the selected property")]
		private string property = "_Shininess";

		/// <summary>
		/// The type of the property
		/// </summary>
		[SerializeField, Tooltip("The type of the property")]
		private PropertyType propertyType;

		/// <summary>
		/// The color to set
		/// </summary>
		[SerializeField, Tooltip("The color to set"), DisplayIf("propertyType", PropertyType.COLOR)]
		public Color colorValue;

		/// <summary>
		/// The float value to set
		/// </summary>
		[SerializeField, Tooltip("The float value to set"), DisplayIf("propertyType", PropertyType.FLOAT)]
		public float floatValue;

		/// <summary>
		/// The vector value to set
		/// </summary>
		[SerializeField, Tooltip("The vector value to set"), DisplayIf("propertyType", PropertyType.VECTOR)]
		public Vector4 vectorValue;

		#endregion // PUBLIC_VARIABLES

		#region ENUMERATORS

		/// <summary>
		/// The type of the property
		/// </summary>
		public enum PropertyType { FLOAT, COLOR, VECTOR }

		#endregion // ENUMERATORS

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Update this instance
		/// </summary>
		public override void ElectedUpdate()
		{
			switch (propertyType)
			{
				case PropertyType.COLOR:
					Shader.SetGlobalColor(property, colorValue);
					break;

				case PropertyType.FLOAT:
					Shader.SetGlobalFloat(property, floatValue);
					break;

				case PropertyType.VECTOR:
					Shader.SetGlobalVector(property, vectorValue);
					break;
			}
		}

		/// <summary>
		/// Set the color value
		/// </summary>
		/// <param name="color"></param>
		public void SetColorValue(Color color)
		{
			colorValue = color;
		}

		/// <summary>
		/// Set the float value
		/// </summary>
		/// <param name="value"></param>
		public void SetFloatValue(float value)
		{
			floatValue = value;
		}

		/// <summary>
		/// Set the vector value
		/// </summary>
		/// <param name="value"></param>
		public void SetVectorValue(Vector4 value)
		{
			vectorValue = value;
		}

        #endregion // MONOBEHAVIOUR_METHODS
    }

#if UNITY_EDITOR

    /// <summary>
    /// Inspector script
    /// </summary>
    [CustomEditor(typeof(ShaderPropertyEditing))]
    public class ShaderPropertyEditingEditor : Editor
    {
        #region METHODS

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Apply"))
                ((ShaderPropertyEditing)target).ElectedUpdate();
        }

        #endregion
    }

#endif
}