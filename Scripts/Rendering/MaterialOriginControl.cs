﻿using UnityEngine;

namespace Bubblegum.Rendering
{
    /// <summary>
    /// Set the origin in the material on enable
    /// </summary>
    [ExecuteInEditMode]
    [RequireComponent(typeof(Renderer))]
    public class MaterialOriginControl : MonoBehaviour
    {
        #region VARIABLES

        /// <summary>
        /// The position key in the material
        /// </summary>
        [SerializeField, Tooltip ("The position key in the material")]
        private string positionKey = "_Position";

        /// <summary>
        /// The offset to add to our position
        /// </summary>
        [SerializeField, Tooltip ("The offset to add to our position")]
        private Vector3 offset;

        /// <summary>
        /// The property block form the material
        /// </summary>
        private MaterialPropertyBlock propertyBlock;

        /// <summary>
        /// The renderer component
        /// </summary>
        private Renderer ourRenderer;

        #endregion

        #region METHODS

        /// <summary>
        /// Validate inspector input
        /// </summary>
        void OnValidate()
        {
            OnEnable();
        }

        /// <summary>
        /// Enable this object
        /// </summary>
        void OnEnable()
        {
            if (!ourRenderer)
                ourRenderer = GetComponent<Renderer>();
            
            if (propertyBlock == null)
                propertyBlock = new MaterialPropertyBlock();

            ourRenderer.GetPropertyBlock(propertyBlock);
            propertyBlock.SetVector(positionKey, transform.position + offset);
            ourRenderer.SetPropertyBlock(propertyBlock);
        }

        #endregion
    }
}