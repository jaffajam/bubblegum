﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Bubblegum.Rendering
{

    /// <summary>
    /// Stored lightmap UV data so that we can apply it to dynamic objects at runtime
    /// </summary>
    public class LightmappedPrefab : MonoBehaviour
    {
        #region VARAIBLES

        /// <summary>
        /// The index of our lightmap
        /// </summary>
        [SerializeField, ReadOnly]
        protected int lightmapIndex;

        /// <summary>
        /// Scale and offset in the lightmap
        /// </summary>
        [SerializeField, ReadOnly]
        protected Vector4 lightmapScaleOffset;

        #endregion

        #region METHODS

        /// <summary>
        /// Awaken this object
        /// </summary>
        protected virtual void Awake()
        {
            MeshRenderer renderer = GetComponent<MeshRenderer>();
            renderer.lightmapIndex = lightmapIndex;
            renderer.lightmapScaleOffset = lightmapScaleOffset;
        }

        /// <summary>
        /// Save the UV data
        /// </summary>
        public virtual void Save()
        {
            MeshRenderer renderer = GetComponent<MeshRenderer>();
            Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
            lightmapIndex = renderer.lightmapIndex;
            lightmapScaleOffset = renderer.lightmapScaleOffset;

#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
#endif
        }

#endregion
    }

#if UNITY_EDITOR

    /// <summary>
    /// Editor script for lightmapping
    /// </summary>
    [CustomEditor(typeof(LightmappedPrefab), true), CanEditMultipleObjects]
    public class LightmappedPrefabEditor : Editor
    {
        #region METHODS

        /// <summary>
        /// Draw the inspector
        /// </summary>
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Bake"))
            {
                LightmappedPrefab[] prefabs = GameObject.FindObjectsOfType<LightmappedPrefab>();

                foreach (LightmappedPrefab prefab in prefabs)
                    prefab.Save();
            }
        }

        #endregion
    }

#endif
}