﻿using UnityEngine;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Bubblegum.Rendering
{

    /// <summary>
    /// Stored lightmap UV data so that we can apply it to dynamic objects at runtime
    /// We put the data into uvs rather than the mesh renderer so batching still works
    /// </summary>
    [ExecuteInEditMode]
    public class LightmappedPrefabBatching : LightmappedPrefab
    {
        #region VARIABLES

        /// <summary>
        /// If we should force not supported lightmap batching
        /// </summary>
        [SerializeField, Tooltip("If we should force not supported lightmap batching")]
        private bool forceNotSupported;

        /// <summary>
        /// Default material to use in the renderer
        /// </summary>
        [SerializeField, Tooltip("Default material to use in the renderer")]
        private Material defaultMaterial;

        /// <summary>
        /// The selected batching material
        /// </summary>
        [SerializeField, ReadOnly]
        private Material selectedBatchingMaterial;

        /// <summary>
        /// Batching material to use in the renderer
        /// </summary>
        [SerializeField, Tooltip("Batching material to use in the renderer")]
        private Material[] batchingMaterials = new Material[0];

        /// <summary>
        /// The stored uv2s with lightmap info
        /// </summary>
        [SerializeField, HideInInspector]
        private Vector2[] uv2s;

        /// <summary>
        /// Get our unique ID
        /// </summary>
        private string ID
        {
            get
            {
                return string.Concat(lightmapIndex, lightmapScaleOffset.x, lightmapScaleOffset.y, lightmapScaleOffset.z, lightmapScaleOffset.w);
            }
        }

        /// <summary>
        /// All of the new meshes we have to create
        /// </summary>
        private static Dictionary<string, Mesh> newMeshes = new Dictionary<string, Mesh>();

        #endregion

        #region METHODS

        /// <summary>
        /// Awaken this object
        /// </summary>
        protected override void Awake()
        {
            MeshRenderer renderer = GetComponent<MeshRenderer>();

            //Editor always needs the standard mat for lightmapping
            if (!Application.isPlaying)
            {
                if (defaultMaterial)
                    renderer.sharedMaterial = defaultMaterial;

                return;
            }
            else
            {
                string id = ID;
                renderer.sharedMaterial = selectedBatchingMaterial;

                if (newMeshes.ContainsKey(id))
                {
                    GetComponent<MeshFilter>().sharedMesh = newMeshes[id];
                    return;
                }

                Mesh mesh = GetComponent<MeshFilter>().mesh;
                mesh.uv2 = uv2s;
                newMeshes.Add(id, mesh);
            }
        }

        /// <summary>
        /// Validate inspector input
        /// </summary>
        void OnValidate()
        {
            FindBatchMaterial();
        }

        /// <summary>
        /// Save the UV data
        /// </summary>
        public override void Save()
        {
            base.Save();
            BakeLightmapUVs();
            FindBatchMaterial();

#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
#endif
        }

        /// <summary>
        /// Bake in the lightmap UVs
        /// </summary>
        void BakeLightmapUVs()
        {
            try
            {
                MeshRenderer renderer = GetComponent<MeshRenderer>();
                Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
                Vector2[] meshUV2s = mesh.uv2;
                uv2s = mesh.uv2;

                for (int i = 0; i < mesh.uv2.Length; i++)
                {
                    uv2s[i] = new Vector2(meshUV2s[i].x * lightmapScaleOffset.x +
                    lightmapScaleOffset.z, meshUV2s[i].y * lightmapScaleOffset.y +
                    lightmapScaleOffset.w);
                }
            }
            catch (System.Exception e)
            {
                Debug.LogError("Exception occured on " + name);
                throw e;
            }
        }

        /// <summary>
        /// Find the batch material
        /// </summary>
        void FindBatchMaterial()
        {
            if (LightmapSettings.lightmaps.Length <= lightmapIndex || lightmapIndex == -1)
                return;

            Texture ourLM = LightmapSettings.lightmaps[lightmapIndex].lightmapColor;

            foreach (Material material in batchingMaterials)
            {
                Texture matLM = material.GetTexture("_LightmapTex");

                if (ourLM == matLM)
                    selectedBatchingMaterial = material;
            }
        }

        #endregion
    }
}