﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

namespace Bubblegum.Rendering
{

	[RequireComponent(typeof(Renderer))]

	/// <summary>
	/// Loads a texture from the internet and loads it into the renderers material
	/// </summary>
	public class NetworkTexture : MonoBehaviour
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// The url for the given image
		/// </summary>
		[Tooltip("The url for the given image")]
		[SerializeField]
		private string url;

		/// <summary>
		/// The default image to use if we couldn't load one
		/// </summary>
		[Tooltip("The default image to use if we couldn't load one")]
		[SerializeField]
		private Texture defaultImage;

		#endregion // PUBLIC_VARIABLES

		#region PRIVATE_VARIABLES

		/// <summary>
		/// The material of the renderer
		/// </summary>
		private Material material;

		#endregion // PRIVATE_VARIABLES

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Called when all objects have been initialized regardless of whether the script is enabled
		/// </summary>
		void Awake()
		{
			material = GetComponent<Renderer>().material;

			StartCoroutine(LoadImage());
		}

		#endregion // MONOBEHAVIOUR_METHODS

		#region PUBLIC_METHODS

		/// <summary>
		/// Loads the image and applies it to the material
		/// </summary>
		public IEnumerator LoadImage()
		{
            // Start a download of the given URL
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);

            // Wait until the download is done
            yield return www.SendWebRequest();

            // Assign the downloaded image to the material
            if (string.IsNullOrEmpty(www.error))
                material.mainTexture = DownloadHandlerTexture.GetContent(www);
            else
            {
                print("Image for " + gameObject.name + " could not be downloaded. " + www.error);
                material.mainTexture = defaultImage;
            }
		}

		#endregion // PUBLIC_METHODS
	}
}