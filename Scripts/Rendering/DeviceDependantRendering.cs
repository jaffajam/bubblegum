﻿using UnityEngine;
using UnityEngine.UI;

namespace Bubblegum.Rendering
{
	/// <summary>
	/// Switches materials on a renderer when the selected platform is changed
	/// </summary>
	public class DeviceDependantRendering : MonoBehaviour
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// Check if we should render low quality mode
		/// </summary>
		public bool IsLowQuality
		{
			get
			{
#if UNITY_ANDROID || UNITY_IOS
				return true;
#else
				return false;
#endif
			}
		}

		/// <summary>
		/// Ths selected mode
		/// </summary>
		[SerializeField, Tooltip("Ths selected mode")]
		private RenderMode mode;

		/// <summary>
		/// The material to use for low performance devices
		/// </summary>
		[SerializeField, Tooltip("The material to use for low performance devices"), DisplayIf("mode", RenderMode.SwitchMaterial)]
		private Material[] lowQualityMaterials;

		/// <summary>
		/// The component to disable
		/// </summary>
		[SerializeField, Tooltip("The component to disable"), DisplayIf("mode", RenderMode.DisableComponent)]
		private MonoBehaviour disableComponent;

		#endregion // PUBLIC_VARIABLES

		#region PRIVATE_VARIABLES

		/// <summary>
		/// The different render modes
		/// </summary>
		private enum RenderMode { Disable, DisableComponent, SwitchMaterial }

		#endregion

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Awaken this component
		/// </summary>
		void Awake()
		{
			Renderer renderer = GetComponent<Renderer>();
			Graphic graphic = GetComponent<Graphic>();

			if (IsLowQuality)
				switch (mode)
				{
					case RenderMode.Disable:
						gameObject.SetActive(false);
						break;

					case RenderMode.DisableComponent:
						disableComponent.enabled = false;
						break;

					case RenderMode.SwitchMaterial:

						if (renderer)
							renderer.sharedMaterials = lowQualityMaterials;

						if (graphic)
							graphic.material = lowQualityMaterials[0];

						break;
				}
		}

		/// <summary>
		/// Try and enable this object
		/// </summary>
		public void TryEnable()
		{
			if (!IsLowQuality)
				gameObject.SetActive(true);
		}

		#endregion // MONOBEHAVIOUR_METHODS
	}
}