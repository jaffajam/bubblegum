﻿using UnityEngine;

namespace Bubblegum
{

	/// <summary>
	/// A filter for targeting objects
	/// </summary>
	[CreateAssetMenu(menuName = "Scriptable Object/Entity Filter")]
	public class EntityFilter : Key, IFilter
	{
		#region PUBLIC_VARIABLES

		/// <summary>
		/// The entities that we are interested in
		/// </summary>
		[SerializeField, Tooltip("The entities that we are interested in")]
		protected Entity[] interests;

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Check if an interest is interesting
		/// </summary>
		/// <param name="interest"></param>
		/// <returns></returns>
		public virtual bool CheckFilter(Identification interest)
		{
			return CheckFilter(interest.Key);
		}

		/// <summary>
		/// Check if an interest is interesting
		/// </summary>
		/// <param name="interest"></param>
		/// <returns></returns>
		public virtual bool CheckFilter(Entity key)
		{
			for (int i = 0; i < interests.Length; i++)
				if (key.IsEntity(interests[i]))
					return true;

			return false;
		}

		/// <summary>
		/// Check if the object is interesting
		/// </summary>
		/// <param name="gameObject"></param>
		/// <param name="interest"></param>
		/// <returns></returns>
		public virtual bool CheckFilter(Identification interest, GameObject self)
		{
			return interest.gameObject != self && CheckFilter(interest.Key);
		}

		/// <summary>
		/// Check all of the interests match the filter
		/// </summary>
		/// <param name="interests"></param>
		/// <param name="self"></param>
		/// <returns></returns>
		public virtual bool CheckFilter(Identification[] interests, GameObject self)
		{
			for (int i = 0; i < interests.Length; i++)
				if (!CheckFilter(interests[i], self))
					return false;

			return true;
		}

		#endregion

	}
}