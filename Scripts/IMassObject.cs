﻿namespace Bubblegum
{

	/// <summary>
	/// An object that contributes to a rigidbody total mass
	/// </summary>
	public interface IMassObject
	{
		#region VARIABLES

		/// <summary>
		/// Get the mass of the object
		/// </summary>
		float Mass { get; }

		#endregion
	}
}
