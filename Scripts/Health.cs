﻿using System;
using UnityEngine;

namespace Bubblegum
{

	/// <summary>
	/// Monitors the health of an entity
	/// </summary>
	public class Health : FastBehaviour, IHealth, ISaveable
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// Gets the health amount.
		/// </summary>
		/// <value>The health amount.</value>
		public int HealthAmount { get { return health; } }

		/// <summary>
		/// Get the health percentage
		/// </summary>
		public float HealthPercent
		{
			get
			{
				if (fullHealth != 0)
					return (float)health / fullHealth;
				else
					return 1f;
			}
		}

		/// <summary>
		/// When the health is depleted
		/// </summary>
		public Action onHealthDepleted { get; set; }

		/// <summary>
		/// When the health is changed
		/// </summary>
		public Action onHealthChanged { get; set; }

		/// <summary>
		/// The health for the entity
		/// </summary>
		[Tooltip("The health for the entity")]
		[SerializeField]
		private int health = 100;

		/// <summary>
		/// If the health should slowly regenerate
		/// </summary>
		[Tooltip("If the health should slowly regenerate")]
		[SerializeField]
		private bool regenerate;

		#endregion // PUBLIC_VARIABLES

		#region PRIVATE_VARIABLES

		/// <summary>
		/// Key for saving to player prefs
		/// </summary>
		private const string PREFS_KEY = "Health";

		/// <summary>
		/// The full health.
		/// </summary>
		protected int fullHealth;

		/// <summary>
		/// If we are dead
		/// </summary>
		private bool dead;

		#endregion // PRIVATE_VARIABLES

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// NOTE - Causes errors if set as Awake, due to sending off health percent too early
		/// </summary>
		protected virtual void Start()
		{
			fullHealth = health;
		}

		/// <summary>
		/// Update this AIBehaviour, will only happen at fixed frame intervals
		/// </summary>
		protected override void IntervalUpdate()
		{
			if (!dead && regenerate)
				health = Mathf.Clamp(++health, 0, fullHealth);
		}

		#endregion // MONOBEHAVIOUR_METHODS

		#region PUBLIC_METHODS

		/// <summary>
		/// Hurt the specified damage.
		/// </summary>
		/// <param name="damage">Damage.</param>
		public void Hurt(int damage)
		{
			health -= damage;
			CheckHealth();
		}

		/// <summary>
		/// Hurt the specified damage.
		/// </summary>
		/// <param name="damage">Damage.</param>
		public void Hurt(float damage)
		{
			health -= (int)damage;
			CheckHealth();
		}

		/// <summary>
		/// Save the health state
		/// </summary>
		public virtual void Save(Key id)
		{
			id.Save(PREFS_KEY, HealthAmount);
		}

		/// <summary>
		/// Load the health state
		/// </summary>
		public virtual void Load(Key id)
		{
			health = (int)id.Load(PREFS_KEY, fullHealth);
			CheckHealth();
		}

		/// <summary>
		/// Clear the state of the object
		/// </summary>
		/// <param name="id"></param>
		public virtual void Clear(Key id)
		{
            PersistentData.DeleteKey(PREFS_KEY);
		}

		/// <summary>
		/// Update the max health
		/// </summary>
		/// <param name="heal"></param>
		public void SetMaxHealth(int health, bool heal)
		{
			fullHealth = health;

			if (heal)
				this.health = health;
		}

		#endregion // PUBLIC_METHODS

		#region PRIVATE_METHODS

		/// <summary>
		/// Checks the health.
		/// </summary>
		private void CheckHealth()
		{
			if (!dead)
			{
				if (health <= 0)
				{
					dead = true;

					if (onHealthDepleted != null)
						onHealthDepleted();
				}
			}

			health = Mathf.Clamp(health, 0, fullHealth);

			if (onHealthChanged != null)
				onHealthChanged();
		}

		#endregion // PRIVATE_METHODS
	}
}