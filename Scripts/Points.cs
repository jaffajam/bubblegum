﻿using UnityEngine;

namespace Bubblegum
{

	/// <summary>
	/// Class that manages points from the player
	/// </summary>
	[CreateAssetMenu(menuName = "Scriptable Object/Points")]
	public class Points : ScriptableObject, IPoints
	{
		#region PUBLIC_METHODS

		/// <summary>
		/// Methods to invoke when the score changes
		/// </summary>
		public System.Action onPointsChanged { get; set; }

		/// <summary>
		/// Get the int value for this score
		/// </summary>
		public int Value { get; protected set; }

		[Header("Score")]

		/// <summary>
		/// The expected default value
		/// </summary>
		[Tooltip("The expected default value")]
		public int defaultValue = 0;

		/// <summary>
		/// The max value for the score
		/// </summary>
		[Tooltip("The max value for the score")]
		public int maxValue = int.MaxValue;

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Enable this object
		/// </summary>
		void OnEnable()
		{
			Value = defaultValue;
		}

		/// <summary>
		/// Add points to the score that matches the given key
		/// </summary>
		public void AddPoints(int points)
		{
			Value = Mathf.Clamp(Value + points, -maxValue, maxValue);
			onPointsChanged?.Invoke();
		}

		/// <summary>
		/// Set the points for this score
		/// </summary>
		/// <param name="points"></param>
		public void SetPoints(int points)
		{
			Value = Mathf.Clamp(points, -maxValue, maxValue);
			onPointsChanged?.Invoke();
		}

		/// <summary>
		/// Reset to the default score
		/// </summary>
		public virtual void Reset()
		{
			SetPoints(defaultValue);
		}

		#endregion // PUBLIC_METHODS
	}
}