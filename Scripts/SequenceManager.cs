﻿using System.Collections.Generic;

namespace Bubblegum
{
    /// <summary>
    /// Managing display sequence based on priority
    /// </summary>
    public class SequenceManager : Singleton<SequenceManager>
    {
        #region VARIABLES

        /// <summary>
        /// Queue of all displays
        /// </summary>
        private List<ISequenced> queue = new List<ISequenced>();

        #endregion

        #region METHODS

        /// <summary>
        /// Add the builder to the queue based on priority
        /// </summary>
        public void Queue(ISequenced data)
        {
            if (queue.Count == 0 || queue[0] != data)
            {
                if (queue.Count == 0)
                {
                    queue.Add(data);

                    data.BeforeTask();
                    StartCoroutine(data.StartTask());
                }
                else
                {
                    bool inserted = false;

                    // Never queue before the current showing display, therefore start with index 1
                    for (int i = 1; i < queue.Count; i++)
                    {
                        if (queue[i].Priority < data.Priority)
                        {
                            queue.Insert(i, data);
                            inserted = true;
                            break;
                        }
                    }

                    if (!inserted)
                        queue.Add(data);
                }
            }
        }

        /// <summary>
        /// Dequeue current and show next
        /// </summary>
        public void StartNext(ISequenced previous)
        {
            if (queue.Count > 0 && previous == queue[0])
                queue.RemoveAt(0);

            if (queue.Count > 0)
            {
                queue[0].BeforeTask();
                StartCoroutine(queue[0].StartTask());
            }
        }

        #endregion
    }
}