﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Bubblegum
{

    /// <summary>
    /// Used as a key in the editor rather than strings in dictionaries and similar
    /// </summary>
    [CreateAssetMenu(menuName = "Scriptable Object/Key")]
    public class Key : ScriptableObject, IPatchable
    {
        #region PUBLIC_VARIABLES

        /// <summary>
        /// Get the name of the key in display format
        /// </summary>
        public virtual string Name
        {
            get
            {
                if (string.IsNullOrEmpty(cachedName))
                    cachedName = System.Text.RegularExpressions.Regex.Replace(name, @"^(\(.*?\)\s*|_)", string.Empty);

                return cachedName;
            }

            set
            {
                name = value;
                cachedName = System.Text.RegularExpressions.Regex.Replace(value, @"^(\(.*?\)\s*|_)", string.Empty);
            }
        }

        /// <summary>
        /// Get the key that deifnes this entity
        /// </summary>
        public int ID { get { return id; } }

        [Header("Key")]

        /// <summary>
        /// The key used for serialization of this object
        /// </summary>
        [SerializeField, ReadOnly]
        protected int id;

        /// <summary>
        /// If we should debug this object
        /// </summary>
        [Tooltip("If we should debug this object")]
        public bool debug = true;

        #endregion

        #region PRIVATE_VARAIBLES

        /// <summary>
        /// When all keys have been initialized
        /// </summary>
        private static System.Action OnKeysReady;

        /// <summary>
        /// If all keys are ready
        /// </summary>
        private static bool ready;

        /// <summary>
        /// All of the unique entities
        /// </summary>
        protected static Dictionary<int, Key> allKeys = new Dictionary<int, Key>();

        /// <summary>
        /// All of the saved data we have run through this key
        /// </summary>
        private Dictionary<string, object> savedData = new Dictionary<string, object>();

        /// <summary>
        /// Cache name to prevent garbage
        /// </summary>
        private string cachedName;

        #endregion

        #region PUBLIC_METHODS

        /// <summary>
        /// Initialize all keys
        /// </summary>
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        static void Run()
        {
            AssetManager.InvokeOnResourcesLoaded(() => InitializeKeys(AssetManager.LoadAllResources<Key>()));
            AssetManager.InvokeOnAssetsLoaded(() =>
            {
                InitializeKeys(AssetManager.LoadAllContent<Key>());
                InitializeAll();
            });
        }

        /// <summary>
        /// Init all keys
        /// </summary>
        static void InitializeKeys(IEnumerable<Object> keys)
        {
            if (keys != null)
                foreach (Key key in keys)
                    if (allKeys.ContainsKey(key.id))
                        Debug.LogError("Duplicate keys found on " + key.name + " with ID " + key.id);
                    else
                        allKeys[key.id] = key;

        }

        /// <summary>
        /// Initialize all keys
        /// </summary>
        static void InitializeAll()
        {
            foreach (Key key in allKeys.Values)
                key.Initialize();

            OnKeysReady?.Invoke();
            ready = true;
        }

        /// <summary>
        /// Invoke when all key objects are ready
        /// </summary>
        /// <param name="action"></param>
        public static void InvokeOnKeysReady(System.Action action)
        {
            if (ready)
                action?.Invoke();
            else
                OnKeysReady += action;
        }

        /// <summary>
        /// Get the key with the given id
        /// </summary>
        /// <param name="id"></param>
        public static T GetKey<T>(int id) where T : Key
		{
			if (allKeys.ContainsKey(id))
				return allKeys[id] as T;

			return default;
		}

        /// <summary>
        /// Get the key with the given id
        /// </summary>
        /// <param name="id"></param>
        public static Key GetKey(int id)
        {
            if (allKeys.ContainsKey(id))
                return allKeys[id];

            return default;
        }

        /// <summary>
        /// Get the key with the given id
        /// </summary>
        /// <param name="id"></param>
        public static Key GetKey(string name)
        {
            return allKeys.Values.FirstOrDefault(key => key.name == name);
        }

        /// <summary>
        /// Find all keys matching the given type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T[] FindAll<T>() where T : Key
        {
            return allKeys.Values.Where(key => key is T).Select(obj => obj as T).ToArray();
        }

        /// <summary>
        /// Awaken this object
        /// </summary>
        protected virtual void Awake()
        {
#if UNITY_EDITOR
            if (id == 0)
            {
                GenerateID();
                AssetDatabase.SaveAssets();
            }
#endif
        }

        /// <summary>
        /// Initialize this key
        /// </summary>
        public virtual void Initialize()
        {
            cachedName = null;
        }

        /// <summary>
        /// Save into this key
        /// </summary>
        /// <param name="key"></param>
        public void Save(string key, object value)
		{
			if (value is int)
                PersistentData.SetInt(key + ID, (int)value);

			else if (value is float)
                PersistentData.SetFloat(key + ID, (float)value);

			else if (value is string)
                PersistentData.SetString(key + ID, (string)value);

			else
				throw new System.Exception("Saving data of type " + value.GetType().Name + " is not supported");

			//Debug data presentation
			savedData[key] = value;
		}

		/// <summary>
		/// Save into this key
		/// </summary>
		/// <param name="key"></param>
		public object Load(string key, object defaultValue)
		{
			object value;

			if (defaultValue is int)
				value = PersistentData.GetInt(key + ID, (int)defaultValue);

			else if (defaultValue is float)
				value = PersistentData.GetFloat(key + ID, (float)defaultValue);

			else if (defaultValue is string)
				value = PersistentData.GetString(key + ID, (string)defaultValue);

			else
				throw new System.Exception("Saving data of type " + defaultValue.GetType().Name + " is not supported");

			//Debug data presentation
			savedData[key] = value;

			return value;
		}

        /// <summary>
        /// Clear the object with the given key
        /// </summary>
        /// <param name="key"></param>
        public void Clear(string key)
        {
            PersistentData.DeleteKey(key);

            //Debug data presentation
            savedData[key] = null;
        }

        /// <summary>
        /// Apply the patch using the given object(will be the same object with updated data)
        /// </summary>
        public virtual bool ApplyPatch()
        {
            Key real = GetKey(id);

            if (!real)
            {
                Debug.LogError("Can't apply patch to " + name + " as no object exists with ID " + id);
                return false;
            }

            if (debug)
                Debug.Log("Patched object " + name);

            //Should not actually apply patches in editor since editor values are final values anyway
            if (Application.platform == RuntimePlatform.WindowsEditor ||
                Application.platform == RuntimePlatform.OSXEditor)
                return false;

            real.name = name;
            return true;
        }

        /// <summary>
        /// Conversion of key to int
        /// </summary>
        /// <param name="key"></param>
        public static explicit operator int(Key key)
        {
            if (!key)
                return 0;

            return key.ID;
        }

        /// <summary>
        /// Conversion of key to int
        /// </summary>
        /// <param name="key"></param>
        public static explicit operator Key(int id)
        {
            return GetKey(id);
        }

#if UNITY_EDITOR

        /// <summary>
        /// Create a new id for this object
        /// </summary>
        private void GenerateID()
        {
            List<Key> allKeys = FindAllKeyAssets();
            id = Random.Range(0, int.MaxValue).GetHashCode();

            foreach (Key key in allKeys)
                if (key != this && key.id == id)
                {
                    GenerateID();
                    return;
                }

            Debug.Log("Set ID for " + name);
            EditorUtility.SetDirty(this);
        }

        /// <summary>
        /// Get all of the key objects in the project
        /// </summary>
        /// <returns></returns>
        private List<Key> FindAllKeyAssets()
        {
            List<Key> assets = new List<Key>();
            string[] guids = AssetDatabase.FindAssets(string.Format("t:{0}", typeof(Key)));

            for (int i = 0; i < guids.Length; i++)
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
                Key asset = AssetDatabase.LoadAssetAtPath<Key>(assetPath);

                if (asset != null)
                    assets.Add(asset);
            }

            return assets;
        }

#endif
        #endregion


        #region SUB_CLASSES

#if UNITY_EDITOR
        /// <summary>
        /// Editor script for the key
        /// </summary>
        [CustomEditor(typeof(Key), true), CanEditMultipleObjects]
        public class KeyEditor : Editor
        {
            #region VARIABLES

            /// <summary>
            /// The id for the object
            /// </summary>
            private int id;

            /// <summary>
            /// The linked keys for this object
            /// </summary>
            private List<Key> linkedKeys;

            #endregion

            #region METHODS

            /// <summary>
            /// Draw the inspector
            /// </summary>
            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();

                Key key = (Key)target;

                if (id != key.id)
                {
                    id = key.id;
                    linkedKeys = key.FindAllKeyAssets().Where(item => item.id == key.id).ToList();
                }

                if (linkedKeys.Count > 1)
                {
                    EditorGUILayout.HelpBox("Linked Keys:", MessageType.Warning);
                    GUI.enabled = false;

                    foreach (Key item in linkedKeys)
                        if (item != key)
                            EditorGUILayout.ObjectField(item.name, item, typeof(Key), false);

                    GUI.enabled = true;
                }

                if (GUILayout.Button("Generate ID"))
                    key.GenerateID();
            }

            #endregion
        }
#endif
        #endregion

    }
}