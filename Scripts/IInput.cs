﻿using UnityEngine;

namespace Bubblegum
{

	/// <summary>
	/// A class that must provide input information
	/// </summary>
	public interface IInput
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// Gets the mouse position, lowercase since Unity Input is too... :(
		/// </summary>
		Vector3 mousePosition { get; }

		#endregion // PUBLIC_VARIABLES

		#region PUBLIC_METHODS

		/// <summary>
		/// Get the input from the axis matching the name
		/// </summary>
		/// <param name="axis"></param>
		float GetAxis(string axis);

		/// <summary>
		/// Get the input from the axis matching the name
		/// </summary>
		/// <param name="axis"></param>
		float GetAxisRaw(string axis);

		/// <summary>
		/// Get the input from the button matching the name
		/// </summary>
		/// <param name="buttonName"></param>
		bool GetButton(string buttonName);

		/// <summary>
		/// Get the input from the button matching the name
		/// </summary>
		/// <param name="buttonName"></param>
		bool GetButtonDown(string buttonName);

		/// <summary>
		/// Get the input from the button matching the name
		/// </summary>
		/// <param name="buttonName"></param>
		bool GetButtonUp(string buttonName);

		#endregion // PUBLIC_METHODS
	}
}