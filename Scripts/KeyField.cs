﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Bubblegum
{
    /// <summary>
    /// Key field object that saves the key reference rather than a key object
    /// Makes Keys work inside asset bundles
    /// </summary>
    [System.Serializable]
    public class KeyField
    {
        #region VARIABLES

        /// <summary>
        /// The actual key object
        /// </summary>
        public Key Key
        {
            get
            {
#if UNITY_EDITOR
                if (!Application.isPlaying || !Key.GetKey(keyID))
                    return AssetDatabase.LoadAssetAtPath<Key>(AssetDatabase.GUIDToAssetPath(guid));
#endif

                return Key.GetKey(keyID);
            }
        }

        /// <summary>
        /// This is required for inspector to read from when drawing arrays
        /// Otherwise the GUID will display instead of the element index
        /// </summary>
        [SerializeField]
        private string name = "";

        /// <summary>
        /// The actual key object
        /// </summary>
        [SerializeField]
        private string guid;

        /// <summary>
        /// The stored key ID
        /// </summary>
        [SerializeField]
        private int keyID;

        #endregion

        #region METHODS

        /// <summary>
        /// Create a new key field
        /// </summary>
        public KeyField() { }

#if UNITY_EDITOR
        /// <summary>
        /// Create a new key field
        /// </summary>
        public KeyField(Key key)
        {
            guid = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(key));

            if (key)
                keyID = key.ID;
        }
#endif

        /// <summary>
        /// Makes this object behave like a standard key object
        /// </summary>
        /// <param name="sceneField"></param>
        public static explicit operator Key(KeyField keyField)
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
                return keyField?.Key;

            return keyField != null ? AssetDatabase.LoadAssetAtPath<Key>(AssetDatabase.GUIDToAssetPath(keyField.guid)) : null;
#else
            return keyField.Key;
#endif
        }

        /// <summary>
        /// Overload for == operator
        /// </summary>
        /// <param name="keyField1"></param>
        /// <param name="keyField2"></param>
        /// <returns></returns>
        public static bool operator ==(KeyField keyField1, KeyField keyField2)
        {
            if (ReferenceEquals(keyField1, keyField2))
                return true;

            if (ReferenceEquals(keyField1, null))
                return false;

            if (ReferenceEquals(keyField2, null))
                return false;

            return keyField1.Key == keyField2.Key;
        }

        /// <summary>
        /// Overload for != operator
        /// </summary>
        /// <param name="keyField1"></param>
        /// <param name="keyField2"></param>
        /// <returns></returns>
        public static bool operator !=(KeyField keyField1, KeyField keyField2)
        {
            return !(keyField1 == keyField2);
        }

        /// <summary>
        /// Check if two objects equal each other
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        /// <summary>
        /// Get the object hash code
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Key.GetHashCode();
        }

        #endregion
    }

#if UNITY_EDITOR

    /// <summary>
    /// Drawer for the key field
    /// </summary>
    [CustomPropertyDrawer(typeof(KeyField))]
    public class KeyFieldPropertyDrawer : PropertyDrawer
    {
        #region METHODS

        /// <summary>
        /// Draw the inspector
        /// </summary>
        /// <param name="position"></param>
        /// <param name="property"></param>
        /// <param name="label"></param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, GUIContent.none, property);

            SerializedProperty keyID = property.FindPropertyRelative("keyID");
            SerializedProperty guid = property.FindPropertyRelative("guid");
            string path = AssetDatabase.GUIDToAssetPath(guid.stringValue);
            Key key = AssetDatabase.LoadAssetAtPath<Key>(path);
            key = EditorGUI.ObjectField(position, label, key, typeof(Key), false) as Key;

            if (key)
            {
                keyID.intValue = key.ID;
                path = AssetDatabase.GetAssetPath(key);
                guid.stringValue = AssetDatabase.AssetPathToGUID(path);
            }

            EditorGUI.EndProperty();
        }

        #endregion
    }
#endif
}