﻿namespace Bubblegum
{
    /// <summary>
    /// Leaderboard entry interface
    /// </summary>
    public interface ILeaderboardEntry
    {
        /// <summary>
        /// Player id that achieved this
        /// </summary>
        string PlayerId { get; }

        /// <summary>
        /// Player Rank in leaderboard
        /// </summary>
        int PlayerRank { get; }

        /// <summary>
        /// Nickname of player that achieved this
        /// </summary>
        string PlayerNickname { get; }

        /// <summary>
        /// Score that achieved this entry
        /// </summary>
        string PlayerScore { get; }
    }
}