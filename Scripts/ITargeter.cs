﻿namespace Bubblegum
{

	/// <summary>
	/// An object that will target other objects
	/// </summary>
	public interface ITargeter
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// Get or set the targetable object
		/// </summary>
		ITargetable Targetable { get; set; }

		#endregion

	}
}
