﻿using UnityEngine;

namespace Bubblegum
{

	/// <summary>
	/// Provides information on what object this one is connected to
	/// </summary>
	public interface IContactControl
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// If even one foot/wheel is touching the ground
		/// </summary>
		bool Contacting { get; }

		/// <summary>
		/// Get the point of contact
		/// </summary>
		Vector3 ContactPoint { get; }

		/// <summary>
		/// The ground/object we are touching
		/// </summary>
		Collider Contact { get; }

		#endregion
	}
}