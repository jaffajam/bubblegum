﻿using UnityEngine;

namespace Bubblegum
{
    /// <summary>
    /// Triggers a hierarchy refresh by enabling all children
    /// </summary>
    public class Refresh : MonoBehaviour
    {
        #region METHODS

        /// <summary>
        /// Enable this object
        /// </summary>
        void OnEnable()
        {
            foreach (Transform child in transform)
                child.gameObject.SetActive(true);
        }

        #endregion
    }
}