﻿using UnityEngine;
using UnityEngine.Events;
using System;

namespace Bubblegum
{
	/// <summary>
	/// A unity event that also passes a boolean parameter
	/// </summary>
	[Serializable]
	public class UnityBoolEvent : UnityEvent<bool> { }

	/// <summary>
	/// A unity event that also passes a float parameter
	/// </summary>
	[Serializable]
	public class UnityFloatEvent : UnityEvent<float> { }

	/// <summary>
	/// A unity event that also passes a integer parameter
	/// </summary>
	[Serializable]
	public class UnityIntEvent : UnityEvent<int> { }

	/// <summary>
	/// A unity event that also passes a string parameter
	/// </summary>
	[Serializable]
	public class UnityStringEvent : UnityEvent<string> { }

	/// <summary>
	/// A unity event that also passes an object parameter
	/// </summary>
	[Serializable]
	public class UnitySystemObjectEvent : UnityEvent<object> { }

	/// <summary>
	/// A unity event that also passes an Object parameter
	/// </summary>
	[Serializable]
	public class UnityObjectEvent : UnityEvent<UnityEngine.Object> { }

	/// <summary>
	/// A unity event that also passes a game Object parameter
	/// </summary>
	[Serializable]
	public class UnityGameObjectEvent : UnityEvent<GameObject> { }

	/// <summary>
	/// A unity event that also passes a transform parameter
	/// </summary>
	[Serializable]
	public class UnityTransformEvent : UnityEvent<Transform> { }

	/// <summary>
	/// A unity event that also passes a Vector2 parameter
	/// </summary>
	[Serializable]
	public class UnityVector2Event : UnityEvent<Vector2> { }

	/// <summary>
	/// A unity event that also passes a Vector3 parameter
	/// </summary>
	[Serializable]
	public class UnityVector3Event : UnityEvent<Vector3> { }

	/// <summary>
	/// A unity event that also passes a Vector3 parameter
	/// </summary>
	[Serializable]
	public class UnityQuaternionEvent : UnityEvent<Quaternion> { }

	/// <summary>
	/// A unity event that also passes a collider parameter
	/// </summary>
	[Serializable]
	public class UnityComponentEvent : UnityEvent<Component> { }

	/// <summary>
	/// A unity event that also passes a collider parameter
	/// </summary>
	[Serializable]
	public class UnityColliderEvent : UnityEvent<Collider> { }

	/// <summary>
	/// A unity event that also passes a collider parameter
	/// </summary>
	[Serializable]
	public class UnityColliderEvent2D : UnityEvent<Collider2D> { }

	/// <summary>
	/// A unity event that also passes a raycast hit parameter
	/// </summary>
	[Serializable]
	public class UnityRaycastHitEvent : UnityEvent<RaycastHit> { }

	/// <summary>
	/// Unity event that also passes a key
	/// </summary>
	[Serializable]
	public class UnityKeyEvent : UnityEvent<Key> { }

	/// <summary>
	/// A unity event that passes a collider and float representing force
	/// </summary>
	[Serializable]
	public class UnityComponentFloatEvent : UnityEvent<Component, float> { }
}