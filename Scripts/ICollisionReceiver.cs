﻿using UnityEngine;

namespace Bubblegum
{

	/// <summary>
	/// An object that will receive unity collision events
	/// </summary>
	public interface ICollisionReceiver
	{

		#region PUBLIC_METHODS

		/// <summary>
		/// Trigger the collision enter event
		/// </summary>
		/// <param name="collision"></param>
		void TriggerCollisionEnter(Collision collision);

		/// <summary>
		/// Trigger the collision exit event
		/// </summary>
		/// <param name="collision"></param>
		void TriggerCollisionExit(Collision collision);

		/// <summary>
		/// Trigger the collision stay event
		/// </summary>
		/// <param name="collision"></param>
		void TriggerCollisionStay(Collision collision);

		#endregion
	}
}