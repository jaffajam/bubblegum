﻿namespace Bubblegum
{

	/// <summary>
	/// Context manager object
	/// </summary>
	public interface IContextManager
	{
        #region METHODS

        /// <summary>
        /// When this context changes
        /// </summary>
        System.Action onContextChanged { get; set; }

		/// <summary>
		/// Get the object in context
		/// </summary>
		object Context { get; }

		/// <summary>
		/// Get context as type
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		T GetContext<T>();

		/// <summary>
		/// Check if the given item is in context
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		bool IsInContext(object value);

		/// <summary>
		/// Set context
		/// </summary>
		void Set(object obj);

        /// <summary>
        /// Clear the context to nothing
        /// </summary>
        void Clear();

		#endregion
	}
}