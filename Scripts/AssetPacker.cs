﻿using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Bubblegum
{
    /// <summary>
    /// Used to reference assets in the application so that they are packed into the build
    /// </summary>
    public class AssetPacker : MonoBehaviour
    {
        #region VARIABLES

        /// <summary>
        /// All of the paths to assets
        /// </summary>
        [SerializeField]
        private List<Object> paths = new List<Object>();

        /// <summary>
        /// All of the assets that we want to load
        /// </summary>
        [SerializeField, ReadOnly]
        private List<Object> assets = new List<Object>();

        #endregion

        #region METHODS

        /// <summary>
        /// Awaken this object
        /// </summary>
        void Awake()
        {
            AssetManager.AddAssets(assets);
        }

        #endregion

#if UNITY_EDITOR

        /// <summary>
        /// Asset packer inspector
        /// </summary>
        [CustomEditor(typeof(AssetPacker))]
        public class AssetPackerEditor : Editor
        {
            #region METHODS

            /// <summary>
            /// Draw the inspector
            /// </summary>
            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();

                if (GUILayout.Button("Save"))
                    LoadAssets();
            }

            /// <summary>
            /// Load in the given assets
            /// </summary>
            void LoadAssets()
            {
                AssetPacker packer = ((AssetPacker)target);
				packer.assets = packer.paths.PathsToAssets();

                EditorUtility.SetDirty(packer);
            }

            #endregion
        }
#endif
    }
}