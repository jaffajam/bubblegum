﻿using UnityEngine;
using System.Collections;

namespace Bubblegum
{
	/// <summary>
	/// Mono object integrated with animator
	/// </summary>
	public class DisplayMonoBehaviour : CacheBehaviour
	{
		#region VARIABLES

		/// <summary>
		/// Get the attached animator
		/// </summary>
		protected Animator Anim
		{
			get
			{
				if (!animator)
					animator = GetComponent<Animator>();

				return animator;
			}
		}

        [Header("Display MonoBehaviour")]

        /// <summary>
        /// If this object should show using animations triggers/bools
        /// </summary>
        [SerializeField, Tooltip("If this object should show using animations triggers/bools")]
        protected bool showWithAnimation = true;

		/// <summary>
		/// If this animator will be expected to be disabled when show/hide is triggered
		/// </summary>
		[SerializeField, Tooltip("If this animator will be expected to be disabled when show/hide is triggered"), DisplayIf("showWithAnimation", true)]
		private bool expectTemporaryAnimator = true;

		/// <summary>
		/// If we should show this object using triggers instead of bools
		/// </summary>
		[SerializeField, Tooltip("If we should show this object using triggers instead of bools"), DisplayIf("showWithAnimation", true)]
        protected bool showWithTriggers;

        /// <summary>
        /// If this object should use realtime instead of scaled time
        /// </summary>
        [SerializeField, Tooltip("If this object should use realtime instead of scaled time")]
        protected bool realtimeCoroutines = false;

        /// <summary>
        /// If this object should hide on awake
        /// </summary>
        [SerializeField, Tooltip("If this object should hide on awake")]
        protected bool hideOnAwake = true;

        /// <summary>
        /// If this object should hide automatically after Showing
        /// </summary>
        [SerializeField, Tooltip("If this object should hide automatically after Showing")]
        protected bool autoHide = false;

        /// <summary>
        /// Time to stay visible before Hide
        /// </summary>
        [SerializeField, DisplayIf("autoHide", true)]
        protected float showDuration = 2f;

        /// <summary>
        /// If this object should be deactivated automatically after Hide
        /// </summary>
        [SerializeField, Tooltip("If this object should be deactivated automatically after Hide")]
        protected bool autoDeactivate = false;

        /// <summary>
        /// Delay before deactive this object after Hide
        /// </summary>
        [SerializeField, DisplayIf("autoDeactivate", true)]
        protected float deactivateDelay = 0.5f;

        /// <summary>
        /// The animator component
        /// </summary>
        protected Animator animator;

        /// <summary>
        /// Coroutines, to be used when interrupted during auto hide/auto deactivate
        /// </summary>
        private Coroutine autoHideCoroutine, autoDeactivateCoroutine;

        /// <summary>
        /// Might have cancas group attached
        /// </summary>
        protected CanvasGroup canvasGroup;

		/// <summary>
		/// The showing hash
		/// </summary>
		protected static int showingHash, showHash, hideHash;

		/// <summary>
		/// Showing key for the animator
		/// </summary>
		protected const string SHOWING_KEY = "Showing", SHOW_KEY = "Show", HIDE_KEY = "Hide";

		#endregion

		#region METHODS

		/// <summary>
		/// Initialize data
		/// </summary>
		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		public static void Initialize()
		{
			showingHash = Animator.StringToHash(SHOWING_KEY);
			showHash = Animator.StringToHash(SHOW_KEY);
			hideHash = Animator.StringToHash(HIDE_KEY);
		}

        /// <summary>
        /// Start this object
        /// </summary>
        protected virtual void Start()
        {
            gameObject.SetActive(!hideOnAwake);
            canvasGroup = GetComponent<CanvasGroup>();
        }

        /// <summary>
        /// Show the object
        /// </summary>
        public virtual void Show()
		{
            if (canvasGroup)
                canvasGroup.interactable = true;

            if (autoHideCoroutine != null)
                ApplicationManager.Instance.StopCoroutine(autoHideCoroutine);

            if (autoDeactivateCoroutine != null)
                ApplicationManager.Instance.StopCoroutine(autoDeactivateCoroutine);

            if (autoHide)
                autoHideCoroutine = ApplicationManager.Instance.StartCoroutine(ShowAndHide());
            else
                ShowNow();
        }

		/// <summary>
		/// Hide the object
		/// </summary>
		public virtual void Hide()
		{
            if (canvasGroup)
                canvasGroup.interactable = false;

            if (autoHideCoroutine != null)
                ApplicationManager.Instance.StopCoroutine(autoHideCoroutine);

            if (autoDeactivateCoroutine != null)
                ApplicationManager.Instance.StopCoroutine(autoDeactivateCoroutine);

            if (autoDeactivate)
                autoDeactivateCoroutine = ApplicationManager.Instance.StartCoroutine(HideAndDeactivate());
            else
                HideNow();
        }

		/// <summary>
		/// Show after the given time
		/// </summary>
		public void ShowAfterSeconds(float seconds)
		{
			ApplicationManager.Instance.StartCoroutine(ShowAfter(seconds));
		}

		/// <summary>
		/// Set the given bool in the animator true
		/// </summary>
		/// <param name="key"></param>
		public void SetBoolTrue(string key)
		{
            if (showWithAnimation && Anim)
                Anim.SetBool(key, true);
		}

		/// <summary>
		/// Set the given bool in the animator false
		/// </summary>
		/// <param name="key"></param>
		public void SetBoolFalse(string key)
		{
            if (showWithAnimation && Anim)
                Anim.SetBool(key, false);
		}

		/// <summary>
		/// Toggle the bool with the given key in the animator
		/// </summary>
		/// <param name="key"></param>
		public void ToggleBool(string key)
		{
            if (showWithAnimation && Anim)
			    Anim.SetBool(key, !Anim.GetBool(key));
		}

		/// <summary>
		/// Set the showing state
		/// </summary>
		/// <param name="show"></param>
		public void SetShowing(bool show)
		{
            if (showWithAnimation && Anim)
                Anim.SetBool(showingHash, show);
		}

		/// <summary>
		/// Set inverse showing in animator
		/// </summary>
		/// <param name="notShowing"></param>
		public void SetNotShowing(bool notShowing)
		{
            if (showWithAnimation && Anim)
			    Anim.SetBool(showingHash, !notShowing);
		}

		/// <summary>
		/// Hide the display if we are active
		/// </summary>
		public void HideIfActive()
		{
			if (gameObject.activeInHierarchy)
				gameObject.SetActive(false);
		}

		/// <summary>
		/// Show after the given seconds
		/// </summary>
		/// <param name="seconds"></param>
		/// <returns></returns>
		IEnumerator ShowAfter(float seconds)
		{
            if (realtimeCoroutines)
                yield return new WaitForSecondsRealtime(showDuration);
            else
                yield return Yielder.Get(seconds);

			if (this)
				Show();
		}

        /// <summary>
        /// Show the object
        /// </summary>
        void ShowNow()
        {
            gameObject.SetActive(true);

            if (showWithAnimation && Anim)
            {
				if (expectTemporaryAnimator)
					Anim.enabled = true;

                if (showWithTriggers)
                    Anim.SetTrigger(showHash);
                else
                    Anim.SetBool(showingHash, true);
            }

			if (debug)
				print("Showing - " + name);
        }

        /// <summary>
        /// Show the object and hide automatically
        /// </summary>
        IEnumerator ShowAndHide()
        {
            ShowNow();

            if (autoHide)
            {
                if (realtimeCoroutines)
                    yield return new WaitForSecondsRealtime(showDuration);
                else
                    yield return Yielder.Get(showDuration);

                Hide();
            }
        }

        /// <summary>
        /// Hide the object
        /// </summary>
        void HideNow()
        {
            if (showWithAnimation && Anim)
            {
				if (expectTemporaryAnimator)
					Anim.enabled = true;

				if (showWithTriggers)
                    Anim.SetTrigger(hideHash);
                else
                    Anim.SetBool(showingHash, false);
            }

			if (debug)
				print("Hiding - " + name);
		}

        /// <summary>
        /// Hide the object and deactivate it
        /// </summary>
        IEnumerator HideAndDeactivate()
        {
            HideNow();

            if (realtimeCoroutines)
                yield return new WaitForSecondsRealtime(deactivateDelay);
            else
                yield return Yielder.Get(deactivateDelay);

            gameObject.SetActive(false);
        }

        #endregion
    }
}