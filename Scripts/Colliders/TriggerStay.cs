﻿using UnityEngine;
using System.Linq;

namespace Bubblegum.Colliders
{

	/// <summary>
	/// Triggers events when an object either enters the trigger or collides with this object
	/// </summary>
	public class TriggerStay : TriggerBase
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// If the position of the object also needs to be inside the trigger
		/// </summary>
		[SerializeField, Tooltip("If the position of the object also needs to be inside the trigger")]
		public bool positionPerfect;

		/// <summary>
		/// Methods to invoke when the trigger is entered
		/// </summary>
		[Tooltip("Methods to invoke when the trigger is entered")]
		[SerializeField]
		public UnityComponentEvent onTriggerStay;

		#endregion // PUBLIC_VARIABLES

		#region PRIVATE_METHODS

		/// <summary>
		/// Collider that we are using as the trigger bounds
		/// </summary>
		private Component trigger;

		#endregion

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Awaken this component
		/// </summary>
		void Awake()
		{
			trigger = GetTrigger();
		}

		/// <summary>
		/// Raises the trigger enter event.
		/// </summary>
		/// <param name="collider">Collider.</param>
		protected void OnTriggerStay(Collider collider)
		{
			if (!is2D)
				OnStay(collider);
		}

		/// <summary>
		/// Raises the trigger enter event.
		/// </summary>
		/// <param name="collider">Collider.</param>
		protected void OnTriggerStay2D(Collider2D collider)
		{
			if (is2D)
				OnStay(collider);
		}

		#endregion // MONOBEHAVIOUR_METHODS

		#region PROTECTED_METHODS

		/// <summary>
		/// Check filters then invoke event
		/// </summary>
		/// <param name="collider"></param>
		/// <returns></returns>
		protected virtual bool OnStay(Component collider)
		{
			if (debug)
				print("Trigger stayed on " + name + " by " + collider.name + " and filtering = " + CheckFilters(collider.gameObject));

			if (!CheckFilters(collider.gameObject))
				return false;

			if (positionPerfect)
				if (is2D)
				{
					if (!((Collider2D)trigger).bounds.Contains((Vector2)collider.transform.position))
						return false;
				}
				else if (!((Collider)trigger).bounds.Contains(collider.transform.root.position))
					return false;

			onTriggerStay.Invoke(collider);

			return true;
		}

		#endregion

	}
}