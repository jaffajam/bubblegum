﻿using UnityEngine;

namespace Bubblegum.Colliders
{

	/// <summary>
	/// Sends collision events to child components
	/// </summary>
	public class CollisionPropagator : MonoBehaviour, IInitializable
	{
		#region PUBLIC_VARIABLES

		/// <summary>
		/// Check if we are initialized
		/// </summary>
		public bool Initialized
		{
			get
			{
				return false;
			}
		}

		#endregion

		#region PRIVATE_VARIABLES

		/// <summary>
		/// All of the receiving objects
		/// </summary>
		private ICollisionReceiver[] receivers;

		#endregion

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Awaken this object
		/// </summary>
		void Awake()
		{
			Initialize();
		}

		/// <summary>
		/// Collision enter event
		/// </summary>
		/// <param name="collision"></param>
		void OnCollisionEnter(Collision collision)
		{
			for (int i = 0; i < receivers.Length; i++)
				receivers[i].TriggerCollisionEnter(collision);
		}

		/// <summary>
		/// Collision stay event
		/// </summary>
		/// <param name="collision"></param>
		void OnCollisionStay(Collision collision)
		{
			for (int i = 0; i < receivers.Length; i++)
				receivers[i].TriggerCollisionStay(collision);
		}

		/// <summary>
		/// Collision exit event
		/// </summary>
		/// <param name="collision"></param>
		void OnCollisionExit(Collision collision)
		{
			for (int i = 0; i < receivers.Length; i++)
				receivers[i].TriggerCollisionExit(collision);
		}

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Initialize this object
		/// </summary>
		public void Initialize()
		{
			receivers = GetComponentsInChildren<ICollisionReceiver>(true);
		}

		#endregion

	}
}