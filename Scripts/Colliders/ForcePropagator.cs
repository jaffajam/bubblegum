﻿using UnityEngine;

namespace Bubblegum.Colliders
{

	/// <summary>
	/// Sends force events to other components in this hierarchy
	/// </summary>
	public class ForcePropagator : MonoBehaviour, IInitializable
	{
		#region VARIABLES

		/// <summary>
		/// Check if this component is initialized
		/// </summary>
		public bool Initialized { get { return false; } }

		/// <summary>
		/// All of the other receivers in this hierarchy
		/// </summary>
		private IForceReceiver[] receivers;

		#endregion

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Start this object
		/// </summary>
		void Start()
		{
			Initialize();
		}

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Initialize this component
		/// </summary>
		public void Initialize()
		{
			receivers = GetComponentsInChildren<IForceReceiver>();
		}

		/// <summary>
		/// Apply the force to all receivers
		/// </summary>
		/// <param name="position"></param>
		/// <param name="force"></param>
		public void ApplyForce(Vector3 position, float force, float radius)
		{
			for (int i = 0; i < receivers.Length; i++)
				receivers[i].ApplyForce(position, force, radius);
		}

		#endregion
	}
}