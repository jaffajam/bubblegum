﻿using UnityEngine;

namespace Bubblegum.Colliders
{

	/// <summary>
	/// Simple grounded behaviour
	/// </summary>
	public class ColliderGroundedControl : MonoBehaviour, IContactControl, ICollisionReceiver, IInitializable
	{
		#region PUBLIC_VARIABLES

		/// <summary>
		/// If even one foot/wheel is touching the ground
		/// </summary>
		public bool Contacting { get; private set; }

		/// <summary>
		/// The ground we are currently on
		/// </summary>
		public Collider Contact { get; private set; }

		/// <summary>
		/// The last point that the raycast hit
		/// </summary>
		public Vector3 ContactPoint { get; private set; }

		/// <summary>
		/// If we are initialized
		/// </summary>
		public bool Initialized
		{
			get
			{
				return false;
			}
		}

		/// <summary>
		/// Mask for our checks
		/// </summary>
		[SerializeField, Tooltip("Mask for our checks")]
		private LayerMask layerMask;

		#endregion

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Collision enter event
		/// </summary>
		/// <param name="collision"></param>
		void OnCollisionEnter(Collision collision)
		{
			Contacting = true;
			Contact = collision.collider;
			ContactPoint = collision.contacts[0].point;
		}

		/// <summary>
		/// Collision stay event
		/// </summary>
		/// <param name="collision"></param>
		void OnCollisionStay(Collision collision)
		{
			Contacting = true;
			Contact = collision.collider;
			ContactPoint = collision.contacts[0].point;
		}

		/// <summary>
		/// Collision exit event
		/// </summary>
		/// <param name="collision"></param>
		void OnCollisionExit(Collision collision)
		{
			Contacting = false;
			Contact = null;
		}

		#endregion // MONOBEHAVIOUR_METHODS

		#region PUBLIC_METHODS

		/// <summary>
		/// Trigger the collision enter event
		/// </summary>
		/// <param name="collision"></param>
		public void TriggerCollisionEnter(Collision collision)
		{
			OnCollisionEnter(collision);
		}

		/// <summary>
		/// Trigger the collision exit event
		/// </summary>
		/// <param name="collision"></param>
		public void TriggerCollisionExit(Collision collision)
		{
			OnCollisionExit(collision);
		}

		/// <summary>
		/// Trigger the collision stay event
		/// </summary>
		/// <param name="collision"></param>
		public void TriggerCollisionStay(Collision collision)
		{
			OnCollisionStay(collision);
		}

		/// <summary>
		/// Initialize this object
		/// </summary>
		public void Initialize()
		{
			Contacting = false;
			Contact = null;
		}

		#endregion
	}
}