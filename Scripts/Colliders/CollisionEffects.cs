﻿using UnityEngine;
using System.Collections.Generic;
using Bubblegum.Particles;

namespace Bubblegum.Colliders
{

	[RequireComponent(typeof(Rigidbody))]

	/// <summary>
	/// Activates a particle effect on the point of collision
	/// </summary>
	public class CollisionEffects : MonoBehaviour
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// All of the effects to trigger
		/// </summary>
		[SerializeField, Tooltip("All of the effects to trigger")]
		private CollisionEffect[] collisionEffects;

		/// <summary>
		/// All of the effects to trigger
		/// </summary>
		[SerializeField, Tooltip("All of the effects to trigger")]
		private CollisionEffect[] collisionStayEffects;

		#endregion // PUBLIC_VARIABLES

		#region PRIVATE_VARIABLES

		/// <summary>
		/// The connected rigidbody
		/// </summary>
		private Rigidbody rBody;

		#endregion

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Awaken this instance
		/// </summary>
		void Awake()
		{
			rBody = GetComponent<Rigidbody>();
		}

		/// <summary>
		/// When a collision is happening
		/// </summary>
		/// <param name="collision"></param>
		void OnCollisionEnter(Collision collision)
		{
			for (int i = 0; i < collisionEffects.Length; i++)
				collisionEffects[i].OnCollision(collision, rBody);

			for (int i = 0; i < collisionStayEffects.Length; i++)
				collisionStayEffects[i].OnCollision(collision, rBody);
		}

		/// <summary>
		/// When a collision is happening
		/// </summary>
		/// <param name="collision"></param>
		void OnCollisionStay(Collision collision)
		{
			for (int i = 0; i < collisionStayEffects.Length; i++)
				collisionStayEffects[i].OnCollision(collision, rBody);
		}

		#endregion // MONOBEHAVIOUR_METHODS

		#region PUBLIC_METHODS

		/// <summary>
		/// Trigger collision at given component position
		/// </summary>
		/// <param name="collider"></param>
		public void TriggerCollision(Component collider, float force)
		{
			for (int i = 0; i < collisionEffects.Length; i++)
				collisionEffects[i].TriggerCollision(collider, force);

			for (int i = 0; i < collisionStayEffects.Length; i++)
				collisionStayEffects[i].TriggerCollision(collider, force);
		}

		#endregion

		#region SUB_CLASSES

		/// <summary>
		/// Effect to play
		/// </summary>
		[System.Serializable]
		private class CollisionEffect
		{
			#region VARIABLES

			/// <summary>
			/// Get the particle system we are using
			/// </summary>
			public ParticleSystem Effect
			{
				get
				{
					if (!effect)
						effect = GlobalParticleSystem.GetSystem(effectKey);

					return effect;
				}
			}

			/// <summary>
			/// The effect to trigger
			/// </summary>
			[SerializeField, Tooltip("The effect to trigger")]
			private Key effectKey;

			/// <summary>
			/// The min force to trigger effects
			/// </summary>
			[SerializeField, Tooltip("The min force to trigger effects")]
			protected float minForceThreshold = 10f;

			/// <summary>
			/// Max force to trigger effects
			/// </summary>
			[SerializeField, Tooltip("Max force to trigger effects")]
			protected float maxForceThreshold = 100f;

			/// <summary>
			/// The min particles to emit at once
			/// </summary>
			[SerializeField, Tooltip("The min particles to emit at once")]
			protected int minEmission = 1;

			/// <summary>
			/// The max particles to emit at once
			/// </summary>
			[SerializeField, Tooltip("The max particles to emit at once")]
			protected int maxEmission = 10;

			/// <summary>
			/// All of the materials that will trigger the effect
			/// </summary>
			[SerializeField, Tooltip("All of the materials that will trigger the effect")]
			protected List<PhysicMaterial> reactMaterials = new List<PhysicMaterial>();

			/// <summary>
			/// The local effect reference
			/// </summary>
			private ParticleSystem effect;

			#endregion

			#region METHODS

			/// <summary>
			/// Check if this layer reacts to the given material
			/// </summary>
			/// <param name="material"></param>
			/// <returns></returns>
			public bool ReactsToMaterial(PhysicMaterial material)
			{
				return reactMaterials.Count == 0 || reactMaterials.Contains(material);
			}

			/// <summary>
			/// Get emission count from the force
			/// </summary>
			/// <param name="force"></param>
			/// <returns></returns>
			public int GetEmitCount(float force)
			{
				return (int)Mathf.Lerp(minEmission, maxEmission, force / maxForceThreshold);
			}

			/// <summary>
			/// When the collision is occuring
			/// </summary>
			/// <param name="collision"></param>
			/// <param name="rBody"></param>
			public void OnCollision(Collision collision, Rigidbody rBody)
			{
				float force = collision.GetForce();

				if (force > minForceThreshold)
				{
					Effect.transform.position = collision.contacts.GetAveragePosition();
					Effect.Emit(GetEmitCount(force));
				}
			}

			/// <summary>
			/// Trigger collision with the given collider
			/// </summary>
			/// <param name="component"></param>
			public void TriggerCollision(Component collider, float force)
			{
				Effect.transform.position = collider.transform.position;
				Effect.Emit(GetEmitCount(force));
			}

			#endregion
		}

		#endregion
	}
}