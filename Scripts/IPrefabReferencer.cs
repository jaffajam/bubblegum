﻿using UnityEngine;

namespace Bubblegum
{

	/// <summary>
	/// Denotes an object that uses a prefab
	/// </summary>
	public interface IPrefabReferencer
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// The target prefab
		/// </summary>
		GameObject Prefab { get; set; }

		#endregion // PUBLIC_VARIABLES
	}
}