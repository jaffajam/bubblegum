﻿using UnityEngine;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Bubblegum
{

	/// <summary>
	/// Used to propagate intialize events to other components
	/// </summary>
	public class Initializer : MonoBehaviour, IInitializable
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// If this component is initialized
		/// </summary>
		public bool Initialized
		{
			get
			{
				return false;
			}
		}

		/// <summary>
		/// If we should initialize receivers even if they already are
		/// </summary>
		[SerializeField, Tooltip("If we should initialize receivers even if they already are")]
		private bool forceReceiverInitialization;

		/// <summary>
		/// If we should find the receiver objects when we initialize this object
		/// </summary>
		[SerializeField, Tooltip("If we should find the receiver objects when we initialize this object")]
		private bool findReceiversOnInitialize;

		/// <summary>
		/// The method that we use for initialization
		/// </summary>
		[SerializeField, Tooltip("The method that we use for initialization")]
		private InitializeMethod initializeMethod;

		/// <summary>
		/// All of the comonents that we will send events to
		/// </summary>
		[SerializeField, Tooltip("All of the comonents that we will send events to"), DisplayIf("findReceiversOnInitialize", false)]
		private Object[] receiverObjects;

		#endregion

		#region PRIVATE_VARIABLES

		/// <summary>
		/// All of the receivers
		/// </summary>
		private IInitializable[] receivers;

		#endregion

		/// <summary>
		/// Awaken this object
		/// </summary>
		void Awake()
		{
			if (initializeMethod == InitializeMethod.Awake)
				Initialize();
		}

		/// <summary>
		/// Start this object
		/// </summary>
		void Start()
		{
			if (initializeMethod == InitializeMethod.Start)
				Initialize();
		}

		/// <summary>
		/// Awaken this object
		/// </summary>
		void OnEnable()
		{
			if (initializeMethod == InitializeMethod.Enable)
				Initialize();
		}

		/// <summary>
		/// Validate inspector input
		/// </summary>
		void OnValidate()
		{
			receivers = receiverObjects.Validate<IInitializable>();
		}

		#region PUBLIC_METHODS

		/// <summary>
		/// Initialize this component
		/// </summary>
		public void Initialize()
		{
			if (findReceiversOnInitialize)
				receivers = GetComponentsInChildren<IInitializable>(true);
			else
				OnValidate();

			for (int i = 0; i < receivers.Length; i++)
				if (forceReceiverInitialization || !receivers[i].Initialized)
					receivers[i].Initialize();
		}

		#endregion

		#region SUB_CLASSES

#if UNITY_EDITOR

		/// <summary>
		/// Editor class for the initializer
		/// </summary>
		[CustomEditor(typeof(Initializer), true)]
		public class InitializerEditor : Editor
		{
			#region METHODS

			/// <summary>
			/// Draw inspector objects
			/// </summary>
			public override void OnInspectorGUI()
			{
				Initializer initializer = (Initializer)target;
				base.OnInspectorGUI();

				if (GUILayout.Button("Find Recievers"))
				{
					initializer.receiverObjects = initializer.GetComponentsInChildren<IInitializable>(true).Where(initer => initer as Object != initializer).Select(item => item as Object).ToArray();
					EditorUtility.SetDirty(initializer);
				}
			}

			#endregion
		}

#endif

		#endregion

	}
}