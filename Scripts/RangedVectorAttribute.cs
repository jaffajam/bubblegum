﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Bubblegum
{

	/// <summary>
	/// Attribute that will restrict displaying the variable its attached to
	/// </summary>
	public class RangedVectorAttribute : PropertyAttribute
	{
		#region PUBLIC_VARIABLES

		/// <summary>
		/// The min and max values for the vector
		/// </summary>
		public float min, max;

		#endregion

		#region CONSTRUCTORS

		/// <summary>
		/// Selected types that we can use
		/// </summary>
		/// <param name="types"></param>
		public RangedVectorAttribute(float min, float max)
		{
			this.min = min;
			this.max = max;
		}

		#endregion
	}

#if UNITY_EDITOR

	/// <summary>
	/// Drawer for the ranged vector
	/// </summary>
	[CustomPropertyDrawer(typeof(RangedVectorAttribute), true)]
	public class RangedVectorAttributeDrawer : PropertyDrawer
	{
		/// <summary>
		/// Draw the inspector
		/// </summary>
		/// <param name="position"></param>
		/// <param name="property"></param>
		/// <param name="label"></param>
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			label = EditorGUI.BeginProperty(position, label, property);
			position = EditorGUI.PrefixLabel(position, label);

			//Get range attribute
			RangedVectorAttribute rangeAttribute = (RangedVectorAttribute)attribute;

			SerializedProperty xProp = property.FindPropertyRelative("x");
			SerializedProperty yProp = property.FindPropertyRelative("y");
			float x = xProp.floatValue;
			float y = yProp.floatValue;

			const float rangeBoundsLabelWidth = 40f;

			var rangeBoundsLabel1Rect = new Rect(position);
			rangeBoundsLabel1Rect.width = rangeBoundsLabelWidth;
			GUI.Label(rangeBoundsLabel1Rect, new GUIContent(x.ToString("F2")));
			position.xMin += rangeBoundsLabelWidth;

			var rangeBoundsLabel2Rect = new Rect(position);
			rangeBoundsLabel2Rect.xMin = rangeBoundsLabel2Rect.xMax - rangeBoundsLabelWidth;
			GUI.Label(rangeBoundsLabel2Rect, new GUIContent(y.ToString("F2")));
			position.xMax -= rangeBoundsLabelWidth;

			EditorGUI.BeginChangeCheck();
			EditorGUI.MinMaxSlider(position, ref x, ref y, rangeAttribute.min, rangeAttribute.max);

			if (EditorGUI.EndChangeCheck())
			{
				xProp.floatValue = x;
				yProp.floatValue = y;
			}

			EditorGUI.EndProperty();
		}
	}

#endif
}