﻿namespace Bubblegum
{
    /// <summary>
    /// Contract to provide other objects with points information
    /// </summary>
    public interface IPoints
    {
		#region METHODS

		/// <summary>
		/// When points change
		/// </summary>
		System.Action onPointsChanged { get; set; }

        /// <summary>
        /// The integer value of the score
        /// </summary>
        int Value { get; }

        /// <summary>
        /// Add int points
        /// </summary>
        /// <param name="points"></param>
        void AddPoints(int points);

        /// <summary>
        /// Set int points
        /// </summary>
        /// <param name="points"></param>
        void SetPoints(int points);

        #endregion
    }
}