﻿namespace Bubblegum
{

	/// <summary>
	/// Class to define what constraints to apply to a 2 axis object
	/// </summary>
	[System.Serializable]
	public struct Toggle2
	{
		/// <summary>
		/// The two axis objects
		/// </summary>
		public bool x, y;
	}

	/// <summary>
	/// Class to define what constraints to apply to a 3 axis object
	/// </summary>
	[System.Serializable]
	public struct Toggle3
	{
		/// <summary>
		/// The three axis objects
		/// </summary>
		public bool x, y, z;
	}

	/// <summary>
	/// Class to define what constraints to apply to a 4 axis object
	/// </summary>
	[System.Serializable]
	public struct Toggle4
	{
		/// <summary>
		/// The four axis objects
		/// </summary>
		public bool x, y, z, w;
	}
}