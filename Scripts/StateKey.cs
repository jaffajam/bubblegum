﻿using UnityEngine;

namespace Bubblegum
{
	/// <summary>
	/// Key that will save and load its state
	/// </summary>
	public abstract class StateKey : Key
	{
		#region VARIABLES

		/// <summary>
		/// If the object should autosave
		/// </summary>
		[SerializeField, Tooltip("If the object should autosave")]
		protected bool autoLoad = true;

		#endregion

		#region METHODS

		/// <summary>
		/// Initialize the state key
		/// </summary>
		public override void Initialize()
		{
            base.Initialize();

			if (autoLoad)
				Load();
		}

		/// <summary>
		/// Load the state of the object
		/// </summary>
		public abstract void Load();

		/// <summary>
		/// Save the state of the object
		/// </summary>
		public abstract void Save();

        /// <summary>
        /// Apply the patch using the given object(will be the same object with updated data)
        /// </summary>
        public override bool ApplyPatch()
        {
            if (!base.ApplyPatch())
                return false;

            StateKey real = GetKey<StateKey>(id);
            real.autoLoad = autoLoad;

            return true;
        }

        #endregion
    }
}