﻿using UnityEngine;

namespace Bubblegum
{
	/// <summary>
	/// A filter that can be applied when monitoring interest objects
	/// </summary>
	public interface IFilter
	{
		#region METHODS

		/// <summary>
		/// Check if the object is interesting
		/// </summary>
		/// <param name="interest"></param>
		/// <returns></returns>
		bool CheckFilter(Identification interest);

		/// <summary>
		/// Check if the object is interesting
		/// </summary>
		/// <param name="interest"></param>
		/// <returns></returns>
		bool CheckFilter(Identification interest, GameObject self);

		#endregion
	}
}