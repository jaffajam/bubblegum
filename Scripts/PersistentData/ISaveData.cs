﻿using System.Collections.Generic;

namespace Bubblegum
{
    /// <summary>
    /// An object that can set/get data from/to a dictionary
    /// </summary>
    public interface ISaveData
    {
        /// <summary>
        /// Set data from dictionary to the object
        /// </summary>
        void SetSaveData(Dictionary<string, SaveDataKeyValuePair> data);

        /// <summary>
        /// Get the save data from this object and save it to the dictionary
        /// </summary>
        void GetSaveData(Dictionary<string, SaveDataKeyValuePair> data);
    }

    /// <summary>
    /// Key Value pair struct for save data
    /// </summary>
    public struct SaveDataKeyValuePair
    {
        /// <summary>
        /// Key for storing the name of the save data
        /// </summary>
        public string key;

        /// <summary>
        /// Saved Value
        /// </summary>
        public object value;

        public SaveDataKeyValuePair(string key, object value)
        {
            this.key = key;
            this.value = value;
        }
    }
}