﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bubblegum
{
    /// <summary>
    /// A wrapper for Unity's PlayerPrefs
    /// </summary>
    public static class PersistentData
    {
        #region METHODS

        /// <summary>
        /// Removes all keys and values from the preferences. Use with caution.
        /// </summary>
        public static void DeleteAll()
        {
            PlayerPrefs.DeleteAll();
        }

        /// <summary>
        /// Removes key and its corresponding value from the preferences.
        /// </summary>
        public static void DeleteKey(string key)
        {
            PlayerPrefs.DeleteKey(key);
        }

        /// <summary>
        /// Sets the int value of the preference identified by key.
        /// </summary>
        public static void SetInt(string key, int value)
        {
            PlayerPrefs.SetInt(key, value);
        }

        /// <summary>
        /// Returns the int value corresponding to key in the preference file if it exists.
        /// </summary>
        public static int GetInt(string key, int defaultValue = 0)
        {
            return PlayerPrefs.GetInt(key, defaultValue);
        }

        /// <summary>
        /// Sets the float value of the preference identified by key.
        /// </summary>
        public static void SetFloat(string key, float value)
        {
            PlayerPrefs.SetFloat(key, value);
        }

        /// <summary>
        /// Returns the float value corresponding to key in the preference file if it exists.
        /// </summary>
        public static float GetFloat(string key, float defaultValue = 0)
        {
            return PlayerPrefs.GetFloat(key, defaultValue);
        }

        /// <summary>
        /// Sets the string value of the preference identified by key.
        /// </summary>
        public static void SetString(string key, string value)
        {
            PlayerPrefs.SetString(key, value);
        }

        /// <summary>
        /// Returns the string value corresponding to key in the preference file if it exists.
        /// </summary>
        public static string GetString(string key, string defaultValue = null)
        {
            return PlayerPrefs.GetString(key, defaultValue);
        }

        /// <summary>
        /// Writes all modified preferences to disk.
        /// </summary>
        public static void Save()
        {
            PlayerPrefs.Save();
        }

        /// <summary>
        /// Returns true if key exists in the preferences.
        /// </summary>
        public static bool HasKey(string key)
        {
            return PlayerPrefs.HasKey(key);
        }

        #endregion
    }

}