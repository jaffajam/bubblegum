﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UnityEngine;

namespace Bubblegum
{
    /// <summary>
    /// A wrapper for Unity's PlayerPrefs
    /// </summary>
    public static class PersistentData
    {
        #region VARIABLES

        /// <summary>
        /// Filename for the save file
        /// </summary>
        private static readonly string SaveFileName = "saveFile.dat";

        /// <summary>
        /// Save file path
        /// </summary>
        private static string saveFilePath;

        /// <summary>
        /// Dictionary containing all the persistent data
        /// </summary>
        private static Dictionary<string, object> saveData;

        /// <summary>
        /// Formatter used for serialization
        /// </summary>
        private static BinaryFormatter formatter;

        #endregion

        #region METHODS

        /// <summary>
        /// 
        /// </summary>
        static PersistentData ()
        {
            saveData = new Dictionary<string, object>();
            formatter = new BinaryFormatter();
            saveFilePath = Application.persistentDataPath + Path.DirectorySeparatorChar + SaveFileName;
            Load();
        }

        /// <summary>
        /// Removes all keys and values from the preferences. Use with caution.
        /// </summary>
        public static void DeleteAll()
        {
            saveData.Clear();
            Save();
        }

        /// <summary>
        /// Removes key and its corresponding value from the preferences.
        /// </summary>
        public static void DeleteKey(string key)
        {
            saveData.Remove(key);
            Save();
        }

        /// <summary>
        /// Sets the int value of the preference identified by key.
        /// </summary>
        public static void SetInt(string key, int value)
        {
            saveData[key] = value;
            Save();
        }

        /// <summary>
        /// Returns the int value corresponding to key in the preference file if it exists.
        /// </summary>
        public static int GetInt(string key, int defaultValue = 0)
        {
            if (HasKey(key) && saveData[key] is int)
                return (int)saveData[key];
            else
                return defaultValue;
        }

        /// <summary>
        /// Sets the float value of the preference identified by key.
        /// </summary>
        public static void SetFloat(string key, float value)
        {
            saveData[key] = value;
            Save();
        }

        /// <summary>
        /// Returns the float value corresponding to key in the preference file if it exists.
        /// </summary>
        public static float GetFloat(string key, float defaultValue = 0)
        {
            if (HasKey(key) && saveData[key] is float)
                return (float)saveData[key];
            else
                return defaultValue;
        }

        /// <summary>
        /// Sets the string value of the preference identified by key.
        /// </summary>
        public static void SetString(string key, string value)
        {
            saveData[key] = value;
            Save();
        }

        /// <summary>
        /// Returns the string value corresponding to key in the preference file if it exists.
        /// </summary>
        public static string GetString(string key, string defaultValue = null)
        {
            if (HasKey(key) && saveData[key] is string)
                return (string)saveData[key];
            else
                return defaultValue;
        }

        /// <summary>
        /// Writes all modified preferences to disk.
        /// </summary>
        public static void Save()
        {
            string encoded = SerializeSaveData();
            File.WriteAllText(saveFilePath, encoded, Encoding.Unicode);
        }

        /// <summary>
        /// Returns true if key exists in the preferences.
        /// </summary>
        public static bool HasKey(string key)
        {
            return saveData.ContainsKey(key);
        }

        /// <summary>
        /// Load data from the saved file
        /// </summary>
        private static void Load()
        {
            try
            {
                if (File.Exists(saveFilePath))
                {
                    string encoded = File.ReadAllText(saveFilePath, Encoding.Unicode);
                    DeserializeSaveData(encoded);
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        /// <summary>
        /// Serialize save data to a string
        /// </summary>
        private static string SerializeSaveData()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                formatter.Serialize(stream, saveData);
                string data = Convert.ToBase64String(stream.ToArray());
                return data;
            }
        }

        /// <summary>
        /// Deserialize save data from a string
        /// </summary>
        private static void DeserializeSaveData(string encodedData)
        {
            using (MemoryStream stream = new MemoryStream(Convert.FromBase64String(encodedData), true))
                saveData = formatter.Deserialize(stream) as Dictionary<string, object>;
        }

        #endregion
    }

}