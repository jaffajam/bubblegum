﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Bubblegum.UI
{

    /// <summary>
    /// Creates a popup based on selected options
    /// </summary>
    [CreateAssetMenu(menuName = "Scriptable Object/Popup Builder")]
	public class PopupBuilder : Key, IPopupData, ISequenced
	{
        #region VARIABLES

        /// <summary>
        /// An ID represent this popup data
        /// </summary>
        public int PopupDataID => ID;

        /// <summary>
        /// Get the priority of this popup
        /// </summary>
        public int Priority => priority;

        /// <summary>
        /// Get the title string
        /// </summary>
        public string Title
        {
            get
            {
                return string.IsNullOrEmpty(title) ? null : title;
            }
            set
            {
                title = value;
            }
        }

        /// <summary>
        /// Get the message string
        /// </summary>
        public string Message
        {
            get
            {
                return string.IsNullOrEmpty(message) ? null : message;
            }
            set
            {
                message = value;
            }
        } 

        /// <summary>
        /// Get the icon
        /// </summary>
        public Sprite Icon
        {
            get
            {
                return icon;
            }
            set
            {
                icon = value;
            }
        }

        /// <summary>
        /// Get the audio to play
        /// </summary>
        public AudioClip Audio => audio;

        /// <summary>
        /// Get the options that we want to show
        /// </summary>
        public GameObject AdditionalContent => additionalContent;

        /// <summary>
        /// Get the confirm action
        /// </summary>
        public Action ConfirmAction => showConfirm ? Confirm : emptyAction;

        /// <summary>
        /// Get the cancel action
        /// </summary>
        public Action CancelAction => showCancel ? Cancel : emptyAction;

        /// <summary>
        /// Get the type of this popup
        /// </summary>
        public string PopupType => popupType;

        /// <summary>
        /// If this task is complete
        /// </summary>
        public bool Complete => throw new NotImplementedException();

        /// <summary>
        /// Type of this popup
        /// </summary>
        [Tooltip("Type of this popup")]
        public string popupType;

        /// <summary>
        /// The priority of this display
        /// </summary>
        [Tooltip("The priority of this display")]
		public int priority = 1;

		/// <summary>
		/// The title to display
		/// </summary>
		[SerializeField, Tooltip("The title to display")]
		private string title = "Error";

		/// <summary>
		/// The message to display
		/// </summary>
		[SerializeField, TextArea, Tooltip("The message to display")]
		private string message = "Relevant user information goes here";

		/// <summary>
		/// The icon to display
		/// </summary>
		[SerializeField, Tooltip("The icon to display")]
		private Sprite icon;

		/// <summary>
		/// The audio to play
		/// </summary>
		[SerializeField, Tooltip("The audio to play")]
		private AudioClip audio;

		/// <summary>
		/// Additional content to add to default display, can be a list of options, images, anything
		/// </summary>
		[SerializeField, Tooltip("Additional content to add to default display")]
		private GameObject additionalContent;

		/// <summary>
		/// If we should show the confirmation action
		/// </summary>
		[SerializeField, Tooltip("If we should show the confirmation action")]
		private bool showConfirm = true;

		/// <summary>
		/// The confirm acton to invoke
		/// </summary>
		[SerializeField, Tooltip("The confirm acton to invoke")]
		private UnityEvent confirmAction;

		/// <summary>
		/// If we should show the cancel action
		/// </summary>
		[SerializeField, Tooltip("If we should show the cancel action")]
		private bool showCancel = true;

		/// <summary>
		/// The cancel acton to invoke
		/// </summary>
		[SerializeField, Tooltip("The cancel acton to invoke")]
		private UnityEvent cancelAction;

		/// <summary>
		/// An empty action to return
		/// </summary>
		private Action emptyAction = null;

		#endregion

		#region METHODS

		/// <summary>
		/// Try and show this dialog
		/// </summary>
		public void Show()
		{
            SequenceManager.Instance.Queue(this);
		}

        /// <summary>
        /// Hide this popup
        /// </summary>
        public void Hide()
        {
            Popup.HidePopup(this);
        }

        /// <summary>
        /// Apply patch to the real object
        /// </summary>
        public override bool ApplyPatch()
        {
            if (!base.ApplyPatch())
                return false;

            PopupBuilder real = GetKey<PopupBuilder>(ID);
            real.priority = priority;
            real.title = title;
            real.message = message;
            real.icon = icon;
            real.audio = audio;
            real.additionalContent = additionalContent;
            real.showConfirm = showConfirm;
            real.confirmAction = confirmAction;
            real.showCancel = showCancel;
            real.cancelAction = cancelAction;

            return true;
        }

        /// <summary>
        /// Trigger confirm
        /// </summary>
        void Confirm()
        {
            confirmAction.Invoke();
        }

        /// <summary>
        /// Trigger cancel
        /// </summary>
        void Cancel()
        {
            cancelAction.Invoke();
        }

        /// <summary>
        /// Before start this task
        /// </summary>
        public void BeforeTask() { }

        /// <summary>
        /// Skip this task
        /// </summary>
        public void SkipTask() { }

        /// <summary>
        /// Start Task
        /// </summary>
        public IEnumerator StartTask()
        {
            Popup.ShowPopup(this);

            while (Popup.GetPopup(popupType).gameObject.activeSelf)
                yield return null;

            SequenceManager.Instance.StartNext(this);
        }

        #endregion
    }
}