﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Bubblegum.UI
{
    /// <summary>
    /// Used to display a popup dialog from anywhere in the app
    /// </summary>
    public class Popup : DisplayMonoBehaviour
	{
        #region PUBLIC_VARIABLES

        /// <summary>
        /// Event invoked when a popup needs to be shown
        /// </summary>
        public static event Action<IPopupData> OnShowPopupEvent;

        /// <summary>
        /// Event invoked when a popup needs to be hidden
        /// </summary>
        public static event Action<IPopupData> OnHidePopupEvent;

        [Header("Popup")]

        /// <summary>
        /// Type of this popup
        /// </summary>
        [SerializeField, Tooltip("Type of this popup")]
        private string popupType;

		/// <summary>
		/// The title to display
		/// </summary>
		[SerializeField, Tooltip("The title to display")]
		private Text title;

		/// <summary>
		/// The message to display
		/// </summary>
		[SerializeField, Tooltip("The message to display")]
		private Text message;

		/// <summary>
		/// The icon to display
		/// </summary>
		[SerializeField, Tooltip("The icon to display")]
		private Image icon;

		/// <summary>
		/// The button for confirmation
		/// </summary>
		[SerializeField, Tooltip("The button for confirmation")]
		private Button confirmButton;

		/// <summary>
		/// The button for cancelation
		/// </summary>
		[SerializeField, Tooltip("The button for cancelation")]
		private Button cancelButton;

        /// <summary>
        /// The parent transform for additional content to go under
        /// </summary>
        [SerializeField, Tooltip("The parent transform for additional content to go under")]
		private Transform additionalContentParent;

		/// <summary>
		/// The audio source to play
		/// </summary>
		private AudioSource audioSource;

        /// <summary>
        /// Cache current displayed popupdata
        /// </summary>
        private IPopupData current;

		/// <summary>
		/// All of the popups
		/// </summary>
		private static Dictionary<string, Popup> popups = new Dictionary<string, Popup>();

        #endregion

        #region STATIC_METHODS

		/// <summary>
		/// Get the popup with the key
		/// </summary>
		/// <param name="key"></param>
		public static Popup GetPopup(string key)
		{
			return popups[key];
		}

        /// <summary>
        /// Show the popup
        /// </summary>
        public static void ShowPopup(IPopupData data)
        {
            OnShowPopupEvent?.Invoke(data);
        }

        /// <summary>
        /// Hide the popup
        /// </summary>
        public static void HidePopup(IPopupData data)
        {
            OnHidePopupEvent?.Invoke(data);
        }

		#endregion

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Awaken this object
		/// </summary>
		void Awake()
		{
			popups.Add(popupType, this);
		}

		/// <summary>
		/// Awaken this object
		/// </summary>
		protected override void Start()
        {
            base.Start();

			audioSource = GetComponent<AudioSource>();
			ResetComponents();

            OnShowPopupEvent += OnShowPopup;
            OnHidePopupEvent += OnHidePopup;
		}

        /// <summary>
        /// On destroy
        /// </summary>
        private void OnDestroy()
        {
            OnShowPopupEvent -= OnShowPopup;
            OnHidePopupEvent -= OnHidePopup;

			popups.Remove(popupType);
		}

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Show popup
		/// </summary>
		public void OnShowPopup(IPopupData popupData)
        {
            if (popupData.PopupType == popupType)
                ApplicationManager.Instance.StartCoroutine(OnShowPopupCoroutine(popupData));
        }

        /// <summary>
        /// Show the popup and wait until hidden
        /// </summary>
        IEnumerator OnShowPopupCoroutine(IPopupData popupData)
        {
            Show(popupData);

            yield return WaitUntilHidden();
        }

        /// <summary>
        /// Hide the popup
        /// </summary>
        public void OnHidePopup(IPopupData popupData)
        {
            if (current == popupData && popupData.PopupType == popupType)
                ApplicationManager.Instance.StartCoroutine(OnHidePopupCoroutine(popupData));
        }

        /// <summary>
        /// Hide the popup and wait until hidden
        /// </summary>
        IEnumerator OnHidePopupCoroutine(IPopupData popupData)
        {
            Hide();

            yield return WaitUntilHidden();
        }

        /// <summary>
        /// Hide current displayed popup
        /// </summary>
        public void HideCurrent()
        {
            ApplicationManager.Instance.StartCoroutine(OnHidePopupCoroutine(current));
        }

		/// <summary>
		/// Show a very simple dialog
		/// </summary>
		/// <param name="title"></param>
		public void Show(string title)
		{
			if (title != null)
			{
				this.title.gameObject.SetActive(true);
				this.title.text = title;
			}

			Show();
		}

		/// <summary>
		/// Show dialog with message
		/// </summary>
		/// <param name="title"></param>
		/// <param name="message"></param>
		public void Show(string title, string message)
		{
			if (message != null && this.message != null)
			{
				this.message.gameObject.SetActive(true);
				this.message.text = message;
			}

            Show(title);
        }

		/// <summary>
		/// Show the dialog with an icon
		/// </summary>
		/// <param name="title"></param>
		/// <param name="icon"></param>
		public void Show(string title, string message, Sprite icon, AudioClip clip)
		{
            //Need this here because otherwise clip wont play
            gameObject.SetActive(true);

			if (clip != null)
				audioSource.PlayOneShot(clip, 1f);

			if (icon != null)
			{
				this.icon.gameObject.SetActive(true);
				this.icon.sprite = icon;
			}

            Show(title, message);
		}

		/// <summary>
		/// Show dialog with options
		/// </summary>
		/// <param name="title"></param>
		/// <param name="message"></param>
		/// <param name="onConfirm"></param>
		/// <param name="onDecline"></param>
		public void Show(string title, string message, Sprite icon, AudioClip clip, Action onConfirm, Action onCancel, GameObject additionalContent)
		{
			if (onConfirm != null)
			{
				confirmButton.gameObject.SetActive(true);
				confirmButton.onClick.AddListener(() =>
                    {
                        Hide();
                        onConfirm();
                    });
			}

			if (onCancel != null)
			{
				cancelButton.gameObject.SetActive(true);
				cancelButton.onClick.AddListener(() =>
                    {
                        Hide();
                        onCancel();
                    });
			}

            confirmButton?.transform.parent.gameObject.SetActive(onCancel != null || onConfirm != null);

            if (additionalContent != null)
                AddAdditionalContent(additionalContent);

            Show(title, message, icon, clip);
		}

		/// <summary>
		/// Show using the given popup builder
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public void Show(IPopupData data)
		{
            current = data;
            Show(data.Title, data.Message, data.Icon, data.Audio, data.ConfirmAction, data.CancelAction, data.AdditionalContent);
		}

		/// <summary>
		/// Add the options to this popup
		/// </summary>
		/// <param name="options"></param>
		/// <returns></returns>
		public void AddAdditionalContent(GameObject additionalContent)
		{
			if (additionalContent != null)
			{
				additionalContentParent.gameObject.SetActive(true);
        		Instantiate(additionalContent, additionalContentParent);
			}
		}

        /// <summary>
        /// Wait until the popup is hidden
        /// </summary>
        /// <returns></returns>
        public IEnumerator WaitUntilHidden()
        {
            while (gameObject.activeSelf)
                yield return Yielder.NullYield;
        }

		#endregion

		#region PRIVATE_METHODS

        /// <summary>
        /// Invoke the method with a realtime delay
        /// </summary>
        /// <returns></returns>
        private IEnumerator InvokeRealtime(Action action, float delay)
        {
            yield return new WaitForSecondsRealtime(delay);
            action?.Invoke();
        }

		/// <summary>
		/// Deactivate this component
		/// </summary>
		private void OnDisable()
		{
			ResetComponents();
		}

		/// <summary>
		/// Set the components inactive
		/// </summary>
		private void ResetComponents()
		{
            if (additionalContentParent != null)
            {
                additionalContentParent.DestroyChildren();
                additionalContentParent.gameObject.SetActive(false);
            }

			title?.gameObject.SetActive(false);
			message?.gameObject.SetActive(false);
			icon?.gameObject.SetActive(false);
			confirmButton?.gameObject.SetActive(false);
			cancelButton?.gameObject.SetActive(false);

			confirmButton?.onClick.RemoveAllListeners();
			cancelButton?.onClick.RemoveAllListeners();

            current = null;
        }

        #endregion
    }
}