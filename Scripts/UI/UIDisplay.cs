﻿using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System.Collections;
using Bubblegum.Utility;

namespace Bubblegum.UI
{
	/// <summary>
	/// Class that will display information to the UI
	/// </summary>
	public class UIDisplay : MonoBehaviour
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// If the ui display is currently animating
		/// </summary>
		public virtual bool Animating
		{
			get
			{
				return textAnimation != null;
			}
		}

		/// <summary>
		/// The format to set for the string
		/// </summary>
		[SerializeField, Tooltip("The format to set for the string")]
		protected string format = "{0:0.00}";

		/// <summary>
		/// The max length that we expect the string to be
		/// </summary>
		[SerializeField, Tooltip("The max length that we expect the string to be")]
		protected int capacity = 10;

		#endregion // PUBLIC_VARIABLES

		#region PRIVATE_VARIABLES

		/// <summary>
		/// Empty char
		/// </summary>
		protected const char NULL_CHAR = (char)0;

		/// <summary>
		/// The UI component that displays the text
		/// </summary>
		protected Text display;

        /// <summary>
        /// UI component using tmp pro
        /// </summary>
        protected TMPro.TMP_Text tmpDisplay;

		/// <summary>
		/// The text value that will be displayed
		/// </summary>
		protected StringBuilder textBuilder;

		/// <summary>
		/// The old length of the string
		/// </summary>
		private int oldLength;

		/// <summary>
		/// The length of the string
		/// </summary>
		private int length;

		/// <summary>
		/// The current text animation
		/// </summary>
		private Coroutine textAnimation;

		#endregion // PRIVATE_VARIABLES

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Awake this component
		/// </summary>
		protected virtual void Awake()
		{
			display = GetComponent<Text>();
            tmpDisplay = GetComponent<TMPro.TMP_Text>();
			textBuilder = new StringBuilder(capacity);

			FillBuilder();
		}

        #endregion // MONOBEHAVIOUR_METHODS

        #region PRIVATE_METHODS

        /// <summary>
        /// Set the value of the display without allocating garbage
        /// </summary>
        /// <param name="value"></param>
        public void SetDisplayText(System.IConvertible value)
        {
            oldLength = textBuilder.Length;
            textBuilder.Length = 0;
            textBuilder.AppendFormat(format, value);
            UpdateText();
		}

		/// <summary>
		/// Set the value of the display without allocating garbage
		/// </summary>
		/// <param name="value"></param>
		public void SetDisplayText(string value)
		{
			oldLength = textBuilder.Length;
			textBuilder.Length = 0;
			textBuilder.Append(value);
            UpdateText();
		}

		/// <summary>
		/// Animate in text
		/// </summary>
		public void AnimateText(string value)
		{
			SetDisplayText(value);

			if (textAnimation != null)
				StopCoroutine(textAnimation);

			textAnimation = StartCoroutine(AnimateShowText());
		}

		/// <summary>
		/// Jump to the end of the animation
		/// </summary>
		public void AnimateJump()
		{
			if (textAnimation != null)
				StopCoroutine(textAnimation);

            if (display)
			    display.text = textBuilder.ToString();

            if (tmpDisplay)
                tmpDisplay.text = textBuilder.ToString();

            textAnimation = null;
		}

		/// <summary>
		/// Fill the string builders empty characters
		/// </summary>
		private void FillBuilder()
		{
			for (int i = textBuilder.Length; i < textBuilder.Capacity; i++)
				textBuilder.Append(NULL_CHAR);
		}

		/// <summary>
		/// Animate the text in
		/// </summary>
		/// <returns></returns>
		private IEnumerator AnimateShowText()
		{
			for (int i = 0; i < length; i++)
			{
                if (display)
				    display.text = textBuilder.ToString(0, i + 1);

                if (tmpDisplay)
                    tmpDisplay.text = textBuilder.ToString(0, i + 1);

                yield return Yielder.NullYield;
			}

			textAnimation = null;
		}

        /// <summary>
        /// Set the text display
        /// </summary>
        private void UpdateText()
        {
            if (textBuilder.Length < oldLength)
                FillBuilder();

            if (display)
            {
                display.text = textBuilder.ToString();
                length = display.text.Length;
            }

            if (tmpDisplay)
            {
                tmpDisplay.text = textBuilder.ToString();
                length = tmpDisplay.text.Length;
            }
        }

        #endregion
    }
}