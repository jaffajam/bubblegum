﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TextMesh))]

/// <summary>
/// Edits values for the text mesh
/// </summary>
public class TextMeshEditor : MonoBehaviour
{
	#region VARIABLES

	/// <summary>
	/// The color value to set
	/// </summary>
	[SerializeField, Tooltip("The color value to set")]
	private Color color;

	/// <summary>
	/// The text mesh object
	/// </summary>
	private TextMesh textMesh;

	#endregion

	#region METHODS

	/// <summary>
	/// Awaken this object
	/// </summary>
	void Awake()
	{
		textMesh = GetComponent<TextMesh>();
		color = textMesh.color;
	}

	/// <summary>
	/// Update this object
	/// </summary>
	void Update()
	{
		if (color != textMesh.color)
			textMesh.color = color;
	}

	#endregion
}
