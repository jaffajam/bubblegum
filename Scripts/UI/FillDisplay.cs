﻿using UnityEngine;
using UnityEngine.UI;

namespace Bubblegum.UI
{
    /// <summary>
    /// Display that will work with either a slider or filled image
    /// </summary>
    public class FillDisplay : MonoBehaviour
    {
        #region VARIABLES

        /// <summary>
        /// Get the min value
        /// </summary>
        public float MinValue
        {
            get
            {
                if (slider)
                    return slider.minValue;

                return minValue;
            }

            set
            {
                if (slider)
                    slider.minValue = value;

                minValue = value;
            }
        }

        /// <summary>
        /// Get the max value
        /// </summary>
        public float MaxValue
        {
            get
            {
                if (slider)
                    return slider.maxValue;

                return maxValue;
            }

            set
            {
                if (slider)
                    slider.maxValue = value;

                maxValue = value;
            }
        }

        /// <summary>
        /// Get the value
        /// </summary>
        public float Value
        {
            get
            {
                if (slider)
                    return slider.value;

                return value;
            }

            set
            {
                if (slider)
                    slider.value = value;

                this.value = value;
                UpdateFillAmount();
            }
        }

        /// <summary>
        /// Controls how much we can fill the image
        /// </summary>
        [SerializeField, Tooltip("Controls how much we can fill the image"), Range(0f, 1f)]
        private float imageFillMin = 0f, imageFillMax = 1f;

        /// <summary>
        /// Slider component
        /// </summary>
        private Slider slider;

        /// <summary>
        /// Image component
        /// </summary>
        private Image image;

        /// <summary>
        /// Custom min/max values for image
        /// </summary>
        private float value, minValue, maxValue;

        #endregion

        #region METHODS

        /// <summary>
        /// Awaken this object
        /// </summary>
        void Awake()
        {
            slider = GetComponent<Slider>();
            image = GetComponent<Image>();
        }

        /// <summary>
        /// Update the image fill amount
        /// </summary>
        void UpdateFillAmount()
        {
            float percent = Mathf.Clamp01((value - minValue) / (maxValue - minValue));

            if (image != null)
                image.fillAmount = percent * (imageFillMax - imageFillMin) + imageFillMin;
        }

        #endregion
    }
}