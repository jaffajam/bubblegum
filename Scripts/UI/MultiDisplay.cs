﻿using UnityEngine;
using UnityEngine.UI;

namespace Bubblegum.UI
{

	/// <summary>
	/// Display that can update dial or slider
	/// </summary>
	public class MultiDisplay : MonoBehaviour
	{
		#region VARIABLES

		/// <summary>
		/// If we should target the player motor
		/// </summary>
		[SerializeField, Tooltip("If we should target the player motor")]
		protected bool targetPlayer;

		/// <summary>
		/// The mode for the display
		/// </summary>
		[SerializeField, Tooltip("The mode for the display")]
		protected Mode mode;

		/// <summary>
		/// The slider showing the value
		/// </summary>
		[SerializeField, Tooltip("The slider showing the value"), DisplayIf("mode", Mode.Slider)]
		protected Slider slider;

		/// <summary>
		/// The image showing the value
		/// </summary>
		[SerializeField, Tooltip("The slider showing the value"), DisplayIf("mode", Mode.Image)]
		protected Image image;

		/// <summary>
		/// The needle showing the value
		/// </summary>
		[SerializeField, Tooltip("The needle showing the value"), DisplayIf("mode", Mode.Dial)]
		protected Transform needle;

		/// <summary>
		/// Text component to update
		/// </summary>
		[SerializeField, Tooltip("The text showing the value"), DisplayIf("mode", Mode.Text)]
		protected Text text;

		/// <summary>
		/// The minimum value to display when running
		/// </summary>
		[SerializeField, Tooltip("The minimum value to display when running")]
		protected Vector2 range = new Vector2(0.1f, 0.975f);

		/// <summary>
		/// The range of rpm displayed
		/// </summary>
		protected float rangeSize;

		#endregion

		#region ENUMS

		/// <summary>
		/// The mode for the dial
		/// </summary>
		protected enum Mode { Slider, Image, Dial, Text }

		#endregion

		#region METHODS

		/// <summary>
		/// Awaken this object
		/// </summary>
		protected virtual void Awake()
		{
			rangeSize = range.y - range.x;
		}

		/// <summary>
		/// Set the value for the display
		/// </summary>
		/// <param name="value"></param>
		protected void SetValue(float value)
		{
			if (slider)
				slider.value = value;
			else if (image)
				image.fillAmount = (value - range.x) / (range.y - range.x);
			else if (needle)
				needle.localEulerAngles = new Vector3(0f, 180f, value);
			else if (text)
				text.text = value.ToString("00");
		}

		#endregion
	}
}