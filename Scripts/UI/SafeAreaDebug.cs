﻿using UnityEngine;

namespace Bubblegum.UI
{

    /// <summary>
    /// Sets safe area for the app
    /// </summary>
    public class SafeAreaDebug : MonoBehaviour
    {
        #region VARIABLES

        /// <summary>
        /// If debug is active for the safe area
        /// </summary>
        public static bool DebugActive { get; private set; }

        /// <summary>
        /// Get the debug safe area
        /// </summary>
        public static Rect SafeArea { get; private set; }

        /// <summary>
        /// If we should enable the notch
        /// </summary>
        public bool enableNotch = true;

        /// <summary>
        /// If we should enable the virtual home button
        /// </summary>
        public bool enableVirtualHomeButton = true;

        /// <summary>
        /// Size of the notch
        /// </summary>
        public Vector2 notchSize = new Vector2(0.5f, 0.05f);

        /// <summary>
        /// Size of the home button
        /// </summary>
        public Vector2 vhomebuttonSize = new Vector2(1f, 0.075f);

        #endregion

        #region METHODS

        /// <summary>
        /// Awaken this object
        /// </summary>
        void Awake()
        {
#if RELEASE
            Destroy(this);
            return;
#endif
            DebugActive = true;
            float yOffset = enableVirtualHomeButton ? vhomebuttonSize.y * Screen.height : 0f;
            float ySize = enableVirtualHomeButton ? Screen.height - vhomebuttonSize.y * Screen.height : Screen.height;
            ySize -= enableNotch ? notchSize.y * Screen.height : 0f;
            SafeArea = new Rect(0f, yOffset, Screen.width, ySize);
        }

        /// <summary>
        /// Draw gui
        /// </summary>
        void OnGUI()
        {
            if (enableNotch)
            {
                //Notch
                Vector2 notch = notchSize * new Vector2(Screen.width, Screen.height);
                Rect notchRect = new Rect(new Vector2(Screen.width * 0.5f - notch.x * 0.5f, 0f), notch);

                GUI.color = Color.black;
                GUI.Box(notchRect, "Notch - Safe Area");
            }

            if (enableVirtualHomeButton)
            {
                //Virtual home button
                Vector2 homeButton = vhomebuttonSize * new Vector2(Screen.width, Screen.height);
                Rect vhomeRect = new Rect(new Vector2(Screen.width * 0.5f - homeButton.x * 0.5f, Screen.height - homeButton.y), homeButton);

                GUI.color = Color.white;
                GUI.Box(vhomeRect, "Virtual Home Button - Safe Area");
            }
        }

        #endregion
    }
}