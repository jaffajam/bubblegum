﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Bubblegum.UI
{
    /// <summary>
    /// Base display setter
    /// </summary>
    public abstract class BaseDisplayListener<T> : BaseDisplaySetter<T>
    {
        #region VARIABLES

        /// <summary>
        /// The original value we edited
        /// </summary>
        public T OriginalValue { get; protected set; }

        /// <summary>
        /// Display setter before this
        /// </summary>
        public IDisplaySetter<T> ParentDisplay
        {
            get
            {
                if (parentDisplay == null)
                    OnValidate();

                return parentDisplay;
            }
        }

        /// <summary>
        /// If we should wait for another display to set the value
        /// </summary>
        [SerializeField, Tooltip("If we should wait for another display to set the value")]
        protected bool waitForParentDisplay;

        /// <summary>
        /// Display object to wait for before setting value
        /// </summary>
        [SerializeField, Tooltip("Display object to wait for before setting value"), DisplayIf("waitForParentDisplay", true)]
        protected Object parentDisplaySetter;

        /// <summary>
        /// Parent display object
        /// </summary>
        private IDisplaySetter<T> parentDisplay;

        #endregion

        #region METHODS

        /// <summary>
        /// On validate
        /// </summary>
        void OnValidate()
        {
            parentDisplay = parentDisplaySetter.Validate<IDisplaySetter<T>>(ref parentDisplaySetter);
        }

        /// <summary>
        /// Initialize using the given action
        /// </summary>
        /// <param name="intializeAction"></param>
        protected virtual void Initialize(Action<T> intializeAction, T value)
        {
            if (!waitForParentDisplay)
                intializeAction?.Invoke(value);
            else
                ParentDisplay.RegisterOnSetDisplay(intializeAction, value);
        }

        #endregion
    }
}