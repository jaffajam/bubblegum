﻿using UnityEngine;

namespace Bubblegum.UI
{
    /// <summary>
    /// Controls activity indicator show/hide
    /// </summary>
    public class ActivityIndicatorDisplay : DisplayMonoBehaviour
    {
        #region VARIABLES

        /// <summary>
        /// Number of activities
        /// </summary>
        private int activityCount;

        #endregion

        #region METHODS

        /// <summary>
        /// Show activity indicator
        /// </summary>
        public override void Show()
        {
            activityCount++;

            if (!gameObject.activeSelf)
                base.Show();
        }

        /// <summary>
        /// Hide Activity indicator
        /// </summary>
        public override void Hide()
        {
            activityCount--;

            if (activityCount <= 0)
            {
                activityCount = 0;
                base.Hide();
            }
        }

        #endregion
    }
}