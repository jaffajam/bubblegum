﻿using UnityEngine;
using UnityEngine.UI;

namespace Bubblegum.UI
{
    [ExecuteInEditMode]
	[RequireComponent(typeof(CanvasRenderer))]
	[RequireComponent(typeof(ParticleSystem))]

	/// <summary>
	/// From here https://forum.unity3d.com/threads/free-script-particle-systems-in-ui-screen-space-overlay.406862/
	/// </summary>
	public class UIParticleSystem : MaskableGraphic
	{
        #region VARIABLES

        /// <summary>
        /// Texture that we want to render
        /// </summary>
        public override Texture mainTexture
        {
            get
            {
                if (particleTexture)
                    return particleTexture;

                return null;
            }
        }

        /// <summary>
        /// The target system
        /// </summary>
        public ParticleSystem System { get; private set; }

        /// <summary>
        /// The texture to use
        /// </summary>
        public Texture particleTexture;

        /// <summary>
        /// Cached transform
        /// </summary>
		private Transform _transform;

        /// <summary>
        /// Individual particles
        /// </summary>
        private ParticleSystem.Particle[] _particles;

        /// <summary>
        /// Vertex setup
        /// </summary>
		private UIVertex[] _quad = new UIVertex[4];

        /// <summary>
        /// UV
        /// </summary>
		private Vector4 _uv = Vector4.zero;

        /// <summary>
        /// The texture sheed animation
        /// </summary>
		private ParticleSystem.TextureSheetAnimationModule _textureSheetAnimation;

        /// <summary>
        /// Texture sheet aniamtion frames
        /// </summary>
		private int _textureSheetAnimationFrames;
    
        /// <summary>
        /// Texture sheet animation frame size
        /// </summary>
		private Vector2 _textureSheedAnimationFrameSize;
    
        /// <summary>
        /// Main particle system module
        /// </summary>
		private ParticleSystem.MainModule main;

        #endregion

        #region METHODS

        /// <summary>
        /// Initialize the system
        /// </summary>
        /// <returns></returns>
		protected bool Initialize()
		{
			if (_transform == null)
				_transform = transform;

			//Prepare particle system
			ParticleSystemRenderer renderer = GetComponent<ParticleSystemRenderer>();

			if (System == null)
			{
				System = GetComponent<ParticleSystem>();
				main = System.main;

				if (System == null)
					return false;

				//Get current particle texture
				if (renderer == null)
					renderer = System.gameObject.AddComponent<ParticleSystemRenderer>();

				Material currentMaterial = renderer.sharedMaterial;

				if (!particleTexture && currentMaterial && currentMaterial.HasProperty("_MainTex"))
					particleTexture = currentMaterial.mainTexture;

				_particles = null;
			}

			//Prepare particles array
			if (_particles == null)
				_particles = new ParticleSystem.Particle[main.maxParticles];

			//Prepare uvs
			if (particleTexture)
				_uv = new Vector4(0, 0, 1, 1);

			//Prepare texture sheet animation
			_textureSheetAnimation = System.textureSheetAnimation;
			_textureSheetAnimationFrames = 0;
			_textureSheedAnimationFrameSize = Vector2.zero;

			if (_textureSheetAnimation.enabled)
			{
				_textureSheetAnimationFrames = _textureSheetAnimation.numTilesX * _textureSheetAnimation.numTilesY;
				_textureSheedAnimationFrameSize = new Vector2(1f / _textureSheetAnimation.numTilesX, 1f / _textureSheetAnimation.numTilesY);
			}

			return true;
		}

        /// <summary>
        /// Awaken this object
        /// </summary>
		protected override void Awake()
		{
			base.Awake();

			if (!Initialize())
				enabled = false;
		}

        /// <summary>
        /// Populate mesh
        /// </summary>
        /// <param name="vh"></param>
		protected override void OnPopulateMesh(VertexHelper vh)
		{
#if UNITY_EDITOR
            if (!Application.isPlaying)
                if (!Initialize())
                    return;
#endif

            //Prepare vertices
            vh.Clear();

			if (!gameObject.activeInHierarchy)
				return;

			//Iterate through current particles
			int count = System.GetParticles(_particles);

			for (int i = 0; i < count; ++i)
			{
				ParticleSystem.Particle particle = _particles[i];

				//Get particle properties
				Vector2 position = (main.simulationSpace == ParticleSystemSimulationSpace.Local ? particle.position : _transform.InverseTransformPoint(particle.position));
				float rotation = -particle.rotation * Mathf.Deg2Rad;
				float rotation90 = rotation + Mathf.PI / 2;
				Color32 color = particle.GetCurrentColor(System);
				float size = particle.GetCurrentSize(System) * 0.5f;

				//Apply scale
				if (main.scalingMode == ParticleSystemScalingMode.Shape)
					position /= canvas.scaleFactor;

				//Apply texture sheet animation
				Vector4 particleUV = _uv;

				if (_textureSheetAnimation.enabled)
				{
					float frameProgress = 1 - (particle.remainingLifetime / particle.startLifetime);
					frameProgress = Mathf.Repeat(frameProgress * _textureSheetAnimation.cycleCount, 1);
					int frame = 0;

					switch (_textureSheetAnimation.animation)
					{

						case ParticleSystemAnimationType.WholeSheet:
							frame = Mathf.FloorToInt(frameProgress * _textureSheetAnimationFrames);
							break;

						case ParticleSystemAnimationType.SingleRow:
							frame = Mathf.FloorToInt(frameProgress * _textureSheetAnimation.numTilesX);
							int row = _textureSheetAnimation.rowIndex;
							frame += row * _textureSheetAnimation.numTilesX;
							break;

					}

					frame %= _textureSheetAnimationFrames;

					particleUV.x = (frame % _textureSheetAnimation.numTilesX) * _textureSheedAnimationFrameSize.x;
					particleUV.y = Mathf.FloorToInt(frame / _textureSheetAnimation.numTilesX) * _textureSheedAnimationFrameSize.y;
					particleUV.z = particleUV.x + _textureSheedAnimationFrameSize.x;
					particleUV.w = particleUV.y + _textureSheedAnimationFrameSize.y;
				}

				_quad[0] = UIVertex.simpleVert;
				_quad[0].color = color;
				_quad[0].uv0 = new Vector2(particleUV.x, particleUV.y);

				_quad[1] = UIVertex.simpleVert;
				_quad[1].color = color;
				_quad[1].uv0 = new Vector2(particleUV.x, particleUV.w);

				_quad[2] = UIVertex.simpleVert;
				_quad[2].color = color;
				_quad[2].uv0 = new Vector2(particleUV.z, particleUV.w);

				_quad[3] = UIVertex.simpleVert;
				_quad[3].color = color;
				_quad[3].uv0 = new Vector2(particleUV.z, particleUV.y);

				if (rotation == 0)
				{
					//No rotation
					Vector2 corner1 = new Vector2(position.x - size, position.y - size);
					Vector2 corner2 = new Vector2(position.x + size, position.y + size);

					_quad[0].position = new Vector2(corner1.x, corner1.y);
					_quad[1].position = new Vector2(corner1.x, corner2.y);
					_quad[2].position = new Vector2(corner2.x, corner2.y);
					_quad[3].position = new Vector2(corner2.x, corner1.y);
				}
				else
				{
					//Apply rotation
					Vector2 right = new Vector2(Mathf.Cos(rotation), Mathf.Sin(rotation)) * size;
					Vector2 up = new Vector2(Mathf.Cos(rotation90), Mathf.Sin(rotation90)) * size;

					_quad[0].position = position - right - up;
					_quad[1].position = position - right + up;
					_quad[2].position = position + right + up;
					_quad[3].position = position + right - up;
				}

				vh.AddUIVertexQuad(_quad);
			}
		}

        /// <summary>
        /// Update this object
        /// </summary>
		void Update()
		{
            if (Application.isPlaying && System.isPlaying)
                SetAllDirty();
		}

#if UNITY_EDITOR

        /// <summary>
        /// End of frame update
        /// </summary>
        void LateUpdate()
        {
            if (!Application.isPlaying)
                SetAllDirty();
        }
#endif

        /// <summary>
        /// Copy values
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
		public static void Copy(UIParticleSystem from, UIParticleSystem to)
		{
			to.particleTexture = from.particleTexture;
			to.material = from.material;
			to.color = from.color;
			to.raycastTarget = from.raycastTarget;
		}
        
        #endregion
    }

}