﻿using System;
using UnityEngine;

namespace Bubblegum.UI
{
    /// <summary>
    /// Base display setter
    /// </summary>
    public abstract class BaseDisplaySetter<T> : MonoBehaviour, IDisplaySetter<T>
    {
        #region VARIABLES

        /// <summary>
        /// The stored value we set
        /// </summary>
        public T SetValue { get; protected set; }

        /// <summary>
        /// Event invoked after setting text
        /// </summary>
        protected Action<T> OnSetDisplay;

        /// <summary>
        /// Whether OnSetDisplay is already invoked
        /// </summary>
        private bool alreadyInvoked;

        #endregion

        #region METHODS

        /// <summary>
        /// Invoke action on set display
        /// </summary>
        public void RegisterOnSetDisplay(Action<T> action, T value)
        {
            if (alreadyInvoked)
                action?.Invoke(value);

            OnSetDisplay += action;
        }

        /// <summary>
        /// Unregister on set display event listener
        /// </summary>
        /// <param name="action"></param>
        public void UnregisterOnSetDisplay(Action<T> action)
        {
            OnSetDisplay -= action;
        }

        /// <summary>
        /// Invoke OnSetDisplay event
        /// </summary>
        protected void InvokeOnSetDisplay(T value)
        {
            alreadyInvoked = true;
            OnSetDisplay?.Invoke(value);
        }

        /// <summary>
        /// Invoke OnSetDisplay event
        /// </summary>
        protected bool TryInvokeOnSetDisplay(T value)
        {
            InvokeOnSetDisplay(value);

            return OnSetDisplay != null;
        }

        #endregion
    }
}