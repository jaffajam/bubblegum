﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace Bubblegum.UI
{
	/// <summary>
	/// Control the Unity quality settings dynamically
	/// </summary>
	public class QualityControl : MonoBehaviour
	{
		#region VARIABLES

		/// <summary>
		/// Our desired quality value
		/// </summary>
		[SerializeField, Tooltip("Our desired quality value")]
		private int desiredValue;

		/// <summary>
		/// Invoked one frame before settings are applied
		/// </summary>
		[Tooltip("Invoked one frame before settings are applied")]
		public UnityEvent onPreApply;

		/// <summary>
		/// Prefs key to use when saving
		/// </summary>
		private const string QUALITY_PREF = "Quality";

		#endregion

		#region METHODS

		/// <summary>
		/// Awaken this object
		/// </summary>
		void Awake()
		{
			desiredValue = PersistentData.GetInt(QUALITY_PREF, desiredValue);
			QualitySettings.SetQualityLevel(desiredValue);
			Slider slider = GetComponent<Slider>();
            Toggle toggle = GetComponent<Toggle>();

			if (slider != null)
				slider.SetValueWithoutNotify(desiredValue);

            if (toggle)
                toggle.SetIsOnWithoutNotify(desiredValue == 0);

            ApplyDesiredValue();
		}

		/// <summary>
		/// Set the desired quality level
		/// </summary>
		/// <param name="level"></param>
		public void SetDesiredValue(float level)
		{
			desiredValue = (int)level;
		}

		/// <summary>
		/// Apply the desired quality setting
		/// </summary>
		public void ApplyDesiredValue()
		{
            if (!gameObject.activeSelf)
                return;

            PersistentData.SetInt(QUALITY_PREF, desiredValue);

			if (QualitySettings.GetQualityLevel() != desiredValue)
				StartCoroutine(ApplyDesiredValueAfterOneFrame());
		}

        /// <summary>
        /// Toggle to low quality setting
        /// </summary>
        /// <param name="enabled"></param>
        public void ToggleLowQuality(bool enabled)
        {
            desiredValue = enabled ? 0 : QualitySettings.names.Length - 1;
            ApplyDesiredValue();
        }

        /// <summary>
        /// Toggle to low quality setting
        /// </summary>
        /// <param name="enabled"></param>
        public void ToggleHighQuality(bool enabled)
        {
            desiredValue = enabled ? QualitySettings.names.Length - 1 : 0;
            ApplyDesiredValue();
        }

        /// <summary>
        /// Method name says it all
        /// </summary>
        /// <returns></returns>
        IEnumerator ApplyDesiredValueAfterOneFrame()
		{
			onPreApply.Invoke();

			yield return null;

			QualitySettings.SetQualityLevel(desiredValue);
		}

		#endregion
	}
}