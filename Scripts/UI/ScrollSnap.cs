﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Bubblegum.UI
{

    /// <summary>
    /// Snaps scroller to positions
    /// </summary>
    public class ScrollSnap : ScrollRect
    {
        #region VARIABLES

        /// <summary>
        /// Get the element cound
        /// </summary>
        public int ElementCount
        {
            get
            {
                return content.GetActiveChildCount();
            }
        }

        /// <summary>
        /// Get the current index
        /// </summary>
        public int CurrentIndex { get; private set; }

        /// <summary>
        /// When the index of the scroller changes
        /// </summary>
        public System.Action<int> onIndexChanged;

        /// <summary>
        /// The current snap target
        /// </summary>
        private float snapTarget;

        /// <summary>
        /// If we are currently snapping
        /// </summary>
        private bool snapping = true;

        /// <summary>
        /// The position that we started the drag
        /// </summary>
        private Vector2 startDragPosition;

        /// <summary>
        /// The snap speed multiplier
        /// </summary>
        private const float SNAP_MULTIPLIER = 100f, SMOOTH_MULTIPLIER = 0.1f;

        #endregion

        #region METHODS

        /// <summary>
        /// Update this object
        /// </summary>
        void Update()
        {
            if (snapping)
            {
                if (horizontal)
                    horizontalNormalizedPosition = Mathf.Lerp(horizontalNormalizedPosition, snapTarget, Time.deltaTime * elasticity * SNAP_MULTIPLIER);

                else if (vertical)
                    verticalNormalizedPosition = Mathf.Lerp(verticalNormalizedPosition, snapTarget, Time.deltaTime * elasticity * SNAP_MULTIPLIER);
            }
        }

        /// <summary>
        /// When the pointer is released
        /// </summary>
        /// <param name="eventData"></param>
        public override void OnEndDrag(PointerEventData eventData)
        {
            base.OnEndDrag(eventData);

            snapping = true;
            float direction = horizontal ? Mathf.Sign(startDragPosition.x - eventData.position.x) : Mathf.Sign(startDragPosition.y - eventData.position.y);
            float directionalSmooth = direction * SMOOTH_MULTIPLIER;
            int index = Mathf.Clamp(Mathf.RoundToInt(((horizontal ? horizontalNormalizedPosition : verticalNormalizedPosition) + directionalSmooth) * (ElementCount - 1)), 0, ElementCount - 1);

            if (CurrentIndex != index)
            {
                snapTarget = (float)index / (ElementCount - 1);
                CurrentIndex = index;
                onIndexChanged?.Invoke(CurrentIndex);
            }
        }

        /// <summary>
        /// When the pointer is pressed
        /// </summary>
        /// <param name="eventData"></param>
        public override void OnBeginDrag(PointerEventData eventData)
        {
            base.OnBeginDrag(eventData);
            snapping = false;
            startDragPosition = eventData.position;
        }

        /// <summary>
        /// Select the item at the given index
        /// </summary>
        /// <param name="index"></param>
        public void Select(int index)
        {
            var newIndex = horizontal ? index : Mathf.Clamp(index, 0, ElementCount - 1);
            snapTarget = (float)newIndex / (ElementCount - 1);

            if (CurrentIndex != newIndex)
            {
                CurrentIndex = newIndex;
                onIndexChanged?.Invoke(CurrentIndex);
            }
        }

        /// <summary>
        /// Jump to the item at the given index
        /// </summary>
        /// <param name="index"></param>
        public void Jump(int index)
        {
            Select(index);

            if (horizontal)
                horizontalNormalizedPosition = snapTarget;

            else if (vertical)
                verticalNormalizedPosition = snapTarget;
        }

        /// <summary>
        /// Reset the index
        /// </summary>
        public void ResetIndex()
        {
            if (horizontal)
                Jump(0);

            else if (vertical)
                Jump(ElementCount - 1);
        }

		/// <summary>
		/// Go to the next index
		/// </summary>
		public void Next()
		{
			Select(CurrentIndex + 1);
		}

		/// <summary>
		/// Go to the previous page
		/// </summary>
		public void Previous()
		{
            Select(CurrentIndex - 1);
		}

		/// <summary>
		/// Try and increment the index
		/// </summary>
		public void TryIncrementIndex()
		{
			if (CurrentIndex < ElementCount - 1)
			{
				CurrentIndex++;
				snapTarget = (float)CurrentIndex / (ElementCount - 1);
				onIndexChanged.Invoke(CurrentIndex);
			}
		}

		/// <summary>
		/// Try and increment the index
		/// </summary>
		public void TryDecrementIndex()
		{
			if (CurrentIndex > 0)
			{
				CurrentIndex--;
				snapTarget = (float)CurrentIndex / (ElementCount - 1);
				onIndexChanged.Invoke(CurrentIndex);
			}
		}

        #endregion
    }
}