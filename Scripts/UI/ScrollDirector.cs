﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Bubblegum.UI
{
    /// <summary>
    /// Directs scroll input out to different scroll containers depending on axis of drag
    /// </summary>
    public class ScrollDirector : MonoBehaviour, IInitializePotentialDragHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        #region VARIABLES

        /// <summary>
        /// The horizontal scroller
        /// </summary>
        [SerializeField, Tooltip ("The horizontal scroller")]
        private ScrollRect horizontalScroller;

        /// <summary>
        /// The vertical scroller
        /// </summary>
        [SerializeField, Tooltip("The vertical scroller")]
        private ScrollRect verticalScroller;

        /// <summary>
        /// The selected scroll rect
        /// </summary>
        private ScrollRect selected;

        #endregion

        #region METHODS

        /// <summary>
        /// Initialize a potential drag
        /// </summary>
        /// <param name="eventData"></param>
        public void OnInitializePotentialDrag(PointerEventData eventData)
        {
            horizontalScroller.OnInitializePotentialDrag(eventData);
            verticalScroller.OnInitializePotentialDrag(eventData);
        }

        /// <summary>
        /// When a drag has begun
        /// </summary>
        /// <param name="eventData"></param>
        public void OnBeginDrag(PointerEventData eventData)
        {
            selected = Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y) ? horizontalScroller : verticalScroller;
            selected.OnBeginDrag(eventData);
        }

        /// <summary>
        /// While we are dragging
        /// </summary>
        /// <param name="eventData"></param>
        public void OnDrag(PointerEventData eventData)
        {
            if (!selected)
                return;

            selected.OnDrag(eventData);
        }

        /// <summary>
        /// When a drag ends
        /// </summary>
        /// <param name="eventData"></param>
        public void OnEndDrag(PointerEventData eventData)
        {
            if (!selected)
                return;

            selected.OnEndDrag(eventData);
            selected = null;
        }

        /// <summary>
        /// Update to a new vertical scroller
        /// </summary>
        public void UpdateVerticalScroller(int index)
        {
            verticalScroller = horizontalScroller.content.GetChild(index).GetComponent<ScrollRect>();
        }

        /// <summary>
        /// Update to a new horizontal scroller
        /// </summary>
        public void UpdateHorizontalScroller(int index)
        {
            horizontalScroller = verticalScroller.content.GetChild(index).GetComponent<ScrollRect>();
        }

        #endregion
    }
}