﻿using UnityEngine;

namespace Bubblegum.UI
{
    /// <summary>
    /// Controls the target FPS
    /// </summary>
    public class FPSControl : MonoBehaviour
    {
        #region VARIABLES

        /// <summary>
        /// Get or set the target fps
        /// </summary>
        public int TargetFPS
        {
            get
            {
                return Application.targetFrameRate;
            }

            set
            {
                Application.targetFrameRate = value;
                PersistentData.SetInt(PREFS_KEY, value);
            }
        }

        /// <summary>
        /// Target default fps
        /// </summary>
        [SerializeField, Tooltip("Target default fps")]
        private int defaultFPS = 60;

        /// <summary>
        /// Saved prefs key for target fps
        /// </summary>
        private const string PREFS_KEY = "TargetFPS";

        /// <summary>
        /// Low/high fps
        /// </summary>
        private const int DEFAULT_FPS = -1, LOW_FPS = 30, HIGH_FPS = 60;

        #endregion

        #region METHODS

        /// <summary>
        /// Awaken this object
        /// </summary>
        void Awake()
        {
            Revert();
        }

        /// <summary>
        /// Togge low fps
        /// </summary>
        /// <param name="enabled"></param>
        public void ToggleLowFPS(bool enabled)
        {
            TargetFPS = enabled ? LOW_FPS : HIGH_FPS;
        }

        /// <summary>
        /// Togge high fps
        /// </summary>
        /// <param name="enabled"></param>
        public void ToggleHighFPS(bool enabled)
        {
            TargetFPS = enabled ? HIGH_FPS : LOW_FPS;
        }

        /// <summary>
        /// Revert to saved settings
        /// </summary>
        public void Revert()
        {
            int fps = PersistentData.GetInt(PREFS_KEY, defaultFPS);
            Application.targetFrameRate = fps;
        }

        #endregion
    }
}