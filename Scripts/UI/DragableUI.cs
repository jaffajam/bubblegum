﻿using UnityEngine;
using UnityEngine.Events;

namespace Bubblegum.UI
{

	public class DragableUI : MonoBehaviour
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// If this element is currently being dragged
		/// </summary>
		[Tooltip("If this element is currently being dragged")]
		[SerializeField]
		private bool dragging;

		/// <summary>
		/// Methods to invoke when dragging stops
		/// </summary>
		[Tooltip("Methods to invoke when dragging stops")]
		[SerializeField]
		private UnityEvent onStopDragging;

		#endregion // PUBLIC_VARIABLES

		#region PRIVATE_VARIABLES

		/// <summary>
		///	Used when the image is created to wait for the mouse to be released and the image placed
		/// </summary>
		private bool waitForMouseUp;

		/// <summary>
		/// The offset from this transform to the mouse position when it was clicked,
		/// this avoids the image moving when it is selected
		/// </summary>
		private Vector3 mouseOffset;

		#endregion // PRIVATE_VARIABLES

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Called every frame
		/// </summary>
		void Update()
		{
			Move();
		}

		#endregion // MONOBEHAVIOUR_METHODS

		#region PUBLIC_METHODS

		/// <summary>
		/// Starts the dragging.
		/// </summary>
		public void StartDragging()
		{
			dragging = true;
			mouseOffset = transform.position - UnityEngine.Input.mousePosition;
		}

		/// <summary>
		/// Deselects the image.
		/// </summary>
		public void StopDragging()
		{
			dragging = false;
			onStopDragging.Invoke();
		}

		#endregion // PUBLIC_METHODS

		#region PRIVATE_METHODS

		/// <summary>
		/// Move the dragable
		/// </summary>
		private void Move()
		{
			if (dragging)
			{
				transform.position = UnityEngine.Input.mousePosition + mouseOffset;
			}
		}

		#endregion // PRIVATE_METHODS
	}
}