﻿using UnityEngine;

namespace Bubblegum.UI
{
    /// <summary>
    /// Adjust UI based on device safe area
    /// </summary>
    public class SafeAreaCanvas : MonoBehaviour
    {
        #region VARIABLES

        /// <summary>
        /// Canvas where this UI belongs to
        /// </summary>
        private Canvas canvas;

        /// <summary>
        /// Canvas rect transform
        /// </summary>
        private RectTransform canvasRectTransform;

        /// <summary>
        /// Safe area rect transform
        /// </summary>
        private RectTransform safeAreaTransform;

        #endregion

        #region METHODS

        /// <summary>
        /// Start
        /// </summary>
        void Start()
        {
            canvas = GetComponentInParent<Canvas>();
            canvasRectTransform = canvas.GetComponent<RectTransform>();

            safeAreaTransform = GetComponent<RectTransform>();

            ApplySafeArea();
        }

        /// <summary>
        /// Apply safe area
        /// </summary>
        void ApplySafeArea()
        {
            if (safeAreaTransform == null)
                return;

            var safeArea = SafeAreaDebug.DebugActive ? SafeAreaDebug.SafeArea : Screen.safeArea;

            var anchorMin = safeArea.position;
            var anchorMax = safeArea.position + safeArea.size;
            anchorMin.x /= canvas.pixelRect.width;
            anchorMin.y /= canvas.pixelRect.height;
            anchorMax.x /= canvas.pixelRect.width;
            anchorMax.y /= canvas.pixelRect.height;

            safeAreaTransform.anchorMin = anchorMin;
            safeAreaTransform.anchorMax = anchorMax;
        }

        #endregion
    }
}