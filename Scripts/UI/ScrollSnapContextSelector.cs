﻿using UnityEngine;
using Bubblegum.UI;

namespace Bubblegum.UI
{
	/// <summary>
	/// Applies one context to another when selected
	/// </summary>
	public class ScrollSnapContextSelector : MonoBehaviour
	{
		#region VARIABLES

		/// <summary>
		/// The context that identifies this object
		/// </summary>
		[SerializeField, Tooltip("The context that identifies this object")]
		private Object identifyingContextObject;

		/// <summary>
		/// The context that we will put our context into
		/// </summary>
		[SerializeField, Tooltip("The context that we will put our context into")]
		private Object selectingContextObject;

		/// <summary>
		/// Scroll snap object to listen to
		/// </summary>
		private ScrollSnap scrollSnap;

		/// <summary>
		/// The converted context object
		/// </summary>
		private IContextManager identifyingContext, selectingContext;

		#endregion

		#region METHODS

		/// <summary>
		/// Awaken this object
		/// </summary>
		void Awake()
		{
			scrollSnap = GetComponentInParent<ScrollSnap>();
			scrollSnap.onIndexChanged += TrySelect;
		}

		/// <summary>
		/// Validate inspector input
		/// </summary>
		void OnValidate()
		{
			identifyingContext = identifyingContextObject.Validate<IContextManager>(ref identifyingContextObject);
			selectingContext = selectingContextObject.Validate<IContextManager>(ref selectingContextObject);
		}

		/// <summary>
		/// Destroy this object
		/// </summary>
		void OnDestroy()
		{
			scrollSnap.onIndexChanged -= TrySelect;
		}

		/// <summary>
		/// Try select this selector
		/// </summary>
		/// <param name="index"></param>
		public void TrySelect(int index)
		{
			if (transform.GetSiblingIndex() == index)
				Select();
		}

		/// <summary>
		/// Select this context object
		/// </summary>
		public void Select()
		{
			selectingContext.Set(identifyingContext.Context);
		}

		#endregion
	}
}