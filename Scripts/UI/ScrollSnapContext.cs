﻿using UnityEngine;

namespace Bubblegum.UI
{
    [RequireComponent(typeof(ScrollSnap))]

    /// <summary>
    /// When a scroll snap changes index, this object displays information to the user
    /// </summary>
    public class ScrollSnapContext : MonoBehaviour
    {
        #region VARIABLES

        /// <summary>
        /// The context object to use
        /// </summary>
        [SerializeField, Tooltip("The context object to use")]
        private Object contextObject;

        /// <summary>
        /// The scroll snap object
        /// </summary>
        private ScrollSnap scrollSnap;

        /// <summary>
        /// The converted context object
        /// </summary>
        private IContextManager context;

        #endregion

        #region METHODS

        /// <summary>
        /// Awaken this object
        /// </summary>
        void Awake()
        {
            scrollSnap = GetComponent<ScrollSnap>();
            scrollSnap.onIndexChanged += UpdateContext;

            OnValidate();
            UpdateContext(0);
        }

        /// <summary>
        /// Destroy this object
        /// </summary>
        void OnDestroy()
        {
            scrollSnap.onIndexChanged -= UpdateContext;
        }

        /// <summary>
        /// Validate inspector input
        /// </summary>
        void OnValidate()
        {
            context = contextObject.Validate<IContextManager>(ref contextObject);
        }

        /// <summary>
        /// Update information based on the latest scroll snap index
        /// </summary>
        void UpdateContext(int index)
        {
            context.Set(scrollSnap.content.GetActiveChild(index).GetComponent<Identification>().Key);
        }

        #endregion
    }
}