﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Bubblegum
{
	/// <summary>
	/// Tag that is attached to an object to give it more functionality
	/// </summary>
	public abstract class Tag : ScriptableObject
	{
        #region PUBLIC_METHODS

        /// <summary>
        /// The owner for this tag
        /// </summary>
        [SerializeField, ReadOnly]
        private PrefabConnection owner;

        /// <summary>
        /// If the tag is enabled
        /// </summary>
        [SerializeField, Tooltip("If the tag is enabled")]
        private bool enabled = true;

        #endregion

        #region METHODS

        /// <summary>
        /// Run the tag on the given object
        /// </summary>
        public void RunTag(PrefabConnection connection, GameObject gameObject)
        {
            if (enabled)
                Run(connection, gameObject);
        }

        /// <summary>
        /// Run the tag on the given object
        /// </summary>
        protected abstract void Run(PrefabConnection connection, GameObject gameObject);

        /// <summary>
        /// Initialize this object
        /// </summary>
        public void Initialize(PrefabConnection owner)
        {
            this.owner = owner;
            name = GetType().Name;
        }

		/// <summary>
		/// Get a component or display an error
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="gameObject"></param>
		/// <returns></returns>
		protected bool GetComponent<T>(GameObject gameObject, out T component) where T : Component
		{
			component = gameObject.GetComponent<T>();

			if (!component)
				Debug.LogError("Trying to apply tag of type " + GetType().Name + " but it does not contain a component of type " + typeof(T).Name);

			return component;
		}

        #endregion

#if UNITY_EDITOR

        /// <summary>
        /// Editor script for the tag
        /// </summary>
        [CustomEditor(typeof(Tag), true)]
        public class TagEditor : Editor
        {
            #region METHODS

            /// <summary>
            /// Draw the inspector
            /// </summary>
            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();

                if (GUILayout.Button("Remove Tag"))
                    if (((Tag)target).owner)
                        ((Tag)target).owner.RemoveTag((Tag)target);
                    else
                        DestroyImmediate(target, true);
            }

            #endregion
        }

#endif
    }
}