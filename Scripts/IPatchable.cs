﻿using UnityEngine;

namespace Bubblegum
{
    /// <summary>
    /// An object that can have a patch applied to it
    /// </summary>
    public interface IPatchable
    {
        #region METHODS

        /// <summary>
        /// Apply the patch using the given object(will be the same object with updated data)
        /// </summary>
        bool ApplyPatch();

        #endregion
    }
}