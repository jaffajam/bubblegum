﻿using UnityEngine;

namespace Bubblegum
{
    /// <summary>
    /// Leaderboard prize interface
    /// </summary>
    public interface ILeaderboardPrize
    {
        /// <summary>
        /// Prize criteria
        /// </summary>
        string PrizeCriteria { get; }

        /// <summary>
        /// Prize name
        /// </summary>
        string PrizeName { get; }

        /// <summary>
        /// Prize icon
        /// </summary>
        Sprite PrizeIcon { get; }
    }
}