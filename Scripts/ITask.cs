﻿namespace Bubblegum
{
	/// <summary>
	/// Object that incrementally complete tasks
	/// </summary>
	public interface ITask
	{
        #region METHODS

        /// <summary>
        /// The key that denotes this task type
        /// </summary>
        string TaskKey { get; }

        /// <summary>
        /// The task message to describe what is happening
        /// </summary>
        string TaskMessage { get; }

		/// <summary>
		/// Get the total tasks to complete
		/// </summary>
		int TotalTasks { get; }

		/// <summary>
		/// Get the total tasks completed
		/// </summary>
		int CompletedTasks { get; }

		#endregion
	}
}