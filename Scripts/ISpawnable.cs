﻿namespace Bubblegum
{

	/// <summary>
	/// An item that is intended to be spawned
	/// </summary>
	public interface ISpawnable
	{
		#region PUBLIC_METHODS

		/// <summary>
		/// Invoked when the object is spawned
		/// </summary>
		void OnSpawn();

        /// <summary>
        /// Invoked when this object is killed
        /// </summary>
        void OnKill();

		#endregion // PUBLIC_METHODS
	}
}