﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif
using System.Reflection;

namespace Bubblegum
{

	/// <summary>
	/// Wrapper for sorting layer so it can be drawn
	/// </summary>
	[System.Serializable]
	public struct SortingSetLayer
	{
		#region PUBLIC_VARIABLES

		/// <summary>
		/// The id that links to the sorting layer
		/// </summary>
		[SerializeField]
		private int id;

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Set the value on the given renderer
		/// </summary>
		/// <param name="renderer"></param>
		public void Set(MeshRenderer renderer)
		{
			renderer.sortingLayerID = id;
		}

		#endregion

	}

#if UNITY_EDITOR

	/// <summary>
	/// Sorting layer inspector drawer
	/// Taken from https://think24code.wordpress.com/2014/11/14/unity3d-sorting-layer-property-drawer/
	/// </summary>
	[CustomPropertyDrawer(typeof(SortingSetLayer))]
	public class SortingSetLayerDrawer : PropertyDrawer
	{

		/// <summary>
		/// Is called to draw a property.
		/// </summary>
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.LabelField(position, label);
			SerializedProperty id = property.FindPropertyRelative("id");

			position.x += EditorGUIUtility.labelWidth;
			position.width -= EditorGUIUtility.labelWidth;

			string[] sortingLayerNames = GetSortingLayerNames();
			int[] sortingLayerIDs = GetSortingLayerIDs();

			int sortingLayerIndex = Mathf.Max(0, System.Array.IndexOf(sortingLayerIDs, id.intValue));
			sortingLayerIndex = EditorGUI.Popup(position, sortingLayerIndex, sortingLayerNames);
			id.intValue = sortingLayerIDs[sortingLayerIndex];
		}

		/// <summary>
		/// Retrives list of sorting layer names.
		/// </summary>
		private string[] GetSortingLayerNames()
		{
			System.Type internalEditorUtilityType = typeof(InternalEditorUtility);
			PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty(
					"sortingLayerNames", BindingFlags.Static | BindingFlags.NonPublic);

			return sortingLayersProperty.GetValue(null, new object[0]) as string[];
		}

		/// <summary>
		/// Retrives list of sorting layer identifiers.
		/// </summary>
		private int[] GetSortingLayerIDs()
		{
			System.Type internalEditorUtilityType = typeof(InternalEditorUtility);
			PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty(
					"sortingLayerUniqueIDs", BindingFlags.Static | BindingFlags.NonPublic);

			return sortingLayersProperty.GetValue(null, new object[0]) as int[];
		}

	}
#endif
}
