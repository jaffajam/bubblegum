﻿
using UnityEngine;

namespace Bubblegum
{
	/// <summary>
	/// Identification that pulls from context
	/// </summary>
	public class ContextIdentification : Identification
	{
        #region VARIABLES

        /// <summary>
        /// If we should update the ID objects on enable
        /// </summary>
        [SerializeField, Tooltip ("If we should update the ID objects on enable")]
        private bool initializeOnEnable = true;

		/// <summary>
		/// The context manager for this object
		/// </summary>
		[SerializeField, Tooltip("The context manager for this object")]
		private Object contextObject;

		/// <summary>
		/// The context object
		/// </summary>
		private IContextManager context;

		#endregion

		#region METHODS

		/// <summary>
		/// Awaken this object
		/// </summary>
		protected override void OnEnable()
		{
			OnValidate();

            if (initializeOnEnable)
			    UpdateID();

			context.onContextChanged += UpdateID;

			base.OnEnable();
		}

		/// <summary>
		/// Validate inspector input
		/// </summary>
		void OnValidate()
		{
			context = contextObject.Validate<IContextManager>(ref contextObject);
		}

		/// <summary>
		/// Destroy this object
		/// </summary>
		void OnDestroy()
		{
			context.onContextChanged -= UpdateID;
		}

		/// <summary>
		/// Initialize this object
		/// </summary>
		public void UpdateID()
		{
            if (!gameObject.activeInHierarchy)
                return;

			if (context.Context != null)
			{
				Key = context.GetContext<Entity>();

				foreach (IInitializable initializable in GetComponentsInChildren<IInitializable>())
					initializable.Initialize();
			}
		}

		#endregion

	}
}