﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bubblegum
{
    /// <summary>
    /// Display object that can be shown in seqence
    /// </summary>
    public class SequenceDisplay : DisplayMonoBehaviour, ISequenced
    {
        #region VARIABLES

        /// <summary>
        /// If this task is complete
        /// </summary>
        public bool Complete { get; private set; }

        /// <summary>
        /// Sequence priority
        /// </summary>
        public int Priority => priority;

        /// <summary>
        /// Sequence priority
        /// </summary>
        [SerializeField]
        private int priority;

        /// <summary>
        /// The time to show this object
        /// </summary>
        [SerializeField, Tooltip("The time to show this object")]
        private float showTime = 0.5f;

        #endregion

        #region METHODS

        /// <summary>
        /// Before the sequence starts
        /// </summary>
        public virtual void BeforeTask()
        {
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Skip this task
        /// </summary>
        public virtual void SkipTask() { }

        /// <summary>
        /// Start this task
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerator StartTask()
        {
            Complete = false;
            Show();

            yield return new WaitForSeconds(showTime);
            Complete = true;
        }

        #endregion
    }
}