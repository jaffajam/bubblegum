﻿using UnityEngine;

namespace Bubblegum
{

    /// <summary>
    /// An object that stores information about the game
    /// </summary>
    public abstract class GameController<T> : ScriptableObject
    {
        #region VARIABLES

        /// <summary>
        /// Get or set the game controller
        /// </summary>
        public static T Current { get; protected set; }

        /// <summary>
        /// If the game is playing
        /// </summary>
        public bool Playing { get; protected set; }

        /// <summary>
        /// Get the player gameobject
        /// </summary>
        public GameObject Player
        {
            get
            {
                if (!playerGameObject)
                    playerGameObject = GameObject.FindWithTag(CommonTags.PLAYER_TAG);

                return playerGameObject;
            }

            set
            {
                playerGameObject = value;
                player = null;
            }
        }

        /// <summary>
        /// The player object
        /// </summary>
        private Component player;

        /// <summary>
        /// The player game object
        /// </summary>
        private GameObject playerGameObject;

        #endregion

        #region METHODS

        /// <summary>
        /// Load this game
        /// </summary>
        public abstract void OpenGame();

        /// <summary>
        /// Start the game
        /// </summary>
        public abstract void StartGame();

        /// <summary>
        /// End the game
        /// </summary>
        public abstract void EndGame();

        /// <summary>
        /// Exit the game
        /// </summary>
        public abstract void ExitGame();

        /// <summary>
        /// Reset the game
        /// </summary>
        public abstract void ResetGame();

        /// <summary>
        /// Send a message to the player object
        /// </summary>
        public void SendMessageToPlayer(string message)
        {
            Player?.SendMessage(message);
        }

        /// <summary>
        /// Find the player object
        /// </summary>
        public TYPE GetPlayer<TYPE>() where TYPE : Component
        {
            if (!player || player.GetType() != typeof(TYPE))
            {
                playerGameObject = GameObject.FindWithTag(CommonTags.PLAYER_TAG);
                player = playerGameObject?.GetComponent<TYPE>();
            }

            return player as TYPE;
        }

        #endregion
    }
}