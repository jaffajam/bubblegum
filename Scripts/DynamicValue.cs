﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;
using Bubblegum.Serialization;
using Object = UnityEngine.Object;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Bubblegum
{

    /// <summary>
    /// Selected types attribute
    /// </summary>
    public class SelectedTypesAttribute : Attribute
    {
        #region PUBLIC_VARIABLES

        /// <summary>
        /// Get all of the supported types
        /// </summary>
        public Type[] SupportedTypes
        {
            get
            {
                if (supportedTypes.IsNullOrEmpty())
                    return DynamicValue.SupportedTypes.ToArray();

                return supportedTypes;
            }
        }

        #endregion

        #region PRIVATE_VARIABLES

        /// <summary>
        /// All of the supported types
        /// </summary>
        private Type[] supportedTypes;

        #endregion

        #region CONSTRUCTORS

        /// <summary>
        /// Selected types that we can use
        /// </summary>
        /// <param name="types"></param>
        public SelectedTypesAttribute(params Type[] supportedTypes)
        {
            this.supportedTypes = supportedTypes;
        }

        #endregion

    }

    /// <summary>
    /// Wrapper class to associate a string with a dynamic value
    /// </summary>
    [Serializable]
    public class DynamicValueData
    {
        #region VARIABLES

        /// <summary>
        /// Key to order the value with
        /// </summary>
        public string key;

        /// <summary>
        /// The value to hold
        /// </summary>
        public DynamicValue dynamicValue;

        #endregion

        #region METHODS

        /// <summary>
        /// Create an empty object
        /// </summary>
        public DynamicValueData() { }

        /// <summary>
        /// Create a new dynamic value object
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public DynamicValueData(string key, object value)
        {
            this.key = key;
            dynamicValue = new DynamicValue(value);
        }

        #endregion
    }

    /// <summary>
    /// A wrapper class used to hold a varitey of value types
    /// </summary>
    [Serializable]
    public class DynamicValue
    {

        #region PUBLIC_VARIABLES

        /// <summary>
        /// Type of the value to draw
        /// </summary>
        public Type ValueSetType
        {
            get
            {
                if (!string.IsNullOrEmpty(storedType))
                    return Type.GetType(storedType);
                else
                    return typeof(bool);
            }
        }

        /// <summary>
        /// Get or set the value object
        /// </summary>
        public object Value
        {
            get
            {
                if (value == null || value.GetType() != ValueSetType)                    
                    if (ValueSetType.IsSubclassOf(typeof(Object)) || ValueSetType == typeof(Object))
                        value = storedObjectValue;
                    else if (string.IsNullOrEmpty(storedValue))
                        value = GetDefault(ValueSetType);
                    else
                        value = Serialize.DeserializeObject(storedValue, ValueSetType);

                if (value is KeyField)
                {
                    value = (KeyField)value;
                    return (Key)(KeyField)value;
                }

                return value;
            }

            set
            {
                this.value = value;

                //Object
                if (value is Object || value == null)
                {
                    storedObjectValue = (Object)value;

                    if (value != null)
                        storedType = value.GetType().AssemblyQualifiedName;
                    else
                        storedType = typeof(Object).AssemblyQualifiedName;
                }
                //Other
                else
                {
                    storedType = value.GetType().AssemblyQualifiedName;
                    storedValue = Serialize.SerializeObject(value);
                }
            }
        }

        #endregion // PUBLIC_VARIABLES

        #region PRIVATE_VARIABLES

        /// <summary>
        /// Get all of the supported types to display
        /// </summary>
        public static List<Type> SupportedTypes
        {
            get
            {
                return new List<Type> {
                    typeof (bool),
                    typeof (int),
                    typeof (float),
                    typeof (string),
                    typeof (Vector2),
                    typeof (Vector3),
                    typeof (Vector4),
                    typeof (Quaternion),
                    typeof (Color),
                    typeof (AnimationCurve),
                    typeof (Sprite),
                    typeof (Transform),
                    typeof (GameObject),
                    typeof (Object),
                    typeof (KeyField)
                };
            }
        }

        /// <summary>
        /// The stored type of the object
        /// </summary>
        [SerializeField]
        private string storedType;

        /// <summary>
        /// The value stored for serialization
        /// </summary>
        [SerializeField]
        private string storedValue;

        /// <summary>
        /// The stored object reference, since it cant be serialized
        /// </summary>
        [SerializeField]
        private Object storedObjectValue;

        /// <summary>
        /// The value we are representing
        /// </summary>
        private object value;

        #endregion // PRIVATE_VARIABLES

        #region CONSTRUCTORS

        /// <summary>
        /// Create a new dynamic drawer
        /// </summary>
        /// <param name="bounds"></param>
        public DynamicValue(object value)
        {
            if (IsTypeSupported(value.GetType()))
            {
                storedType = value.GetType().AssemblyQualifiedName;
                Value = value;
            }
            else
                throw new Exception("Object of type " + value.GetType() + " is not supported by DynamicValue");
        }

        #endregion

        #region PUBLIC_METHODS

        /// <summary>
        /// Get the default type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static object GetDefault(Type type)
        {
            if (type == typeof(string))
                return "";
            else
                try
                {
                    return Activator.CreateInstance(type);
                }
                catch (MissingMethodException ex)
                {
                    Debug.LogError(ex.ToString());
                    return false;
                }
        }

        /// <summary>
        /// Check if the given type is supported for drawing
        /// </summary>
        /// <param name="type"></param>
        public static bool IsTypeSupported(Type type)
        {
            for (int i = 0; i < SupportedTypes.Count; i++)
                if (type.IsSubclassOf(SupportedTypes[i]) || SupportedTypes[i] == type)
                    return true;

            return false;
        }

        /// <summary>
        /// Compare the two objects
        /// </summary>
        /// <param name="value"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public bool Compare(object check, ComparisonMode mode)
        {
            if (check == null)
                return Value == null;

            if (!IsTypeSupported(check.GetType()))
                throw new Exception("Object of type " + check.GetType() + " is not a number type or is not supported");

            return Value.Compare(check, mode);
        }

        #endregion
    }

#if UNITY_EDITOR

    /// <summary>
    /// Custom drawer for the dynamic value class
    /// </summary>
    [CustomPropertyDrawer(typeof(DynamicValue), true)]
    public class DynamicValueDrawer : PropertyDrawer
    {

        #region PRIVATE_VARIABLES

        /// <summary>
        /// Layout values
        /// </summary>
        private const float
            PROPERTY_HEIGHT = 16f,
            LABEL_WIDTH = 100f,
            POPUP_WIDTH = 100f,
            VALUE_WIDTH = 70f,
            SPACING = 10f;

        /// <summary>
        /// Get the label size
        /// </summary>
        private Vector2 LabelSize
        {
            get
            {
                float width = Mathf.Min((currentPosition.width - EditorGUIUtility.labelWidth) / 3f, LABEL_WIDTH);
                return new Vector2(width, EditorGUIUtility.singleLineHeight);
            }
        }

        /// <summary>
        /// Get the value size
        /// </summary>
        private Vector2 ValueSize
        {
            get
            {
                float width = Mathf.Max(VALUE_WIDTH, currentPosition.width - EditorGUIUtility.labelWidth - LabelSize.x - SPACING);
                return new Vector2(width, EditorGUIUtility.singleLineHeight);
            }
        }

        /// <summary>
        /// Get the popup offset
        /// </summary>
        private Vector2 PopupOffset
        {
            get
            {
                return new Vector2(EditorGUIUtility.labelWidth, (currentPosition.height - EditorGUIUtility.singleLineHeight)/2);
            }
        }

        /// <summary>
        /// Get the value offset
        /// </summary>
        private Vector2 ValueOffset
        {
            get
            {
                return new Vector2(PopupOffset.x + LabelSize.x + SPACING, PopupOffset.y);
            }
        }

        private Rect currentPosition;

        #endregion

        #region DRAWER_METHODS

        /// <summary>
        /// Draw the inspector
        /// </summary>
        /// <param name="position"></param>
        /// <param name="property"></param>
        /// <param name="label"></param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            currentPosition = position;

            //label.text = "Value";
            label = EditorGUI.BeginProperty(position, label, property);

            SerializedProperty typeProp = property.FindPropertyRelative("storedType");
            SerializedProperty valueProp = property.FindPropertyRelative("storedValue");
            SerializedProperty objectValueProp = property.FindPropertyRelative("storedObjectValue");
            object value;
            Type type;

            //Default type
            if (string.IsNullOrEmpty(typeProp.stringValue))
                typeProp.stringValue = typeof(bool).AssemblyQualifiedName;

            type = Type.GetType(typeProp.stringValue);

            //Get value
            if (type == typeof(Object) || type.IsSubclassOf(typeof(Object)))
                value = objectValueProp.objectReferenceValue;
            //Default
            else if (string.IsNullOrEmpty(valueProp.stringValue))
                value = DynamicValue.GetDefault(type);
            //Other value
            else
                value = Serialize.DeserializeObject(valueProp.stringValue, type);

            //Label
            if (label.text.Length > 10)
                label.text = label.text.Substring(0, 10) + "...";

            EditorGUI.LabelField(position, label.text);

            //Get available types
            SelectedTypesAttribute supportedTypesAttribute = fieldInfo.GetAttribute<SelectedTypesAttribute>();

            if (supportedTypesAttribute == null)
                supportedTypesAttribute = new SelectedTypesAttribute();

            //Get selected type
            int selectedIndex = Array.IndexOf(supportedTypesAttribute.SupportedTypes, type);

            // Remove indent when drawing GUI elements
            var indentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            //Draw type popup
            EditorGUI.BeginChangeCheck();
            Type selectedType = null;
            Rect typePosition = new Rect(position.position + PopupOffset, LabelSize);

            selectedIndex = EditorGUI.Popup(typePosition, selectedIndex, supportedTypesAttribute.SupportedTypes.Select(item => item.Name).ToArray());
            selectedIndex = selectedIndex >= supportedTypesAttribute.SupportedTypes.Length || selectedIndex == -1 ? 0 : selectedIndex;
            selectedType = supportedTypesAttribute.SupportedTypes[selectedIndex];

            //Type changed
            if (EditorGUI.EndChangeCheck())
            {
                typeProp.stringValue = selectedType.AssemblyQualifiedName;
                valueProp.stringValue = null;
                objectValueProp.objectReferenceValue = null;
            }

            //Value
            EditorGUI.BeginChangeCheck();
            value = DrawValueField(new Rect(position.position + ValueOffset, ValueSize), selectedType, value);

            if (EditorGUI.EndChangeCheck())
            {
                if (value is Object)
                    objectValueProp.objectReferenceValue = (Object)value;
                else
                    valueProp.stringValue = Serialize.SerializeObject(value);
            }

            EditorGUI.indentLevel = indentLevel;

            EditorGUI.EndProperty();
        }

        #endregion

        #region PRIVATE_METHODS

        /// <summary>
        /// Draw the value field for the instruction
        /// </summary>
        private object DrawValueField(Rect position, Type valueType, object value)
        {
            object newValue = value;

            //Create default value if none
            if ((value == null) || value.GetType() != valueType && !value.GetType().IsSubclassOf(valueType))
            {
                if (valueType == typeof(string))
                    newValue = string.Empty;
                else
                    newValue = valueType.GetDefault();
            }

            //Bool
            if (valueType == typeof(bool))
                newValue = EditorGUI.Toggle(position, (bool)newValue);

            //Int
            else if (valueType == typeof(int))
                newValue = EditorGUI.IntField(position, (int)newValue);

            //Float
            else if (valueType == typeof(float))
                newValue = EditorGUI.FloatField(position, (float)newValue);

            //String
            else if (valueType == typeof(string))
                newValue = EditorGUI.TextField(position, (string)newValue);

            //Vector2
            else if (valueType == typeof(Vector2))
                newValue = EditorGUI.Vector2Field(position, "", (Vector2)newValue);

            //Vector3
            else if (valueType == typeof(Vector3))
                newValue = EditorGUI.Vector3Field(position, "", (Vector3)newValue);

            //Vector4
            else if (valueType == typeof(Vector4))
                newValue = EditorGUI.Vector4Field(position, "", (Vector4)newValue);

            //Color
            else if (valueType == typeof(Color))
                newValue = EditorGUI.ColorField(position, (Color)newValue);

            //Quaternion
            else if (valueType == typeof(Quaternion))
            {
                Quaternion quaternion = (Quaternion)newValue;
                Vector4 vector = new Vector4(quaternion.x, quaternion.y, quaternion.z, quaternion.w);
                vector = EditorGUI.Vector4Field(position, "", vector);
                newValue = new Quaternion(vector.x, vector.y, vector.z, vector.w);
            }

            //Curve
            else if (valueType == typeof(AnimationCurve))
                newValue = EditorGUI.CurveField(position, (AnimationCurve)newValue);

            //Sprite
            else if (valueType == typeof(Sprite))
                newValue = EditorGUI.ObjectField(position, (Sprite)newValue, typeof(Sprite), true);

            //Transform
            else if (valueType == typeof(Transform))
                newValue = EditorGUI.ObjectField(position, (Transform)newValue, typeof(Transform), true);

            //GameObject
            else if (valueType == typeof(GameObject))
                newValue = EditorGUI.ObjectField(position, (GameObject)newValue, typeof(GameObject), true);

            //Object
            else if (valueType == typeof(Object) || valueType.IsSubclassOf(typeof(Object)))
                newValue = EditorGUI.ObjectField(position, (Object)newValue, typeof(Object), true);

            //Key Field
            else if (valueType == typeof(KeyField))
                newValue = new KeyField((Key)EditorGUI.ObjectField(position, (Key)(KeyField)newValue, typeof(Key), true));

            else
            {
                Debug.LogError(valueType + " is not supported by the DynamicValue class");
                return false;
            }

            return newValue;
        }

        #endregion
    }

#endif
}