﻿using UnityEngine;

namespace Bubblegum
{

	/// <summary>
	/// Object responsible for spawning other objects
	/// </summary>
	public interface ISpawner
	{
		#region METHODS

		/// <summary>
		/// Spawn this object
		/// </summary>
		/// <param name="obj"></param>
		void Spawn(GameObject obj);

		#endregion
	}
}