﻿using System.Linq;
using System.Collections.Generic;

namespace Bubblegum
{
    /// <summary>
	/// Loading manager object
	/// </summary>
	public static class TaskObserver
    {
        #region VARIABLES

        /// <summary>
        /// Generic loading text
        /// </summary>
        public const string LOADING_GENERIC = "Loading";

        /// <summary>
        /// When tasks are updated
        /// </summary>
        public static System.Action OnTasksUpdated;

        /// <summary>
        /// All of the tasks that we are trying to complete
        /// </summary>
        private static Dictionary<string, List<ITask>> tasks = new Dictionary<string, List<ITask>>();

        #endregion

        #region METHODS

        /// <summary>
        /// Register a lot of tasks
        /// </summary>
        /// <param name="loaders"></param>
        public static void RegisterTask(ITask task)
        {
            CheckKey(task.TaskKey);

            tasks[task.TaskKey].Add(task);
            OnTasksUpdated?.Invoke();
        }

        /// <summary>
        /// Deregister the given task
        /// </summary>
        /// <param name="task"></param>
        public static void DeregisterTask(ITask task)
        {
            CheckKey(task.TaskKey);

            if (tasks[task.TaskKey].Contains(task))
            {
                tasks[task.TaskKey].Remove(task);
                OnTasksUpdated?.Invoke();
            }
        }

        /// <summary>
        /// Get the current loading message
        /// </summary>
        public static string GetTaskMessage(string key)
        {
            CheckKey(key);

            ITask task = tasks[key].FirstOrDefault(item => item.CompletedTasks != item.TotalTasks);
            return task != null ? task.TaskMessage : string.Empty;
        }

        /// <summary>
        /// Get the total loaded percent for all tasks
        /// </summary>
        public static float GetLoadedPercent(string key)
        {
            CheckKey(key);

            if (!tasks.ContainsKey(key) || tasks[key].Count == 0)
                return 1f;

            return (float)tasks[key].Sum(task => task.CompletedTasks) / tasks[key].Sum(task => task.TotalTasks);
        }

        /// <summary>
        /// Get the total task count
        /// </summary>
        public static int GetTaskCount(string key)
        {
            CheckKey(key);
            return tasks[key].Count;
        }

        /// <summary>
        /// Check the key
        /// </summary>
        /// <param name="key"></param>
        static void CheckKey(string key)
        {
            if (!tasks.ContainsKey(key))
                tasks.Add(key, new List<ITask>());
        }

        #endregion
    }
}