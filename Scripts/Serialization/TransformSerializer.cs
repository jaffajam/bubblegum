﻿using UnityEngine;

namespace Bubblegum.Serialization
{

	/// <summary>
	/// Data class for the state of a transform
	/// </summary>
	[System.Serializable]
	public class TransformSerializer : ComponentSerializer<Transform>
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// The position and scale that will be serialized
		/// </summary>
		public Float3 positon, scale;

		/// <summary>
		/// The rotation to be serialized
		/// </summary>
		public Float4 rotation;

		/// <summary>
		/// The parent we belong to in relation to its root
		/// </summary>
		public string parent;

		#endregion // PUBLIC_VARIABLES

		#region CONSTRUCTORS

		/// <summary>
		/// Create new data
		/// </summary>
		public TransformSerializer(Transform transform, Transform root = null)
		{
			Update(transform, root);
		}

		/// <summary>
		/// Update data
		/// </summary>
		/// <param name="transform"></param>
		/// <param name="root"></param>
		public void Update(Transform transform, Transform root = null)
		{
			positon = new Float3(transform.localPosition);
			rotation = new Float4(transform.localRotation);
			scale = new Float3(transform.localScale);

			if (root == null)
				root = transform.root;

			//Special way to find root
			Transform secondRoot = transform.parent;

			while (secondRoot != null && secondRoot.parent != root)
				secondRoot = secondRoot.parent;

			if (secondRoot != null)
				parent = transform.parent.GetPath(secondRoot);
		}

		/// <summary>
		/// Apply the data to the transform
		/// </summary>
		/// <param name="transform"></param>
		public override void Apply(Transform transform)
		{
			transform.localPosition = positon.ToVector3();
			transform.localRotation = rotation.ToQuaternion();
			transform.localScale = scale.ToVector3();
		}

		/// <summary>
		/// Set the parent based on our saved parent which finds the parent based of the root
		/// </summary>
		/// <param name="transform"></param>
		/// <param name="root"></param>
		public void SetParent(Transform transform, Transform root)
		{
			if (root == null)
			{
				transform.SetParent(root);
				return;
			}

			//Force find the root since its possible to pass in a non-root
			root = root.root;
			Transform parentTransform = root.Find(parent);

			//Still cant find parent? Then set under root
			if (!parentTransform)
				parentTransform = root;

			transform.SetParent(parentTransform);
		}

		#endregion
	}
}