﻿using UnityEngine;

namespace Bubblegum.Serialization
{

	/// <summary>
	/// Data class for the state of a transform
	/// </summary>
	[System.Serializable]
	public abstract class ComponentSerializer<T> where T : Component
	{
		#region CONSTRUCTORS

		/// <summary>
		/// Apply the data to object
		/// </summary>
		public abstract void Apply(T obj);

		#endregion
	}
}