﻿using UnityEngine;

namespace Bubblegum.Serialization
{
	/// <summary>
	/// Stores two int parameters
	/// </summary>
	[System.Serializable]
	public struct Int3
	{
		#region Fields

		/// <summary>
		/// Gets or sets the x.
		/// </summary>
		public int x;

		/// <summary>
		/// Gets or sets the y.
		/// </summary>
		public int y;

		/// <summary>
		/// Gets or sets the z.
		/// </summary>
		public int z;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref=""/> class.
		/// </summary>
		/// <param name="x">The x coordinate.</param>
		/// <param name="y">The y coordinate.</param>
		/// <param name="z">The z<see cref="int"/></param>
		public Int3(int x, int y, int z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref=""/> class.
		/// </summary>
		/// <param name="vector3">Vector2.</param>
		public Int3(Vector3 vector3)
		{
			x = (int)vector3.x;
			y = (int)vector3.y;
			z = (int)vector3.z;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref=""/> class.
		/// </summary>
		/// <param name="vector3">Vector2.</param>
		public Int3(Vector3Int vector3)
		{
			x = vector3.x;
			y = vector3.y;
			z = vector3.z;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Check if the two objects are the same
		/// </summary>
		/// <param name="o"></param>
		/// <returns></returns>
		public override bool Equals(object o)
		{
			try
			{
				return (this == (Int3)o);
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// Get the hashcode for this object
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			unchecked
			{
				int hash = 17;
				hash = hash * 23 + x.GetHashCode();
				hash = hash * 23 + y.GetHashCode();
				hash = hash * 23 + z.GetHashCode();
				return hash;
			}
		}

		/// <summary>
		/// Return a new vector 3 from this object
		/// </summary>
		/// <returns></returns>
		public Vector3Int ToVector3Int()
		{
			return new Vector3Int(x, y, z);
		}

		#endregion




		/// <summary>
		/// Check if all of the values within the Int2 are the same
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static bool operator ==(Int3 a, Int3 b)
		{
			return a.x == b.x && a.y == b.y && a.z == b.z;
		}

		/// <summary>
		/// Check if any of the values within the Int2 are different
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static bool operator !=(Int3 a, Int3 b)
		{
			return a.x != b.x || a.y != b.y || a.z != b.z;
		}

		/// <summary>
		/// Multiply both values by the given multiplier
		/// </summary>
		/// <param name="a"></param>
		/// <param name="multiplier"></param>
		/// <returns></returns>
		public static Int3 operator *(Int3 int3, int multiplier)
		{
			int3.x = int3.x * multiplier;
			int3.y = int3.y * multiplier;
			int3.z = int3.z * multiplier;

			return int3;
		}
	}
}
