﻿using UnityEngine;

namespace Bubblegum.Serialization
{

	/// <summary>
	/// Stores two int parameters
	/// </summary>
	[System.Serializable]
	public struct Int2
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// Gets or sets the x.
		/// </summary>
		/// <value>The x.</value>
		public int x;

		/// <summary>
		/// Gets or sets the y.
		/// </summary>
		/// <value>The y.</value>
		public int y;

		#endregion // PUBLIC_VARIABLES

		#region CONSTRUCTORS

		/// <summary>
		/// Initializes a new instance of the class.
		/// </summary>
		/// <param name="x">The x coordinate.</param>
		/// <param name="y">The y coordinate.</param>
		public Int2(int x, int y)
		{
			this.x = x;
			this.y = y;
		}

		/// <summary>
		/// Initializes a new instance of the class.
		/// </summary>
		/// <param name="vector2">Vector2.</param>
		public Int2(Vector2Int vector2)
		{
			x = vector2.x;
			y = vector2.y;
		}

		/// <summary>
		/// Initializes a new instance of the class.
		/// </summary>
		/// <param name="vector2">Vector2.</param>
		public Int2(Vector2 vector2)
		{
			x = (int)vector2.x;
			y = (int)vector2.y;
		}

		#endregion // CONSTRUCTORS

		#region OPERATORS

		/// <summary>
		/// Check if all of the values within the Int2 are the same
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static bool operator ==(Int2 a, Int2 b)
		{
			return a.x == b.x && a.y == b.y;
		}

		/// <summary>
		/// Check if any of the values within the Int2 are different
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static bool operator !=(Int2 a, Int2 b)
		{
			return a.x != b.x || a.y != b.y;
		}

		/// <summary>
		/// Multiply both values by the given multiplier
		/// </summary>
		/// <param name="a"></param>
		/// <param name="multiplier"></param>
		/// <returns></returns>
		public static Int2 operator *(Int2 int2, int multiplier)
		{
			int2.x = int2.x * multiplier;
			int2.y = int2.y * multiplier;

			return int2;
		}

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Check if the two objects are the same
		/// </summary>
		/// <param name="o"></param>
		/// <returns></returns>
		public override bool Equals(object o)
		{
			try
			{
				return (this == (Int2)o);
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// Get the hashcode for this object
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			unchecked
			{
				int hash = 17;
				hash = hash * 23 + x.GetHashCode();
				hash = hash * 23 + y.GetHashCode();
				return hash;
			}
		}

		/// <summary>
		/// Return a new vector 3 from this object
		/// </summary>
		/// <returns></returns>
		public Vector2Int ToVector2Int()
		{
			return new Vector2Int(x, y);
		}

		#endregion
	}
}