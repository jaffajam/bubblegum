﻿using UnityEngine;

namespace Bubblegum.Serialization
{
	/// <summary>
	/// Data to serialize the state of a rigidbody
	/// </summary>
	[System.Serializable]
	public class RigidbodySerializer
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// Mass of rigidbody
		/// </summary>
		public float mass;

		/// <summary>
		/// If the rigidbody will use gravity
		/// </summary>
		public bool useGravity;

		/// <summary>
		/// If the rigidbody is kinematic
		/// </summary>
		public bool isKinematic;

		/// <summary>
		/// Angular velocity of the rigidbody
		/// </summary>
		public Float3 angularVelocity;

		/// <summary>
		/// Velocity of the rigidbody
		/// </summary>
		public Float3 velocity;

		#endregion // PUBLIC_VARIABLES

		#region CONSTRUCTORS

		/// <summary>
		/// Create new data
		/// </summary>
		public RigidbodySerializer(Rigidbody rigidbody)
		{
			mass = rigidbody.mass;
			useGravity = rigidbody.useGravity;
			isKinematic = rigidbody.isKinematic;
			angularVelocity = new Float3(rigidbody.angularVelocity);
			velocity = new Float3(rigidbody.velocity);
		}

		#endregion

		#region METHODS

		/// <summary>
		/// Apply the basic data to the rigidbody
		/// This allows skipping physics processes when performance is critical
		/// </summary>
		/// <param name="rigidbody"></param>
		public void ApplyLight(Rigidbody rigidbody)
		{
			rigidbody.mass = mass;
			rigidbody.useGravity = useGravity;
			rigidbody.isKinematic = isKinematic;
		}

		/// <summary>
		/// Apply the data to the rigidbody
		/// </summary>
		/// <param name="rigidbody"></param>
		public void Apply(Rigidbody rigidbody)
		{
			ApplyLight(rigidbody);
			rigidbody.velocity = velocity.ToVector3();
			rigidbody.angularVelocity = velocity.ToVector3();
		}

		#endregion
	}


	/// <summary>
	/// Data to serialize the state of a rigidbody
	/// </summary>
	[System.Serializable]
	public class Rigidbody2DSerializer
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// If we shoulds 
		/// </summary>
		public bool autoMass;

		/// <summary>
		/// Mass of rigidbody
		/// </summary>
		public float mass;

		/// <summary>
		/// If the rigidbody will use gravity
		/// </summary>
		public float gravityScale;

		/// <summary>
		/// If the rigidbody is kinematic
		/// </summary>
		public bool isKinematic;

		/// <summary>
		/// Angular velocity of the rigidbody
		/// </summary>
		public float angularVelocity;

		/// <summary>
		/// Velocity of the rigidbody
		/// </summary>
		public Float3 velocity;

		#endregion // PUBLIC_VARIABLES

		#region CONSTRUCTORS

		/// <summary>
		/// Create new data
		/// </summary>
		public Rigidbody2DSerializer(Rigidbody2D rigidbody)
		{
			autoMass = rigidbody.useAutoMass;
			mass = rigidbody.mass;
			gravityScale = rigidbody.gravityScale;
			isKinematic = rigidbody.isKinematic;
			angularVelocity = rigidbody.angularVelocity;
			velocity = new Float3(rigidbody.velocity);
		}

		/// <summary>
		/// Apply the data to the rigidbody
		/// </summary>
		/// <param name="rigidbody"></param>
		public void Apply(Rigidbody2D rigidbody)
		{
			rigidbody.useAutoMass = autoMass;
			rigidbody.gravityScale = gravityScale;
			rigidbody.isKinematic = isKinematic;
			rigidbody.velocity = velocity.ToVector3();
			rigidbody.angularVelocity = angularVelocity;

			if (!autoMass)
				rigidbody.mass = mass;
		}

		#endregion
	}
}