﻿using UnityEngine;

namespace Bubblegum
{

	/// <summary>
	/// A class that can be used to identify this objects type using an Entity type
	/// </summary>
	public class Identification : MonoBehaviour
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// Gets the key that defines this object
		/// </summary>
		/// <value>The key.</value>
		public Entity Key
		{
			get
			{
				return entity;
			}

			set
			{
                if (entity)
                    Deregister();

				entity = value;
                Register();
			}
		}

		/// <summary>
		/// The cached transform
		/// </summary>
		public Transform CachedTransform { get; private set; }

		/// <summary>
		/// Gets the object that this identity represents
		/// </summary>
		/// <value>The represented object.</value>
		public GameObject RepresentedObject { get { return gameObject; } }

		/// <summary>
		/// The entity that is used to define what this object is
		/// </summary>
		[Tooltip("The entity that is used to define what this object is")]
		[SerializeField]
		protected Entity entity;

		/// <summary>
		/// If we should rename ourself with the entity name
		/// </summary>
		[SerializeField, Tooltip("If we should rename ourself with the entity name")]
		protected bool useEntityName = true;

		#endregion // PUBLIC_VARIABLES

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// When this object is enabled
		/// </summary>
		protected virtual void OnEnable()
		{
			CachedTransform = transform;

			if (useEntityName && entity)
				gameObject.name = name + " " + entity.Name;

            if (entity)
			    Register();
		}

		/// <summary>
		/// When the object is disabled
		/// </summary>
		void OnDisable()
		{
			Deregister();
		}

		#endregion // MONOBEHAVIOUR_METHODS

		#region PUBLIC_METHODS

		/// <summary>
		/// Register this identifiaction this should be a call to the entity type for sorting purposes
		/// </summary>
		public void Register()
		{
			Entity.RegisterEntity(entity, this);
		}

		/// <summary>
		/// Deregister this identification
		/// </summary>
		public void Deregister()
		{
            Entity.DeregisterEntity(entity, this);
        }

        #endregion // PUBLIC_METHODS
    }
}