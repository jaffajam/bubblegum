using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Bubblegum.Collections;

namespace Bubblegum
{

	/// <summary>
	/// Spawns objects automatically
	/// </summary>
	public class SpawnerSimple : MonoBehaviour, IPrefabReferencer, IContextListener, ISpawner
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// The prefab that you want to set
		/// </summary>
		public GameObject Prefab { get { return prefabToSpawn; } set { prefabToSpawn = value; } }

		/// <summary>
		/// The event for when we spawn an object
		/// </summary>
		public Action<GameObject> onSpawn;

		/// <summary>
		/// If we should spawn on start
		/// </summary>
		public bool spawnOnStart;

		/// <summary>
		/// The delay before the first spawn
		/// </summary>
		[DisplayIf("spawnOnStart", true)]
		public float startDelay = 0f;

		/// <summary>
		/// If we should loop our spawning
		/// </summary>
		public bool loopSpawning;

		/// <summary>
		/// If we should apply our scale to spawned object
		/// </summary>
		public bool applyScale;

		/// <summary>
		/// The delay between each spawn
		/// </summary>
		[DisplayIf("loopSpawning", true)]
		public float spawnRate = 1f;

		/// <summary>
		/// The prefab to spawn.
		/// </summary>
		[Tooltip("The prefab to spawn.")]
		[SerializeField]
		protected GameObject prefabToSpawn;

		#endregion // PUBLIC_VARIABLES

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Start this behaviour
		/// </summary>
		void Start()
		{
			Prefab = prefabToSpawn;

			if (spawnOnStart)
				SpawnAfterSeconds(startDelay);
		}

		#endregion // MONOBEHAVIOUR_METHODS

		#region PUBLIC_METHODS

		/// <summary>
		/// Invoke the spawn method after a few seconds
		/// </summary>
		/// <param name="seconds">Seconds.</param>
		public void SpawnAfterSeconds(float seconds)
		{
			Invoke("Spawn", seconds);
		}

		/// <summary>
		/// Spawns the object
		/// </summary>
		public virtual void Spawn()
		{
			GameObject obj = Instantiate(prefabToSpawn, transform.position, transform.rotation);

			if (applyScale)
				obj.transform.localScale = transform.localScale;

			onSpawn?.Invoke(obj);

			if (loopSpawning)
				Invoke("Spawn", spawnRate);
		}

		/// <summary>
		/// Set the new spawn object then spawn
		/// </summary>
		/// <param name="component"></param>
		public void Spawn(GameObject obj)
		{
			Prefab = obj;
			Spawn();
		}

		/// <summary>
		/// Spawn the given amount of times
		/// </summary>
		/// <param name="amount"></param>
		public void Spawn(int amount)
		{
			for (int i = 0; i < amount; i++)
				Spawn();
		}

		/// <summary>
		/// Maybe spawn depending on random chance
		/// </summary>
		public void RandomSpawn(int spawnChance)
		{
			if (UnityEngine.Random.Range(0, spawnChance) == 0)
				Spawn();
		}

		/// <summary>
		/// Maybe spawn depending on random chance
		/// </summary>
		public void RandomSpawn(int spawnChance, System.Random random)
		{
			if (random.Next(0, spawnChance) == 0)
				Spawn();
		}

		/// <summary>
		/// When the context is changed we update the object to spawn
		/// </summary>
		/// <param name="obj"></param>
		public void ContextChanged(object obj)
		{
			if (obj is GameObject)
				prefabToSpawn = (GameObject)obj;
		}

		#endregion // PUBLIC_METHODS
	}
}