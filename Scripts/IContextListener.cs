﻿using UnityEngine;

namespace Bubblegum
{

	/// <summary>
	/// An object that is interested in the application context
	/// </summary>
	public interface IContextListener
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// When the context has 
		/// </summary>
		/// <param name="obj"></param>
		void ContextChanged(object obj);

		#endregion // PUBLIC_VARIABLES
	}
}