﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Bubblegum
{
    /// <summary>
    /// An object that can specify which player preference to be saved
    /// </summary>
    [CreateAssetMenu(menuName = "Scriptable Object/Preference Save Data")]
    public class PreferenceSaveData : Key, ISaveData
    {
        /// <summary>
        /// Value type
        /// </summary>
        public enum ValueType
        {
            String,
            Int,
            Float
        }

        /// <summary>
        /// PlayerPrefs key
        /// </summary>
        public string prefsKey;

        /// <summary>
        /// Value type stored in the PlayerPrefs
        /// </summary>
        public ValueType valueType;

        /// <summary>
        /// Default int value
        /// </summary>
        [DisplayIf("valueType", ValueType.Int)]
        public int defaultIntValue;

        /// <summary>
        /// Default float value
        /// </summary>
        [DisplayIf("valueType", ValueType.Float)]
        public float defaultFloatValue;

        /// <summary>
        /// Default string value
        /// </summary>
        [DisplayIf("valueType", ValueType.String)]
        public string defaultStringValue;

        /// <summary>
        /// Set data from dictionary to the object
        /// </summary>
        public void SetSaveData(Dictionary<string, SaveDataKeyValuePair> data)
        {
            switch (valueType)
            {
                case ValueType.Int:
                    PersistentData.SetInt(prefsKey, data.ContainsKey(prefsKey) ? Convert.ToInt32(data[prefsKey].value) : defaultIntValue);
                    break;
                
                case ValueType.Float:
                    PersistentData.SetFloat(prefsKey, data.ContainsKey(prefsKey) ? Convert.ToSingle(data[prefsKey].value) : defaultFloatValue);
                    break;

                case ValueType.String:
                    PersistentData.SetString(prefsKey, data.ContainsKey(prefsKey) ? (string)(data[prefsKey].value) : defaultStringValue);
                    break;
            }
        }

        /// <summary>
        /// Get the save data from this object and save it to the dictionary
        /// </summary>
        public void GetSaveData(Dictionary<string, SaveDataKeyValuePair> data)
        {
            object value;
            
            switch (valueType)
            {
                case ValueType.Int:
                    value = PersistentData.GetInt(prefsKey, defaultIntValue);
                    break;
                
                case ValueType.Float:
                    value = PersistentData.GetFloat(prefsKey, defaultFloatValue);
                    break;

                case ValueType.String:
                default:
                    value = PersistentData.GetString(prefsKey, defaultStringValue);
                    break;
            }

            data[prefsKey] = new SaveDataKeyValuePair(Name, value);
        }
    }
}