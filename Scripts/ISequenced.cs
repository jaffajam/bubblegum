﻿using System.Collections;

namespace Bubblegum
{
    /// <summary>
    /// An object that has its execution sequenced
    /// </summary>
    public interface ISequenced
    {
        #region METHODS

        /// <summary>
        /// The priority of this display
        /// </summary>
        int Priority { get; }

        /// <summary>
        /// If this task is complete
        /// </summary>
        bool Complete { get; }

        /// <summary>
        /// Invoked before the entire sequence is started
        /// </summary>
        void BeforeTask();

        /// <summary>
        /// Skip this task
        /// </summary>
        void SkipTask();

        /// <summary>
        /// Start this task
        /// </summary>
        /// <returns></returns>
        IEnumerator StartTask();

        #endregion
    }
}