﻿using UnityEngine;

namespace Bubblegum.Particles
{

	[RequireComponent(typeof(ParticleSystem))]

	/// <summary>
	/// Custom particle emission system
	/// </summary>
	public class ParticlePool : GlobalParticleSystem
	{
		#region VARIABLES

		/// <summary>
		/// Get the particle count
		/// </summary>
		public int Count { get; private set; }

		/// <summary>
		/// Get all of the particles
		/// </summary>
		public ParticleSystem.Particle[] Particles { get; private set; }

		/// <summary>
		/// The current particle index
		/// </summary>
		protected int particleIndex;

		#endregion

		/// <summary>
		/// Start this object
		/// </summary>
		protected override void Awake()
		{
			base.Awake();

			Particles = new ParticleSystem.Particle[System.main.maxParticles];
			ParticleSystem.MainModule main = System.main;
			main.playOnAwake = false;
		}

		/// <summary>
		/// Set particle data for an item
		/// </summary>
		/// <param name="position"></param>
		/// <param name="normal"></param>
		/// <param name="color"></param>
		public int Emit(Vector3 position, Vector3 normal, bool setParticles = true)
		{
			//Loop
			if (particleIndex >= System.main.maxParticles)
				particleIndex = 0;

			//Create particle
			Vector3 particleRotationEuler = Quaternion.LookRotation(normal).eulerAngles;
			particleRotationEuler.z = Random.Range(0, 360);

			//Update particle
			Particles[particleIndex].position = position;
			Particles[particleIndex].rotation3D = particleRotationEuler;
			Particles[particleIndex].startSize = Random.Range(System.main.startSize.constantMin, System.main.startSize.constantMax);
			Particles[particleIndex].startColor = System.main.startColor.color;

			//Set system
			Count = Mathf.Clamp(Count + 1, 0, System.main.maxParticles);

			if (setParticles)
				System.SetParticles(Particles, Count);

			return particleIndex++;
		}

		/// <summary>
		/// Set particle data for an item
		/// </summary>
		/// <param name="position"></param>
		/// <param name="normal"></param>
		/// <param name="color"></param>
		public int Emit(Vector3 position, Vector3 normal, float size, Color color, bool setParticles = true)
		{
			int index = Emit(position, normal, false);
			Particles[index].startSize = size;
			Particles[index].startColor = color;

			if (setParticles)
				System.SetParticles(Particles, Count);

			return index;
		}

		/// <summary>
		/// Update the particle system
		/// </summary>
		public void UpdateSystem()
		{
			System.SetParticles(Particles, Count);
		}
	}
}