﻿using UnityEngine;

namespace Bubblegum.Particles
{

	[RequireComponent(typeof(ParticleSystem))]

	/// <summary>
	/// Extention for the particle system that stores extra info and allows access to extra features
	/// </summary>
	public class ParticleSystemExtension : FastBehaviour
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// The particle system
		/// </summary>
		public ParticleSystem System { get; private set; }

		/// <summary>
		/// If the particle system should destroy itself when empty
		/// </summary>
		[SerializeField, Tooltip("If the particle system should destroy itself when empty")]
		private bool destroyWhenEmpty = true;

		/// <summary>
		/// Delay for destroying the system
		/// </summary>
		[SerializeField, Tooltip("Delay for destroying the system"), DisplayIf("destroyWhenEmpty", true)]
		private float destroyDelay = 2f;

		#endregion // PUBLIC_VARIABLES

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Awake this component
		/// </summary>
		protected virtual void Awake()
		{
			System = GetComponent<ParticleSystem>();
		}

		/// <summary>
		/// Update this object
		/// </summary>
		protected override void NewUpdate()
		{
			UpdateParticles();
		}

		/// <summary>
		/// Update this component
		/// </summary>
		protected override void IntervalUpdate()
		{
			CheckStatus();
		}

		#endregion // MONOBEHAVIOUR_METHODS

		#region PUBLIC_METHODS

		/// <summary>
		/// Fade into the desired emission rate
		/// </summary>
		/// <param name="rate"></param>
		public void LerpEmissionRate(float rate)
		{
#if UNITY_5_5_OR_NEWER
			System.SetEmissionRate(Mathf.Lerp(System.emission.rateOverTime.constantMax, rate, Time.deltaTime));
#else
			System.SetEmissionRate (Mathf.Lerp (System.emission.rate.constantMax, rate, Time.deltaTime));
#endif
		}

		#endregion // PUBLIC_METHODS

		#region PRIVATE_METHODS

		/// <summary>
		/// Updates the particles with the latest info that might have changed
		/// </summary>
		private void UpdateParticles()
		{

		}

		/// <summary>
		/// Checks the status.
		/// </summary>
		private void CheckStatus()
		{
			if (destroyWhenEmpty && System.particleCount == 0)
			{
				Destroy(this);
				Destroy(gameObject, destroyDelay);
			}
		}

		#endregion // PRIVATE_METHODS
	}
}