﻿using UnityEngine;

namespace Bubblegum.Particles
{

	/// <summary>
	/// Accesses global particle systems to trigger effects
	/// </summary>
	public class GlobalParticleWorker : CacheBehaviour
	{
		#region PUBLIC_METHODS

		/// <summary>
		/// Get the particle system we are representing
		/// </summary>
		public ParticleSystem System
		{
			get
			{
				if (!system)
					system = GlobalParticleSystem.GetSystem(key);

				return system;
			}
		}

		/// <summary>
		/// The key used to access this particles system
		/// </summary>
		[SerializeField, Tooltip("The key used to access this particles system")]
		private Key key;

		/// <summary>
		/// If we should apply our scale to the system
		/// </summary>
		[SerializeField, Tooltip("If we should apply our scale to the system")]
		private bool applyScale;

		/// <summary>
		/// If we should apply our rotation to the system
		/// </summary>
		[SerializeField, Tooltip("If we should apply our rotation to the system")]
		private bool applyRotation;

		/// <summary>
		/// The mode to emit with
		/// </summary>
		[SerializeField, Tooltip("The mode to emit with")]
		private EmissionMode mode = EmissionMode.SIMPLE;

		/// <summary>
		/// The minimum amount of particles to emit
		/// </summary>
		[SerializeField, Tooltip("The minimum amount of particles to emit")]
		protected int minEmission = 5;

		/// <summary>
		/// The maximum amount of particles to emit
		/// </summary>
		[SerializeField, Tooltip("The maximum amount of particles to emit")]
		protected int maxEmission = 10;

		/// <summary>
		/// If we should apply a custom velocity to the particles
		/// </summary>
		[SerializeField, Tooltip("If we should apply a custom velocity to the particles")]
		private bool applyVelocity;

		/// <summary>
		/// The velocity to set for the particles
		/// </summary>
		[SerializeField, Tooltip("The velocity to set for the particles"), DisplayIf("applyVelocity", true)]
		private Vector3 velocity;

		#endregion

		#region PRIVATE_VARIABLES

		/// <summary>
		/// The particle system emission control
		/// </summary>
		protected ParticleSystem.EmitParams emission = new ParticleSystem.EmitParams();

		/// <summary>
		/// The position altered to the correct space to spawn from
		/// </summary>
		protected Vector3 emissionPosition;

		/// <summary>
		/// The shape module from the system
		/// </summary>
		private ParticleSystem.ShapeModule shape;

		/// <summary>
		/// The cached system ref
		/// </summary>
		private ParticleSystem system;

		/// <summary>
		/// The type of shape for the particle system
		/// </summary>
		private ParticleSystemShapeType shapeType;

		#endregion

		#region ENUMS

		/// <summary>
		/// Emission mode for the system
		/// </summary>
		public enum EmissionMode { SIMPLE, ADVANCED }

		#endregion

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Start this component
		/// </summary>
		protected virtual void Start()
		{
			if (!System)
			{
				Debug.LogError("Could not find global particle system with key " + key.name);
				return;
			}

			//Shape
			shape = System.shape;
			shapeType = shape.shapeType;

			//Emission
			if (applyVelocity)
				emission.velocity = velocity;

			emission.applyShapeToPosition = true;
			emissionPosition = System.transform.InverseTransformPoint(CachedTransform.position);
		}

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Emit the given amount of particles from the system
		/// </summary>
		/// <param name="amount"></param>
		public virtual void Emit()
		{
			if (applyRotation)
				System.transform.rotation = CachedTransform.rotation;

			if (mode == EmissionMode.ADVANCED)
			{
				if (applyScale)
					AlterShape();

				emission.position = emissionPosition;
				System.Emit(emission, Random.Range(minEmission, maxEmission));
			}
			else
			{
				if (applyScale)
					System.transform.localScale = CachedTransform.localScale;

				System.transform.position = CachedTransform.position;
				System.Emit(Random.Range(minEmission, maxEmission));
			}
		}

		#endregion

		#region PRIVATE_METHODS

		/// <summary>
		/// Alter the shape using our multiplier
		/// </summary>
		/// <param name="scaleUp"></param>
		private void AlterShape()
		{
			if (!applyScale)
				return;

			switch (shapeType)
			{
				case ParticleSystemShapeType.Sphere:
				case ParticleSystemShapeType.Circle:
					shape.radius = CachedTransform.localScale.magnitude;
					break;

				case ParticleSystemShapeType.Box:
				case ParticleSystemShapeType.BoxEdge:
				case ParticleSystemShapeType.BoxShell:
					shape.scale = CachedTransform.localScale;
					break;

				default:
					Debug.LogWarning("Cant scale particle system " + System.name + " as we do not support shapes of type " + shape.shapeType);
					break;
			}
		}

		#endregion
	}
}