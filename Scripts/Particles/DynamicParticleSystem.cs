﻿using UnityEngine;

namespace Bubblegum.Particles
{

	[RequireComponent(typeof(ParticleSystem))]

	/// <summary>
	/// Particle system that includes extra functionality
	/// </summary>
	public class DynamicParticleSystem : MonoBehaviour
	{

		#region PRIVATE_VARIABLES

		/// <summary>
		/// The current particle system we are using
		/// </summary>
		protected ParticleSystem system;

		/// <summary>
		/// The amount of particles alive
		/// </summary>
		protected int particleCount;

		/// <summary>
		/// All of the particles in the system
		/// </summary>
		protected ParticleSystem.Particle[] particles;

		/// <summary>
		/// All of our dynamic particle references
		/// </summary>
		protected DynamicParticle[] dynamicParticles;

		/// <summary>
		/// Our emission parameters
		/// </summary>
		protected ParticleSystem.EmitParams emission = new ParticleSystem.EmitParams();

		#endregion

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Awaken this component
		/// </summary>
		protected virtual void Awake()
		{
			system = GetComponent<ParticleSystem>();
			particles = new ParticleSystem.Particle[system.main.maxParticles];
			dynamicParticles = new DynamicParticle[system.main.maxParticles];
			emission.applyShapeToPosition = true;

			for (int i = 0; i < dynamicParticles.Length; i++)
				dynamicParticles[i] = new DynamicParticle();
		}

		/// <summary>
		/// Update this object
		/// </summary>
		protected virtual void Update()
		{
			if (system.particleCount != particleCount)
				Debug.LogWarning("Particles were added or removed from " + name + " without using the DynamicParticles component");

			particleCount = system.GetParticles(particles);

			//Kill particles
			for (int i = 0; i < particleCount; i++)
				if (particles[i].remainingLifetime < 1f || dynamicParticles[i].index == -1)
				{
					dynamicParticles[i].index = -1;
					particles[i].remainingLifetime = 0f;
					particles.Swap(i, particleCount - 1);
					dynamicParticles.Swap(i, particleCount - 1);
					dynamicParticles[i].index = i;
					i--;
					particleCount--;
				}

			//Update system
			if (system.particleCount != particleCount)
				system.SetParticles(particles, particleCount);
		}

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Emit the amount of particles given
		/// </summary>
		/// <param name="count"></param>
		public virtual void Emit(int count)
		{
			int index = system.particleCount;

			while (count > 0 && index < dynamicParticles.Length)
			{
				dynamicParticles[index].Initialize(index);
				index++;
			}

			particleCount = system.particleCount + count;
			system.Emit(emission, count);
		}

		/// <summary>
		/// Remove the given particle
		/// </summary>
		public void RemoveParticle(DynamicParticle particle)
		{
			if (particle.index != -1)
				particle.index = -1;
		}

		/// <summary>
		/// Get a particle reference in the given range
		/// </summary>
		/// <param name="position"></param>
		/// <param name="range"></param>
		/// <returns></returns>
		public DynamicParticle GetParticleInArea(Vector3 position, float range)
		{
			range = range * range;

			for (int i = 0; i < particleCount; i++)
				if ((particles[i].position - position).sqrMagnitude < range)
					return dynamicParticles[i];

			return null;
		}

		#endregion

		#region SUB_CLASSES

		/// <summary>
		/// A particle within our system
		/// </summary>
		public class DynamicParticle
		{
			#region VARIABLES

			/// <summary>
			/// The index of the particle in the system
			/// </summary>
			public int index = -1;

			#endregion

			#region PUBLIC_METHODS

			/// <summary>
			/// Create a new particle reference
			/// </summary>
			/// <param name="lifetime"></param>
			public void Initialize(int index)
			{
				this.index = index;
			}

			#endregion
		}

		#endregion
	}
}