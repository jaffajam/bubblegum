﻿using System.Collections.Generic;
using UnityEngine;

namespace Bubblegum.Particles
{

	[RequireComponent(typeof(ParticleSystem))]

	/// <summary>
	/// A particle system that exists globally so is used by multiple objects
	/// </summary>
	public class GlobalParticleSystem : MonoBehaviour
	{
		#region PUBLIC_METHODS

		/// <summary>
		/// The particle system we are using
		/// </summary>
		public ParticleSystem System { get; protected set; }

		/// <summary>
		/// The key used to access this particles system
		/// </summary>
		[SerializeField, Tooltip("The key used to access this particles system")]
		private Key key;

		#endregion

		#region PRIVATE_VARIABLES

		/// <summary>
		/// All of the systems sorted by their keys
		/// </summary>
		private static Dictionary<Key, ParticleSystem> systems = new Dictionary<Key, ParticleSystem>();

		#endregion

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Awaken this component
		/// </summary>
		protected virtual void Awake()
		{
			System = GetComponent<ParticleSystem>();
			ParticleSystem.EmissionModule emission = System.emission;
			emission.rateOverTimeMultiplier = 0;

			if (!systems.ContainsKey(key))
				systems.Add(key, System);
			else
				Debug.LogError(GetType().Name + " already registered a system with key " + key.name);
		}

		/// <summary>
		/// Destroy this component
		/// </summary>
		void OnDestroy()
		{
			systems.Remove(key);
		}

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Get the particle system with the given key
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public static ParticleSystem GetSystem(Key key)
		{
			if (systems.ContainsKey(key))
				return systems[key];
			else
				Debug.LogError("Can't find particle system with key " + key.name);

			return null;
		}

		#endregion
	}
}