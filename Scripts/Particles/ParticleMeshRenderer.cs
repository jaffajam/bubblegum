﻿using UnityEngine;

namespace Bubblegum.Particles
{

	/// <summary>
	/// Uses a particle system to show the fractured parts of the mesh
	/// </summary>
	public class ParticleMeshRenderer : DynamicParticleSystem
	{
		#region PRIVATE_VARIABLES

		/// <summary>
		/// The layer to draw on
		/// </summary>
		private int layer;

		/// <summary>
		/// The material we are rendering with
		/// </summary>
		private Material material;

		#endregion

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Awaken this object
		/// </summary>
		protected override void Awake()
		{
			base.Awake();

			//Set up system
			ParticleSystemRenderer systemRenderer = system.GetComponent<ParticleSystemRenderer>();
			material = systemRenderer.material;
			systemRenderer.enabled = false;
			layer = gameObject.layer;

			for (int i = 0; i < dynamicParticles.Length; i++)
				dynamicParticles[i] = new MeshParticle();
		}

		/// <summary>
		/// Update this object
		/// </summary>
		protected override void Update()
		{
			if (system.particleCount != particleCount)
				Debug.LogWarning("Particles were added or removed from " + name + " without using the DynamicParticles component");

			particleCount = system.GetParticles(particles);

			//Kill particles
			for (int i = 0; i < particleCount; i++)
			{
				if (particles[i].remainingLifetime < 1f || dynamicParticles[i].index == -1)
				{
					particleCount--;

					if (particleCount != 0)
					{

						dynamicParticles[i].index = -1;
						particles[i].remainingLifetime = 0f;
						particles.Swap(i, particleCount);
						dynamicParticles.Swap(i, particleCount);
						dynamicParticles[i].index = i;
						i--;

						continue;
					}
				}

				//Draw
				Matrix4x4 matrix = Matrix4x4.TRS(particles[i].position, Quaternion.Euler(particles[i].rotation3D), particles[i].startSize3D);
				Graphics.DrawMesh(((MeshParticle)dynamicParticles[i]).mesh, matrix, material, layer);
			}

			//Update system
			if (system.particleCount != particleCount)
				system.SetParticles(particles, particleCount);
		}

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Emit all of the particles that need to represent the fractured chunks
		/// </summary>
		public void Emit(Vector3 position, Quaternion rotation, Mesh mesh)
		{
			int index = particleCount;

			if (index < dynamicParticles.Length)
			{
				emission.position = position;
				emission.rotation3D = rotation.eulerAngles;
				((MeshParticle)dynamicParticles[index]).Initialize(particleCount, mesh);
				system.Emit(emission, 1);
				particleCount++;
			}
		}

		/// <summary>
		/// Emit particles with the following values
		/// </summary>
		/// <param name="positions"></param>
		/// <param name="rotations"></param>
		/// <param name="meshes"></param>
		public void Emit(Vector3[] positions, Quaternion[] rotations, Mesh[] meshes)
		{
			if (positions.Length != rotations.Length && rotations.Length != meshes.Length)
				throw new System.Exception("Positions, rotations, and meshes lengths need to be equal");

			for (int i = 0; i < meshes.Length; i++)
				Emit(positions[i], rotations[i], meshes[i]);
		}

		#endregion

		#region SUB_CLASSES

		/// <summary>
		/// A reference to a particle in the system
		/// </summary>
		public class MeshParticle : DynamicParticle
		{
			#region VARIABLES

			/// <summary>
			/// The mesh used to render the particle
			/// </summary>
			public Mesh mesh;

			#endregion

			#region PUBLIC_METHODS

			/// <summary>
			/// Create a new particle reference
			/// </summary>
			/// <param name="mesh"></param>
			public void Initialize(int index, Mesh mesh)
			{
				base.Initialize(index);
				this.mesh = mesh;
			}

			#endregion
		}

		#endregion
	}
}