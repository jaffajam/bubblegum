﻿using UnityEngine;

namespace Bubblegum.Particles
{

    /// <summary>
    /// Controls the emission of particles as a percentage
    /// </summary>
    public class ParticleEmitter : MonoBehaviour
    {
        #region VARIABLES

        /// <summary>
        /// Get or set the emission amount as a percent
        /// </summary>
        public float EmissionAmount
        {
            get
            {
                return emissionCurve.constant / emissionRate;
            }

            set
            {
                emissionCurve.constant = Mathf.Clamp01(value) * emissionRate;
                emission.rateOverTime = emissionCurve;
            }
        }

        /// <summary>
        /// If enabling this component starts the particle system
        /// </summary>
        [Tooltip("If enabling this component starts the particle system")]
        public bool enableStartsSystem;

        /// <summary>
        /// The current emission rate
        /// </summary>
        [Tooltip("The current emission rate")]
        public float emissionRate = 10f;

        /// <summary>
        /// The starting emission amount as a percent
        /// </summary>
        [SerializeField, Tooltip ("The starting emission amount as a percent"), Range(0f, 1f)]
        public float startingEmissionAmount = 1f;

        /// <summary>
        /// The attached particle system
        /// </summary>
        private ParticleSystem system;

        /// <summary>
        /// The emission module attached
        /// </summary>
        private ParticleSystem.EmissionModule emission;

        /// <summary>
        /// The set curve in the emission module
        /// </summary>
        private ParticleSystem.MinMaxCurve emissionCurve;

        #endregion

        #region METHODS

        /// <summary>
        /// Awaken this object
        /// </summary>
        void Awake()
        {
            system = GetComponent<ParticleSystem>();
            emission = system.emission;
            emissionCurve = emission.rateOverTime;
            EmissionAmount = startingEmissionAmount;
        }

        /// <summary>
        /// Enable this object
        /// </summary>
        void OnEnable()
        {
            if (enableStartsSystem)
                system.Play();
        }

        /// <summary>
        /// Disable this object
        /// </summary>
        void OnDisable()
        {
            if (enableStartsSystem)
                system.Stop();
        }

        #endregion
    }
}