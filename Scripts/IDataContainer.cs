﻿namespace Bubblegum
{

	/// <summary>
	/// An object that holds data for another object
	/// </summary>
	public interface IDataContainer
	{
		#region METHODS

		/// <summary>
		/// Key for the data
		/// </summary>
		string Key { get; }

		/// <summary>
		/// Get the data
		/// </summary>
		string Data { get; }

		/// <summary>
		/// Initialize this object
		/// </summary>
		/// <param name="data"></param>
		/// <param name="type"></param>
		void SetData(string data);

		/// <summary>
		/// Update the name of the object
		/// </summary>
		/// <param name="name"></param>
		void SetName(string name);

		/// <summary>
		/// Clear all of the data
		/// </summary>
		void ClearData();

		#endregion
	}
}