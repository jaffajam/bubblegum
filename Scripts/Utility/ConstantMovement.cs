﻿using UnityEngine;

namespace Bubblegum.Utility
{
    /// <summary>
    /// Moves a kinematic rigidbody at a constant speed
    /// </summary>
    public class ConstantMovement : MonoBehaviour
    {
        #region VARIABLES

        /// <summary>
        /// If we should apply movement locally or in world space
        /// </summary>
        [SerializeField, Tooltip("If we should apply movement locally or in world space")]
        public bool isLocalVelocity;

        /// <summary>
        /// The velocity we want to move at
        /// </summary>
        [SerializeField, Tooltip ("The velocity we want to move at")]
        public Vector3 velocity;

        /// <summary>
        /// The attached rigidbody
        /// </summary>
        private Rigidbody rbody;

        #endregion

        #region METHODS

        /// <summary>
        /// Awaken this object
        /// </summary>
        void Awake()
        {
            rbody = GetComponent<Rigidbody>();
        }

        /// <summary>
        /// Physics update loop
        /// </summary>
        void FixedUpdate()
        {
            Vector3 movement = isLocalVelocity ? transform.TransformDirection(velocity) : velocity;
            rbody.MovePosition(transform.position + movement * Time.deltaTime);
        }

        #endregion
    }
}