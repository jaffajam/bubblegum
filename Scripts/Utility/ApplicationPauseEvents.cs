﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ApplicationPauseEvents : MonoBehaviour
{
    #region VARIABLES

    /// <summary>
    /// When pause occurs
    /// </summary>
    public UnityEvent onPause, onResume;

    /// <summary>
    /// When we lose focus
    /// </summary>
    public UnityEvent onFocusLost, onFocusGained;

    #endregion

    #region METHODS

    /// <summary>
    /// When the app is paused
    /// </summary>
    /// <param name="pause"></param>
    void OnApplicationPause(bool pause)
    {
        if (pause)
            onPause.Invoke();
        else
            onResume.Invoke();
    }

    /// <summary>
    /// When app loses focus
    /// </summary>
    /// <param name="focus"></param>
    void OnApplicationFocus(bool focus)
    {
        if (focus)
            onFocusGained.Invoke();
        else
            onFocusLost.Invoke();
    }

    #endregion
}
