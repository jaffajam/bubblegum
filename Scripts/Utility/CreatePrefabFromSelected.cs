﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System;

namespace Bubblegum.Utility
{

	class CreatePrefabFromSelected : ScriptableObject
	{
		const string menuTitle = "Tools/Bubblegum/Create Prefab From Selected";

		/// <summary>
		/// Creates a prefab from the selected game object.
		/// </summary>
		[MenuItem(menuTitle)]
		static void CreatePrefab()
		{
			var objs = Selection.gameObjects;

			string pathBase = UnityEditor.EditorUtility.SaveFolderPanel("Choose save folder", "Assets", "");

			if (!String.IsNullOrEmpty(pathBase))
			{

				pathBase = pathBase.Remove(0, pathBase.IndexOf("Assets")) + "/";

				foreach (var go in objs)
				{
					String localPath = pathBase + go.name + ".prefab";

					if (AssetDatabase.LoadAssetAtPath(localPath, typeof(GameObject)))
					{
						if (UnityEditor.EditorUtility.DisplayDialog("Are you sure?",
								"The prefab already exists. Do you want to overwrite it?",
								"Yes",
								"No"))
							CreateNew(go, localPath);
					}
					else
						CreateNew(go, localPath);
				}
			}

			AssetDatabase.Refresh();
		}

		/// <summary>
		/// Create new object
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="localPath"></param>
		static void CreateNew(GameObject obj, string localPath)
		{
			PrefabUtility.SaveAsPrefabAssetAndConnect(obj, localPath, InteractionMode.AutomatedAction);
		}

		/// <summary>
		/// Validates the menu.
		/// </summary>
		/// <remarks>The item will be disabled if no game object is selected.</remarks>
		[MenuItem(menuTitle, true)]
		static bool ValidateCreatePrefab()
		{
			return Selection.activeGameObject != null;
		}
	}
}

#endif