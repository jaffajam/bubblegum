﻿using UnityEngine;
using UnityEngine.UI;

namespace Bubblegum.Utility
{

	/// <summary>
	/// Used to set prefernces from anywhere in the application
	/// </summary>
	[CreateAssetMenu(menuName = "Scriptable Object/Preference Control")]
	public class PreferenceControl : Key
	{
		#region VARIABLES

		/// <summary>
		/// Get the key to save with
		/// </summary>
		public string PreferenceKey
		{
			get
			{
				return name;
			}
		}

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Reset the player prefs
		/// </summary>
		public void DeleteAll()
		{
            PersistentData.DeleteAll();
		}

		/// <summary>
		/// Set as bool
		/// </summary>
		public void SetBool(bool value)
		{
            PersistentData.SetInt(PreferenceKey, value ? 1 : 0);
		}

		/// <summary>
		/// Increment the preference by the given amount
		/// </summary>
		public void Increment(int amount)
		{
            PersistentData.SetInt(PreferenceKey, PersistentData.GetInt(PreferenceKey) + amount);
		}

		/// <summary>
		/// Increment the preference by the given amount
		/// </summary>
		public void Deccrement(int amount)
		{
            PersistentData.SetInt(PreferenceKey, PersistentData.GetInt(PreferenceKey) - amount);
		}

		/// <summary>
		/// Set the int for the current preference
		/// </summary>
		/// <param name="value"></param>
		public void SetInt(int value)
		{
            PersistentData.SetInt(PreferenceKey, value);
		}

		/// <summary>
		/// Apply on/off value to toggle
		/// </summary>
		/// <param name="toggle"></param>
		public void ApplyToToggle(Toggle toggle)
		{
			toggle.isOn = PersistentData.GetInt(PreferenceKey, toggle.isOn ? 1 : 0) == 1;
		}

		#endregion // PUBLIC_METHODS
	}
}