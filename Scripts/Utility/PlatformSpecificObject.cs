﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bubblegum.Utility
{
    /// <summary>
    /// Disable object based on current platform
    /// </summary>
    public class PlatformSpecificObject : MonoBehaviour
    {
        /// <summary>
        /// Available platforms enum
        /// </summary>
        public enum Platforms
        {
            None =              0,
            iOS =               1 << 1,
            Android =           1 << 2,
            WindowsStore =      1 << 3,
            WindowsStandAlone = 1 << 4,
            Switch =            1 << 5,
            PS4 =               1 << 6
        }

        #region VARIABLES

        /// <summary>
        /// Platforms that this object should be included
        /// </summary>
        [EnumFlag]
        public Platforms includePlatforms;

        #endregion

        #region METHODS

        /// <summary>
        /// Disable object if it is not included in current platform
        /// </summary>
        private void OnEnable()
        {
#if UNITY_IOS
            if (!includePlatforms.HasFlag(Platforms.iOS))
#elif UNITY_ANDROID
            if (!includePlatforms.HasFlag(Platforms.Android))
#elif UNITY_WINRT
            if (!includePlatforms.HasFlag(Platforms.WindowsStore))
#elif UNITY_STANDALONE_WIN
            if (!includePlatforms.HasFlag(Platforms.WindowsStandAlone))
#elif UNITY_SWITCH
            if (!includePlatforms.HasFlag(Platforms.Switch))
#elif UNITY_PS4
            if (!includePlatforms.HasFlag(Platforms.PS4))
#endif
                gameObject.SetActive(false);
        }

        #endregion
    }
}