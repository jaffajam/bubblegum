﻿using UnityEngine;
using UnityEngine.Events;

namespace Bubblegum.Utility
{

	/// <summary>
	/// Controls object state based on the selected pref
	/// </summary>
	public class PreferenceFilter : MonoBehaviour
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// What to do when the filter passes true
		/// </summary>
		[SerializeField, Tooltip("What to do when the filter passes true")]
		private StateMethod filterPassAction;

		/// <summary>
		/// All the scenes that we want to show this object in
		/// </summary>
		[SerializeField, Tooltip("All the scenes that we want to show this object in")]
		private PreferenceControl filter;

		/// <summary>
		/// The value to check against
		/// </summary>
		[SerializeField, Tooltip("The value to check against"), SelectedTypes(typeof(bool), typeof(int), typeof(float), typeof(string))]
		private DynamicValue checkValue;

		/// <summary>
		/// The default value to pull
		/// </summary>
		[SerializeField, Tooltip("The default value to pull"), SelectedTypes(typeof(bool), typeof(int), typeof(float), typeof(string))]
		private DynamicValue defaultValue;

		/// <summary>
		/// Method of comparison to use
		/// </summary>
		[SerializeField, Tooltip("Method of comparison to use")]
		private ComparisonMode compareMode;

        /// <summary>
        /// Events to invoke with custom filter pass
        /// </summary>
        [SerializeField, Tooltip("Events to invoke with custom filter pass")]
        private UnityEvent onFilterCustomPass;

        #endregion

        #region MONOBEHAVIOUR_METHODS

        /// <summary>
        /// Enable this component
        /// </summary>
        void OnEnable()
		{
			bool filterPass = false;

			//Bool
			if (checkValue.Value is bool)
				filterPass = PersistentData.GetInt(filter.PreferenceKey, (bool)defaultValue.Value ? 1 : 0) != 0;

			//Int
			else if (checkValue.Value is int)
				filterPass = checkValue.Compare(PersistentData.GetInt(filter.PreferenceKey, (int)defaultValue.Value), compareMode);

			//Float
			else if (checkValue.Value is float)
				filterPass = checkValue.Compare(PersistentData.GetFloat(filter.PreferenceKey, (float)defaultValue.Value), compareMode);

			//String
			else if (checkValue.Value is string)
				filterPass = PersistentData.GetString(filter.PreferenceKey, (string)defaultValue.Value).Equals((string)checkValue.Value);

			//Apply
			switch (filterPassAction)
			{
				case StateMethod.Disable:
					gameObject.SetActive(filterPass);
					break;

				case StateMethod.Destroy:
					if (!filterPass)
						Destroy(gameObject);
					break;

                case StateMethod.None:
                    if (filterPass)
                        onFilterCustomPass.Invoke();
                    break;
			}
		}

		#endregion // MONOBEHAVIOUR_METHODS
	}
}