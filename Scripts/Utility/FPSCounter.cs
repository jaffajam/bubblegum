﻿using UnityEngine;
using System.Collections;

namespace Bubblegum.Utility
{

    /// <summary>
    /// FPS counter for the screen
    /// </summary>
    public class FPSCounter : MonoBehaviour
    {
        #region VARIABLES

        /// <summary>
        /// Rect to draw with
        /// </summary>
        public Rect rect = new Rect(5, 5, 200, 50);

        /// <summary>
        /// Style for text
        /// </summary>
        private GUIStyle myStyle = new GUIStyle();

        /// <summary>
        /// Label display
        /// </summary>
        private string label = "";

        /// <summary>
        /// FPS count
        /// </summary>
        private float count;

        #endregion

        #region METHODS

        /// <summary>
        /// Start this object
        /// </summary>
        /// <returns></returns>
        IEnumerator Start()
        {
            myStyle.fontSize = 32;
            GUI.depth = 2;

            while (true)
            {
                if (Time.timeScale == 1)
                {
                    yield return Yielder.Get(0.1f);
                    count = (1 / Time.deltaTime);
                    label = "FPS :" + (Mathf.Round(count));
                }
                else
                    label = "Pause";

                yield return Yielder.Get(0.5f);
            }
        }

        /// <summary>
        /// Draw gui
        /// </summary>
        void OnGUI()
        {
            GUI.contentColor = Color.black;
            GUI.Label(new Rect(5, 5, 200, 50), label, myStyle);
        }

        #endregion
    }
}