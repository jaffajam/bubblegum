﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Bubblegum.Utility
{

	/// <summary>
	/// Implements alternate methods of automatic memory management
	/// </summary>
	public class MemoryManagement : MonoBehaviour
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		///The different modes for memory management, use smooth for games that go a long period without pausing (ie. FPS, Racer)
		///Use the staggered mode for games that pause often, note that this mode requires that you either call ApplicationManager.Pause or
		///call System.GC.Collect() yourself
		/// </summary>
		[Tooltip("The different modes for memory management, use smooth for games that go a long period without pausing (ie. FPS, Racer) " +
				  "Use the staggered mode for games that pause often, note that this mode requires that you either call ApplicationManager.Pause or " +
				  "call System.GC.Collect() yourself")]
		[SerializeField]
		private MemeoryManagementMode memoryManagementMode;

		/// <summary>
		/// The memory management rate in fps
		/// </summary>
		[Tooltip("The memory management rate in fps")]
		[SerializeField]
		private int memoryManagementRate = 30;

		/// <summary>
		/// The size of the memory heap when using staggered memory
		/// </summary>
		[Tooltip("The size of the memory heap when using staggered memory")]
		[SerializeField]
		private int memoryHeapSize = 1024;

		#endregion // PUBLIC_VARIABLES

		#region ENUMERATORS

		//The different modes for memory management, use smooth for games that go a long period without pausing (ie. FPS, Racer)
		//Use the staggered mode for games that pause often, note that this mode requires that you either call ApplicationManager.Pause or
		//call System.GC.Collect() yourself
		public enum MemeoryManagementMode { NONE, SMOOTH, STAGGERED }

		#endregion // ENUMERATORS

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Called when all objects have been initialized regardless of whether the script is enabled
		/// </summary>
		void Awake()
		{
			//Implement selected memory management technique
			if (memoryManagementMode == MemeoryManagementMode.STAGGERED)
				InitializeStaggeredMemory();
		}

		/// <summary>
		/// Update this instance.
		/// </summary>
		void Update()
		{
			ManageMemory();
		}

		#endregion // MONOBEHAVIOUR_METHODS

		#region PRIVATE_METHODS

		/// <summary>
		/// Initializes the staggered memory.
		/// </summary>
		private void InitializeStaggeredMemory()
		{
			var tmp = new System.Object[memoryHeapSize];

			// make allocations in smaller blocks to avoid them to be treated in a special way, which is designed for large blocks
			for (int i = 0; i < memoryHeapSize; i++)
				tmp[i] = new byte[memoryHeapSize];

			// release reference
			tmp = null;
		}

		/// <summary>
		/// Manage the memory to hopefully aid performance
		/// </summary>
		private void ManageMemory()
		{
			switch (memoryManagementMode)
			{
				case MemeoryManagementMode.SMOOTH:
					if (Time.frameCount % memoryManagementRate == 0)
						System.GC.Collect();

					break;
				case MemeoryManagementMode.STAGGERED:
					if (ApplicationManager.paused)
						System.GC.Collect();

					break;
			}
		}

		#endregion // PRIVATE_METHODS

		#region SUB_CLASSES

#if UNITY_EDITOR
		[CustomEditor(typeof(MemoryManagement)), CanEditMultipleObjects]
		public class MemoryManagementEditor : Editor
		{

			//Declaring properties here means we can have multi object editing
			SerializedProperty memoryManagementMode;
			SerializedProperty memoryManagementRate;
			SerializedProperty memoryHeapSize;

			/// <summary>
			/// Raises the enable event.
			/// </summary>
			private void OnEnable()
			{
				memoryManagementMode = serializedObject.FindProperty("memoryManagementMode");
				memoryManagementRate = serializedObject.FindProperty("memoryManagementRate");
				memoryHeapSize = serializedObject.FindProperty("memoryHeapSize");
			}

			/// <summary>
			/// Raises the inspector GUI event.
			/// </summary>
			public override void OnInspectorGUI()
			{
				serializedObject.Update();
				SerializedProperty prop = serializedObject.FindProperty("m_Script");
				EditorGUILayout.PropertyField(prop, true, new GUILayoutOption[0]);

				serializedObject.Update();

				//Do we need to rely on the memory management tools
				EditorGUILayout.PropertyField(memoryManagementMode);

				if (memoryManagementMode.enumValueIndex == (int)MemeoryManagementMode.SMOOTH)
					EditorGUILayout.PropertyField(memoryManagementRate);

				if (memoryManagementMode.enumValueIndex == (int)MemeoryManagementMode.STAGGERED)
					EditorGUILayout.PropertyField(memoryHeapSize);

				serializedObject.ApplyModifiedProperties();
			}
		}
#endif

		#endregion // SUB_CLASSES
	}
}