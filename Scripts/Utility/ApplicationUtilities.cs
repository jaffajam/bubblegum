﻿using UnityEngine;
using System.Linq;

namespace Bubblegum.Utility
{

	/// <summary>
	/// Opens a url, ideal for UI buttons
	/// </summary>
	[CreateAssetMenu(menuName = "Scriptable Object/Application Utilities")]
	public class ApplicationUtilities : ScriptableObject
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// Get the current orientation setting that is in use
		/// </summary>
		public OrientationSetting CurrentOrientation
		{
			get
			{
				if (currentOrientation == null)
				{
					if (orientationSettings.Length > 0)
						currentOrientation = orientationSettings[0];
					else
						currentOrientation = new OrientationSetting();

					currentOrientation.ApplyLocks();
				}

				return currentOrientation;
			}
		}

		[Header("Rotation")]

		/// <summary>
		/// If we should auto save rotation changes
		/// </summary>
		[SerializeField, Tooltip("If we should auto save rotation changes")]
		private bool saveOrientation;

		/// <summary>
		/// All of the orientation settings to allow for this app
		/// </summary>
		[SerializeField, Tooltip("All of the orientation settings to allow for this app")]
		private OrientationSetting[] orientationSettings;

		#endregion

		#region PRIVATE_VARIABLES

		/// <summary>
		/// The player prefs key for the orientation
		/// </summary>
		private const string ORIENTATION_KEY = "SavedOrientation";

		/// <summary>
		/// An empty orientation setting to apply
		/// </summary>
		private OrientationSetting currentOrientation;

		#endregion

		#region EVENTS/DELEGATES

		/// <summary>
		/// When the orientation setting is changed
		/// </summary>
		public static event System.Action onOrientationChanged;

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Opens the URL
		/// </summary>
		/// <param name="url">URL.</param>
		public void OpenURL(string url)
		{
			Application.OpenURL(url);
		}

		/// <summary>
		/// Toggle the device orientation to based on the current one
		/// </summary>
		public void ToggleDeviceOrientation()
		{
			CurrentOrientation.NextOrientation(Screen.orientation);

			if (saveOrientation)
                PersistentData.SetInt(ORIENTATION_KEY, (int)Screen.orientation);

			if (onOrientationChanged != null)
				onOrientationChanged();
		}

		/// <summary>
		/// Set the orientation setting to the one matching the given key
		/// </summary>
		/// <param name="key"></param>
		public void SetOrientationSetting(string key)
		{
			currentOrientation = orientationSettings.FirstOrDefault(setting => setting.key == key);

			if (currentOrientation != null)
			{
				currentOrientation.ApplyLocks();
				Screen.orientation = currentOrientation.mode;
			}

			if (onOrientationChanged != null)
				onOrientationChanged();
		}

		/// <summary>
		/// Auto lock the screen orientation to the last used device orientation
		/// </summary>
		public void AutoLockOrientation(string key)
		{
			currentOrientation = orientationSettings.FirstOrDefault(setting => setting.key == key);

			if (currentOrientation != null)
				currentOrientation.ApplyLocks();

			Screen.orientation = ApplicationManager.LastScreenOrientation.ToScreenOrientation();
		}

		/// <summary>
		/// Quit the application
		/// </summary>
		public void Quit()
		{
			Application.Quit();
		}

		#endregion // PUBLIC_METHODS

		#region PRIVATE_METHODS

		/// <summary>
		/// Initialize any static variables
		/// </summary>
		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		static void Initialize()
		{
			if (PersistentData.HasKey(ORIENTATION_KEY))
				Screen.orientation = (ScreenOrientation)PersistentData.GetInt(ORIENTATION_KEY);
		}

		#endregion

		#region SUB_CLASSES

		/// <summary>
		/// A collection of allowed orientations that are applied at runtime
		/// </summary>
		[System.Serializable]
		public class OrientationSetting
		{
			#region PUBLIC_METHODS

			/// <summary>
			/// The key used to set this orientation
			/// </summary>
			public string key = "Key";

			/// <summary>
			/// The screen orientation mode to apply
			/// </summary>
			public ScreenOrientation mode;

			/// <summary>
			/// All of our allowed rotations
			/// </summary>
			public bool allowLandscapeLeft, allowLandscapeRight, allowPortrait, allowPortraitUpsideDown;

			#endregion

			#region PUBLIC_METHODS

			/// <summary>
			/// Apply the allowed orientations
			/// </summary>
			public void ApplyLocks()
			{
				Screen.autorotateToLandscapeLeft = allowLandscapeLeft;
				Screen.autorotateToLandscapeRight = allowLandscapeRight;
				Screen.autorotateToPortrait = allowPortrait;
				Screen.autorotateToPortraitUpsideDown = allowPortraitUpsideDown;
			}

			/// <summary>
			/// Switch to the next orientation
			/// </summary>
			/// <param name="currentOrientation"></param>
			public void NextOrientation(ScreenOrientation currentOrientation)
			{
				switch (currentOrientation)
				{
					case ScreenOrientation.LandscapeLeft:
						if (allowLandscapeRight)
							Screen.orientation = ScreenOrientation.LandscapeRight;
						else
							NextOrientation(ScreenOrientation.LandscapeRight);

						break;

					case ScreenOrientation.Portrait:
						if (allowLandscapeRight)
							Screen.orientation = ScreenOrientation.LandscapeRight;
						else
							NextOrientation(ScreenOrientation.LandscapeRight);

						break;

					case ScreenOrientation.LandscapeRight:
						if (allowPortraitUpsideDown)
							Screen.orientation = ScreenOrientation.PortraitUpsideDown;
						else
							NextOrientation(ScreenOrientation.PortraitUpsideDown);

						break;

					case ScreenOrientation.PortraitUpsideDown:
						if (allowLandscapeLeft)
							Screen.orientation = ScreenOrientation.LandscapeLeft;
						else
							NextOrientation(ScreenOrientation.LandscapeLeft);

						break;
				}
			}

			#endregion
		}

		#endregion
	}
}