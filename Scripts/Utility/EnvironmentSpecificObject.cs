﻿using System;
using UnityEngine;
using Bubblegum;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Bubblegum.Utility
{
    /// <summary>
    /// Disable object based on current build environment
    /// </summary>
    public class EnvironmentSpecificObject : MonoBehaviour
    {
        /// <summary>
        /// Available platforms enum
        /// </summary>
        public enum BuildEnvironment
        {
            None = 0,
            Debug = 1 << 1,
            Release = 1 << 2,
            Test = 1 << 3
        }

        #region VARIABLES

        /// <summary>
        /// Build Environments that this object should be included
        /// </summary>
        [EnumFlag]
        public BuildEnvironment includeEnvironment;

        #endregion

        #region METHODS

        /// <summary>
        /// Disable object if it is not included in current build environment
        /// </summary>
        private void OnEnable()
        {
#if RELEASE
        if (!includeEnvironment.HasFlag(BuildEnvironment.Release))
#elif TEST
        if (!includeEnvironment.HasFlag(BuildEnvironment.Test))
#elif DEBUG || UNITY_EDITOR
            if (!includeEnvironment.HasFlag(BuildEnvironment.Debug))
#endif
                gameObject.SetActive(false);
        }

        #endregion

#if UNITY_EDITOR
        /// <summary>
        /// Set environment to release
        /// </summary>
        [MenuItem("Tools/Environment/Release")]
        public static void SetRelease()
        {
            ReplaceOrAddBuildEnvironment(BuildEnvironment.Release);
        }

        /// <summary>
        /// Set environment to debug
        /// </summary>
        [MenuItem("Tools/Environment/Debug")]
        public static void SetDebug()
        {
            ReplaceOrAddBuildEnvironment(BuildEnvironment.Debug);
        }

        /// <summary>
        /// Set environment to Test
        /// </summary>
        [MenuItem("Tools/Environment/Test")]
        public static void SetTest()
        {
            ReplaceOrAddBuildEnvironment(BuildEnvironment.Test);
        }

        /// <summary>
        /// Add/Update the define symbol
        /// </summary>
        public static void ReplaceOrAddBuildEnvironment(BuildEnvironment buildEnvironment)
        {
            BuildTargetGroup buildTargetGroup = BuildTargetGroup.Android;

            switch (EditorUserBuildSettings.activeBuildTarget)
            {
                case BuildTarget.Android:
                    buildTargetGroup = BuildTargetGroup.Android;
                    break;

                case BuildTarget.iOS:
                    buildTargetGroup = BuildTargetGroup.iOS;
                    break;
            }

            string symbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTargetGroup);
            string buildEnvironmentString = buildEnvironment.ToString().ToUpper();
            Debug.Log("Setting Build Environment to " + buildEnvironmentString + " for build target " + buildTargetGroup.ToString());

            foreach (var old in Enum.GetValues(typeof(BuildEnvironment)))
            {
                if (symbols.Contains(old.ToString().ToUpper()))
                {
                    symbols = symbols.Replace(old.ToString().ToUpper(), buildEnvironmentString);
                    PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, symbols);
                    return;
                }
            }

            // an eventual unnecessary ';' won't be a problem
            symbols += ";" + buildEnvironmentString;
            PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, symbols);
        }
#endif
    }
}