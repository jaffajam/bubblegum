using UnityEngine;
using System.IO;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Bubblegum.Utility
{

	/// <summary>
	/// Used to capture the screen render
	/// </summary>
	public class ScreenRecord : MonoBehaviour
	{
		#region VARIABLES

		/// <summary>
		/// If we are recording
		/// </summary>
		public bool Recording { get; set; }

		/// <summary>
		/// Path to save captures
		/// </summary>
		public string path = "Captures";

		/// <summary>
		/// Frame rate to capture at
		/// </summary>
		public int frameRate = 60;

		/// <summary>
		/// How large to scale up screenshot
		/// </summary>
		public int overSize = 1;

		/// <summary>
		/// Capture keys to use
		/// </summary>
		public KeyCode screenshotKey = KeyCode.Space, screenRecordKey = KeyCode.R;

		/// <summary>
		/// Screenshot index
		/// </summary>
		private int index;

		#endregion

		#region METHODS

		/// <summary>
		/// Capture screen shot
		/// </summary>
		public void Capture()
		{
			StartCoroutine(CaptureAtEndOfFrame());
		}

		IEnumerator CaptureAtEndOfFrame()
		{
			yield return new WaitForEndOfFrame();
			RecordFrame();
		}

		/// <summary>
		/// Record the current frame
		/// </summary>
		void RecordFrame()
		{
			if (!Directory.Exists(path))
				Directory.CreateDirectory(path);

			var savePath = string.Format("{0}/Screenshot {1:D04}.png", path, index);
			ScreenCapture.CaptureScreenshot(savePath, overSize);
			index++;
		}

		/// <summary>
		/// Update the 
		/// </summary>
		void Update()
		{
			// Toggle capturing
			if (Input.GetKeyDown(screenRecordKey))
				Recording = !Recording;

			// Take screen grab
			if (Input.GetKeyDown(screenshotKey))
				Capture();
		}

		/// <summary>
		/// Late update method
		/// </summary>
		void LateUpdate()
		{
			if (Recording)
			{
				// Set the playback framerate!
				// (real time doesn't influence time anymore)
				if (Time.captureFramerate != frameRate)
					Time.captureFramerate = frameRate;

				RecordFrame();

			}
			else if (Time.captureFramerate != 0)
					Time.captureFramerate = 0;
		}

		/// <summary>
		/// Find the index
		/// </summary>
		public void FindIndex()
		{
			if (Directory.Exists(path))
				index = Directory.GetFiles(path).Length;
		}

		#endregion
	}

#if UNITY_EDITOR

	/// <summary>
	/// Screen capture editor
	/// </summary>
	[CustomEditor(typeof(ScreenRecord))]
	public class ScreenRecordEditor : Editor
	{
		#region METHODS

		/// <summary>
		/// Draw the inspector
		/// </summary>
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			var screenCapture = (ScreenRecord)target;

			if (GUILayout.Button("Capture"))
			{
				screenCapture.FindIndex();
				screenCapture.Capture();
			}

			if (!screenCapture.Recording)
			{
				if (GUILayout.Button("Start Recording"))
				{
					screenCapture.FindIndex();
					screenCapture.Recording = true;
				}
			}
			else
			{
				if (GUILayout.Button("Stop Recording"))
				{
					screenCapture.FindIndex();
					screenCapture.Recording = false;
				}
			}
		}

		#endregion
	}

#endif
}