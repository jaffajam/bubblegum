﻿using System;

namespace Bubblegum
{

	/// <summary>
	/// An object that keeps time for the application
	/// </summary>
	public interface ITimeKeeper
	{
		#region METHODS

		/// <summary>
		/// The current time in seconds
		/// </summary>
		float Time { get; }

        /// <summary>
        /// Get or set the time scale
        /// </summary>
        float TimeScale { get; set; }

		/// <summary>
		/// Invoke the given event with the set delay
		/// </summary>
		void InvokeAction(Action action, float delay, string id = null);

		/// <summary>
		/// Cancel the given action
		/// </summary>
		void CancelAction(string id);

		#endregion
	}
}