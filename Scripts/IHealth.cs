﻿using System;

namespace Bubblegum
{

	/// <summary>
	/// An object representing the health of itself
	/// </summary>
	public interface IHealth
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// Gets the health amount.
		/// </summary>
		/// <value>The health amount.</value>
		int HealthAmount { get; }

		/// <summary>
		/// Get the health percentage
		/// </summary>
		float HealthPercent { get; }

		/// <summary>
		/// Get the health depleted event
		/// </summary>
		Action onHealthChanged { get; set; }

		/// <summary>
		/// Get the health depleted event
		/// </summary>
		Action onHealthDepleted { get; set; }

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Hurt the specified damage.
		/// </summary>
		/// <param name="damage">Damage.</param>
		void Hurt(int damage);

		#endregion
	}
}
