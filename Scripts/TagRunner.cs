﻿using UnityEngine;

namespace Bubblegum
{
    /// <summary>
    /// Triggers all tags to run in the app
    /// </summary>
    public class TagRunner : MonoBehaviour
    {
        #region VARIABLES

        /// <summary>
        /// The selected initialize method
        /// </summary>
        [SerializeField, Tooltip("The selected initialize method")]
        private InitializeMethod initializeMethod;

        #endregion

        #region METHODS

        /// <summary>
        /// Awaken this object
        /// </summary>
        void Awake()
        {
			if (initializeMethod == InitializeMethod.Awake)
				Key.InvokeOnKeysReady(Run);
        }

        /// <summary>
        /// Start this object
        /// </summary>
        void Start()
        {
            if (initializeMethod == InitializeMethod.Start)
				Key.InvokeOnKeysReady(Run);
		}

		/// <summary>
		/// Awaken this object
		/// </summary>
		void OnEnable()
        {
            if (initializeMethod == InitializeMethod.Enable)
				Key.InvokeOnKeysReady(Run);
		}

		/// <summary>
		/// Run all tags
		/// </summary>
		public void Run()
        {
            Key.InvokeOnKeysReady(() =>
            {
                foreach (PrefabConnection prefabConnection in AssetManager.LoadAll<PrefabConnection>())
                    prefabConnection.RunTags();
            });
        }

        #endregion
    }
}