﻿using UnityEngine;

namespace Bubblegum
{

    /// <summary>
    /// Responsible for keeping track of the current context of the app
    /// </summary>
    [CreateAssetMenu(menuName = "Scriptable Object/Context Basic")]
    public class ContextBasic : ScriptableObject, IContextManager
    {

        #region PUBLIC_VARIABLES

        /// <summary>
        /// Get the current context
        /// </summary>
        public object Context
		{
			get
			{
				return context != null ? context : defaultContext.Value;
			}

			set
			{
				context = value;
			}
		}

		/// <summary>
		/// The default context to use
		/// </summary>
		public DynamicValue defaultContext;

		/// <summary>
		/// Context object
		/// </summary>
		private object context;

        #endregion // PUBLIC_VARIABLES

        #region EVENTS/DELEGATES

        /// <summary>
        /// Methods to invoke when the context changes
        /// </summary>
        public System.Action onContextChanged { get; set; }

        #endregion // EVENTS/DELEGATES

        #region SCRIPTABLEOBJECT_METHODS

        /// <summary>
        /// When the object is enabled
        /// </summary>
        void OnEnable()
        {
            Clear();
        }

        #endregion

        #region PUBLIC_METHODS

        /// <summary>
        /// Get the context as the given type
        /// </summary>
        public T GetContext<T>()
        {
            if (Context is T)
                return (T)Context;

            return default;
        }

        /// <summary>
        /// Set the context to the given object
        /// </summary>
        public void Set(object obj)
        {
            Context = obj;
            onContextChanged?.Invoke();
        }


        /// <summary>
        /// Set the context to the given object
        /// </summary>
        public void SetUnityObject(Object obj)
        {
            Set(obj);
        }

        /// <summary>
        /// Clear the context to nothing
        /// </summary>
        public void Clear()
        {
            Context = null;
            onContextChanged?.Invoke();
        }

        /// <summary>
        /// Check if the given object is in context
        /// </summary>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool IsInContext(object obj)
        {
            if (obj == null)
                return Context == null;

            return obj.Equals(Context);
        }

        #endregion
    }
}