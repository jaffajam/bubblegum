﻿using UnityEngine;

namespace Bubblegum
{

	/// <summary>
	/// Detects entities using detector methods
	/// </summary>
	public class EntityDetector : Detector
	{
		#region VARIABLES

		/// <summary>
		/// Filter used to check for other entities
		/// </summary>
		[SerializeField, Tooltip("Filter used to check for other entities")]
		private EntityFilter filter;

		/// <summary>
		/// When we detect an entity
		/// </summary>
		[SerializeField, Tooltip("When we detect an entity")]
		private UnityKeyEvent onEntityDetected;

		#endregion

		#region METHODS

		/// <summary>
		/// Perform a check for this combo
		/// </summary>
		public override void Check()
		{
			base.Check();

			for (int i = 0; i < DetectionCount; i++)
			{
				Identification id = Detections[i].transform.root.GetComponent<Identification>();

				if (id && filter.CheckFilter(id, gameObject))
					onEntityDetected.Invoke(id.Key);
			}
		}

		#endregion
	}
}