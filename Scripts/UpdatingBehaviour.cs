﻿using UnityEngine;
namespace Bubblegum
{

	/// <summary>
	/// A behaviour that updates in a selected method
	/// </summary>
	public abstract class UpdatingBehaviour : MonoBehaviour
	{

		#region PUBLIC_VARIABLES

		[Header("Updating Behaviour")]

		/// <summary>
		/// When to check and update the movement
		/// </summary>
		[SerializeField, Tooltip("When to check and update the movement")]
		private UpdateMethod updateMethod;

		#endregion // PUBLIC_VARIABLES

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Update this component
		/// </summary>
		void Update()
		{
			if (updateMethod == UpdateMethod.Update)
				ElectedUpdate();
		}

		/// <summary>
		/// Update this component
		/// </summary>
		void FixedUpdate()
		{
			if (updateMethod == UpdateMethod.FixedUpdate)
				ElectedUpdate();
		}

		/// <summary>
		/// Update this component
		/// </summary>
		void LateUpdate()
		{
			if (updateMethod == UpdateMethod.LateUpdate)
				ElectedUpdate();
		}

		#endregion // MONOBEHAVIOUR_METHODS

		#region PUBLIC_METHODS

		/// <summary>
		/// The update method selected
		/// </summary>
		public abstract void ElectedUpdate();

		#endregion // PUBLIC_METHODS
	}
}