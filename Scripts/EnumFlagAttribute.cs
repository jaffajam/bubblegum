using UnityEngine;
using System;
using System.Reflection;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Bubblegum
{
	/// <summary>
	/// Flag attribute for any enum field
	/// </summary>
	public class EnumFlagAttribute : PropertyAttribute
	{

		/// <summary>
		/// Create an empty attribute
		/// </summary>
		public EnumFlagAttribute() { }

	}

#if UNITY_EDITOR

	/// <summary>
	/// Draws an enum that has the Flag attribute set
	/// </summary>
	[CustomPropertyDrawer(typeof(EnumFlagAttribute))]
	public class EnumFlagDrawer : PropertyDrawer
	{
		/// <summary>
		/// Draw inspector objects
		/// </summary>
		/// <param name="position"></param>
		/// <param name="property"></param>
		/// <param name="label"></param>
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EnumFlagAttribute flagSettings = (EnumFlagAttribute)attribute;
			Enum targetEnum = property.FindPropertyObject<Enum>() as Enum;
			EditorGUI.BeginProperty(position, label, property);

			Enum enumNew = EditorGUI.EnumFlagsField(position, label, targetEnum);
			property.intValue = (int)Convert.ChangeType(enumNew, targetEnum.GetType());

			EditorGUI.EndProperty();
		}
	}
#endif
}