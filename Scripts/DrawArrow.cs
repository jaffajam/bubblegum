﻿using UnityEngine;

namespace Bubblegum
{

	/// <summary>
	/// Used for creating arrows in debug, taken from http://forum.unity3d.com/threads/debug-drawarrow.85980/
	/// </summary>
	public static class DrawArrow
	{
		/// <summary>
		/// Draw arrow under gizmos method
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="direction"></param>
		/// <param name="arrowHeadLength"></param>
		/// <param name="arrowHeadAngle"></param>
		public static void ForGizmo(Vector3 pos, Vector3 direction, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
		{
			Gizmos.DrawRay(pos, direction);
			DrawArrowEnd(true, pos, direction, Gizmos.color, arrowHeadLength, arrowHeadAngle);
		}

		/// <summary>
		/// Draw arrow under gizmos method
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="direction"></param>
		/// <param name="color"></param>
		/// <param name="arrowHeadLength"></param>
		/// <param name="arrowHeadAngle"></param>
		public static void ForGizmo(Vector3 pos, Vector3 direction, Color color, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
		{
			Gizmos.DrawRay(pos, direction);
			DrawArrowEnd(true, pos, direction, color, arrowHeadLength, arrowHeadAngle);
		}

		/// <summary>
		/// Draw arrow under debug method
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="direction"></param>
		/// <param name="arrowHeadLength"></param>
		/// <param name="arrowHeadAngle"></param>
		public static void ForDebug(Vector3 pos, Vector3 direction, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
		{
			Debug.DrawRay(pos, direction);
			DrawArrowEnd(false, pos, direction, Gizmos.color, arrowHeadLength, arrowHeadAngle);
		}

		/// <summary>
		/// Draw arrow under debug method
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="direction"></param>
		/// <param name="color"></param>
		/// <param name="arrowHeadLength"></param>
		/// <param name="arrowHeadAngle"></param>
		public static void ForDebug(Vector3 pos, Vector3 direction, Color color, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
		{
			Debug.DrawRay(pos, direction, color);
			DrawArrowEnd(false, pos, direction, color, arrowHeadLength, arrowHeadAngle);
		}

		/// <summary>
		/// Draw the arrow tip
		/// </summary>
		/// <param name="gizmos"></param>
		/// <param name="pos"></param>
		/// <param name="direction"></param>
		/// <param name="color"></param>
		/// <param name="arrowHeadLength"></param>
		/// <param name="arrowHeadAngle"></param>
		private static void DrawArrowEnd(bool gizmos, Vector3 pos, Vector3 direction, Color color, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
		{
			Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(arrowHeadAngle, 0, 0) * Vector3.back;
			Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(-arrowHeadAngle, 0, 0) * Vector3.back;
			Vector3 up = Quaternion.LookRotation(direction) * Quaternion.Euler(0, arrowHeadAngle, 0) * Vector3.back;
			Vector3 down = Quaternion.LookRotation(direction) * Quaternion.Euler(0, -arrowHeadAngle, 0) * Vector3.back;

			if (gizmos)
			{
				Gizmos.color = color;
				Gizmos.DrawRay(pos + direction, right * arrowHeadLength);
				Gizmos.DrawRay(pos + direction, left * arrowHeadLength);
				Gizmos.DrawRay(pos + direction, up * arrowHeadLength);
				Gizmos.DrawRay(pos + direction, down * arrowHeadLength);
			}
			else
			{
				Debug.DrawRay(pos + direction, right * arrowHeadLength, color);
				Debug.DrawRay(pos + direction, left * arrowHeadLength, color);
				Debug.DrawRay(pos + direction, up * arrowHeadLength, color);
				Debug.DrawRay(pos + direction, down * arrowHeadLength, color);
			}
		}
	}
}