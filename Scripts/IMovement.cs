﻿namespace Bubblegum
{

	/// <summary>
	/// Defines an object that is used for movement
	/// </summary>
	public interface IMovement
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// Gets a value indicating whether this instance can move (rotate and locomote)
		/// </summary>
		/// <value><c>true</c> if this instance can move; otherwise, <c>false</c>.</value>
		bool CanMove { get; set; }

		/// <summary>
		/// Gets a value indicating whether this instance can rotate.
		/// </summary>
		/// <value><c>true</c> if this instance can rotate; otherwise, <c>false</c>.</value>
		bool CanRotate { get; set; }

		/// <summary>
		/// If the game object is currently moving towards a target
		/// </summary>
		bool Moving { get; }

		/// <summary>
		/// Gets the target that we are moving towards
		/// </summary>
		/// <value>The target.</value>
		Target OurTarget { get; set; }

		/// <summary>
		/// Gets the remaining distanceto the target
		/// </summary>
		/// <value>The remaining distance.</value>
		float RemainingDistance { get; }

		/// <summary>
		/// Gets the speed of the object
		/// </summary>
		/// <value>The speed.</value>
		float Speed { get; }

		#endregion // PUBLIC_VARIABLES

		#region EVENTS/DELEGATES

		/// <summary>
		/// Methods to invoke when a new target is found
		/// </summary>
		event System.Action onNewTarget;

		/// <summary>
		/// Occurs when a new target is reached
		/// </summary>
		event System.Action onTargetReached;

		#endregion // EVENTS/DELEGATES

		#region PUBLIC_METHODS

		/// <summary>
		/// Move to the desired target
		/// </summary>
		/// <param name="target"></param>
		void Move(Target target);

		/// <summary>
		/// Stop movement but remember target
		/// </summary>
		void Pause();

		/// <summary>
		/// Stop movement and forget about target
		/// </summary>
		void Stop();

		#endregion
	}
}