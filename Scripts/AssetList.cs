﻿using UnityEngine;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace Bubblegum
{

    /// <summary>
    /// Model type class with asset bundle information
    /// </summary>
    [System.Serializable]
    public class AssetList
    {
        #region VARIABLES

        /// <summary>
        /// The version of the manifest we are using
        /// </summary>
        public uint manifestVersion;

        /// <summary>
        /// The list of asset bundles
        /// </summary>
        public List<AssetBundleInformation> list = new List<AssetBundleInformation>();

        #endregion

        #region CONSTRUCTORS

        /// <summary>
        /// Create an empty asset list
        /// </summary>
        public AssetList() { }

        /// <summary>
        /// Create a new asset list
        /// </summary>
        /// <param name="manifest"></param>
        public AssetList(AssetBundleManifest manifest)
        {
            list.Clear();

            foreach (string path in manifest.GetAllAssetBundles())
                list.Add(new AssetBundleInformation(path, Path.GetExtension(path), 0));
        }

        #endregion

        #region METHODS

        /// <summary>
        /// Find data at the given path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public AssetBundleInformation Find(string path)
        {
            AssetBundleInformation info = list.FirstOrDefault(item => item.path == path);

            if (info == null)
                return new AssetBundleInformation(path, Path.GetExtension(path), 0);

            return info;
        }

        /// <summary>
        /// Set the version for the bundle at the given path
        /// </summary>
        /// <param name="path"></param>
        /// <param name="version"></param>
        public void SetVersion(string path, uint version)
        {
            AssetBundleInformation info = list.FirstOrDefault(item => item.path == path);

            if (info == null)
                list.Add(new AssetBundleInformation(path, Path.GetExtension(path), version));
            else
                info.version = version;
        }

        #endregion
    }

    /// <summary>
    /// Model type class with asset bundle information
    /// </summary>
    [System.Serializable]
    public class AssetBundleInformation
    {
        #region VARIABLES

        /// <summary>
        /// The name/identifier for this item
        /// </summary>
        public string path;

        /// <summary>
        /// The variant for the bundle
        /// </summary>
        public string variant;

        /// <summary>
        /// The version of this asset bundle
        /// </summary>
        public uint version;

        #endregion

        #region CONSTRUCTORS

        /// <summary>
        /// Create a new asset bundle information object
        /// </summary>
        /// <param name="path"></param>
        /// <param name="version"></param>
        public AssetBundleInformation(string path, string variant, uint version)
        {
            this.path = path;
            this.variant = variant;
            this.version = version;
        }

        #endregion
    }
}