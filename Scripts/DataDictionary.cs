﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Bubblegum
{

	/// <summary>
	/// Stores generic information about something as a typical C# dictionary would, but acts as a Unity Asset Object
	/// </summary>
	[CreateAssetMenu(menuName = "Scriptable Object/Data Dictionary")]
	public class DataDictionary : Key, ISerializationCallbackReceiver
	{
		#region PUBLIC_VARIABLES

		/// <summary>
		/// Get the count for the dictionary
		/// </summary>
		public int Count { get { return dictionary.Count; } }

		/// <summary>
		/// Get all of the keys
		/// </summary>
		public string[] Keys { get { return data.Select(item => item.key).ToArray(); } }

		/// <summary>
		/// All of the stored information
		/// </summary>
		[SerializeField, Tooltip("All of the stored information")]
		private List<Information> data = new List<Information>();

		#endregion

		#region PRIVATE_VARIABLES

		/// <summary>
		/// All of the data placed into a dictionary
		/// </summary>
		protected Dictionary<string, object> dictionary = new Dictionary<string, object>();

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Initialize this object with data
		/// </summary>
		/// <param name="dictionary"></param>
		public void Initialize(Dictionary<string, object> set)
		{
			dictionary = set;
			data.Clear();

			foreach (var pair in dictionary)
				data.Add(new Information(pair.Key, pair.Value));
		}

		/// <summary>
		/// When we are about to serialize this object
		/// </summary>
		public void OnBeforeSerialize()
		{
            //Serializing here overwrites dynamic values
		}

		/// <summary>
		/// After we have deserialized the data from this object
		/// </summary>
		public void OnAfterDeserialize()
		{
			dictionary.Clear();

			for (int i = 0; i < data.Count; i++)
			{
				data[i].key = CheckKey(data[i].key);
				dictionary.Add(data[i].key, data[i].value.Value);
			}
		}

		/// <summary>
		/// Check if the dictionary contains the given key
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public bool ContainsKey(string key)
		{
			return dictionary.ContainsKey(key);
		}

        /// <summary>
        /// Get data type with the given key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public System.Type GetType(string key)
        {
            if (!dictionary.ContainsKey(key))
                return null;

            return dictionary[key].GetType();
        }

		/// <summary>
		/// Get an object from the data
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key"></param>
		/// <returns></returns>
		public virtual T Get<T>(string key, T defaultValue = default)
		{
			if (dictionary.ContainsKey(key))
				return (T)dictionary[key];

			if (debug)
				Debug.LogWarning(name + " does not contain a value with key " + key);

			return defaultValue;
		}

		/// <summary>
		/// Add an item with the value to the data
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key"></param>
		/// <param name="value"></param>
		public virtual void Set(string key, object value)
		{
			Information info = data.FirstOrDefault(item => item.key == key);

			if (info != null)
				info.value.Value = value;
			else
				data.Add(new Information(key, value));

#if UNITY_EDITOR
			UnityEditor.EditorUtility.SetDirty(this);
#endif
		}

		/// <summary>
		/// Remove an item from the list
		/// </summary>
		public void Remove(string key)
		{
			int index = data.FindIndex(item => item.key == key);

			if (index != -1)
				data.RemoveAt(index);

#if UNITY_EDITOR
			UnityEditor.EditorUtility.SetDirty(this);
#endif
		}

		/// <summary>
		/// Update the key with the given name
		/// </summary>
		/// <param name="oldKey"></param>
		/// <param name="newKey"></param>
		public bool UpdateKey(string oldKey, string newKey)
		{
			Information info = data.FirstOrDefault(item => item.key == oldKey);

			if (info != null)
			{
				info.key = newKey;
#if UNITY_EDITOR
				UnityEditor.EditorUtility.SetDirty(this);
#endif
				return true;
			}

			return false;
		}

#if UNITY_EDITOR

		/// <summary>
		/// Draw a generate data button
		/// </summary>
		public static void DrawGenerateButton(string buttonName, string assetName, Dictionary<string, object> defaults)
		{
			if (GUILayout.Button(buttonName))
			{
				DataDictionary data = CreateInstance<DataDictionary>();
				data.name = assetName;
				data.Initialize(defaults);
				data.CreateAssetInCurrentDirectory();
			}
		}

#endif

		#endregion

		#region PRIVATE_METHODS

		/// <summary>
		/// Check the key in the string is available
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		private string CheckKey(string key)
		{
			if (dictionary.ContainsKey(key))
			{
				key += "1";
				return CheckKey(key);
			}

			return key;
		}

		#endregion

		#region SUB_CLASSES

		/// <summary>
		/// A piece of information that is available
		/// </summary>
		[System.Serializable]
		public class Information
		{
			#region VARIABLES

			/// <summary>
			/// The key for the information
			/// </summary>
			public string key;

			/// <summary>
			/// The value for the given key
			/// </summary>
			public DynamicValue value;

			#endregion

			#region CONSTRUCTORS

			/// <summary>
			/// Create a new information object
			/// </summary>
			/// <param name="key"></param>
			/// <param name="value"></param>
			public Information(string key, object value)
			{
				this.key = key;
				this.value = new DynamicValue(value);
			}

			#endregion
		}

		#endregion
	}
}