﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Bubblegum.Inputs
{

	/// <summary>
	/// Used to control the event system
	/// </summary>
	public class InputController : Singleton<InputController>
	{
		#region VARIABLES

		/// <summary>
		/// The event system
		/// </summary>
		public static EventSystem TheSystem
		{
			get
			{
				if (system == null)
					system = Instance.GetComponent<EventSystem>();

				return system;
			}
		}

		/// <summary>
		/// If the system is enabled
		/// </summary>
		public static bool InputEnabled
		{
			get
			{
				return SystemEnabled && inputEnabled;
			}

			set
			{
				inputEnabled = value;
			}
		}

		/// <summary>
		/// If the system is enabled
		/// </summary>
		public static bool SystemEnabled
		{
			get
			{
				return !TheSystem || TheSystem.enabled;
			}
		}

		/// <summary>
		/// The time that we want to enable the system
		/// </summary>
		private static float enableTime;

		/// <summary>
		/// If the input is enabled
		/// </summary>
		private static bool inputEnabled = true;

		/// <summary>
		/// The event system
		/// </summary>
		private static EventSystem system;

		#endregion

		#region METHODS

		/// <summary>
		/// Initialize values
		/// </summary>
		void Start()
		{
			inputEnabled = true;
		}

		/// <summary>
		/// Disable for the given time
		/// </summary>
		/// <param name="time"></param>
		public void Disable(float time)
		{
			if (enableTime < Time.time + time)
			{
				enableTime = Time.time + time;
				TheSystem.enabled = false;
				System.Threading.Timer timer = null;

				timer = new System.Threading.Timer((obj) =>
			   {
				   Dispatcher.Instance.Invoke(() => TheSystem.enabled = true);
				   timer.Dispose();
			   },
				null, (int)(time * 1000f), System.Threading.Timeout.Infinite);
			}
		}

		#endregion
	}
}