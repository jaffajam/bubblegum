﻿using UnityEngine;

namespace Bubblegum.Inputs
{

	/// <summary>
	/// Acts as an injectible input class using Unity Input
	/// </summary>
	[CreateAssetMenu(menuName = "Scriptable Object/Input/Standard Input")]
	public class StandardInput : ScriptableObject, IInput
	{

		#region VARIABLES

		/// <summary>
		/// Gets the mouse position
		/// </summary>
		public Vector3 mousePosition
		{
			get
			{
				return Input.mousePosition;
			}
		}

		#endregion // VARIABLES

		#region PUBLIC_METHODS

		/// <summary>
		/// Get the input from the axis matching the name
		/// </summary>
		/// <param name="axis"></param>
		public float GetAxis(string axis)
		{
			return InputController.InputEnabled ? Input.GetAxis(axis) : 0f;
		}

		/// <summary>
		/// Get the input from the axis matching the name
		/// </summary>
		/// <param name="axis"></param>
		public float GetAxisRaw(string axis)
		{
			return InputController.InputEnabled ? Input.GetAxisRaw(axis) : 0f;
		}

		/// <summary>
		/// Get the input from the button matching the name
		/// </summary>
		/// <param name="buttonName"></param>
		public bool GetButton(string buttonName)
		{
			return InputController.InputEnabled ? Input.GetButton(buttonName) : false;
		}

		/// <summary>
		/// Get the input from the button matching the name
		/// </summary>
		/// <param name="buttonName"></param>
		public bool GetButtonDown(string buttonName)
		{
			return InputController.InputEnabled ? Input.GetButtonDown(buttonName) : false;
		}

		/// <summary>
		/// Get the input from the button matching the name
		/// </summary>
		/// <param name="buttonName"></param>
		public bool GetButtonUp(string buttonName)
		{
			return InputController.InputEnabled ? Input.GetButtonUp(buttonName) : false;
		}

		#endregion // PUBLIC_METHODS
	}
}