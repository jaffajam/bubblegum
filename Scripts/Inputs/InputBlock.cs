﻿using UnityEngine;

namespace Bubblegum.Inputs
{
	/// <summary>
	/// Blocks input while this object is active
	/// </summary>
	public class InputBlock : MonoBehaviour
	{
        #region VARIABLES

        /// <summary>
        /// If we should control the event system
        /// </summary>
        [SerializeField, Tooltip("If we should control the event system")]
        private bool controlEventSystem;

		/// <summary>
		/// The blocking count
		/// </summary>
		private static int blockCount, eventSystemBlockCount;

		#endregion

		#region METHODS

		/// <summary>
		/// Enaable this object
		/// </summary>
		void OnEnable()
		{
			blockCount++;
			InputController.InputEnabled = false;

            if (controlEventSystem)
            {
                eventSystemBlockCount++;
                InputController.TheSystem.enabled = false;
            }
		}

		/// <summary>
		/// Disable this object
		/// </summary>
		void OnDisable()
		{
			blockCount--;

            if (blockCount == 0)
                InputController.InputEnabled = true;

            if (controlEventSystem)
            {
                eventSystemBlockCount--;

                if (eventSystemBlockCount == 0)
                    InputController.TheSystem.enabled = true;
            }
        }

		#endregion
	}
}