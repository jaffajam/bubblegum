﻿using UnityEngine;

namespace Bubblegum.Inputs
{

	/// <summary>
	/// An axis/input link that is used across controls
	/// </summary>
	[System.Serializable]
	public class ControlInput
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// The key used to get the axis from cross platform input
		/// </summary>
		[SerializeField, Tooltip("The key used to get the axis from cross platform input")]
		private string axisKey = "Horizontal";

		/// <summary>
		/// The axis that we are controlling
		/// </summary>
		[SerializeField, Tooltip("The axis that we are controlling")]
		private Vector3 axis = Vector3.left;

		/// <summary>
		/// The sensitivity multiplier to apply to input
		/// </summary>
		[SerializeField, Tooltip("The sensitivity multiplier to apply to input")]
		private float power = 1f;

		#endregion // PUBLIC_VARIABLES

		#region PUBLIC_METHODS

		/// <summary>
		/// Get the input with applied variables
		/// </summary>
		/// <returns></returns>
		public Vector3 GetTranslatedInput(IInput input)
		{
			return input.GetAxis(axisKey) * power * axis;
		}

		#endregion // PUBLIC_METHODS
	}
}