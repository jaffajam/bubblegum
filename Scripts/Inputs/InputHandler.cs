﻿using UnityEngine;
using UnityEngine.Events;

namespace Bubblegum.Inputs
{

	/// <summary>
	/// Convert input into unity event
	/// </summary>
	public class InputHandler : MonoBehaviour
	{
		#region VARIABLES

		/// <summary>
		/// The input key to read
		/// </summary>
		[SerializeField, Tooltip("The input key to read")]
		private string inputKey = "Fire1";

		/// <summary>
		/// Event to invoke when input is pressed
		/// </summary>
		[Tooltip("Event to invoke when input is pressed")]
		public UnityEvent inputAction;

		#endregion

		#region METHODS

		/// <summary>
		/// Update this object
		/// </summary>
		void Update()
		{
			if (InputController.InputEnabled && Input.GetButtonDown(inputKey))
				inputAction.Invoke();
		}

		#endregion
	}
}