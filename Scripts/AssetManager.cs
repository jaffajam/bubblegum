﻿using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Bubblegum
{

    /// <summary>
    /// Manages assets AND asset bundles in both play mode and editor
    /// </summary>
    public class AssetManager : Singleton<AssetManager>, ITask
    {
        #region VARIABLES

        /// <summary>
        /// The key that denotes this task type
        /// </summary>
        public string TaskKey { get { return TaskObserver.LOADING_GENERIC; } }

        /// <summary>
        /// The task message to describe what is happening
        /// </summary>
        public string TaskMessage { get; private set; }

        /// <summary>
        /// Get the total tasks to complete
        /// </summary>
        public int TotalTasks { get; private set; }

        /// <summary>
        /// Get the total tasks completed
        /// </summary>
        public int CompletedTasks { get; private set; }

        /// <summary>
        /// Check if assets are ready yet
        /// </summary>
        public static bool AssetsReady { get; private set; }

        /// <summary>
        /// Check if resources are ready yet
        /// </summary>
        public static bool ResourcesLoaded { get; private set; }

        /// <summary>
        /// When resources are loaded (before OnAssetsUpdated)
        /// </summary>
        private static System.Action OnResourcesLoaded;

        /// <summary>
        /// Invoked once all patches are applied
        /// </summary>
        private static System.Action OnAssetsLoaded;

        /// <summary>
        /// If we should debug this object
        /// </summary>
        [SerializeField, Tooltip("If we should debug this object")]
        private bool debug;

        /// <summary>
        /// If we should use asset bundles for content
        /// </summary>
        [SerializeField, Tooltip("If we should use asset bundles for content")]
        private bool useAssetBundles;

        /// <summary>
        /// If we should force a clear cache on startup
        /// </summary>
        [SerializeField, Tooltip("If we should force a clear cache on startup"), DisplayIf("useAssetBundles", true)]
        private bool forceClearCache;

        /// <summary>
        /// If we should disabled server side assets
        /// </summary>
        [SerializeField, Tooltip("If we should disabled server side assets"), DisplayIf("useAssetBundles", true)]
        private bool forceOffline;

        /// <summary>
        /// The base url to use
        /// </summary>
        [SerializeField, Tooltip("The base url to use"), DisplayIf("useAssetBundles", true)]
        private string baseURL = "http://www.myserver.com/asset-bundles";

        /// <summary>
        /// Items with this tag are patches rather than assets
        /// </summary>
        [SerializeField, Tooltip("Items with this tag are patches rather than assets"), DisplayIf("useAssetBundles", true)]
        private string patchVariant = ".patch";

        /// <summary>
        /// The manifest object
        /// </summary>
        private AssetBundleManifest manifest;

        /// <summary>
        /// Local and server asset list objects
        /// </summary>
        private AssetList localUpdateList, serverUpdateList;

        /// <summary>
        /// All of the patch objects loaded into memory in the correct order that will make them function
        /// </summary>
        private static List<Object> allContent = new List<Object>();

        /// <summary>
        /// All of the resource objects loaded into memory
        /// </summary>
        private static List<Object> allResources = new List<Object>();

        /// <summary>
        /// All of the asset bundle objects
        /// </summary>
        private static Dictionary<string, AssetBundle> patches = new Dictionary<string, AssetBundle>();

        /// <summary>
        /// Get the directory to write patch lists to
        /// </summary>
        private static string AssetListDirectory
        {
            get
            {
#if UNITY_EDITOR
                return Path.Combine(Application.dataPath, "AssetBundles", BundleName);
#else
                return Path.Combine(Application.persistentDataPath);
#endif
            }
        }

        /// <summary>
        /// Get the directory to find asset bundles
        /// </summary>
        private static string AssetBundleDirectory
        {
            get
            {
#if UNITY_EDITOR
                return Path.Combine(Application.dataPath, "AssetBundles", BundleName);
#else
                return Path.Combine(Application.streamingAssetsPath);
#endif
            }
        }

#if UNITY_EDITOR

        /// <summary>
        /// Get the directory to write asset bundles to
        /// </summary>
        private static string WriteDirectory
        {
            get
            {
                return Path.Combine(Application.dataPath, "AssetBundles", EditorUserBuildSettings.activeBuildTarget.ToString());
            }
        }

        /// <summary>
        /// The directory to read bundles from
        /// </summary>
        private static string ReadDirectory
        {
            get
            {
                return Path.Combine(Application.dataPath, "AssetBundles/Editor");
            }
        }
#endif

        /// <summary>
        /// The name the main bundle will be under
        /// </summary>
        private static string BundleName
        {
            get
            {
#if UNITY_EDITOR
                return "Editor";
#elif UNITY_STANDALONE
                return "Standalone";
#elif UNITY_ANDROID
                return "Android";
#elif UNITY_IOS
                return "iOS";
#endif
            }
        }

        #endregion

        #region METHODS

        /// <summary>
        /// Initialize using resources
        /// </summary>
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        static void Initialize()
        {
            allResources.AddRange(Resources.LoadAll(""));
            ResourcesLoaded = true;
            OnResourcesLoaded?.Invoke();
        }

        /// <summary>
        /// Start this object
        /// </summary>
        /// <returns></returns>
        IEnumerator Start()
        {
            if (!useAssetBundles)
            {
                AssetsReady = true;
                OnAssetsLoaded?.Invoke();
            }
            else
            {
                if (forceClearCache)
                    Caching.ClearCache();

                //Create task
                TaskObserver.RegisterTask(this);

                //Update
                yield return LoadUpdateLists();
                yield return LoadManifest();
                yield return UpdateAssets();

                //Save server list since we are now synced
                SaveAssetBundleList(localUpdateList);

                //Finished
                OnAssetsLoaded?.Invoke();
                OnAssetsLoaded = null;
                AssetsReady = true;

                //Clear tasks
                CompletedTasks = TotalTasks;
                TaskObserver.DeregisterTask(this);
            }
        }

        /// <summary>
        /// Invoke when all assets are loaded
        /// </summary>
        /// <param name="action"></param>
        public static void InvokeOnResourcesLoaded(System.Action action)
        {
            if (action == null)
                Debug.LogError("Can't invoke an empty action");

            if (ResourcesLoaded)
                action();
            else
                OnResourcesLoaded += action;
        }

        /// <summary>
        /// Invoke when all assets are loaded
        /// </summary>
        /// <param name="action"></param>
        public static void InvokeOnAssetsLoaded(System.Action action)
        {
            if (action == null)
                Debug.LogError("Can't invoke an empty action");

            if (AssetsReady)
                action();
            else
                OnAssetsLoaded += action;
        }

        /// <summary>
        /// Find all objects that match the selected type in all asset bundles
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static IEnumerable<Object> LoadAll<T>() where T : Object
        {
            return allResources.Where(obj => obj is T).Concat(allContent.Where(content => content is T));
        }

        /// <summary>
        /// Find all objects that match the selected type in all asset bundles
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static IEnumerable<Object> LoadAllContent<T>() where T : Object
        {
            return allContent.Where(obj => obj is T);
        }

        /// <summary>
        /// Find all objects that match the selected type in all asset bundles
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static IEnumerable<Object> LoadAllResources<T>() where T : Object
        {
            return allResources.Where(obj => obj is T);
        }

        /// <summary>
        /// Add assets to the content
        /// </summary>
        /// <param name="objs"></param>
        public static void AddAssets(List<Object> objs)
        {
            allContent.AddRange(objs);
        }

        /// <summary>
        /// Load in the patch list objects
        /// </summary>
        /// <returns></returns>
        private IEnumerator LoadUpdateLists()
        {
            TotalTasks += 2;
            TaskMessage = "Loading Asset List";

            //Local list
            string filePath = Path.Combine(AssetListDirectory, "AssetList.json");

            if (File.Exists(filePath))
            {
                string dataAsJson = File.ReadAllText(filePath);
                localUpdateList = JsonUtility.FromJson<AssetList>(dataAsJson);
            }
            else
            {
                Debug.Log("No LOCAL patch list exists, using default");
                localUpdateList = new AssetList();
            }

            CompletedTasks++;

            //Debug slow down
            if (debug)
                yield return new WaitForSeconds(1f);

            //Server list
            if (!forceOffline)
            {
                string listURL = Path.Combine(baseURL, BundleName, "AssetList.json");
                using (UnityWebRequest wwwList = UnityWebRequest.Get(listURL))
                {
                    yield return wwwList.SendWebRequest();

                    if (wwwList.isNetworkError || wwwList.isHttpError)
                    {
                        serverUpdateList = localUpdateList;
                        Debug.LogError("Could not access SERVER patch list. Reverting to local.\n" + listURL + "\n" + wwwList.error);
                    }
                    else
                    {
                        string jsonData = wwwList.downloadHandler.text;
                        serverUpdateList = JsonUtility.FromJson<AssetList>(jsonData);
                    }
                }
            }
            else
                serverUpdateList = localUpdateList;

            CompletedTasks++;

            //Debug slow down
            if (debug)
                yield return new WaitForSeconds(1f);
        }

        /// <summary>
        /// Load the manifest object
        /// </summary>
        /// <returns></returns>
        private IEnumerator LoadManifest()
        {
            TotalTasks += 2;
            TaskMessage = "Loading Manifest";

            //Server manifest
            if (!forceOffline)
            {
                string serverURL = Path.Combine(baseURL, BundleName, BundleName);
                using (UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(serverURL, serverUpdateList.manifestVersion, 0))
                {
                    yield return www.SendWebRequest();

                    if (www.isNetworkError || www.isHttpError)
                        Debug.LogError("Could not access SERVER manifest. Reverting to local.\n" + serverURL + "\n" + www.error);
                    else
                    {
                        manifest = DownloadHandlerAssetBundle.GetContent(www).LoadAsset<AssetBundleManifest>("AssetBundleManifest");
                        CompletedTasks += 2;

                        yield break;
                    }
                }
            }

            CompletedTasks++;

            //Debug slow down
            if (debug)
                yield return new WaitForSeconds(1f);

            //Local manifest (only reached if no server manifest access)
            string localURL = Path.Combine(AssetBundleDirectory, BundleName);

#if UNITY_IOS
            var request = AssetBundle.LoadFromFileAsync(localURL);
            
            yield return request;

            if (request.assetBundle == null)
                throw new System.Exception("Could not access LOCAL manifest at: " + localURL);
            else
                manifest = request.assetBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");            
#else
            using (UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(localURL, serverUpdateList.manifestVersion, 0))
            {
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                    throw new System.Exception("Could not access LOCAL manifest at: " + localURL + "\n" + www.error);
                else
                    manifest = DownloadHandlerAssetBundle.GetContent(www).LoadAsset<AssetBundleManifest>("AssetBundleManifest");
            }
#endif

            CompletedTasks++;

            //Debug slow down
            if (debug)
                yield return new WaitForSeconds(1f);
        }

        /// <summary>
        /// Update asset bundles with latest from server
        /// </summary>
        private IEnumerator UpdateAssets()
        {
            string[] paths = manifest.GetAllAssetBundles();
            TotalTasks += paths.Length;

            //Compare local vs server
            foreach (string path in paths)
                if (!patches.ContainsKey(path))
                    yield return LoadServerAssets(path);
        }

        /// <summary>
        /// Load asset bundle from the server
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private IEnumerator LoadServerAssets(string path)
        {
            AssetBundle bundle = null;
            AssetBundleInformation serverInfo = serverUpdateList.Find(path);
            TaskMessage = "Loading " + Path.GetFileName(path);

            //Server bundle
            if (!forceOffline)
            {
                string url = Path.Combine(baseURL, BundleName, path);

                using (UnityWebRequest serverRequest = UnityWebRequestAssetBundle.GetAssetBundle(url, serverInfo.version, 0))
                {
                    yield return serverRequest.SendWebRequest();

                    if (serverRequest.isNetworkError || serverRequest.isHttpError)
                        Debug.LogError("Could not access asset bundle. Reverting to LOCAL version.\n:" + path + "\n" + serverRequest.error);
                    else
                    {
                        bundle = DownloadHandlerAssetBundle.GetContent(serverRequest);
                        localUpdateList.SetVersion(path, serverInfo.version);
                        CompletedTasks++;
                    }
                }
            }

            //Debug slow down
            if (debug)
                yield return new WaitForSeconds(1f);

            //Local bundle only needed if we fail to get server
            if (bundle == null)
            {
                string url = Path.Combine(AssetBundleDirectory, path);

#if UNITY_IOS
                var request = AssetBundle.LoadFromFileAsync(url);
                yield return request;
                bundle = request.assetBundle;
#else
                using (UnityWebRequest localRequest = UnityWebRequestAssetBundle.GetAssetBundle(url, serverInfo.version, 0))
                {
                    yield return localRequest.SendWebRequest();
                    bundle = DownloadHandlerAssetBundle.GetContent(localRequest);
                    CompletedTasks++;
                }
#endif
            }

            //Load bundle dependencies
            patches.Add(bundle.name, bundle);
            string[] dependencies = manifest.GetAllDependencies(path);

            foreach (string dependency in dependencies)
                if (!patches.ContainsKey(dependency))
                    yield return LoadServerAssets(dependency);

            //Load actual objects (needs to have dependencies loaded first)
            HandleAssets(serverInfo, bundle);

            //Debug slow down
            if (debug)
                yield return new WaitForSeconds(0.25f);
        }

        /// <summary>
        /// Handle the given assets
        /// </summary>
        /// <param name="assets"></param>
        private void HandleAssets(AssetBundleInformation info, AssetBundle bundle)
        {
            //Patch
            if (info.variant == patchVariant)
            {
                IPatchable[] patches = bundle.LoadAllAssets<Object>().Where(item => item is IPatchable).Select(patch => patch as IPatchable).ToArray();

                foreach (IPatchable patch in patches)
                    patch.ApplyPatch();
            }
            //Content
            else
            {
                allContent.AddRange(bundle.LoadAllAssets());
            }
        }

        /// <summary>
        /// Create asset bundle list with latest build information
        /// </summary>
        private static void SaveAssetBundleList(AssetList list)
        {
            string filePath = Path.Combine(AssetListDirectory, "AssetList.json");
            string dataAsJson = JsonUtility.ToJson(list);
            File.WriteAllText(filePath, dataAsJson);
        }

#if UNITY_EDITOR

        /// <summary>
        /// Build all asset bundles menu item
        /// </summary>
        [MenuItem("Tools/Bubblegum/Build Asset List")]
        private static void BuildAssetList()
        {
            AssetBundle bundle = AssetBundle.LoadFromFile(Path.Combine(AssetBundleDirectory, BundleName));

            try
            {
                AssetBundleManifest manifest = bundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
                AssetList list = new AssetList(manifest);
                SaveAssetBundleList(list);
            }
            finally
            {
                bundle.Unload(true);
                AssetDatabase.Refresh();
            }
        }
#endif

        #endregion
    }
}