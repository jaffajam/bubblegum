﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Bubblegum
{

	/// <summary>
	/// A defining type class that must exist for only one prefab
	/// </summary>
	[CreateAssetMenu(menuName = "Scriptable Object/Prefab Connection")]
	public class PrefabConnection : Entity
	{
		#region PUBLIC_VARIABLES

		/// <summary>
		/// Get the prefab for this entity
		/// </summary>
		public GameObject Prefab
		{
			get
			{
				return prefab;
			}

			set
			{
				prefab = value;
			}
		}

		[Header("Prefab Connection")]

        /// <summary>
        /// The prefab that represents this object
        /// </summary>
        [SerializeField, Tooltip("The prefab that represents this object")]
        private GameObject prefab;

        /// <summary>
        /// The tags that we want to run on the prefab
        /// </summary>
        [SerializeField, HideInInspector]
        private List<Tag> tags = new List<Tag>();

#endregion // PUBLIC_VARIABLES

#region METHODS

		/// <summary>
		/// Get the object relating to the given key
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public static GameObject GetPrefab(int key)
		{
			return ((PrefabConnection)allKeys[key]).Prefab;
		}

        /// <summary>
        /// Add a new tag
        /// </summary>
        /// <param name="tag"></param>
        public void AddTag(Tag tag)
        {
            tags.Add(tag);

#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
#endif
        }

        /// <summary>
        /// Remove the tag
        /// </summary>
        /// <param name="tag"></param>
        public void RemoveTag(Tag tag)
        {
            tags.Remove(tag);

#if UNITY_EDITOR
            Selection.activeObject = this;
            DestroyImmediate(tag, true);
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
#endif
        }

        /// <summary>
        /// Run this objects tags
        /// </summary>
        public void RunTags()
		{
            RunTags(prefab);
		}

        /// <summary>
        /// Run this objects tags
        /// </summary>
        public void RunTags(GameObject gameObject)
        {
            if (debug)
                Debug.Log("Applying tags to " + gameObject.name);

            for (int i = 0; i < tags.Count; i++)
                tags[i].RunTag(this, gameObject);
        }

        #endregion
    }

#if UNITY_EDITOR

    /// <summary>
    /// Editor script for the prefab connection
    /// </summary>
    [CustomEditor(typeof(PrefabConnection))]
    public class PrefabConnectionEditor : Entity.EntityEditor
    {
        #region METHODS

        /// <summary>
        /// Draw the inspector
        /// </summary>
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Add Tag"))
            {
                Type[] tagTypes = AppDomain.CurrentDomain.GetAssemblies().Where(a => !a.IsDynamic).SelectMany(a => a.GetTypes()).Where(type => type.IsSubclassOf(typeof(Tag))).ToArray();

                GenericMenu menu = new GenericMenu();

                foreach (Type type in tagTypes)
                    menu.AddItem(new GUIContent(type.Name), false, OnTagSelected, type);

                menu.ShowAsContext();
            }
        }

        /// <summary>
        /// When a new tag is selected
        /// </summary>
        void OnTagSelected(object tagType)
        {
            Type type = (Type)tagType;
            Tag tag = CreateInstance(type) as Tag;
            tag.Initialize((PrefabConnection)target);
            ((PrefabConnection)target).AddTag(tag);
            AssetDatabase.AddObjectToAsset(tag, target);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        #endregion
    }

#endif
}