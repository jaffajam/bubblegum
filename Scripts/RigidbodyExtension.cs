﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Bubblegum
{

	[RequireComponent(typeof(Rigidbody))]

	/// <summary>
	/// Information type class that stores information about a rigidbody
	/// </summary>
	public class RigidbodyExtension : MonoBehaviour
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// Gets the rigid body.
		/// </summary>
		/// <value>The rigid body.</value>
		public Rigidbody RigidBody
		{
			get
			{
				if (!rBody)
					rBody = GetComponent<Rigidbody>();

				return rBody;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this object can locomote.
		/// </summary>
		/// <value><c>true</c> if can locomote; otherwise, <c>false</c>.</value>
		public bool CanMove { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this object can rotate.
		/// </summary>
		/// <value><c>true</c> if can rotate; otherwise, <c>false</c>.</value>
		public bool CanRotate { get; set; }

		/// <summary>
		/// Gets the speed of the object
		/// </summary>
		/// <value>The speed.</value>
		public float Speed { get; private set; }

		/// <summary>
		/// Velocity along local Z axis
		/// </summary>
		public float ForwardSpeed
		{
			get
			{
				return LocalVelocity.z;
			}
		}

		/// <summary>
		/// Gets the local velocity of the rigidbody
		/// </summary>
		/// <value>The local velocity.</value>
		public Vector3 LocalVelocity { get; private set; }

		/// <summary>
		/// The local angular velocity
		/// </summary>
		public Vector3 LocalAngularVelocity { get; private set; }

		/// <summary>
		/// The normalized velocity of the object
		/// </summary>
		public Vector3 NormalizedVelocity { get; private set; }

		/// <summary>
		/// The current forces applied to this object
		/// </summary>
		public Vector3 Acceleration { get; protected set; }

		/// <summary>
		/// The current local forces applied to this object
		/// </summary>
		public Vector3 LocalAcceleration { get; protected set; }

		/// <summary>
		/// The max speed of the object
		/// </summary>
		public float MaxSpeed { get { return maxSpeed; } set { maxSpeed = value; } }

		/// <summary>
		/// If we want to see debug info
		/// </summary>
		[SerializeField, Tooltip("If we want to see debug info")]
		protected bool debug;

		/// <summary>
		/// Can prevent weird rotation forces when using constraints
		/// </summary>
		[SerializeField, Tooltip("Can prevent weird rotation forces when using constraints")]
		private bool resetInertiaTensorRotation;

		/// <summary>
		/// If we should dynamically collect mass
		/// </summary>
		[SerializeField, Tooltip("If we should dynamically collect mass")]
		private bool collectMass;

		/// <summary>
		/// The center of mass
		/// </summary>
		[Tooltip("The center of mass")]
		[SerializeField]
		protected Vector3 centerOfMassOffset;

		/// <summary>
		/// If we should apply the max speed to the velocity
		/// </summary>
		[SerializeField, Tooltip("If we should apply the max speed to the velocity")]
		protected bool applyMaxSpeed;

		/// <summary>
		/// The max velocity to set
		/// </summary>
		[SerializeField, Tooltip("The max velocity to set")]
		protected float maxSpeed = float.MaxValue;

		#endregion // PUBLIC_VARIABLES

		#region PRIVATE_VARIABLES

		/// <summary>
		/// The attached rigidbody
		/// </summary>
		protected Rigidbody rBody;

		/// <summary>
		/// Defaul mass of the object
		/// </summary>
		protected float defaultMass;

		/// <summary>
		/// The velocity on the last frame
		/// </summary>
		protected Vector3 lastVelocity;

		/// <summary>
		/// All of the mass objects attached to us
		/// </summary>
		protected IMassObject[] massObjects;

		#endregion // PRIVATE_VARIABLES

		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Awake this instance.
		/// </summary>
		protected virtual void Awake()
		{
			defaultMass = RigidBody.mass;
			CanMove = true;
			CanRotate = true;

			SetCOM();
			FindMassObjects();

			if (resetInertiaTensorRotation)
				RigidBody.inertiaTensorRotation = Quaternion.identity;
		}

		/// <summary>
		/// Validate changes to this object
		/// </summary>
		protected virtual void OnValidate()
		{
			SetCOM();
		}

		/// <summary>
		/// Update with physics
		/// </summary>
		protected virtual void FixedUpdate()
		{
			Speed = RigidBody.velocity.magnitude;
			LocalVelocity = transform.InverseTransformVector(RigidBody.velocity);
			LocalAngularVelocity = transform.InverseTransformVector(rBody.angularVelocity);
			NormalizedVelocity = rBody.velocity.normalized;

			//Clamping
			if (NormalizedVelocity == Vector3.zero)
				NormalizedVelocity = transform.forward;

			if (applyMaxSpeed && Speed > maxSpeed)
				RigidBody.velocity = NormalizedVelocity * maxSpeed;

			//Mass
			if (collectMass)
			{
				float mass = defaultMass;

				for (int i = 0; i < massObjects.Length; i++)
					if (massObjects[i] != null)
						mass += massObjects[i].Mass;

				RigidBody.mass = mass;
			}

			//Forces
			Acceleration = (RigidBody.velocity - lastVelocity) / Time.deltaTime;
			LocalAcceleration = transform.InverseTransformVector(Acceleration);
			lastVelocity = RigidBody.velocity;
		}

		/// <summary>
		/// Used to draw info about the rigidbody
		/// </summary>
		protected virtual void OnDrawGizmos()
		{
			if (debug)
			{
				float sphereRadius = 0.2f;

				Gizmos.color = Color.yellow;
				Gizmos.DrawSphere(RigidBody.position + transform.TransformVector(RigidBody.centerOfMass), sphereRadius);
#if UNITY_EDITOR
				Handles.Label(RigidBody.position + transform.TransformVector(RigidBody.centerOfMass), "COM");
#endif
			}
		}

		#endregion // MONOBEHAVIOUR_METHODS

		#region PUBLIC_METHODS

		/// <summary>
		/// Restores the default COM
		/// </summary>
		public void RestoreDefaultCOM()
		{
#if UNITY_5_3_OR_NEWER
			RigidBody.ResetCenterOfMass();
#else
			RigidBody.centerOfMass = Vector3.zero;
#endif
		}

		/// <summary>
		/// Find all of the mass objects attached to us
		/// </summary>
		public void FindMassObjects()
		{
			if (collectMass)
				massObjects = GetComponentsInChildren<IMassObject>();
		}

		/// <summary>
		/// Sets the COM
		/// </summary>
		public void SetCOM()
		{
			RigidBody.centerOfMass = centerOfMassOffset;
		}

		/// <summary>
		/// Set COM to either default or preset value
		/// </summary>
		/// <param name="useDefault"></param>
		public void SetCOM(bool usePreset)
		{
			if (!usePreset)
				RestoreDefaultCOM();
			else
				SetCOM();
		}

		/// <summary>
		/// Removes the rigidbody.
		/// </summary>
		public void RemoveRigidbody()
		{
			Destroy(RigidBody);
		}

		/// <summary>
		/// Sets the Z velocity of the rigidbody
		/// </summary>
		/// <param name="velocity">Velocity.</param>
		public void SetLocalZVelocity(float velocity)
		{
			RigidBody.velocity = transform.TransformVector(new Vector3(RigidBody.velocity.x, RigidBody.velocity.y, velocity));
		}

		/// <summary>
		/// Sets the Y velocity of the rigidbody
		/// </summary>
		/// <param name="velocity">Velocity.</param>
		public void SetLocalYVelocity(float velocity)
		{
			RigidBody.velocity = transform.TransformVector(new Vector3(RigidBody.velocity.x, velocity, RigidBody.velocity.z));
		}

		/// <summary>
		/// Sets the local Y angular velocity.
		/// </summary>
		/// <param name="velocity">Velocity.</param>
		public void SetLocalYAngularVelocity(float velocity)
		{
			RigidBody.angularVelocity = transform.TransformVector(new Vector3(RigidBody.angularVelocity.x, velocity, RigidBody.angularVelocity.z));
		}

		/// <summary>
		/// Sets the local Y angular velocity.
		/// </summary>
		/// <param name="velocity">Velocity.</param>
		public void SetLocalXAngularVelocity(float velocity)
		{
			RigidBody.angularVelocity = transform.TransformVector(new Vector3(velocity, RigidBody.angularVelocity.y, RigidBody.angularVelocity.z));
		}

		/// <summary>
		/// Add a force along the local Z axis
		/// </summary>
		/// <param name="force"></param>
		public void AddForwardForce(float force)
		{
			RigidBody.AddRelativeForce(Vector3.forward * force);
		}

		/// <summary>
		/// Reset all constraints
		/// </summary>
		public void ResetConstraints()
		{
			RigidBody.constraints = RigidbodyConstraints.None;
			RigidBody.ResetInertiaTensor();
		}

		#endregion // PUBLIC_METHODS
	}
}