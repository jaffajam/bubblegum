﻿namespace Bubblegum
{

	/// <summary>
	/// An object that will want to be initialized before being used
	/// </summary>
	public interface IInitializable
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// If the object has been initialized
		/// </summary>
		bool Initialized { get; }

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Initialize the object
		/// </summary>
		void Initialize();

		#endregion
	}
}
