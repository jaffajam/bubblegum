﻿using UnityEngine;

namespace Bubblegum.Collections
{

	/// <summary>
	/// Contains a collection of objects
	/// </summary>
	public abstract class Collection<T> : ScriptableObject
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// Get the collection count
		/// </summary>
		public int Count
		{
			get
			{
				return collection.Length;
			}
		}

		/// <summary>
		/// Gets a random object from the collection
		/// </summary>
		public T Random
		{
			get
			{
				return collection[UnityEngine.Random.Range(0, collection.Length)];
			}
		}

		/// <summary>
		/// The audios to play
		/// </summary>
		[Tooltip("The audios to play")]
		[SerializeField]
		protected T[] collection;

		#endregion // PUBLIC_VARIABLES

		#region PUBLIC_METHODS

		/// <summary>
		/// Check whether the collections contains the object
		/// </summary>
		/// <param name="clip"></param>
		/// <returns></returns>
		public bool Contains(T obj)
		{
			for (int i = 0; i < collection.Length; i++)
			{
				if (collection[i].Equals(obj))
					return true;
			}

			return false;
		}

		/// <summary>
		/// Get the object at the index
		/// </summary>
		/// <param name="index"></param>
		public T Get(int index)
		{
			return collection[index % collection.Length];
		}

		/// <summary>
		/// Get the index of the given object
		/// </summary>
		/// <returns></returns>
		public int Index(T obj)
		{
			return System.Array.IndexOf(collection, obj);
		}

		/// <summary>
		/// Get a random object using the given random
		/// </summary>
		/// <param name="random"></param>
		/// <returns></returns>
		public T GetRandom(System.Random random)
		{
			return collection[random.Next(0, collection.Length)];
		}

		#endregion
	}
}