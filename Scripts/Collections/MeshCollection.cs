﻿using UnityEngine;
using Bubblegum.Collections;

namespace Bubblegum.Rendering.Meshes
{

	/// <summary>
	/// Contains a collection of meshes that can be used to set random meshes
	/// </summary>
	[CreateAssetMenu(menuName = "Scriptable Object/Collection/Mesh Collection")]
	public class MeshCollection : Collection<Mesh>
	{

		#region PUBLIC_METHODS

		/// <summary>
		/// Apply a random mesh to the object
		/// </summary>
		public void Apply(MeshFilter filter)
		{
			filter.mesh = Random;
		}

		#endregion
	}
}