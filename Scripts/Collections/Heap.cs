﻿using UnityEngine;
using System.Collections;
using System;

namespace Bubblegum.Collections
{
	/// <summary>
	/// Any item that goes into a heap must implement this interface
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface IHeapItem<T> : IComparable<T>
	{
		/// <summary>
		/// The index of the item in the heap
		/// </summary>
		int HeapIndex { get; set; }
	}

	/// <summary>
	/// A collection that sorts items in a tree like structure
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class Heap<T> where T : IHeapItem<T>
	{
		#region PUBLIC_VARIABLES

		/// <summary>
		/// Get the amount of items in the heap
		/// </summary>
		public int Count
		{
			get
			{
				return currentItemCount;
			}
		}

		#endregion

		#region PRIVATE_VARIABLES

		/// <summary>
		/// All of the items in the collection
		/// </summary>
		private T[] items;

		/// <summary>
		/// The amount of items in the collection
		/// </summary>
		private int currentItemCount;

		#endregion

		#region CONSTRUCTORS

		/// <summary>
		/// Create a new heap
		/// </summary>
		/// <param name="maxHeapSize"></param>
		public Heap(int maxHeapSize)
		{
			items = new T[maxHeapSize];
		}

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Add a new item to the heap
		/// </summary>
		/// <param name="item"></param>
		public void Add(T item)
		{
			item.HeapIndex = currentItemCount;
			items[currentItemCount] = item;
			SortUp(item);
			currentItemCount++;
		}

		/// <summary>
		/// Remove the first item in the heap array
		/// </summary>
		/// <returns></returns>
		public T RemoveFirst()
		{
			T firstItem = items[0];
			currentItemCount--;
			items[0] = items[currentItemCount];
			items[0].HeapIndex = 0;
			SortDown(items[0]);

			return firstItem;
		}

		/// <summary>
		/// Reposition the item in the array based on its comparable method
		/// </summary>
		/// <param name="item"></param>
		public void UpdateItem(T item)
		{
			SortUp(item);
		}

		/// <summary>
		/// Check if the item is in the heap
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public bool Contains(T item)
		{
			return Equals(items[item.HeapIndex], item);
		}

		#endregion

		#region PRIVATE_METHODS

		/// <summary>
		/// Sort elements in the heap starting from the item and moving down branches
		/// </summary>
		/// <param name="item"></param>
		private void SortDown(T item)
		{
			while (true)
			{
				int childIndexLeft = item.HeapIndex * 2 + 1;
				int childIndexRight = item.HeapIndex * 2 + 2;
				int swapIndex = 0;

				//If there is a child on the left
				if (childIndexLeft < currentItemCount)
				{
					swapIndex = childIndexLeft;

					//If there is a child on the right
					if (childIndexRight < currentItemCount)
						if (items[childIndexLeft].CompareTo(items[childIndexRight]) < 0)
							swapIndex = childIndexRight;

					//Swap the items
					if (item.CompareTo(items[swapIndex]) < 0)
						Swap(item, items[swapIndex]);
					else
						return;
				}
				else
					return;
			}
		}

		/// <summary>
		/// Sort the items by moving up the tree from the given item
		/// </summary>
		/// <param name="item"></param>
		private void SortUp(T item)
		{
			int parentIndex = (item.HeapIndex - 1) / 2;

			while (true)
			{
				T parentItem = items[parentIndex];

				//Swap items
				if (item.CompareTo(parentItem) > 0)
					Swap(item, parentItem);
				else
					break;

				//Next parent to check
				parentIndex = (item.HeapIndex - 1) / 2;
			}
		}

		/// <summary>
		/// Swap two items in the heap
		/// </summary>
		/// <param name="itemA"></param>
		/// <param name="itemB"></param>
		private void Swap(T itemA, T itemB)
		{
			//Swap
			items[itemA.HeapIndex] = itemB;
			items[itemB.HeapIndex] = itemA;

			//Swap indexes
			int itemAIndex = itemA.HeapIndex;
			itemA.HeapIndex = itemB.HeapIndex;
			itemB.HeapIndex = itemAIndex;
		}

		#endregion
	}
}