﻿using UnityEngine;

namespace Bubblegum.Collections
{

	/// <summary>
	/// Contains a collection of audio clips that can be used to play at random
	/// </summary>
	[CreateAssetMenu(menuName = "Scriptable Object/Collection/Texture Collection")]
	public class TextureCollection : Collection<Texture>
	{
		#region VARIABLES

		/// <summary>
		/// Used for setting property block values
		/// </summary>
		private const string MAIN_TEXTURE = "_MainTex";

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Apply a random texture
		/// </summary>
		/// <param name="renderer"></param>
		public void ApplyRandom(MeshRenderer renderer)
		{
			MaterialPropertyBlock block = new MaterialPropertyBlock();
			renderer.GetPropertyBlock(block);

			block.SetTexture(MAIN_TEXTURE, Random);
			renderer.SetPropertyBlock(block);
		}

		#endregion
	}
}