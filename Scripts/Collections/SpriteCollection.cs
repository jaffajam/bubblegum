﻿using UnityEngine;
using Bubblegum.Collections;

namespace Bubblegum.Rendering
{

	/// <summary>
	/// Applies a random sprite to an object from a collection of sprites
	/// </summary>
	[CreateAssetMenu (menuName = "Scriptable Object/Collection/Sprite Collection")]
	public class SpriteCollection : Collection<Sprite>
	{
		#region PUBLIC_METHODS

		/// <summary>
		/// Apply the information from the asset to the game object
		/// </summary>
		/// <param name="obj">Object.</param>
		public void Apply(SpriteRenderer renderer)
		{
			renderer.sprite = Random;
		}

		#endregion // PUBLIC_METHODS
	}
}