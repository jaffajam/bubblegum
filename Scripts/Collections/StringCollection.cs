﻿using UnityEngine;
using UnityEngine.UI;

namespace Bubblegum.Collections
{

	/// <summary>
	/// A collection of strings
	/// </summary>
	[CreateAssetMenu(menuName = "Scriptable Object/Collection/String Collection")]
	public class StringCollection : Collection<string>
	{
		#region MONOBEHAVIOUR_METHODS

		/// <summary>
		/// Apply to the object
		/// </summary>
		/// <param name="obj"></param>
		public void Apply(GameObject obj)
		{
			obj.name = Random;
		}

        /// <summary>
        /// Apply to the UI Text
        /// </summary>
        public void Apply(Text text)
        {
            text.text = Random;
        }

		#endregion // MONOBEHAVIOUR_METHODS
	}
}