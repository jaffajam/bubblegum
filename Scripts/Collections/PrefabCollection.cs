﻿using UnityEngine;

namespace Bubblegum.Collections
{

	/// <summary>
	/// A collection of gameobjects to randomly choose from
	/// </summary>
	[CreateAssetMenu(menuName = "Scriptable Object/Collection/Prefab Collection")]
	public class PrefabCollection : Collection<GameObject>
	{
		#region PUBLIC_METHODS

		/// <summary>
		/// Apply the collection to the given objectg
		/// </summary>
		/// <param name="obj"></param>
		public void Apply(GameObject obj)
		{
			IPrefabReferencer referencer = obj.GetComponentInChildren<IPrefabReferencer>(true);
			referencer.Prefab = Random;
		}

		#endregion
	}
}
