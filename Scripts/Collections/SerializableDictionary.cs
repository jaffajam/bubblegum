﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace Bubblegum.Collections
{
	/// <summary>
	/// Dictionary wrapper that can be serialized
	/// </summary>
	[Serializable]
	public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, ISerializationCallbackReceiver
	{
		#region VARIABLES

		/// <summary>
		/// The stored keys
		/// </summary>
		[SerializeField]
		private List<TKey> keys = new List<TKey>();

		/// <summary>
		/// The stored values
		/// </summary>
		[SerializeField]
		private List<TValue> values = new List<TValue>();

		#endregion

		#region METHODS

		/// <summary>
		/// Save dictionary into lists
		/// </summary>
		public void OnBeforeSerialize()
		{
			keys.Clear();
			values.Clear();
			foreach (KeyValuePair<TKey, TValue> pair in this)
			{
				keys.Add(pair.Key);
				values.Add(pair.Value);
			}
		}

		/// <summary>
		/// Load dictionary from lists
		/// </summary>
		public void OnAfterDeserialize()
		{
			Clear();

			if (keys.Count != values.Count)
				throw new System.Exception(string.Format("there are {0} keys and {1} values after deserialization. Make sure that both key and value types are serializable.", keys.Count, values.Count));

			for (int i = 0; i < keys.Count; i++)
				Add(keys[i], values[i]);
		}

		#endregion
	}
}