﻿using UnityEngine;

namespace Bubblegum.Audio
{

	/// <summary>
	/// Contains a collection of audio clips that can be used to play at random
	/// </summary>
	[CreateAssetMenu(menuName = "Scriptable Object/Collection/Audio Collection")]
	public class AudioCollection : Collections.Collection<AudioClip>
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// Get the audio setting
		/// </summary>
		public AudioSource Setting
		{
			get
			{
				return setting;
			}
		}

		/// <summary>
		/// The setting used to play these audios
		/// </summary>
		[SerializeField, Tooltip("The setting used to play these audios")]
		private AudioSource setting;

		#endregion // PUBLIC_VARIABLES

		#region PUBLIC_METHODS

        /// <summary>
        /// Play clip with given index
        /// </summary>
        /// <param name="index"></param>
        public void PlayClip(int index)
        {
            AudioManager.Instance.PlayWorldAudio(collection[index % Count], Vector3.zero, setting);
        }

        /// <summary>
        /// Play a random clip from the collection
        /// </summary>
        public void PlayRandomClip()
		{
			AudioManager.Instance.PlayWorldAudio(Random, Vector3.zero, setting);
		}

		/// <summary>
		/// Play a random clip from the collection
		/// </summary>
		public void PlayRandomWorldClip(Transform position)
		{
			AudioManager.Instance.PlayWorldAudio(Random, position.position, setting);
		}

		/// <summary>
		/// Play a random clip from the collection
		/// </summary>
		public void PlayRandomLocalClip(Transform transform)
		{
			AudioManager.Instance.PlayWorldAudio(Random, transform.position, setting);
		}

		#endregion
	}
}