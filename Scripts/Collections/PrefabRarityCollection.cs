﻿using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Bubblegum.Collections
{
	/// <summary>
	/// A collection of gameobjects to randomly choose from
	/// </summary>
	[CreateAssetMenu(menuName = "Scriptable Object/Collection/Prefab Rarity Collection")]
	public class PrefabRarityCollection : PrefabCollection
	{
		#region VARIABLES

		/// <summary>
		/// All of the prefabs to populate
		/// </summary>
		[SerializeField, Tooltip("All of the prefabs to populate")]
		private PrefabRarity[] prefabRarity = new PrefabRarity[0];

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Populate collection
		/// </summary>
		public void Populate()
		{
			List<GameObject> prefabs = new List<GameObject>();

			foreach (PrefabRarity rarity in prefabRarity)
			{
				if (!Application.isPlaying)
					rarity.name = rarity.prefab.name;

				for (int i = 0; i < rarity.rarity; i++)
					prefabs.Add(rarity.prefab);
			}

			collection = prefabs.ToArray();

#if UNITY_EDITOR
			if (!Application.isPlaying)
				EditorUtility.SetDirty(this);
#endif
		}

		#endregion
	}

	/// <summary>
	/// Prefab rarity
	/// </summary>
	[System.Serializable]
	public class PrefabRarity
	{
		#region VARIABLES

		/// <summary>
		/// Name of the prefab
		/// </summary>
		[HideInInspector]
		public string name;

		/// <summary>
		/// Rarity of the prefab
		/// </summary>
		[Range(1, 100)]
		public int rarity = 1;

		/// <summary>
		/// The target prefab
		/// </summary>
		public GameObject prefab;

		#endregion
	}

#if UNITY_EDITOR

	/// <summary>
	/// Editor script
	/// </summary>
	[CustomEditor(typeof(PrefabRarityCollection))]
	public class PrefabRarityCollectionEditor : Editor
	{
		#region METHODS

		/// <summary>
		/// Draw inspector
		/// </summary>
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			if (GUILayout.Button("Populate"))
				((PrefabRarityCollection)target).Populate();
		}

		#endregion
	}

#endif
}
