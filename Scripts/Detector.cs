﻿using System;
using UnityEngine;

namespace Bubblegum
{

	/// <summary>
	/// Object that is used to detect other objects
	/// </summary>
	public class Detector : MonoBehaviour
	{
		#region PUBLIC_VARIABLES

		/// <summary>
		/// The detections we have found
		/// </summary>
		public Collider[] Detections { get; private set; }

		/// <summary>
		/// Get or set the detection count
		/// </summary>
		public int DetectionCount { get; private set; }

		/// <summary>
		/// The mode that we want to use
		/// </summary>
		[SerializeField, Tooltip("The mode that we want to use")]
		private CheckType mode;

		/// <summary>
		/// The layers to make the explosion hit
		/// </summary>
		[SerializeField, Tooltip("The layers to make the explosion hit")]
		private LayerMask layermask;

		/// <summary>
		/// The radius of the explosion
		/// </summary>
		[SerializeField, Tooltip("The radius of the explosion")]
		public float radius = 10f;

		/// <summary>
		/// The max detections that we want to look for
		/// </summary>
		[SerializeField, Tooltip("The max detections that we want to look for")]
		protected int maxDetections = 10;

		/// <summary>
		/// When we detect an object
		/// </summary>
		[SerializeField, Tooltip("When we detect an object")]
		public Action onDetect;

		#endregion
		#region PRIVATE_VARIABLES

		#endregion

		#region ENUMS

		/// <summary>
		/// The check types to select between
		/// </summary>
		public enum CheckType { OVERLAP_SPHERE }

		#endregion

		#region METHODS

		/// <summary>
		/// Awaken this object
		/// </summary>
		protected virtual void Awake()
		{
			Detections = new Collider[maxDetections];
		}

		/// <summary>
		/// Perform a check for this combo
		/// </summary>
		public virtual void Check()
		{
			bool detected = false;

			switch (mode)
			{
				case CheckType.OVERLAP_SPHERE:
					detected = (DetectionCount = Physics.OverlapSphereNonAlloc(transform.position, radius, Detections, layermask, QueryTriggerInteraction.UseGlobal)) > 0;
					break;
			}

			if (detected && onDetect != null)
				onDetect();
		}

		#endregion
	}
}