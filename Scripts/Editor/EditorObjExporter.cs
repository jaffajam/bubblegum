﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;

namespace Bubblegum.EditorUtils
{

	struct ObjMaterial
	{
		public string name;
		public string textureName;
	}

	/// <summary>
	/// Taken from Unify http://wiki.unity3d.com/index.php?title=ObjExporter
	/// </summary>
	public class EditorObjExporter : ScriptableObject
	{
		private static int vertexOffset = 0;
		private static int normalOffset = 0;
		private static int uvOffset = 0;
		private static string targetFolder = "Assets/Models/Exported";
        private static MeshFilter[] selection;

		private static string MeshToString(Mesh mesh, Transform transform, Dictionary<string, ObjMaterial> materialList)
		{
			Material[] mats = transform.GetComponent<Renderer>().sharedMaterials;

			StringBuilder sb = new StringBuilder();
			sb.Append("g ").Append(mesh.name).Append("\n");

			foreach (Vector3 lv in mesh.vertices)
			{
				Vector3 wv = transform.TransformPoint(lv);

				//This is sort of ugly - inverting x-component since we're in
				//a different coordinate system than "everyone" is "used to".
				sb.Append(string.Format("v {0} {1} {2}\n", -wv.x, wv.y, wv.z));
			}

			sb.Append("\n");

			foreach (Vector3 lv in mesh.normals)
			{
				Vector3 wv = transform.TransformDirection(lv);

				sb.Append(string.Format("vn {0} {1} {2}\n", -wv.x, wv.y, wv.z));
			}

			sb.Append("\n");

			foreach (Vector3 v in mesh.uv)
				sb.Append(string.Format("vt {0} {1}\n", v.x, v.y));

			for (int material = 0; material < mesh.subMeshCount; material++)
			{
				sb.Append("\n");
				sb.Append("usemtl ").Append(mats[material].name).Append("\n");
				sb.Append("usemap ").Append(mats[material].name).Append("\n");

				//See if this material is already in the materiallist.
				try
				{
					ObjMaterial objMaterial = new ObjMaterial();

					objMaterial.name = mats[material].name;

					if (mats[material].mainTexture)
						objMaterial.textureName = AssetDatabase.GetAssetPath(mats[material].mainTexture);
					else
						objMaterial.textureName = null;

					materialList.Add(objMaterial.name, objMaterial);
				}
				catch (ArgumentException)
				{
					//Already in the dictionary
				}

				int[] triangles = mesh.GetTriangles(material);

				for (int i = 0; i < triangles.Length; i += 3)
				{
					//Because we inverted the x-component, we also needed to alter the triangle winding.
					sb.Append(string.Format("f {1}/{1}/{1} {0}/{0}/{0} {2}/{2}/{2}\n",
						triangles[i] + 1 + vertexOffset, triangles[i + 1] + 1 + normalOffset, triangles[i + 2] + 1 + uvOffset));
				}
			}

			vertexOffset += mesh.vertices.Length;
			normalOffset += mesh.normals.Length;
			uvOffset += mesh.uv.Length;

			return sb.ToString();
		}

		private static void Clear()
		{
			vertexOffset = 0;
			normalOffset = 0;
			uvOffset = 0;
		}

		private static Dictionary<string, ObjMaterial> PrepareFileWrite()
		{
			Clear();

			return new Dictionary<string, ObjMaterial>();
		}

		private static void MaterialsToFile(Dictionary<string, ObjMaterial> materialList, string folder, string filename)
		{
			using (StreamWriter sw = new StreamWriter(Path.Combine(folder, filename + ".mtl")))
			{
				foreach (KeyValuePair<string, ObjMaterial> kvp in materialList)
				{
					sw.Write("\n");
					sw.Write("newmtl {0}\n", kvp.Key);
					sw.Write("Ka  0.6 0.6 0.6\n");
					sw.Write("Kd  0.6 0.6 0.6\n");
					sw.Write("Ks  0.9 0.9 0.9\n");
					sw.Write("d  1.0\n");
					sw.Write("Ns  0.0\n");
					sw.Write("illum 2\n");

					if (kvp.Value.textureName != null)
					{
						string destinationFile = kvp.Value.textureName;


						int stripIndex = destinationFile.LastIndexOf(Path.PathSeparator);

						if (stripIndex >= 0)
							destinationFile = destinationFile.Substring(stripIndex + 1).Trim();


						string relativeFile = destinationFile;

						destinationFile = folder + Path.PathSeparator + destinationFile;

						Debug.Log("Copying texture from " + kvp.Value.textureName + " to " + destinationFile);

						try
						{
							//Copy the source file
							File.Copy(kvp.Value.textureName, destinationFile);
						}
						catch
						{

						}

						sw.Write("map_Kd {0}", relativeFile);
					}

					sw.Write("\n\n\n");
				}
			}
		}

		private static void MeshToFile(Mesh mesh, Transform transform, string folder, string filename)
		{
            Vector3 defaultPosition = transform.position;
            Quaternion defaultRotation = transform.rotation;
            transform.position = Vector3.zero;
            transform.rotation = Quaternion.identity;

			Dictionary<string, ObjMaterial> materialList = PrepareFileWrite();

            using (StreamWriter sw = new StreamWriter(Path.Combine(folder, filename + ".obj")))
            {
                sw.Write("mtllib ./" + filename + ".mtl\n");
                sw.Write(MeshToString(mesh, transform, materialList));
            }

			MaterialsToFile(materialList, folder, filename);
            transform.position = defaultPosition;
            transform.rotation = defaultRotation;
        }

		public static void MeshesToFile(Mesh[] meshes, Transform[] transforms, string folder, string filename)
		{
			Dictionary<string, ObjMaterial> materialList = PrepareFileWrite();

            using (StreamWriter sw = new StreamWriter(Path.Combine(folder, filename + ".obj")))
            {
                sw.Write("mtllib ./" + filename + ".mtl\n");

                for (int i = 0; i < meshes.Length; i++)
                {
                    Vector3 defaultPosition = transforms[i].position;
                    Quaternion defaultRotation = transforms[i].rotation;
                    transforms[i].position = Vector3.zero;
                    transforms[i].rotation = Quaternion.identity;

                    sw.Write(MeshToString(meshes[i], transforms[i], materialList));

                    transforms[i].position = defaultPosition;
                    transforms[i].rotation = defaultRotation;
                }
            }

            MaterialsToFile(materialList, folder, filename);
		}

		private static bool CreateTargetFolder()
		{
			try
			{
				System.IO.Directory.CreateDirectory(targetFolder);
			}
			catch
			{
				EditorUtility.DisplayDialog("Error!", "Failed to create target folder!", "Ok");
				return false;
			}

			return true;
		}

        private static bool GetSelection()
        {
            selection = Selection.GetTransforms(SelectionMode.Editable | SelectionMode.ExcludePrefab).GetComponents<MeshFilter>();

            if (selection.Length == 0)
            {
                EditorUtility.DisplayDialog("No objects selected!", "Please select one or more target objects", "OK");
                return false;
            }

            return true;
        }

		[MenuItem("Tools/Bubblegum/Mesh/Export selection to seperate Obj")]
		public static void ExportSelectionToSeparate()
		{
			if (!CreateTargetFolder() || !GetSelection())
				return;

			for (int i = 0; i < selection.Length; i++)
			{
				MeshFilter[] meshfilters = selection[i].GetComponentsInChildren<MeshFilter>();

				for (int m = 0; m < meshfilters.Length; m++)
					MeshToFile(meshfilters[m].sharedMesh, meshfilters[m].transform, targetFolder, selection[i].name);
			}

            EditorUtility.DisplayDialog("Objects exported", "Exported to " + targetFolder, "Ok");
            AssetDatabase.Refresh();
        }

        [MenuItem("Tools/Bubblegum/Mesh/Export selection to combined Obj")]
        public static void ExportSelectionToCombined()
        {
            if (!CreateTargetFolder() || !GetSelection())
                return;

            Mesh mesh = new Mesh();
            GameObject temp = new GameObject("Temp");
            temp.AddComponent<MeshRenderer>().sharedMaterial = selection[0].GetComponent<Renderer>().sharedMaterial;
            mesh.Combine(selection.Select(filter => filter.sharedMesh).ToArray(), selection.Select(filter => filter.transform).ToArray());
            MeshToFile(mesh, temp.transform, targetFolder, selection[0].name);
            DestroyImmediate(temp, true);

            EditorUtility.DisplayDialog("Objects exported", "Exported to " + targetFolder, "Ok");
            AssetDatabase.Refresh();
        }

        [MenuItem("Tools/Bubblegum/Mesh/Export selection to single Obj")]
		public static void ExportWholeSelectionToSingle()
		{
			if (!CreateTargetFolder() || !GetSelection())
				return;

            string filename = selection[0].name;
			int stripIndex = filename.LastIndexOf(Path.PathSeparator);

			if (stripIndex >= 0)
				filename = filename.Substring(stripIndex + 1).Trim();

			MeshesToFile(selection.Select(filter => filter.sharedMesh).ToArray(), selection.Select(filter => filter.transform).ToArray(), targetFolder, filename);
            EditorUtility.DisplayDialog("Objects exported", "Exported " + selection.Length + " objects to " + targetFolder, "Ok");
            AssetDatabase.Refresh();            
        }
    }
}