﻿using System.Linq;
using System.IO;
using UnityEngine;
using UnityEditor;

namespace Bubblegum.EditorUtils
{
    /// <summary>
    /// Create a texture 2d array
    /// </summary>
    public class CreateTexture2DArray
    {
        #region VARIABLES

        /// <summary>
        /// Menu name in editor
        /// </summary>
        private const string MENU_NAME = "Tools/Bubblegum/Create Texture2DArray From Selection";

        #endregion

        #region METHODS

        /// <summary>
        /// Create a texture 2D array from selected objects
        /// </summary>
        [MenuItem(MENU_NAME)]
        static void CreateTexture2DArrayFromSelection()
        {
            int index = 0;
            Texture2D mainTex = Selection.objects.FirstOrDefault(item => item.GetType() == typeof(Texture2D)) as Texture2D;
            Texture2DArray textureArray = new Texture2DArray(mainTex.width, mainTex.height, Selection.objects.Count(item => item.GetType() == typeof(Texture2D)), mainTex.format, false);

            foreach (Texture2D texture in Selection.objects.Where(item => item.GetType() == typeof(Texture2D)))
            {
                Graphics.CopyTexture(texture, 0, 0, textureArray, index, 0);
                index++;
            }

            AssetDatabase.CreateAsset(textureArray, AssetDatabase.GenerateUniqueAssetPath(GetPath() + "/New Texture2D.asset"));
        }

        /// <summary>
        /// Validates the menu.
        /// </summary>
        /// <remarks>The item will be disabled if no game object is selected.</remarks>
        [MenuItem(MENU_NAME, true)]
        static bool ValidateCreatePrefab()
        {
            return Selection.objects != null && Selection.objects.FirstOrDefault(item => item.GetType() == typeof(Texture2D)) != null;
        }

        /// <summary>
        /// Get current directory
        /// </summary>
        /// <returns></returns>
        static string GetPath()
        {
            string path = AssetDatabase.GetAssetPath(Selection.activeObject);

            if (!string.IsNullOrEmpty(Path.GetExtension(path)))
                path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
            else
                path = "Assets";

            return path;
        }

        #endregion
    }
}