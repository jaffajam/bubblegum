﻿using UnityEditor;

namespace Bubblegum.EditorUtils
{
    /// <summary>
    // Control automatic import of materials for models
    /// </summary>
    [InitializeOnLoad]
    public class MaterialImportInitialize
    {
        /// <summary>
        /// If material import should be enabled
        /// </summary>
        public static bool AllowImportMaterials { get; private set; }

        /// <summary>
        /// Initailize the class
        /// </summary>
        static MaterialImportInitialize()
        {
            AllowImportMaterials = EditorPrefs.GetBool("AllowImportMaterials", false);
        }

        /// <summary>
        /// Enable material import
        /// </summary>
        [MenuItem("Tools/Bubblegum/Models/Enable Material Import")]
        private static void Enable()
        {
            SetEnabled(true);
        }

        /// <summary>
        /// Validate material import button
        /// </summary>
        /// <returns></returns>
        [MenuItem("Tools/Bubblegum/Models/Enable Material Import", true)]
        private static bool EnableValidate()
        {
            return !AllowImportMaterials;
        }

        /// <summary>
        /// Disable material import
        /// </summary>
        [MenuItem("Tools/Bubblegum/Models/Disable Material Import")]
        private static void Disable()
        {
            SetEnabled(false);
        }

        /// <summary>
        /// Enable material import button
        /// </summary>
        /// <returns></returns>
        [MenuItem("Tools/Bubblegum/Models/Disable Material Import", true)]
        private static bool DisableValidate()
        {
            return AllowImportMaterials;
        }

        /// <summary>
        /// Set import enabled
        /// </summary>
        /// <param name="enabled"></param>
        private static void SetEnabled(bool enabled)
        {
            AllowImportMaterials = enabled;
            EditorPrefs.SetBool("AllowImportMaterials", AllowImportMaterials);
        }
    }

	/// <summary>
	/// Disables importing of materials on all models, delete this file to disable
	/// </summary>
	public class DontImportMaterials : AssetPostprocessor
	{
        /// <summary>
        /// Before assets are processed
        /// </summary>
		public void OnPreprocessModel()
		{
			ModelImporter modelImporter = (ModelImporter)assetImporter;
			modelImporter.materialImportMode = MaterialImportInitialize.AllowImportMaterials ? modelImporter.materialImportMode : ModelImporterMaterialImportMode.None;
		}
	}
}