﻿using System;
using System.Reflection;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEditor;

namespace Bubblegum.EditorUtils
{
    /// <summary>
    /// Used to update the packages
    /// </summary>
    public class PackageUpdater
    {
#if UNITY_2020_1_OR_NEWER
        /// <summary>
        /// Path to the package manifest json
        /// </summary>
        private static string PackageManifestPath = Application.dataPath + "/../Packages/packages-lock.json";

        /// <summary>
        /// Regex pattern
        /// </summary>
        private static string RegexPattern = "(,(\\n|\\r|\\r\\n)(.+?)\"{0}\": {{((\\n|\\r|\\r\\n)(.+?))*hash(.+?)((\\n|\\r|\\r\\n)(.+?))*}})|((\\n|\\r|\\r\\n)(.+?)\"{0}\": {{((\\n|\\r|\\r\\n)(.+?))*hash(.+?)((\\n|\\r|\\r\\n)(.+?))*}},?)";
#else
        /// <summary>
        /// Path to the package manifest json
        /// </summary>
        private static string PackageManifestPath = Application.dataPath + "/../Packages/manifest.json";

        /// <summary>
        /// Regex pattern
        /// </summary>
        private static string RegexPattern = "(,(\\n|\\r|\\r\\n)(.+?)\"{0}\": {{((\\n|\\r|\\r\\n)(.+?))*}})|((\\n|\\r|\\r\\n)(.+?)\"{0}\": {{((\\n|\\r|\\r\\n)(.+?))*}},?)";
#endif


        /// <summary>
        /// Wait counter before refresh assets
        /// </summary>
        private static int waitCounter;

        /// <summary>
        /// Update the package to latest
        /// </summary>
        [MenuItem("Assets/Update Package")]
        private static void UpdatePackage()
        {
            var packageName = GetCurrentSelectedObjectName();
            var packageManifest = GetPackageManifest();
            var pattern = string.Format(RegexPattern, packageName.Replace(".", "\\."));
            var newPackageManifest = Regex.Replace(packageManifest, pattern, "");

            using (StreamWriter writer = new StreamWriter(PackageManifestPath, false))
            {
                writer.Write(newPackageManifest);
                waitCounter = 0;
                EditorApplication.update += OnUpdate;
            }
        }

        /// <summary>
        /// Validate that the package exists in the package manifest
        /// </summary>
        [MenuItem("Assets/Update Package", true)]
        private static bool ValidatePackageExist()
        {
            var packageManifest = GetPackageManifest();
            var packageName = GetCurrentSelectedObjectName();
            var pattern = string.Format(RegexPattern, packageName.Replace(".", "\\."));

            return Regex.IsMatch(packageManifest, pattern);
        }

        /// <summary>
        /// Get current selected object name
        /// </summary>
        private static string GetCurrentSelectedObjectName()
        {
            Type projectBrowserType = Type.GetType("UnityEditor.ProjectBrowser,UnityEditor");
            if (projectBrowserType != null)
            {
                FieldInfo lastProjectBrowser = projectBrowserType.GetField("s_LastInteractedProjectBrowser", BindingFlags.Static | BindingFlags.Public);
                if (lastProjectBrowser != null)
                {
                    object lastProjectBrowserInstance = lastProjectBrowser.GetValue(null);
                    FieldInfo projectBrowserViewMode = projectBrowserType.GetField("m_ViewMode", BindingFlags.Instance | BindingFlags.NonPublic);
                    if (projectBrowserViewMode != null)
                    {
                        // 0 - one column, 1 - two column
                        int viewMode = (int)projectBrowserViewMode.GetValue(lastProjectBrowserInstance);
                        if (viewMode == 1)
                        {
                            FieldInfo lastFoldersFieldInfo = projectBrowserType.GetField("m_LastFolders", BindingFlags.Instance | BindingFlags.NonPublic);
                            var lastFolders = lastFoldersFieldInfo.GetValue(lastProjectBrowserInstance) as string[];

                            if (lastFolders != null && lastFolders.Length > 0)
                            {
                                var splitedFolderPath = lastFolders[0].Split('/');
                                var name = splitedFolderPath[splitedFolderPath.Length - 1];

                                if (name == "Packages")
                                    return Selection.activeObject.name;
                                else
                                    return splitedFolderPath[splitedFolderPath.Length - 1];
                            }
                        }
                    }
                }
            }

            return Selection.activeObject?.name;
        }

        /// <summary>
        /// Get package manifest
        /// </summary>
        /// <returns></returns>
        private static string GetPackageManifest()
        {
            if (!File.Exists(PackageManifestPath))
                return null;

            string manifestJson;

            using (StreamReader reader = new StreamReader(PackageManifestPath))
                manifestJson = reader.ReadToEnd();

            return manifestJson;
        }

        /// <summary>
        /// On editor update
        /// </summary>
        private static void OnUpdate()
        {
            while (waitCounter < 10)
                waitCounter++;

            EditorApplication.update -= OnUpdate;
            AssetDatabase.Refresh();
        }
    }
}