﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace Bubblegum.Animations
{

	/// <summary>
	/// Animation clip target renamer.
	/// This script allows animation curves to be moved from one target to another.
	/// </summary>
	public class AnimationReferences : EditorWindow
	{

		#region PUBLIC_VARIABLES

		/// <summary>
		/// The selected clip to edit
		/// </summary>
		public AnimationClip selectedClip;

		/// <summary>
		/// The curve data for the animation.
		/// </summary>
		private EditorCurveBinding[] curveDatas;

		/// <summary>
		/// The names of the original GameObjects.
		/// </summary>
		private List<string> origObjectPaths;

		/// <summary>
		/// The names of the target GameObjects.
		/// </summary>
		private List<string> targetObjectPaths;

		/// <summary>
		/// If the animation is initialized
		/// </summary>
		private bool initialized;

		#endregion

		#region WINDOW_METHODS

		/// <summary>
		/// Open a new animation window
		/// </summary>
		[MenuItem("Window/Bubblegum/Animation References")]
		public static void OpenWindow()
		{
			AnimationReferences renamer = GetWindow<AnimationReferences>("Animation References");
			renamer.Clear();
		}

		/// <summary>
		/// Show lists
		/// </summary>
		void OnGUIShowTargetsList()
		{
			// if we got here, we have all the data we need to work with,
			// so we should be able to build the UI.

			// build the list of textboxes for renaming.
			if (targetObjectPaths != null)
			{
				EditorGUILayout.Space();
				EditorGUIUtility.labelWidth = 250;

				for (int i = 0; i < targetObjectPaths.Count; i++)
				{
					string newName = EditorGUILayout.TextField(origObjectPaths[i], targetObjectPaths[i]);

					if (targetObjectPaths[i] != newName)
						targetObjectPaths[i] = newName;
				}
			}
		}

		/// <summary>
		/// Draw stuff
		/// </summary>
		void OnGUI()
		{
			AnimationClip previous = selectedClip;
			selectedClip = EditorGUILayout.ObjectField("Animation Clip", selectedClip, typeof(AnimationClip), true) as AnimationClip;

			if (selectedClip != previous)
				Clear();

			if (selectedClip != null)
			{
				if (!initialized)
					Initialize();

				EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();

				if (GUILayout.Button("Refresh"))
				{
					Clear();
					Initialize();
				}

				EditorGUILayout.EndHorizontal();

				OnGUIShowTargetsList();

				EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();

				if (GUILayout.Button("Apply"))
					RenameTargets();

				EditorGUILayout.EndHorizontal();
			}
		}

		#endregion

		#region PRIVATE_METHODS

		/// <summary>
		/// Initialize this window
		/// </summary>
		private void Initialize()
		{
			curveDatas = AnimationUtility.GetCurveBindings(selectedClip);

			origObjectPaths = new List<string>();
			targetObjectPaths = new List<string>();

			foreach (EditorCurveBinding curveData in curveDatas)
			{
				if (curveData.path != "" && !origObjectPaths.Contains(curveData.path))
				{
					origObjectPaths.Add(curveData.path);
					targetObjectPaths.Add(curveData.path);
				}
			}

			initialized = true;
		}

		/// <summary>
		/// Clear all data
		/// </summary>
		private void Clear()
		{
			curveDatas = null;
			origObjectPaths = null;
			targetObjectPaths = null;
			initialized = false;
		}

		/// <summary>
		/// Rename targets
		/// </summary>
		private void RenameTargets()
		{
			// set the curve data to the new values. 
			for (int i = 0; i < targetObjectPaths.Count; i++)
			{
				string oldName = origObjectPaths[i];
				string newName = targetObjectPaths[i];

				if (oldName != newName)
					for (int k = 0; k < curveDatas.Length; k++)
						if (curveDatas[k].path == oldName)
							curveDatas[k].path = newName;
			}

			// set up the curves based on the new names.
			AnimationCurve[] curves = new AnimationCurve[curveDatas.Length];

			for (int i = 0; i < curveDatas.Length; i++)
				curves[i] = AnimationUtility.GetEditorCurve(selectedClip, curveDatas[i]);

			selectedClip.ClearCurves();

			for (int i = 0; i < curveDatas.Length; i++)
				selectedClip.SetCurve(curveDatas[i].path, curveDatas[i].type, curveDatas[i].propertyName, curves[i]);

			Clear();
			Initialize();
		}

		#endregion
	}
}