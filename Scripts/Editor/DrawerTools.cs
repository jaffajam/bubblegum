﻿using UnityEngine;
using UnityEditor;

namespace Bubblegum.EditorUtils
{

	/// <summary>
	/// Colors that are used for drawing
	/// </summary>
	public static class DrawerTools
	{
		#region PUBLIC_VARIABLES

		/// <summary>
		/// Different colors to use for drawing
		/// </summary>
		public static readonly Color
			transparent = new Color(0f, 0f, 0f, 0f),
			shadowColor = new Color(0.1f, 0.1f, 0.1f, 0.5f),
			darkGray = new Color(0.26f, 0.27f, 0.3f),
			mediumDarkGray = new Color(0.35f, 0.4f, 0.44f),
			mediumGray = new Color(0.51f, 0.54f, 0.59f),
			lightGray = new Color(0.82f, 0.85f, 0.94f),
			lightBlue = new Color(0.2f, 0.59f, 0.89f),
			mediumBlue = new Color(0.08f, 0.39f, 0.59f),
			mediumDarkBlue = new Color(0f, 0.32f, 0.54f),
			darkBlue = new Color(0f, 0.25f, 0.49f),
			mediumRed = new Color(0.8f, 0.1f, 0.1f),
			darkRed = new Color(0.4f, 0.1f, 0.1f),
			mediumYellow = new Color(0.8f, 0.8f, 0.1f),
			darkYellow = new Color(0.5f, 0.5f, 0.1f);

		/// <summary>
		/// Get the main drawing color
		/// </summary>
		public static Color MainColor
		{
			get
			{
#if UNITY_PRO_LICENSE
				return darkGray;
#else
				return mediumGray;
#endif
			}
		}

		/// <summary>
		/// Get the alt drawing color
		/// </summary>
		public static Color AltColor
		{
			get
			{
#if UNITY_PRO_LICENSE
				return mediumDarkGray;
#else
				return lightGray;
#endif
			}
		}

		/// <summary>
		/// Offset to draw shadows at
		/// </summary>
		public static Vector2 shadowOffset = new Vector2(5f, -5f);

		/// <summary>
		/// Offset to center inside a label
		/// </summary>
		public static Vector2 labelOffset = new Vector2(10f, 10f);

		/// <summary>
		/// Offset to center inside a label
		/// </summary>
		public static Vector2 headerOffset = new Vector2(0f, 20f);

		/// <summary>
		/// Height to draw labels
		/// </summary>
		public const float LABEL_HEIGHT = 20f;

		/// <summary>
		/// Get the default main color
		/// </summary>
		public static Color TextColor
		{
			get
			{
				if (EditorGUIUtility.isProSkin)
					return Color.black;
				else
					return Color.white;
			}
		}

		#endregion

		#region PUBLIC_METHODS

		/// <summary>
		/// Draw a dropshadow (must be done before drawing self)
		/// </summary>
		/// <param name="bounds"></param>
		public static void DrawDropShadow(Rect bounds)
		{
			EditorGUI.DrawRect(new Rect(bounds.position - shadowOffset, bounds.size), shadowColor);
		}

		/// <summary>
		/// Draw the selected amount of spaces
		/// </summary>
		/// <param name="count"></param>
		public static void DrawSpace(int count = 1)
		{
			for (int i = 0; i < count; i++)
				EditorGUILayout.Space();
		}

		#endregion
	}
}
