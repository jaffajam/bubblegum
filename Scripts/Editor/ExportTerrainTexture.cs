﻿using UnityEngine;
using UnityEditor;
using System.IO;

namespace Bubblegum.EditorUtils
{

	public class ExportTerrainTexture
	{

		[MenuItem("Tools/Bubblegum/Terrain/Export Terrain Texture")]
		static void Apply()
		{

			var texture = Selection.activeObject as Texture2D;
			if (texture == null)
			{
				EditorUtility.DisplayDialog("Select Texture", "You Must Select a Texture first!", "Ok");
				return;
			}

			var bytes = texture.EncodeToPNG();
			File.WriteAllBytes(Application.dataPath + "/exported_texture.png", bytes);
			Debug.Log("Exported to: " + Application.dataPath + "/exported_texture.png");
		}
	}
}